﻿using UnityEngine;
using ServerLib;

public class Main : MonoBehaviour
{
    void Awake()
    {
        Debug.Log("Starting server!");
        ConsoleCmdHandlers.Init();
        NetworkServerWorld.CreateWorld<World>();
    }
}
