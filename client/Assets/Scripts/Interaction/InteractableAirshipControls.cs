﻿using UnityEngine;
using System.Collections;
using System;

public class InteractableAirshipControls : BaseInteractable
{
    const string TITLE = "Airship controls";
    Airship _airship;
    public override string GetInteractionTitle()
    {
        return TITLE;
    }

    void Start()
    {
        Init();
    }

    void Init()
    {
        _airship = GetComponentInParent<Airship>();
        SetInteractAction(0, () => World.Instance.RPC.CL_EntityAirshipEnter.Call(_airship.NetworkEntity.EntityId), "Use", false);
        SetInteractAction(1, () => Debug.Log("Sosu hui"), "Pososat hui", false);
    }
}
