﻿using UnityEngine;
using System;

public abstract class BaseInteractable : MonoBehaviour
{
    private Action[] _interactActions = new Action[4];
    private string[] _interactDescriptions = new string[4];
    private bool[] _actions = new bool[4];
    protected bool active;

    public abstract string GetInteractionTitle();

    public virtual bool CanInteract()
    {
        return true;
    }

    protected void SetInteractAction(int index, Action action, string description, bool hold)
    {
        if (index < 0 || index >= 4)
            return;

        _interactActions[index] = action;
        _interactDescriptions[index] = description;
        _actions[index] = true;
    }

    public void Activate()
    {
        active = true;
    }

    public void Deactivate()
    {
        active = false;
    }

    protected virtual void Update()
    {
        if (active)
        {
            var inp = InputController.Instance;
            CheckAction(0, inp.Interact1Press, inp.Interact1Hold);
            CheckAction(1, inp.Interact2Press, inp.Interact2Hold);
            CheckAction(2, inp.Interact3Press, inp.Interact3Hold);
            CheckAction(3, inp.Interact4Press, inp.Interact4Hold);
        }
    }

    void CheckAction(int index, bool press, bool hold)
    {
        if(_interactActions[index] != null)
        {
            if (press)
                _interactActions[index]();
        }
    }

    public bool[] GetActionMask()
    {
        return _actions;
    }

    public string[] GetActionDescriptions()
    {
        return _interactDescriptions;
    }
}
