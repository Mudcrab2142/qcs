﻿using UnityEngine;
using SharedLib;
using UnityEngine.UI;
using System;

public class UIInventoryItemInfo : MonoBehaviour
{
    static readonly Color32 _bgColor = new Color32(0, 0, 0, 180);
    static readonly Color32 _cursedNameColor = new Color32(143, 36, 36, 255);

    static readonly Color32 _mainStatAddColor = new Color32(41, 103, 25, 255);
    static readonly Color32 _mainStatRemoveColor = new Color32(131, 14, 14, 255);

    const string CURSED_PREFIX = "Cursed";
    RectTransform _rectTransform;

    public static UIInventoryItemInfo Create(ItemInstance item)
    {
        var obj = new GameObject("ItemInfo");
        var image = obj.AddComponent<Image>();
        var component = obj.AddComponent<UIInventoryItemInfo>();
        var rectTransform = obj.GetComponent<RectTransform>();
        component._rectTransform = rectTransform;
        Vector2 offset = new Vector2(-10, -10);
        image.color = _bgColor;
        rectTransform.pivot = rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0, 1);
        int nameLenght;
        //draw base info
        DrawName(item, rectTransform, ref offset, out nameLenght);
        DrawWeight(item, rectTransform, ref offset);
        DrawItemType(item, rectTransform, ref offset);

        //draw stats if exists
        DrawDamageAndArmor(item, rectTransform, ref offset);

        DrawMainStatsBonus(item, rectTransform, ref offset, AttributeType.Strength);
        DrawMainStatsBonus(item, rectTransform, ref offset, AttributeType.Dexterity);
        DrawMainStatsBonus(item, rectTransform, ref offset, AttributeType.Constitution);
        DrawMainStatsBonus(item, rectTransform, ref offset, AttributeType.Intelligence);
        DrawMainStatsBonus(item, rectTransform, ref offset, AttributeType.Luck);

        rectTransform.sizeDelta = new Vector2(Mathf.Max(150, item.Name.Length * 12 + 45), -offset.y + 10);
        return component;
    }

    static void DrawName(ItemInstance item, RectTransform panel, ref Vector2 offset, out int nameLenght)
    {
        bool cursed = item[AttributeType.Cursed];
        Text text;
        var rt = UITools.CreateText(out text, UIRoot.Instance.DefaultFont, 18, FontStyle.Bold, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.UpperRight, Vector2.one, Vector2.one, Vector2.one, Vector2.zero);

        if (cursed)
        {
            text.color = _cursedNameColor;
            text.text = string.Format("{0} {1}", CURSED_PREFIX, item.Name);
        }
        else
            text.text = item.Name;

        rt.SetParent(panel);
        rt.anchoredPosition = offset;
        offset -= new Vector2(0, 30);
        nameLenght = text.text.Length;
    }

    static void DrawWeight(ItemInstance item, RectTransform panel, ref Vector2 offset)
    {
        Text text;
        var rt = UITools.CreateText(out text, UIRoot.Instance.DefaultFont, 12, FontStyle.Normal, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.UpperRight, Vector2.one, Vector2.one, Vector2.one, Vector2.zero);
        text.text = "Weight: " + item.Weight;
        rt.SetParent(panel);
        rt.anchoredPosition = offset;
        offset -= new Vector2(0, 14);
    }

    private static void DrawItemType(ItemInstance item, RectTransform panel, ref Vector2 offset)
    {
        Text text;
        var rt = UITools.CreateText(out text, UIRoot.Instance.DefaultFont, 10, FontStyle.Normal, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.UpperRight, Vector2.one, Vector2.one, Vector2.one, Vector2.zero);
        text.text = item.BaseItem.Type.ToString();
        rt.SetParent(panel);
        rt.anchoredPosition = offset;
        offset -= new Vector2(0, 20);
    }

    private static void DrawDamageAndArmor(ItemInstance item, RectTransform panel, ref Vector2 offset)
    {
        if (item.HasAttribute(AttributeType.Damage))
        {
            MinMax<short> dmg = item[AttributeType.Damage];
            Text text;
            var rt = UITools.CreateText(out text, UIRoot.Instance.DefaultFont, 12, FontStyle.Normal, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.UpperRight, Vector2.one, Vector2.one, Vector2.one, Vector2.zero);
            text.text = string.Format("Damage: {0} - {1}", dmg.Min, dmg.Max);
            rt.SetParent(panel);
            rt.anchoredPosition = offset;
            offset -= new Vector2(0, 14);
        }

        if (item.HasAttribute(AttributeType.Armor))
        {
            short armor = item[AttributeType.Armor];
            Text text;
            var rt = UITools.CreateText(out text, UIRoot.Instance.DefaultFont, 12, FontStyle.Normal, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.UpperRight, Vector2.one, Vector2.one, Vector2.one, Vector2.zero);
            text.text = string.Format("Armor: {0}", armor);
            rt.SetParent(panel);
            rt.anchoredPosition = offset;
            offset -= new Vector2(0, 14);
        }
    }

    static void DrawMainStatsBonus(ItemInstance item, RectTransform panel, ref Vector2 offset, AttributeType type)
    {
        if (item.HasAttribute(type))
        {
            var value = (int)item[type];
            Text text;
            var rt = UITools.CreateText(out text, UIRoot.Instance.DefaultFont, 12, FontStyle.Normal, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.UpperRight, Vector2.one, Vector2.one, Vector2.one, Vector2.zero);
            text.text = string.Format("{0}{1} {2}", value < 0 ? "" : "+", value, type);
            text.color = value < 0 ? _mainStatRemoveColor : _mainStatAddColor;
            rt.SetParent(panel);
            rt.anchoredPosition = offset;
            offset -= new Vector2(0, 14);
        }
    }

    void Update()
    {
        _rectTransform.position = Input.mousePosition + new Vector3(10, 5);
    }
}
