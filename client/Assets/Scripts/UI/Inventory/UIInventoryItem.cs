﻿using UnityEngine;
using SharedLib;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using System;

public class UIInventoryItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    private RectTransform _rectTransform;
    private Vector2 _beginDragCursorPosition;
    private Vector2 _beginDragItemPosition;
    private InventoryEntry _inventoryEntry;

    private UIInventoryItemInfo _itemInfoPopup;
    private bool _dragging;
    private Transform _oldParent;
    private RawImage _rawImage;
    private Text _countText;

    public void Init(InventoryEntry entry)
    {
        _inventoryEntry = entry;
        _rawImage = transform.GetComponent<RawImage>();
        _countText = gameObject.GetComponentInChildren<Text>();
        _rectTransform = gameObject.GetComponent<RectTransform>();

        _rawImage.texture = UIImageStorage.GetItemIcon(_inventoryEntry.Item[AttributeType.Icon]);

        UIRoot.Instance.InventoryController.OnItemLockStatusUpdate += UpdateLockStatus;
        UpdateLockStatus();
    }

    void OnDestroy()
    {
        UIRoot.Instance.InventoryController.OnItemLockStatusUpdate -= UpdateLockStatus;
    }

    void UpdateLockStatus()
    {
        var c = _rawImage.color;
        var lockCount = UIRoot.Instance.InventoryController.GetLockCount(_inventoryEntry.PositionX, _inventoryEntry.PositionY);
        if (UIRoot.Instance.InventoryController.IsItemLocked(_inventoryEntry.PositionX, _inventoryEntry.PositionY))
            _rawImage.color = new Color(c.r, c.g, c.b, 0.5f);
        else
            _rawImage.color = new Color(c.r, c.g, c.b, 1f);

        if (_countText)
            _countText.text = (_inventoryEntry.Item.Count - lockCount).ToString();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _beginDragCursorPosition = new Vector2(_rectTransform.position.x, _rectTransform.position.y) - eventData.position;
        _beginDragItemPosition = _rectTransform.anchoredPosition;
        _dragging = true;
        _oldParent = _rectTransform.parent;
        _rectTransform.SetParent(UIRoot.Instance.transform);
        _rectTransform.SetAsLastSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        _rectTransform.position = _beginDragCursorPosition + eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        var raycastPos = new Vector2(_rectTransform.position.x + UIInventoryCell.CELL_SIZE / 2f, _rectTransform.position.y - UIInventoryCell.CELL_SIZE / 2f);
        var raycastResult = UIRoot.Instance.Raycast(eventData, raycastPos);

        if (!UIRoot.Instance.InventoryController.IsItemLocked(_inventoryEntry.PositionX, _inventoryEntry.PositionY))
        {
            CheckMove(raycastResult);
            CheckEquip(raycastResult);
        }

        CheckCraft(raycastResult);

        _dragging = false;
        UITools.SetParent(_rectTransform, _oldParent);
        _rectTransform.anchoredPosition = _beginDragItemPosition;
    }

    void CheckMove(List<RaycastResult> result)
    {
        var cells = result.Where(x => x.gameObject.GetComponent<UIInventoryCell>()).ToArray();
        if (cells.Length == 0 || !World.Instance)
            return;
        var cell = cells[0].gameObject.GetComponent<UIInventoryCell>();
        World.Instance.ClientInventory.TryMoveItem(_inventoryEntry.PositionX, _inventoryEntry.PositionY, cell.PositionX, cell.PositionY, 0, OnItemMoveSuccess, OnItemMoveFail);
    }

    void CheckEquip(List<RaycastResult> result)
    {
        var cells = result.Where(x => x.gameObject.GetComponent<UICharacterInfoEquipmentCell>()).ToArray();
        if (cells.Length == 0 || !World.Instance)
            return;
        var cell = cells[0].gameObject.GetComponent<UICharacterInfoEquipmentCell>();
        World.Instance.ClientInventory.TryEquipItem(_inventoryEntry.PositionX, _inventoryEntry.PositionY, cell.SlotId, OnItemMoveSuccess, OnItemMoveFail);
    }

    void CheckCraft(List<RaycastResult> result)
    {
        var cells = result.Where(x => x.gameObject.GetComponent<UICraftTableCell>()).ToArray();
        if (cells.Length == 0 || !World.Instance)
            return;
        var cell = cells[0].gameObject.GetComponent<UICraftTableCell>();
        if (UIRoot.Instance.Craft.CanAddToTable(cell.Id, _inventoryEntry))
            cell.AddToTable(_inventoryEntry, 1);
    }

    void OnItemMoveSuccess()
    {

    }

    void OnItemMoveFail()
    {

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _itemInfoPopup = UIInventoryItemInfo.Create(_inventoryEntry.Item);
        var rectTransform = _itemInfoPopup.GetComponent<RectTransform>();
        UITools.SetParent(rectTransform, transform.parent);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_itemInfoPopup)
            Destroy(_itemInfoPopup.gameObject);
    }

    void OnDisable()
    {
        if (_itemInfoPopup)
            Destroy(_itemInfoPopup.gameObject);
    }

    void Update()
    {
        if (_itemInfoPopup)
        {
            if (_dragging && _itemInfoPopup.enabled)
                _itemInfoPopup.gameObject.SetActive(false);

            if (!_dragging && !_itemInfoPopup.enabled)
                _itemInfoPopup.gameObject.SetActive(true);
        }
    }
}
