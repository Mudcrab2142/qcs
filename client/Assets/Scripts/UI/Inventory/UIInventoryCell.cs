﻿using UnityEngine;
using UnityEngine.UI;

public class UIInventoryCell : MonoBehaviour
{
    public const int CELL_SIZE = 40;
    public const string CELL_IMAGE_PATH = "Images/UI/Inventory/InventoryCell";

    public static UIInventoryCell Create(short x, short y)
    {
        var obj = new GameObject("UIInventoryCell");
        var component = obj.AddComponent<UIInventoryCell>();
        component.PositionX = x;
        component.PositionY = y;
        component._image = obj.AddComponent<Image>();
        component._image.overrideSprite = Resources.Load<Sprite>(CELL_IMAGE_PATH);
        var rectTransform = obj.GetComponent<RectTransform>();
        rectTransform.anchorMin = rectTransform.anchorMax = rectTransform.pivot = new Vector2(0, 1);
        rectTransform.sizeDelta = new Vector2(CELL_SIZE, CELL_SIZE);
        return component;
    }

    Image _image;
    public short PositionX { get; private set; }
    public short PositionY { get; private set; }

    public void UpdateColor(Color32 color)
    {
        _image.color = color;
    }
}
