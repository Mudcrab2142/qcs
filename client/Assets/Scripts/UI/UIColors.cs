﻿using UnityEngine;
using System.Collections;

public static class UIColors
{
    public static readonly Color32 SpellNameColor = new Color32(0, 163, 218, 255);
    public const string PhysicalDmgColor = "#ff0000ff";

}
