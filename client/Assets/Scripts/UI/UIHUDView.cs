﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIHUDView : MonoBehaviour
{
    public RectTransform Panel;

    [Header("Health")]
    public RectTransform Health;
    private float _healthBarSize;

    [Header("Stamina")]
    public RectTransform Stamina;
    private float _staminaBarSize;

    [Header("Interaction")]
    public Text InteractionText;

    [Header("AirshipHUD")]
    public GameObject AirshipHUDParent;

    void Awake()
    {
        _healthBarSize = Health.parent.GetComponent<RectTransform>().rect.width;
        _staminaBarSize = Stamina.parent.GetComponent<RectTransform>().rect.width;
        HideInteractionText();
    }

	public void ShowHUD()
    {
        Panel.gameObject.SetActive(true);
    }

    public void HideHUD()
    {
        Panel.gameObject.SetActive(false);
    }

    public void ShowAirshipHUD()
    {
        AirshipHUDParent.SetActive(true);
    }

    public void HideAirshipHUD()
    {
        AirshipHUDParent.SetActive(false);
    }

    public void SetHealth(float currHealth, float maxHealth)
    {
        currHealth = Mathf.Clamp(currHealth, 0f, maxHealth);
        Health.offsetMax = new Vector2((1 - currHealth / maxHealth) * -_healthBarSize, 0f);
    }

    public void SetStamina(float currStamina, float maxStamina)
    {
        currStamina = Mathf.Clamp(currStamina, 0f, maxStamina);
        Stamina.offsetMax = new Vector2((1 - currStamina / maxStamina) * -_staminaBarSize, 0f);
    }

    public void ShowInteractionText(string title, string[] descriptions, bool[] mask)
    {
        var text = title;
        for(var i = 0; i < mask.Length; i++)
        {
            if (!mask[i])
                continue;

            text += string.Format("\n[{0}] {1}", GetInteractKeyCode(i), descriptions[i]);
        }

        InteractionText.text = text;
        InteractionText.gameObject.SetActive(true);
    }

    KeyCode GetInteractKeyCode(int index)
    {
        if (index == 0)
            return InputController.Interact1Key;
        if (index == 1)
            return InputController.Interact2Key;
        if (index == 2)
            return InputController.Interact3Key;

        return InputController.Interact4Key;
    }

    public void HideInteractionText()
    {
        InteractionText.gameObject.SetActive(false);
    }
}
