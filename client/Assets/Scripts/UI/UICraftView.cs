﻿using UnityEngine;
using SharedLib;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class UICraftView : UIBaseView
{
    public RectTransform RootTransform;
    public Button CraftButton;

    [Header("RecipeList")]
    public RectTransform RecipeListContainer;
    public GameObject RecipeSelectButtonPrototype;

    [Header("RecipeInfo")]
    public RawImage RecipeIcon;
    public Text RecipeName;

    [Header("RequiredIngredientsList")]
    public RectTransform RequiredIngredientsContainer;
    public GameObject RequiredIngredientCellPrototype;

    [Header("CraftTable")]
    public RectTransform CraftTableCellsContainer;
    public GameObject CraftTableCellPrototype;

    [Header("CraftQueueParent")]
    public RectTransform CraftQueueContainer;
    public GameObject CraftQueueItemPrototype;

    private readonly List<GameObject> _recipeListObjects = new List<GameObject>();
    private readonly List<GameObject> _requiredIngredientsListObjects = new List<GameObject>();
    private readonly List<GameObject> _craftTableCellObjects = new List<GameObject>();
    private readonly Dictionary<int, NetStruct<short, short, int>> _tableIngredients = new Dictionary<int, NetStruct<short, short, int>>();

    private readonly List<GameObject> _craftSessionsObjects = new List<GameObject>();

    private CraftStatType _selectedCategory;
    private CraftRecipe _selectedRecipe;

    private void Awake()
    {
        SelectCategory(CraftStatType.Blacksmithing);
        RecipeSelectButtonPrototype.SetActive(false);
        RequiredIngredientCellPrototype.SetActive(false);
        CraftTableCellPrototype.SetActive(false);
        CraftQueueItemPrototype.SetActive(false);
        CraftButton.onClick.AddListener(Craft);
    }

    protected override bool CanShow()
    {
        return base.CanShow() && World.Instance && World.Instance.ClientInventory.Backpack != null && !((Character)World.Instance.ControlledCharacter.Owner).IsDead;
    }

    protected override void Show()
    {
        RootTransform.gameObject.SetActive(true);
        World.Instance.ClientCraft.OnKnownRecipesUpdate += UpdateRecipeList;
        World.Instance.ClientCraft.OnCraftSessionsUpdate += UpdateCraftSessions;
        UpdateRecipeList();
    }


    protected override void Hide()
    {
        World.Instance.ClientCraft.OnKnownRecipesUpdate -= UpdateRecipeList;
        World.Instance.ClientCraft.OnCraftSessionsUpdate -= UpdateCraftSessions;
        RootTransform.gameObject.SetActive(false);
        ClearTable();
    }

    private void SelectCategory(CraftStatType type)
    {
        if (_selectedCategory == type)
            return;

        _selectedRecipe = null;
        _selectedCategory = type;
    }

    private void UpdateCraftSessions()
    {
        _craftSessionsObjects.ForEach(Destroy);
        _craftSessionsObjects.Clear();
        foreach (var item in World.Instance.ClientCraft.CraftSessions)
        {
            var s = Instantiate(CraftQueueItemPrototype);
            UITools.SetParent(s.GetComponent<RectTransform>(), CraftQueueContainer);
        }
    }

    private void UpdateRecipeList()
    {
        if (!World.Instance)
            return;

        _recipeListObjects.ForEach(Destroy);
        _recipeListObjects.Clear();

        foreach(var recipeId in World.Instance.ClientCraft.KnownRecipes)
        {
            var recipe = World.Instance.ClientCraft.GetRecipeInfo(recipeId);
            if (recipe.Type != _selectedCategory)
                continue;

            if (_selectedRecipe == null)
                SelectRecipe(recipeId);

            var cRecipeId = recipeId;
            var rButtonObj = Instantiate(RecipeSelectButtonPrototype) as GameObject;
            rButtonObj.SetActive(true);
            var rButton = rButtonObj.GetComponent<Button>();
            rButton.onClick.AddListener(() => SelectRecipe(cRecipeId));
            rButtonObj.GetComponentInChildren<Text>().text = recipeId.ToString();
            UITools.SetParent(rButtonObj.GetComponent<RectTransform>(), RecipeListContainer);
            _recipeListObjects.Add(rButtonObj);
        }
    }

    private void SelectRecipe(CraftRecipeId recipe)
    {
        if (!World.Instance || (_selectedRecipe != null && _selectedRecipe.RecipeId == recipe))
            return;
        
        _selectedRecipe = World.Instance.ClientCraft.GetRecipeInfo(recipe);
        var item = World.Instance.ItemStorage.GetItem((short)_selectedRecipe.Item);
        RecipeName.text = recipe.ToString();
        RecipeIcon.texture = UIImageStorage.GetItemIcon(item != null ? item[AttributeType.Icon] : "");
        RebuildRequiredIngredients();
        ClearTable();
    }

    void ClearTable()
    {
        UnlockAllItems();
        RebuildTableIngredients();
    }

    private void Craft()
    {
        if(!World.Instance || _selectedRecipe == null)
            return;

        World.Instance.RPC.CL_CraftStart.Call(new NetStruct<CraftRecipeId, NetStruct<short, short, int>[]>(_selectedRecipe.RecipeId, _tableIngredients.Values.ToArray()));
    }

    private void RebuildRequiredIngredients()
    {
        _requiredIngredientsListObjects.ForEach(Destroy);
        _requiredIngredientsListObjects.Clear();

        foreach(var ingredient in _selectedRecipe.Ingredients)
        {
            var cellObj = Instantiate(RequiredIngredientCellPrototype) as GameObject;
            var cellImg = cellObj.GetComponent<RawImage>();
            cellObj.SetActive(true);
            var item = World.Instance.ItemStorage.GetItem((short)ingredient.Id);
            cellImg.texture = UIImageStorage.GetItemIcon(item[AttributeType.Icon] ?? "");
            cellObj.GetComponentInChildren<Text>().text = ingredient.Count.ToString();
            UITools.SetParent(cellObj.GetComponent<RectTransform>(), RequiredIngredientsContainer);
            _requiredIngredientsListObjects.Add(cellObj);
        }
    }

    private void RebuildTableIngredients()
    {
        _craftTableCellObjects.ForEach(Destroy);
        _craftTableCellObjects.Clear();
        _tableIngredients.Clear();

        for (var i = 0; i < _selectedRecipe.Cells.Length; i++)
        { 
            var cellObj = Instantiate(CraftTableCellPrototype) as GameObject;
            var cellImg = cellObj.GetComponent<RawImage>();
            UITools.SetParent(cellObj.GetComponent<RectTransform>(), CraftTableCellsContainer);
            var cellComponent = cellObj.GetComponent<UICraftTableCell>();
            cellComponent.Init(i, _selectedRecipe.Cells[i].MaxIngredients);
            cellComponent.UpdateData(null, 0);
            cellObj.SetActive(true);
            _craftTableCellObjects.Add(cellObj);
        }
    }

    public bool CanAddToTable(int id, InventoryEntry entry)
    {
        if (!_tableIngredients.ContainsKey(id))
            return true;

        var s = _tableIngredients[id];
        if (s.Value1 != entry.PositionX || s.Value2 != entry.PositionY || s.Value3 >= _selectedRecipe.Cells[id].MaxIngredients)
            return false;

        return true;
    }

    public void AddTableIngredient(int id, InventoryEntry entry, int count)
    {
        if (_tableIngredients.ContainsKey(id))
        {
            var s = _tableIngredients[id];
            count = Mathf.Clamp(count, 0, _selectedRecipe.Cells[id].MaxIngredients - s.Value3);
            _tableIngredients[id] = new NetStruct<short, short, int>(entry.PositionX, entry.PositionY, s.Value3 + count);
            LockInventoryItem(entry.PositionX, entry.PositionY, s.Value3 + count);
            _craftTableCellObjects[id].GetComponent<UICraftTableCell>().UpdateData(entry.Item.BaseItem, s.Value3 + count);
        }
        else
        {
            count = Mathf.Clamp(count, 0, _selectedRecipe.Cells[id].MaxIngredients);
            _tableIngredients[id] = new NetStruct<short, short, int>(entry.PositionX, entry.PositionY, count);
            LockInventoryItem(entry.PositionX, entry.PositionY, count);
            _craftTableCellObjects[id].GetComponent<UICraftTableCell>().UpdateData(entry.Item.BaseItem, count);
        } 
    }

    public void RemoveTableIngredient(int id, int count)
    {
        if (!_tableIngredients.ContainsKey(id))
            return;

        var s = _tableIngredients[id];

        if (_tableIngredients[id].Value3 > count)
        {
            _tableIngredients[id] = new NetStruct<short, short, int>(s.Value1, s.Value2, s.Value3 - count);
            LockInventoryItem(s.Value1, s.Value2, s.Value3 - count);
        }
        else
        {
            _tableIngredients.Remove(id);
            UnlockInventoryItem(s.Value1, s.Value2);
        }
    }

    void LockInventoryItem(short x, short y, int count)
    {
        UIRoot.Instance.InventoryController.LockItem(x, y, count);
    }

    void UnlockInventoryItem(short x, short y)
    {
        UIRoot.Instance.InventoryController.UnlockItem(x, y);
    }

    void UnlockAllItems()
    {
        UIRoot.Instance.InventoryController.UnlockAllItems();
    }
}
