﻿using System.Collections.Generic;
using UnityEngine;

public class UIImageStorage
{
    static Dictionary<string, Texture2D> _itemIconsCache = new Dictionary<string, Texture2D>();

    public static Texture2D GetItemIcon(string name)
    {
        return Get(string.Format("Images/{0}", name));
    }

    public static Texture2D GetInventoryCellIcon()
    {
        return Get("Images/UI/Inventory/InventoryCell");
    }

    private static Texture2D Get(string path)
    {
        if (_itemIconsCache.ContainsKey(path))
            return _itemIconsCache[path];

        _itemIconsCache[path] = Resources.Load(path) as Texture2D;
        return _itemIconsCache[path];
    }
}
