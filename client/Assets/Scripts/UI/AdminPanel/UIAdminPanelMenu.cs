﻿using ClientLib;
using SharedLib;
using UnityEngine;
using UnityEngine.UI;

public class UIAdminPanelMenu : MonoBehaviour
{
    public Button AddItemButton;
    public Button KillButton;

    void Awake()
    {
        AddItemButton.onClick.AddListener(() => UIRoot.Instance.AdminPanel.SelectScreen(UIAdminPanelView.STATE_ADD_ITEM));
        KillButton.onClick.AddListener(() => NetworkClientWorld.ProcessConsoleCommand(ConVar.CMD_Kill.Name));
    }
}
