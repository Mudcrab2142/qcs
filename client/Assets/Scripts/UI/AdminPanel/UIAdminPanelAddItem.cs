﻿using UnityEngine;
using SharedLib;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using ClientLib;

public class UIAdminPanelAddItem : MonoBehaviour
{
    [Header("ItemTypeSelector")]
    public RectTransform ItemTypeSelectorContent;
    public GameObject ItemTypeSelectorButtonPrototype;

    [Header("ItemSelector")]
    public RectTransform ItemSelectorContent;
    public GameObject ItemSelectorButtonPrototype;

    private List<GameObject> _itemObjects = new List<GameObject>();

    void Awake()
    {
        ItemTypeSelectorButtonPrototype.SetActive(false);
        ItemSelectorButtonPrototype.SetActive(false);
        BuildItemTypeList();
        SelectItemType(ItemType.Armor);
    }

    void SelectItemType(ItemType type)
    {
        if (!World.Instance)
            return;

        _itemObjects.ForEach(Destroy);

        foreach (var item in Enum.GetValues(typeof(ItemId)))
        {
            var baseItem = World.Instance.ItemStorage.GetItem((short)item);
            if (baseItem == null || baseItem.Type != type)
                continue;

            var obj = Instantiate(ItemSelectorButtonPrototype);
            obj.SetActive(true);
            obj.GetComponent<RawImage>().texture = UIImageStorage.GetItemIcon(baseItem[AttributeType.Icon]);
            var cItem = (ItemId)item;
            obj.GetComponent<Button>().onClick.AddListener(() => SendCommand(cItem));
            UITools.SetParent(obj.GetComponent<RectTransform>(), ItemSelectorContent);
            _itemObjects.Add(obj);
        }

    }

    void SendCommand(ItemId item)
    {
        string cmd = string.Format("{0} {1}", ConVar.CMD_AddItem.Name, (short)item);
        NetworkClientWorld.ProcessConsoleCommand(cmd);
    }

    void BuildItemTypeList()
    {
        foreach (var item in Enum.GetValues(typeof(ItemType)))
        {
            var obj = Instantiate(ItemTypeSelectorButtonPrototype);
            obj.SetActive(true);
            UITools.SetParent(obj.GetComponent<RectTransform>(), ItemTypeSelectorContent);
            obj.GetComponentInChildren<Text>().text = item.ToString();
            ItemType targetType = (ItemType)item;
            obj.GetComponent<Button>().onClick.AddListener(() => SelectItemType(targetType));
        }
    }
}
