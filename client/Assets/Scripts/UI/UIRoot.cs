﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIRoot : MonoBehaviour
{
    public static UIRoot Instance;
    public RectTransform WorldUI;

    public UIMainMenuView MainMenu;
    public UIInventoryView InventoryController;
    public UIHUDView HUDController;
    public UICharacterInfoView CharacterInfo;
    public UISpellTreeView SpellTree;
    public UICraftView Craft;
    public UIAdminPanelView AdminPanel;
    public UIDeathScreenView DeathScreen;
    public UIDebugView Debug;

    GraphicRaycaster _raycaster;
    List<RaycastResult> _rayrastResults;
    public Font DefaultFont;

    void Awake()
    {
        Instance = this;
        _raycaster = GetComponent<GraphicRaycaster>();
        _rayrastResults = new List<RaycastResult>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if(!InventoryController.Active)
                InventoryController.TryShow();
            else
                InventoryController.TryHide();
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            if (!CharacterInfo.ChraracterInfoPanel.gameObject.activeInHierarchy)
                CharacterInfo.ShowCharacterInfo();
            else
                CharacterInfo.HideCharacterInfo();
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            if (!SpellTree.SpellTreePanel.gameObject.activeInHierarchy)
                SpellTree.ShowSpellTree();
            else
                SpellTree.HideSpellTree();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!Craft.Active)
                Craft.TryShow();
            else
                Craft.TryHide();
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            if (!AdminPanel.AdminPanelRoot.gameObject.activeInHierarchy)
                AdminPanel.ShowAdminPanel();
            else
                AdminPanel.HideAdminPanel();
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            if (!Debug.NetworkStatsRoot.gameObject.activeInHierarchy)
                Debug.ShowNetworkStats();
            else
                Debug.HideNetworkStats();
        }
    }

    public void HideAllGameWindows()
    {
        InventoryController.TryHide();
        CharacterInfo.HideCharacterInfo();
        SpellTree.HideSpellTree();
        Craft.TryHide();
        AdminPanel.HideAdminPanel();
    }

    public List<RaycastResult> Raycast(PointerEventData eventData, Vector2? overridePosition = null)
    {
        _rayrastResults.Clear();
        if (overridePosition.HasValue)
            eventData.position = overridePosition.Value;
        _raycaster.Raycast(eventData, _rayrastResults);
        return _rayrastResults;
    }
}
