﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIAdminPanelView : MonoBehaviour
{
    public const int STATE_MENU = 0;
    public const int STATE_ADD_ITEM = 1;

    public RectTransform AdminPanelRoot;
    public Button BackButton;

    [Header("States")]
    public UIAdminPanelMenu Menu;
    public UIAdminPanelAddItem AddItem;

    void Awake()
    {
        BackButton.onClick.AddListener(() => SelectScreen(STATE_MENU));
        SelectScreen(STATE_MENU);
    }

    public void ShowAdminPanel()
    {
        if (!World.Instance)
            return;

        AdminPanelRoot.gameObject.SetActive(true);
    }

    public void HideAdminPanel()
    {
        if (!World.Instance)
            return;

        AdminPanelRoot.gameObject.SetActive(false);
    }

    public void SelectScreen(int state)
    {
        CloseAll();
        switch (state)
        {
            case STATE_MENU:
                Menu.gameObject.SetActive(true);
                break;
            case STATE_ADD_ITEM:
                AddItem.gameObject.SetActive(true);
                break;
        }
    }

    void CloseAll()
    {
        Menu.gameObject.SetActive(false);
        AddItem.gameObject.SetActive(false);
    }
}
