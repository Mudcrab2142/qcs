﻿using UnityEngine;

public class UIBaseView : MonoBehaviour
{
    public bool Active { get; private set; }

	public void TryShow()
    {
        if(CanShow())
        {
            Active = true;
            Show();
        }
    }

    public void TryHide()
    {
        if (CanHide())
        {
            Active = false;
            Hide();
        }
    }

    protected virtual void Show() { }

    protected virtual void Hide() { }

    protected virtual bool CanShow()
    {
        return !Active;
    }

    protected virtual bool CanHide()
    {
        return Active;
    }
}
