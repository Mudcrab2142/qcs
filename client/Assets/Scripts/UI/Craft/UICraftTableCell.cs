﻿using UnityEngine;
using SharedLib;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class UICraftTableCell : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public int Id { get; private set; }

    private RawImage _image;
    private Text _countText;
    private UICraftView _owner;

    private int _maxCount;

    public void Init(int id, int maxCount)
    {
        _owner = UIRoot.Instance.Craft;
        Id = id;
        _maxCount = maxCount;

        _image = transform.GetChild(0).GetComponent<RawImage>();
        _countText = GetComponentInChildren<Text>();
    }

    public void AddToTable(InventoryEntry entry, int count)
    {
        _owner.AddTableIngredient(Id, entry, count);
    }

    public void RemoveFromTable(int count)
    {
        _owner.RemoveTableIngredient(Id, count);
    }

    public void UpdateData(ItemPrototype item, int count)
    {
        _countText.text = string.Format("{0}/{1}", count, _maxCount);

        if (item == null)
            _image.texture = UIImageStorage.GetInventoryCellIcon();
        else
            _image.texture = UIImageStorage.GetItemIcon(item[AttributeType.Icon]);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        throw new NotImplementedException();
    }

    public void OnDrag(PointerEventData eventData)
    {
        throw new NotImplementedException();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        throw new NotImplementedException();
    }
}
