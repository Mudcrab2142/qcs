﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class UIDraggablePanel : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private RectTransform _rectTransform;
    private bool _dragging;
    private Vector2 _beginDragCursorPosition;
    public RectTransform DragZone;

    void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (DragZone && RectTransformUtility.RectangleContainsScreenPoint(DragZone, eventData.position))
        {
            _dragging = true;
            _beginDragCursorPosition = new Vector2(_rectTransform.position.x, _rectTransform.position.y) - eventData.position;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _dragging = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(_dragging)
            _rectTransform.position = _beginDragCursorPosition + eventData.position;
    }
}
