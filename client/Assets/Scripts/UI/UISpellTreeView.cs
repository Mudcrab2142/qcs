﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UISpellTreeView : MonoBehaviour
{
    public RectTransform SpellTreePanel;

    [Header("CategorySelectButtons")]
    public Button MeleeCombatButton;
    public Button DefenceButton;
    public Button MagicButton;
    public Button UtilityButton;

    [Header("Categories")]
    public GameObject MeleeCombat;
    public GameObject Defence;
    public GameObject Magic;
    public GameObject Utility;

    void Awake()
    {
        MeleeCombatButton.onClick.AddListener(() => Enable(MeleeCombat));
        DefenceButton.onClick.AddListener(() => Enable(Defence));
        MagicButton.onClick.AddListener(() => Enable(Magic));
        UtilityButton.onClick.AddListener(() => Enable(Utility));
    }

    public void ShowSpellTree()
    {
        if (!World.Instance)
            return;

        SpellTreePanel.gameObject.SetActive(true);
    }

    public void HideSpellTree()
    {
        if (!World.Instance)
            return;

        SpellTreePanel.gameObject.SetActive(false);
    }

    void Enable(GameObject obj)
    {
        DisableAll();
        obj.SetActive(true);
    }

    void DisableAll()
    {
        MeleeCombat.SetActive(false);
        Defence.SetActive(false);
        Magic.SetActive(false);
        Utility.SetActive(false);
    }
}
