﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using SharedLib;

public class UICharacterInfoView : MonoBehaviour
{
    public RectTransform ChraracterInfoPanel;

    [Header("MainStats")]
    public Text StrValue;
    public Text DexValue;
    public Text ConValue;
    public Text IntValue;
    public Text LckValue;
    public Text FreePointsText;

    [Header("MainStatsUpgradeButtons")]
    public Button StrButton;
    public Button DexButton;
    public Button ConButton;
    public Button IntButton;
    public Button LckButton;

    [Header("Leveling")]
    public Text LevelValue;
    public Text ExperienceValue;

    [Header("SecondaryStats")]
    public Text DamageValue;
    public Text ArmorValue;

    [Header("Equipment")]
    public UICharacterInfoEquipmentCell Weapon;
    public UICharacterInfoEquipmentCell Helmet;
    public UICharacterInfoEquipmentCell Cuirass;
    public UICharacterInfoEquipmentCell Leggings;
    public UICharacterInfoEquipmentCell Boots;
    public UICharacterInfoEquipmentCell Gloves;

    void Awake()
    {
        StrButton.onClick.AddListener(() => UpgradeMainStat(MainStatType.Strength));
        DexButton.onClick.AddListener(() => UpgradeMainStat(MainStatType.Dexterity));
        ConButton.onClick.AddListener(() => UpgradeMainStat(MainStatType.Constitution));
        IntButton.onClick.AddListener(() => UpgradeMainStat(MainStatType.Intelligence));
        LckButton.onClick.AddListener(() => UpgradeMainStat(MainStatType.Luck));
    }

    void UpgradeMainStat(MainStatType stat)
    {
        if (World.ClientWorldInstance)
            World.ClientWorldInstance.ClientStats.TryUpgradeStat(stat);
    }

    public void ShowCharacterInfo()
    {
        if (!World.Instance || World.Instance.ClientInventory.Backpack == null)
            return;

        ChraracterInfoPanel.gameObject.SetActive(true);
        World.Instance.ClientStats.OnMainStatsUpdate += UpdateMainStats;
        UpdateMainStats();

        World.Instance.ClientStats.OnSecondaryStatsUpdate += UpdateSecondaryStats;
        UpdateSecondaryStats();

        World.Instance.ClientEquipment.OnEquipmentUpdate += UpdateEquipment;
        UpdateEquipment();

        World.ClientWorldInstance.ClientFreeStatPoints.OnValueChanged += UpdateFreePointsText;
        UpdateFreePointsText(World.ClientWorldInstance.ClientFreeStatPoints.Value);

        World.ClientWorldInstance.ClientExp.OnValueChanged += UpdateExp;
        UpdateExp(World.ClientWorldInstance.ClientExp.Value);
    }

    public void HideCharacterInfo()
    {
        ChraracterInfoPanel.gameObject.SetActive(false);
        World.Instance.ClientStats.OnMainStatsUpdate -= UpdateMainStats;
        World.Instance.ClientStats.OnSecondaryStatsUpdate -= UpdateSecondaryStats;
        World.Instance.ClientEquipment.OnEquipmentUpdate -= UpdateEquipment;
        World.ClientWorldInstance.ClientFreeStatPoints.OnValueChanged -= UpdateFreePointsText;
    }

    private void UpdateEquipment()
    {
        var equipment = World.Instance.ClientEquipment;
        Weapon.UpdateCell(equipment[GameConstants.Equipment.WEAPON]);
        Helmet.UpdateCell(equipment[GameConstants.Equipment.HELMET]);
        Cuirass.UpdateCell(equipment[GameConstants.Equipment.CUIRASS]);
        Leggings.UpdateCell(equipment[GameConstants.Equipment.LEGGINGS]);
        Boots.UpdateCell(equipment[GameConstants.Equipment.BOOTS]);
        Gloves.UpdateCell(equipment[GameConstants.Equipment.GLOVES]);
    }

    private void UpdateExp(int exp)
    {
        var level = GameConstants.Experience.GetLevel(exp);
        LevelValue.text = level.ToString();
        var expToNextLevel = GameConstants.Experience.GetExpToNextLevel(level);
        ExperienceValue.text = string.Format("{0}/{1}", exp - GameConstants.Experience.GetTotalExpToLevel(level), expToNextLevel);
    }

    private void UpdateMainStats()
    {
        var stats = World.Instance.ClientStats;
        StrValue.text = stats.Strength.Value.ToString();
        DexValue.text = stats.Dexterity.Value.ToString();
        ConValue.text = stats.Constitution.Value.ToString();
        IntValue.text = stats.Intelligence.Value.ToString();
        LckValue.text = stats.Luck.Value.ToString();
    }

    private void UpdateSecondaryStats()
    {
        var stats = World.Instance.ClientStats;
        DamageValue.text = string.Format("{0}-{1}", (int)stats.MinDamage.Value, (int)stats.MaxDamage.Value);
        ArmorValue.text = stats.Armor.Value.ToString();
    }

    private void UpdateFreePointsText(int points)
    {
        FreePointsText.text = string.Format("Points left: {0}", points);
    }

}
