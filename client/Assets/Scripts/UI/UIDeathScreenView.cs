﻿using UnityEngine;
using UnityEngine.UI;

public class UIDeathScreenView : UIBaseView
{
    public GameObject RootObject;
    public Text KilledByText;
    public Button RespawnButton;

    private string _killerName;
    private float _showTime;
    private Color _killedByTextColor;
    private Color _killedByTextColorTransparent;

    void Awake()
    {
        RespawnButton.onClick.AddListener(Respawn);
        _killedByTextColor = KilledByText.color;
        _killedByTextColorTransparent = _killedByTextColor;
        _killedByTextColorTransparent.a = 0;
    }

    protected override void Show()
    {
        InputController.Instance.LockStateSwitch();
        InputController.Instance.ToUIState();
        RootObject.SetActive(true);

        if (_killerName != null)
        {
            KilledByText.gameObject.SetActive(true);
            _showTime = Time.time + 0.5f;
        }
    }
	
    protected override void Hide()
    {
        InputController.Instance.UnlockStateSwitch();
        RootObject.SetActive(false);
        _killerName = null;
    }

    public void SetKiller(string killerName)
    {
        _killerName = killerName;
        KilledByText.text = string.Format("Killed by {0}", killerName);
        KilledByText.gameObject.SetActive(true);
        _showTime = Time.time + 0.5f;
    }

    void Update()
    {
        if(KilledByText.gameObject.activeSelf)
        {
            KilledByText.color = Color.Lerp(_killedByTextColorTransparent, _killedByTextColor, Time.time - _showTime);
        }
    }

    void Respawn()
    {
        if (!World.Instance)
            return;

        World.Instance.RPC.CL_CharacterRespawn.Call();
    }
}
