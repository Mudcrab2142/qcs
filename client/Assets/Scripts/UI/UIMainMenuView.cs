﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenuView : MonoBehaviour
{
    public enum MenuState
    {
        None,
        Connect
    }

    [Header("ConnectScreen")]
    public GameObject ConnectScreenParent;
    public InputField Ip;
    public InputField Port;
    public Button ConnectButton;


    void Awake()
    {
        ConnectButton.onClick.AddListener(LoginButtonClick);

        if (Main.Test)
            LoginButtonClick();
    }

	public void LoginButtonClick()
    {
        Main.Connect(Ip.text, Port.text);
        SetState(MenuState.None);
    }

    public void SetState(MenuState state)
    {
        switch (state)
        {
            case MenuState.None:
                ConnectScreenParent.SetActive(false);
                break;
            case MenuState.Connect:
                ConnectScreenParent.SetActive(true);
                break;
        }
    }
}
