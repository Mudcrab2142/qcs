﻿using SharedLib;
using System;
using System.Collections.Generic;
using UnityEngine;

public class UIInventoryView : UIBaseView
{
    static readonly Color32 CELL_COLOR_WITH_ITEM = new Color32(208, 113, 202, 255);
    static readonly Color32 CELL_COLOR_DEFAULT = new Color32(140, 140, 140, 255);

    public event Action OnItemLockStatusUpdate;

    public RectTransform InventoryViewParent;
    public RectTransform InventoryContents;

    public GameObject InventoryItemPrototype;

    private Dictionary<NetStruct<short, short>, int> _lockedItems = new Dictionary<NetStruct<short, short>, int>();
    private Dictionary<int, Dictionary<int, GameObject>> _cells = new Dictionary<int, Dictionary<int, GameObject>>();
    private List<GameObject> _cellObjects = new List<GameObject>();
    private List<GameObject> _items = new List<GameObject>();
    private int _lastInvSizeX;
    private int _lastIntSizeY;


    protected override bool CanShow()
    {
        return base.CanShow() && World.Instance && World.Instance.ClientInventory.Backpack != null && !((Character)World.Instance.ControlledCharacter.Owner).IsDead;
    }


    void Awake()
    {
        InventoryItemPrototype.SetActive(false);
    }

    protected override void Show()
    {
        InventoryViewParent.gameObject.SetActive(true);
        Fill(World.Instance.ClientInventory.Backpack);
        World.Instance.ClientInventory.OnUpdate += UpdateInventory;
    }

    protected override void Hide()
    {
        InventoryViewParent.gameObject.SetActive(false);
        World.Instance.ClientInventory.OnUpdate -= UpdateInventory;
    }

    void UpdateInventory(InventoryData data)
    {
        Fill(World.Instance.ClientInventory.Backpack);
    }

    public void Fill(InventoryData data)
    {
        if (_lastInvSizeX != data.SizeX || _lastIntSizeY != data.SizeY)
            RebuildLayout(data.SizeX, data.SizeY);

        RecolorCells(0, 0, data.SizeX, data.SizeY, CELL_COLOR_DEFAULT);

        UpdateItems(data);
    }

    void RebuildLayout(int sizeX, int sizeY)
    {
        _lastInvSizeX = sizeX;
        _lastIntSizeY = sizeY;
        _cellObjects.ForEach(Destroy);

        var rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(sizeX * UIInventoryCell.CELL_SIZE + 10, sizeY * UIInventoryCell.CELL_SIZE + 30);
        for (short x = 0; x < sizeX; x++)
            for (short y = 0; y < sizeY; y++)
            {
                if (!_cells.ContainsKey(x))
                    _cells.Add(x, new Dictionary<int, GameObject>());
                if (!_cells[x].ContainsKey(y))
                    _cells[x].Add(y, null);

                var cell = UIInventoryCell.Create(x, y);
                var cellRectTransform = cell.GetComponent<RectTransform>();
                UITools.SetParent(cellRectTransform, InventoryContents);
                cellRectTransform.anchoredPosition = new Vector2(UIInventoryCell.CELL_SIZE * x, -UIInventoryCell.CELL_SIZE * y);
                cellRectTransform.localScale = Vector3.one;
                cell.gameObject.SetActive(true);

                _cells[x][y] = cell.gameObject;
                _cellObjects.Add(cell.gameObject);
            }
    }

    void UpdateItems(InventoryData data)
    {
        _items.ForEach(Destroy);
        foreach (var item in data.Storage)
        {
            var invItem = Instantiate(InventoryItemPrototype) as GameObject;
            var rectTransform = invItem.GetComponent<RectTransform>();
            invItem.GetComponent<UIInventoryItem>().Init(item);
            UITools.SetParent(rectTransform, InventoryContents);
            rectTransform.anchoredPosition = new Vector2(item.PositionX * UIInventoryCell.CELL_SIZE, -item.PositionY * UIInventoryCell.CELL_SIZE);
            RecolorCells(item.PositionX, item.PositionY, item.Item.Width, item.Item.Height, CELL_COLOR_WITH_ITEM);
            invItem.SetActive(true);
            _items.Add(invItem.gameObject);
        }
        CommonHelper.SafeCall(OnItemLockStatusUpdate);
    }

    void RecolorCells(int cornerX, int cornerY, int countX, int countY, Color32 color)
    {
        for (var x = cornerX; x < cornerX + countX; x++)
            for (var y = cornerY; y < cornerY + countY; y++)
                _cells[x][y].GetComponent<UIInventoryCell>().UpdateColor(color);
    }

    public bool IsItemLocked(short x, short y)
    {
        return _lockedItems.ContainsKey(new NetStruct<short, short>(x, y));
    }

    public int GetLockCount(short x, short y)
    {
        int count = 0;
        _lockedItems.TryGetValue(new NetStruct<short, short>(x, y), out count);
        return count;
    }

    public void LockItem(short x, short y, int count)
    {
        _lockedItems[new NetStruct<short, short>(x, y)] = count;
        CommonHelper.SafeCall(OnItemLockStatusUpdate);
    }

    public void UnlockItem(short x, short y)
    {
        _lockedItems.Remove(new NetStruct<short, short>(x, y));
        CommonHelper.SafeCall(OnItemLockStatusUpdate);
    }

    public void UnlockAllItems()
    {
        _lockedItems.Clear();
        CommonHelper.SafeCall(OnItemLockStatusUpdate);
    }
}
