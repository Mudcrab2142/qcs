﻿using UnityEngine;
using UnityEngine.UI;
using SharedLib;

public class UIWorldDamage : MonoBehaviour
{
    const float LIFE_TIME = 2f;
    static readonly Color32 _color = new Color32(144, 20, 20, 255);

    public static UIWorldDamage Create(Vector3 worldPos, Damage dmg)
    {
        Text text;
        var rectTransform = UITools.CreateText(out text, UIRoot.Instance.DefaultFont, 50, FontStyle.Normal, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.MiddleCenter, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero);

        UITools.SetParent(rectTransform, UIRoot.Instance.WorldUI);
        var c = rectTransform.gameObject.AddComponent<UIWorldDamage>();
        rectTransform.position = worldPos;
        text.text = dmg.Amount.ToString();
        text.color = _color;
        text.gameObject.AddComponent<Outline>();
        rectTransform.localScale = Vector3.one * 0.008f;
        c._text = text;
        return c;
    }

    float _deathTime;
    Text _text;
    float _random;

    void Start()
    {
        _deathTime = Time.time + LIFE_TIME;
        _random = Random.Range(-1f, 1f);
    }

    void Update()
    {
        if (Time.time > _deathTime)
            Destroy(gameObject);

        var t = (_deathTime - Time.time) / LIFE_TIME;

        transform.position += Vector3.up * t * 2f * Time.deltaTime;
        transform.position += transform.right * t * _random * Time.deltaTime;
        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, t);
        if(World.ClientWorldInstance)
            transform.eulerAngles = CameraController.Instance.transform.eulerAngles;
    }
}
