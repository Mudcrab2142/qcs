﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UISpellInfo : MonoBehaviour
{
    public static UISpellInfo Create(string spellName, byte spellLevel, string spellDescription)
    {
        var obj = new GameObject("UISpellInfo");
        var c = obj.AddComponent<UISpellInfo>();
        var bg = obj.AddComponent<Image>();
        bg.color = new Color32(9, 9, 9, 181);
        c._rectTransform = c.GetComponent<RectTransform>();
        c._rectTransform.pivot = c._rectTransform.anchorMin = c._rectTransform.anchorMax = new Vector2(0, 1);
        UITools.SetParent(c._rectTransform, UIRoot.Instance.transform);
        c._rectTransform.sizeDelta = new Vector2(300, 100);

        //spell name
        Text spellNameText;
        var spellNameTextRt = UITools.CreateText(out spellNameText, UIRoot.Instance.DefaultFont, 14, FontStyle.Bold, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.UpperRight, Vector2.one, Vector2.one, Vector2.one, Vector2.zero);
        spellNameText.color = UIColors.SpellNameColor;
        spellNameText.text = spellName;
        UITools.SetParent(spellNameTextRt, c._rectTransform);
        spellNameTextRt.anchoredPosition = new Vector2(-10, -10);

        //spell level
        Text spellLevelText;
        var spellLevelTextRt = UITools.CreateText(out spellLevelText, UIRoot.Instance.DefaultFont, 10, FontStyle.Normal, HorizontalWrapMode.Overflow, VerticalWrapMode.Overflow, TextAnchor.UpperRight, Vector2.one, Vector2.one, Vector2.one, Vector2.zero);
        spellLevelText.text = "Level: " + spellLevel;
        UITools.SetParent(spellLevelTextRt, c._rectTransform);
        spellLevelTextRt.anchoredPosition = new Vector2(-10, -30);

        //spell description
        Text spellDescriptionText;
        var spellDescriptionTextRt = UITools.CreateText(out spellDescriptionText, UIRoot.Instance.DefaultFont, 12, FontStyle.Normal, HorizontalWrapMode.Wrap, VerticalWrapMode.Truncate, TextAnchor.UpperLeft, new Vector2(0, 1), new Vector2(0, 1), new Vector2(0, 1), Vector2.zero);
        UITools.SetParent(spellDescriptionTextRt, c._rectTransform);
        spellDescriptionTextRt.anchoredPosition = new Vector2(10, -60);
        spellDescriptionTextRt.sizeDelta = new Vector2(280, 10);
        spellDescriptionText.supportRichText = true;
        spellDescriptionText.text = spellDescription;
        spellDescriptionTextRt.sizeDelta = new Vector2(280, spellDescriptionText.preferredHeight);
        c._rectTransform.sizeDelta = new Vector2(300, 70 + spellDescriptionText.preferredHeight);
        return c;
    }

    private RectTransform _rectTransform;

    void Update()
    {
        _rectTransform.position = Input.mousePosition + new Vector3(10, 5);
    }
}
