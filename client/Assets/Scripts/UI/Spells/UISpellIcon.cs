﻿using UnityEngine;
using SharedLib;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Lidgren.Network;

public class UISpellIcon : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public SpellName SpellName;
    private UISpellInfo _spellInfoPopup;
    private Button _learnButton;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!World.ClientWorldInstance)
            return;

        CreateSpellInfo();
    }

    void CreateSpellInfo()
    {
        var spellName = Localization.GetValue(string.Format("{0}_name", SpellName));
        var spellLevel = World.ClientWorldInstance.ClientSpells.Level[SpellName];
        var spellDescription = Localization.GetSpellDescription(SpellName, spellLevel);

        _spellInfoPopup = UISpellInfo.Create(spellName, spellLevel, spellDescription);
    }

    void DestroySpellInfo()
    {
        if (_spellInfoPopup)
            Destroy(_spellInfoPopup.gameObject);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        DestroySpellInfo();
    }

    void OnDisable()
    {
        DestroySpellInfo();
    }

    void Awake()
    {
        _learnButton = GetComponentInChildren<Button>();
        _learnButton.onClick.AddListener(TryLearnSpell);
    }

    void TryLearnSpell()
    {
        if (World.Instance)
            World.Instance.ClientSpells.TryLearnSpell(SpellName, LearnSpellCallback);
    }

    void LearnSpellCallback(bool result, NetConnection connection)
    {
        Debug.LogFormat("Learn spell {0} result = {1}.", SpellName, result);
        if (_spellInfoPopup && result)
        {
            DestroySpellInfo();
            CreateSpellInfo();
        }
    }
}
