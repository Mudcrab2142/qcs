﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UITools
{
	public static RectTransform CreateText(out Text text, Font font, int fontSize, FontStyle fontStyle, HorizontalWrapMode hWrap, VerticalWrapMode vWrap, TextAnchor anchor, Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Vector2 size)
    {
        var obj = new GameObject("Text");
        text = obj.AddComponent<Text>();
        var rectTransform = obj.GetComponent<RectTransform>();
        text.font = font;
        text.fontSize = fontSize;
        text.fontStyle = fontStyle;
        text.horizontalOverflow = hWrap;
        text.verticalOverflow = vWrap;
        text.alignment = anchor;
        rectTransform.anchorMin = anchorMin;
        rectTransform.anchorMax = anchorMax;
        rectTransform.pivot = pivot;
        rectTransform.sizeDelta = size;
        return rectTransform;
    }
    

    public static RectTransform CreateImage(out Image img, Sprite sprite, Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Vector2 size)
    {
        var obj = new GameObject("Image");
        img = obj.AddComponent<Image>();
        var rectTransform = obj.GetComponent<RectTransform>();
        img.overrideSprite = sprite;
        rectTransform.anchorMin = anchorMin;
        rectTransform.anchorMax = anchorMax;
        rectTransform.pivot = pivot;
        rectTransform.sizeDelta = size;
        return rectTransform;
    }

    public static Image CreateImage(out RectTransform rectTransform, Sprite sprite, Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Vector2 size)
    {
        var obj = new GameObject("Image");
        var img = obj.AddComponent<Image>();
        rectTransform = obj.GetComponent<RectTransform>();
        img.overrideSprite = sprite;
        rectTransform.anchorMin = anchorMin;
        rectTransform.anchorMax = anchorMax;
        rectTransform.pivot = pivot;
        rectTransform.sizeDelta = size;
        return img;
    }

    public static void SetParent(RectTransform child, Transform parent)
    {
        child.SetParent(parent);
        child.localScale = Vector3.one;
    }
}
