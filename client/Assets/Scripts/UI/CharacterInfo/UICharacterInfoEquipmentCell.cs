﻿using UnityEngine;
using SharedLib;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using System.Linq;

public class UICharacterInfoEquipmentCell : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    public RawImage EquippedItemIcon;

    private ItemInstance _item;
    private UIInventoryItemInfo _itemInfoPopup;

    private Vector3 _originalScale;
    private Vector2 _beginDragCursorPosition;
    private Vector2 _beginDragItemPosition;
    private bool _dragging;
    private RectTransform _rectTransform;

    public byte SlotId { get; private set; }

    void Awake()
    {
        _rectTransform = EquippedItemIcon.GetComponent<RectTransform>();
        _originalScale = _rectTransform.localScale;
    }

    public void UpdateCell(EquipmentSlot slot)
    {
        if (slot == null)
            return;

        _item = slot.Item;
        SlotId = slot.SlotId;

        if (slot.Item == null)
            EquippedItemIcon.texture = null;
        else
            EquippedItemIcon.texture = UIImageStorage.GetItemIcon(slot.Item.Icon);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_item == null)
            return;

        _itemInfoPopup = UIInventoryItemInfo.Create(_item);
        var rectTransform = _itemInfoPopup.GetComponent<RectTransform>();
        UITools.SetParent(rectTransform, transform.parent);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_itemInfoPopup)
            Destroy(_itemInfoPopup.gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _beginDragCursorPosition = new Vector2(_rectTransform.position.x, _rectTransform.position.y) - eventData.position;
        _beginDragItemPosition = _rectTransform.anchoredPosition;
        _dragging = true;
        UITools.SetParent(_rectTransform, UIRoot.Instance.transform);
        _rectTransform.SetAsLastSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        _rectTransform.position = _beginDragCursorPosition + eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        var raycastPos = new Vector2(_rectTransform.position.x + UIInventoryCell.CELL_SIZE / 2f, _rectTransform.position.y - UIInventoryCell.CELL_SIZE / 2f);
        var raycastResult = UIRoot.Instance.Raycast(eventData, raycastPos);
        CheckUnequip(raycastResult);
        _dragging = false;
        UITools.SetParent(_rectTransform, transform);
        _rectTransform.anchoredPosition = _beginDragItemPosition;
        _rectTransform.localScale = _originalScale;
    }

    void CheckUnequip(List<RaycastResult> result)
    {
        var cells = result.Where(x => x.gameObject.GetComponent<UIInventoryCell>()).ToArray();
        if (cells.Length == 0 || !World.Instance)
            return;
        var cell = cells[0].gameObject.GetComponent<UIInventoryCell>();
        World.Instance.ClientInventory.TryUnequipItem(SlotId, cell.PositionX, cell.PositionY, OnUnequipSuccess, OnUnequipFail);
    }

    void OnUnequipSuccess()
    {

    }

    void OnUnequipFail()
    {

    }

    void Update()
    {
        if (_itemInfoPopup)
        {
            if (_dragging && _itemInfoPopup.enabled)
                _itemInfoPopup.gameObject.SetActive(false);

            if (!_dragging && !_itemInfoPopup.enabled)
                _itemInfoPopup.gameObject.SetActive(true);
        }
    }
}
