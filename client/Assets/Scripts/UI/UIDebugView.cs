﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ClientLib;
using System;

public class UIDebugView : MonoBehaviour
{
    [Header("Network")]
    public GameObject NetworkStatsRoot;
    public Text BytesIn;
    public Text BytesOut;
    public Text PacketsIn;
    public Text PacketsOut;

    public void ShowNetworkStats()
    {
        NetworkStatsRoot.SetActive(true);

        LidgrenRootClient.BytesReceivedPerSecond.OnValueChanged += BytesInChanged;
        LidgrenRootClient.BytesSentPerSecond.OnValueChanged += BytesOutChanged;
        LidgrenRootClient.PacketsReceivedPerSecond.OnValueChanged += PacketsInChanged;
        LidgrenRootClient.PacketsSentPerSecond.OnValueChanged += PacketsOutChanged;

        BytesInChanged(LidgrenRootClient.BytesReceivedPerSecond.Value);
        BytesOutChanged(LidgrenRootClient.BytesSentPerSecond.Value);
        PacketsInChanged(LidgrenRootClient.PacketsReceivedPerSecond.Value);
        PacketsInChanged(LidgrenRootClient.PacketsSentPerSecond.Value);
    }

    public void HideNetworkStats()
    {
        NetworkStatsRoot.SetActive(false);

        LidgrenRootClient.BytesReceivedPerSecond.OnValueChanged -= BytesInChanged;
        LidgrenRootClient.BytesSentPerSecond.OnValueChanged -= BytesOutChanged;
        LidgrenRootClient.PacketsReceivedPerSecond.OnValueChanged -= PacketsInChanged;
        LidgrenRootClient.PacketsSentPerSecond.OnValueChanged -= PacketsOutChanged;
    }

    private void PacketsOutChanged(int obj)
    {
        PacketsOut.text = obj.ToString();
    }

    private void PacketsInChanged(int obj)
    {
        PacketsIn.text = obj.ToString();
    }

    private void BytesOutChanged(int obj)
    {
        BytesOut.text = obj.ToString();
    }

    private void BytesInChanged(int obj)
    {
        BytesIn.text = obj.ToString();
    }
}
