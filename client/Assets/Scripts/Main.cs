﻿using UnityEngine;
using ClientLib;
using System;

public class Main : MonoBehaviour
{
    public static bool Test;

	void Awake()
    {
        Test = Environment.CommandLine.Contains("-test");
        InputController.CreateInstance(true);
        Pool.CreateInstance(true);
        Localization.Init();
    }

    void Start()
    {
        UIRoot.Instance.MainMenu.SetState(UIMainMenuView.MenuState.Connect);
    }

    public static void Connect(string ip, string port)
    {
        NetworkClientWorld.JoinWorld<World>(ip, int.Parse(port));
    }
}
