﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class SpellTraceGenerator : MonoBehaviour {

	[MenuItem("Tools/GenerateSpellTrace")]
    static void Generate()
    {
        if (Selection.gameObjects.Length != 1)
            return;

        var obj = Selection.gameObjects[0].transform;

        var so = ScriptableObjectUtility.CreateAsset<SpellTraceConfig>();
        so.TraceList = new System.Collections.Generic.List<SpellTraceConfig.TraceConfigItem>();
        for (var i = 0; i < obj.childCount; i++)
        {
            var c = obj.GetChild(i);
            var item = new SpellTraceConfig.TraceConfigItem();
            if (c.name.Contains("Cube"))
            {
                item.Type = SpellTraceConfig.TraceType.Box;
                item.Size = c.transform.localScale / 2f;
                item.Orientation = c.transform.rotation;
                item.Center = c.transform.position;
                item.Enabled = true;
                so.TraceList.Add(item);
            }
        }
    }
}
