﻿using SharedLib;
using System.Text;

public static class SpellLocalizationHelper
{
    public static void AppendSkillDamageText(this StringBuilder sb, short min, short max, DamageType type)
    {
        sb.AppendFormat("<b><color={0}>{1} - {2}</color></b>", UIColors.PhysicalDmgColor, min, max);
    }

    public static void AppendSkillDescriptionTitleText(this StringBuilder sb, string title)
    {
        sb.AppendFormat("<b>{0}</b>", title);
    }

    public static void AppendSkillDescriptionText(this StringBuilder sb, string text)
    {
        sb.Append(text);
    }
}
