﻿using SharedLib;
using System;
using System.Collections.Generic;
using UnityEngine;

public static class Localization
{
    public static Language CurrentLanguage;
    private static bool _initialized;

    private readonly static Dictionary<string, string> _data = new Dictionary<string, string>();
    private readonly static Dictionary<string, Language> _lTable = new Dictionary<string, Language>()
    {
        { "EN", Language.English },
        { "RU", Language.Russian }
    };

    public static void Init()
    {
        CurrentLanguage = Language.English;
        LoadFile("spells");
        _initialized = true;
    }

    public static string GetValue(string key)
    {
        if (!_initialized)
            return "not initialized";

        key = GetKey(key, CurrentLanguage);
        string result = "null";
        _data.TryGetValue(key, out result);
        return result;
    }

    public static string GetSpellDescription(SpellName name, byte level, params object[] data)
    {
        if (!_initialized || !World.ClientWorldInstance)
            return "not initialized";

        var key = string.Format("{0}_description_{1}", name, CurrentLanguage);
        string result;
        _data.TryGetValue(key, out result);
        Action a = () => result = string.Format(result, data);
        try
        {
            result = string.Format(result, data);
        }
        catch(Exception e)
        {

        }
        return result;
    }

    static void LoadFile(string fileName)
    {
        var asset = Resources.Load("Localization/" + fileName) as TextAsset;
        string currentKey = "null";

        foreach (var line in asset.text.Split(new[] { '\n' }))
        {
            var tLine = line.Trim();
            if (tLine.Length == 0)
                continue;

            if (tLine[0] != '[')
                continue;

            if (tLine[1] == '.')
            {
                currentKey = ParseKey(line);
                continue;
            }

            Language lang = Language.Null;
            string data = "";
            ParseData(line, out lang, out data);

            _data[GetKey(currentKey, lang)] = data;
        }
    }

    static string ParseKey(string str)
    {
        var l = str.IndexOf(']');
        return str.Substring(2, l - 2);
    }

    static void ParseData(string str, out Language lang, out string data)
    {
        lang = Language.Null;
        data = "";

        var l = str.IndexOf(']');
        var lStr = str.Substring(1, l - 1);
        if (_lTable.ContainsKey(lStr))
            lang = _lTable[lStr];
        data = str.Substring(l + 1, str.Length - l - 1);
        data = data.Replace('@', '\n');
    }

    static string GetKey(string key, Language lang)
    {
        return string.Format("{0}_{1}", key, lang);
    }
}
