﻿using UnityEngine;
using System.Collections.Generic;

public static class PhysicsHelper
{
    static readonly Collider[] _results = new Collider[1024];

    public static List<Creature> BoxOverlapCreatures(Vector3 position, Vector3 size, Quaternion rotation)
    {
        var count = Physics.OverlapBoxNonAlloc(position, size, _results, rotation);

       // DebugHelper.DrawBox(position, size, rotation, Color.red, 5f);
        var result = new List<Creature>();
        for(var i = 0; i < count; i++)
        {
            var c = _results[i].attachedRigidbody.GetComponent<Creature>();
            
            if (!result.Contains(c) && c != null)
                result.Add(c);
        }
        return result;
    }
}
