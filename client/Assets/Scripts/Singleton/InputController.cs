﻿using SharedLib;
using UnityEngine;

public class InputController : MonoSingleton<InputController>
{
    public const KeyCode Interact1Key = KeyCode.E;
    public const KeyCode Interact2Key = KeyCode.Q;
    public const KeyCode Interact3Key = KeyCode.Z;
    public const KeyCode Interact4Key = KeyCode.X;

    public enum InputState
    {
        Game,
        UI
    }

    public InputState State { get; private set; }

    public Vector2 InputAxis { get; private set; }
    public Vector2 MouseAxis { get; private set; }

    public bool AirshipUp { get; private set; }
    public bool AirshipDown { get; private set; }
    public bool AirshipForward { get; private set; }
    public bool AirshipBack { get; private set; }
    public bool AirshipLeft { get; private set; }
    public bool AirshipRight { get; private set; }

    public bool Interact1Press { get; private set; }
    public bool Interact2Press { get; private set; }
    public bool Interact3Press { get; private set; }
    public bool Interact4Press { get; private set; }

    public bool Interact1Hold { get; private set; }
    public bool Interact2Hold { get; private set; }
    public bool Interact3Hold { get; private set; }
    public bool Interact4Hold { get; private set; }


    private PlayerInputState _playerInput = new PlayerInputState();

    private float _lastForwardPressTime;
    private float _lastBackPressTime;
    private float _lastLeftPressTime;
    private float _lastRightPressTime;
    private bool _stateLocked;

    void Update()
    {
        _playerInput.Horizontal = Input.GetAxis("Horizontal");
        _playerInput.Vectical = Input.GetAxis("Vertical");
        _playerInput.AttackPress = Input.GetMouseButtonDown(0);
        _playerInput.AttackHold = Input.GetMouseButton(0);
        _playerInput.ForwardDoublePress = _lastForwardPressTime + 0.5f > Time.time && !_playerInput.ForwardDoublePress && Input.GetKeyDown(KeyCode.W);
        _playerInput.BackDoublePress = _lastBackPressTime + 0.5f > Time.time && !_playerInput.BackDoublePress && Input.GetKeyDown(KeyCode.S);
        _playerInput.LeftDoublePress = _lastLeftPressTime + 0.5f > Time.time && !_playerInput.LeftDoublePress && Input.GetKeyDown(KeyCode.A);
        _playerInput.RightDoublePress = _lastRightPressTime + 0.5f > Time.time && !_playerInput.RightDoublePress && Input.GetKeyDown(KeyCode.D);

        if (Input.GetKeyDown(KeyCode.W))
            _lastForwardPressTime = Time.time;
        if (Input.GetKeyDown(KeyCode.S))
            _lastBackPressTime = Time.time;
        if (Input.GetKeyDown(KeyCode.A))
            _lastLeftPressTime = Time.time;
        if (Input.GetKeyDown(KeyCode.D))
            _lastRightPressTime = Time.time;

        if (Input.GetKeyDown(KeyCode.LeftAlt) && !_stateLocked)
        {
            if (State == InputState.Game)
                ToUIState();
            else
                ToGameState();
        }


        if (State == InputState.Game)
        {
            AirshipUp = Input.GetKeyDown(KeyCode.Q);
            AirshipDown = Input.GetKeyDown(KeyCode.Z);
            AirshipForward = Input.GetKeyDown(KeyCode.W);
            AirshipBack = Input.GetKeyDown(KeyCode.S);
            AirshipLeft = Input.GetKeyDown(KeyCode.A);
            AirshipRight = Input.GetKeyDown(KeyCode.D);
            MouseAxis = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

            Interact1Press = Input.GetKeyDown(Interact1Key);
            Interact2Press = Input.GetKeyDown(Interact2Key);
            Interact3Press = Input.GetKeyDown(Interact3Key);
            Interact4Press = Input.GetKeyDown(Interact4Key);

            Interact1Hold = Input.GetKey(Interact1Key);
            Interact2Hold = Input.GetKey(Interact2Key);
            Interact3Hold = Input.GetKey(Interact3Key);
            Interact4Hold = Input.GetKey(Interact4Key);
        }
        else
        {
            AirshipUp = false;
            AirshipDown = false;
            AirshipForward = false;
            AirshipBack = false;
            AirshipLeft = false;
            AirshipRight = false;
            _playerInput.Horizontal = 0f;
            _playerInput.Vectical = 0f;
            MouseAxis = Vector2.zero;
            _playerInput.AttackPress = false;
            _playerInput.AttackHold = false;
            _playerInput.ForwardDoublePress = false;
            _playerInput.BackDoublePress = false;
            _playerInput.LeftDoublePress = false;
            _playerInput.RightDoublePress = false;
            Interact1Press = false;
            Interact2Press = false;
            Interact3Press = false;
            Interact4Press = false;
            Interact1Hold = false;
            Interact2Hold = false;
            Interact3Hold = false;
            Interact4Hold = false;
        }
    }

    public void ToGameState()
    {
        if (State == InputState.Game)
            return;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        State = InputState.Game;
    }

    public void ToUIState()
    {
        if (State == InputState.UI)
            return;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        State = InputState.UI;
    }

    public PlayerInputState GetInputState()
    {
        return _playerInput;
    }

    public void LockStateSwitch()
    {
        _stateLocked = true;
    }

    public void UnlockStateSwitch()
    {
        _stateLocked = false;
    }
}
