using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoSingleton<CameraController>
{
    public const byte STATE_DEFAULT = 1;
    public const byte STATE_CROUCH = 2;

    public static readonly Dictionary<byte, CameraState> CameraStates = new Dictionary<byte, CameraState>()
    {
        {STATE_DEFAULT, new CameraState { Id = STATE_DEFAULT, Forward = -1, Right = 0.15f, DefaultDistance = 3f, MaxDistance = 4f, MinDistance = 1f, Height = 0.4f, SmoothFollow = 5f, YMinLimit = -40, YMaxLimit = 80, XMinLimit = -360f, XMaxLimit = 360f, CullingHeight = 0.8f, CullingMinDist = 0.1f, FixedAngle = Vector2.zero, UseZoom = true, CameraMode = TPCameraMode.FreeDirectional } } ,
        {STATE_CROUCH, new CameraState { Id = STATE_CROUCH, Forward = -1, Right = 0.13f, DefaultDistance = 2f, MaxDistance = 3f, MinDistance = 1f, Height = 0.3f, SmoothFollow = 5f, YMinLimit = -40, YMaxLimit = 80, XMinLimit = -360f, XMaxLimit = 360f, CullingHeight = 0f, CullingMinDist = 0.1f, FixedAngle = Vector2.zero, UseZoom = true, CameraMode = TPCameraMode.FreeDirectional } }
    };

    public Camera Camera { get; private set; }

    public float MouseSensitivityX = 3f;
    public float MouseSensitivityY = 3f;
    public float SmoothBetweenState = 0.05f;
    public float smoothCameraRotation = 12f;
    public float ScrollSpeed = 10f;
    public LayerMask CullingLayer;
    public float ClipPlaneMargin;

    public bool LockCamera;

    public float OffSetPlayerPivot;
    public byte CurrentStateIndex;
    public bool OnlyLookAt;
    public CameraState CurrentState;

    private CameraState _lerpState;
    private Transform _targetLookAt;
    private Transform _currentTarget;
    private Vector3 _currentTargetPos;
    private Vector3 _cPos;
    private Vector3 _oldTargetPos;
    private bool _useSmooth;

    private float _distance = 5f;
    private float _mouseY = 0f;
    private float _mouseX = 0f;
    private float _targetHeight;
    private float _currentZoom;
    private float _desiredDistance;
    private float _oldDistance;

    private BaseInteractable _currentInteractionTarget;
    private Collider _currentInteractionCollider;

    void Awake()
    {
        Camera = gameObject.AddComponent<Camera>();
    }

    public void Init(Transform target)
    { 
        _currentTarget = target;
        _currentTargetPos = new Vector3(_currentTarget.position.x, _currentTarget.position.y + OffSetPlayerPivot, _currentTarget.position.z);
        _targetLookAt = new GameObject("targetLookAt").transform;
        _targetLookAt.position = _currentTarget.position;
        _targetLookAt.hideFlags = HideFlags.HideInHierarchy;
        _targetLookAt.rotation = _currentTarget.rotation;
        // initialize the first camera state
        _mouseY = _currentTarget.eulerAngles.x;
        _mouseX = _currentTarget.eulerAngles.y;

        _lerpState = CameraStates[STATE_DEFAULT];
        CurrentState = CameraStates[STATE_DEFAULT];

        _currentZoom = CurrentState.DefaultDistance;
        _distance = CurrentState.DefaultDistance;
        _targetHeight = CurrentState.Height;
    }

    void Update()
    {
        CameraMovement(Time.deltaTime);
        if (_currentInteractionTarget == null && Camera != null)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.transform.position, Camera.transform.forward, out hit, 7f, (1 << 0) | (1 << 10)))
            {
                _currentInteractionTarget = hit.collider.GetComponent<BaseInteractable>();

                if (_currentInteractionTarget != null && !_currentInteractionTarget.CanInteract())
                    _currentInteractionTarget = null;

                if (_currentInteractionTarget != null)
                {
                    _currentInteractionCollider = hit.collider;
                    _currentInteractionTarget.Activate();
                    UIRoot.Instance.HUDController.ShowInteractionText(_currentInteractionTarget.GetInteractionTitle(), _currentInteractionTarget.GetActionDescriptions(), _currentInteractionTarget.GetActionMask());
                }
            }
        }
        else if (_currentInteractionCollider != null)
        {
            RaycastHit hit;
            if (!_currentInteractionCollider.Raycast(new Ray(Camera.transform.position, Camera.transform.forward), out hit, 7f))
            {
                UIRoot.Instance.HUDController.HideInteractionText();
                _currentInteractionTarget.Deactivate();
                _currentInteractionTarget = null;
                _currentInteractionCollider = null;
            }
        }
    }

    /// <summary>
    /// Change CameraState
    /// </summary>
    /// <param name="stateName"></param>
    /// <param name="Use smoth"></param>
    public void ChangeState(byte targetState, bool hasSmooth)
    {
        if (!CameraStates.ContainsKey(targetState))
            return;

        if (/*CurrentState != null &&*/ CurrentState.Id == targetState)
            return;

        var state = CameraStates[targetState];

        CurrentStateIndex = targetState;
        CurrentState.CameraMode = state.CameraMode;
        _lerpState = state;

        if (!hasSmooth)
            CurrentState = state;

        _currentZoom = state.DefaultDistance;
        CurrentState.FixedAngle = new Vector3(_mouseX, _mouseY);
    }

    public void Zoom(float scroolValue)
    {
        _currentZoom -= scroolValue * ScrollSpeed;
    }

    public void RotateCamera(Vector2 inp)
    {
        if (!CurrentState.CameraMode.Equals(TPCameraMode.FixedAngle))
        {
            // free rotation
            _mouseX += inp.x * MouseSensitivityX;
            _mouseY -= inp.y * MouseSensitivityY;
            if (!LockCamera)
            {
                _mouseY = Extensions.ClampAngle(_mouseY, CurrentState.YMinLimit, CurrentState.YMaxLimit);
                _mouseX = Extensions.ClampAngle(_mouseX, CurrentState.XMinLimit, CurrentState.XMaxLimit);
            }
            else
            {
                _mouseY = _currentTarget.root.localEulerAngles.x;
                _mouseX = _currentTarget.root.localEulerAngles.y;
            }
        }
        else
        {
            // fixed rotation
            _mouseX = CurrentState.FixedAngle.x;
            _mouseY = CurrentState.FixedAngle.y;
        }
    }

    /// <summary>
    /// Camera behaviour
    /// </summary>    
    public void CameraMovement(float deltaTime)
    {
        if (_currentTarget == null)
            return;

     //   if (_useSmooth)
            CurrentState = CurrentState.Slerp(_lerpState, SmoothBetweenState);
  /*      else
            CurrentState = _lerpState;*/

        if (CurrentState.UseZoom)
        {
            _currentZoom = Mathf.Clamp(_currentZoom, CurrentState.MinDistance, CurrentState.MaxDistance);
            _distance = _useSmooth ? Mathf.Lerp(_distance, _currentZoom, 2f * deltaTime) : _currentZoom;
        }
        else
        {
            _distance = _useSmooth ? Mathf.Lerp(_distance, CurrentState.DefaultDistance, 2f * deltaTime) : CurrentState.DefaultDistance;
            _currentZoom = _distance;
        }

        _desiredDistance = _distance;
        var camDir = (CurrentState.Forward * _targetLookAt.forward) + (CurrentState.Right * _targetLookAt.right);
        camDir = camDir.normalized;

        var targetPos = new Vector3(_currentTarget.position.x, _currentTarget.position.y + OffSetPlayerPivot, _currentTarget.position.z);
        _currentTargetPos = /*_useSmooth ? Vector3.Lerp(_currentTargetPos, targetPos, _lerpState.SmoothFollow * deltaTime) :*/ targetPos;
        _cPos = _currentTargetPos + new Vector3(0, _targetHeight, 0);
        _oldTargetPos = targetPos + new Vector3(0, CurrentState.Height, 0);

        RaycastHit hitInfo;
        ClipPlanePoints planePoints = Camera.NearClipPlanePoints(_cPos + (camDir * (_distance)), ClipPlaneMargin);
        ClipPlanePoints oldPoints = Camera.NearClipPlanePoints(_oldTargetPos + (camDir * _oldDistance), ClipPlaneMargin);
        if (CullingRayCast(_cPos, planePoints, out hitInfo, _distance + 0.2f, CullingLayer)) _distance = _desiredDistance;

        if (CullingRayCast(_oldTargetPos, oldPoints, out hitInfo, _oldDistance + 0.2f, CullingLayer))
        {
            var t = _distance - 0.2f;
            t -= CurrentState.CullingMinDist;
            t /= (_distance - CurrentState.CullingMinDist);
            _targetHeight = Mathf.Lerp(CurrentState.CullingHeight, CurrentState.Height, Mathf.Clamp(t, 0.0f, 1.0f));
            _cPos = _currentTargetPos + new Vector3(0, _targetHeight, 0);
        }
        else
        {
            _oldDistance = _useSmooth ? Mathf.Lerp(_oldDistance, _distance, 2f * deltaTime) : _distance;
            _targetHeight = _useSmooth ? Mathf.Lerp(_targetHeight, CurrentState.Height, 2f * deltaTime) : CurrentState.Height;
        }

        var lookPoint = _cPos;
        lookPoint += (_targetLookAt.right * Vector3.Dot(camDir * (_distance), _targetLookAt.right));
        _targetLookAt.position = _cPos;
        Quaternion newRot = Quaternion.Euler(_mouseY, _mouseX, 0);
        _targetLookAt.rotation = _useSmooth ? Quaternion.Slerp(_targetLookAt.rotation, newRot, smoothCameraRotation * deltaTime) : newRot;
        transform.position = _cPos + (camDir * (_distance));

        transform.LookAt(lookPoint);
    }

    /// <summary>
    /// Custom Raycast using NearClipPlanesPoints
    /// </summary>
    /// <param name="to"></param>
    /// <param name="from"></param>
    /// <param name="hitInfo"></param>
    /// <param name="distance"></param>
    /// <param name="cullingLayer"></param>
    /// <returns></returns>
    bool CullingRayCast(Vector3 from, ClipPlanePoints to, out RaycastHit hitInfo, float distance, LayerMask cullingLayer)
    {
        bool value = false;
        /*  if (_showGizmos)
          {
              Debug.DrawRay(from, to.LowerLeft - from);
              Debug.DrawLine(to.LowerLeft, to.LowerRight);
              Debug.DrawLine(to.UpperLeft, to.UpperRight);
              Debug.DrawLine(to.UpperLeft, to.LowerLeft);
              Debug.DrawLine(to.UpperRight, to.LowerRight);
              Debug.DrawRay(from, to.LowerRight - from);
              Debug.DrawRay(from, to.UpperLeft - from);
              Debug.DrawRay(from, to.UpperRight - from);
          }*/
        if (Physics.Raycast(from, to.LowerLeft - from, out hitInfo, distance, cullingLayer))
        {
            value = true;
            _desiredDistance = hitInfo.distance;
        }

        if (Physics.Raycast(from, to.LowerRight - from, out hitInfo, distance, cullingLayer))
        {
            value = true;
            if (_desiredDistance > hitInfo.distance) _desiredDistance = hitInfo.distance;
        }

        if (Physics.Raycast(from, to.UpperLeft - from, out hitInfo, distance, cullingLayer))
        {
            value = true;
            if (_desiredDistance > hitInfo.distance) _desiredDistance = hitInfo.distance;
        }

        if (Physics.Raycast(from, to.UpperRight - from, out hitInfo, distance, cullingLayer))
        {
            value = true;
            if (_desiredDistance > hitInfo.distance) _desiredDistance = hitInfo.distance;
        }

        return value;
    }
}
