﻿using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    public static void CreateInstance(bool dontDestroyOnLoad)
    {
        _instance = new GameObject(typeof(T).ToString()).AddComponent<T>();
        if (dontDestroyOnLoad)
            DontDestroyOnLoad(_instance.gameObject);
    }

    private static T _instance = null;
    public static T Instance
    {
        get
        {
            if (_instance)
                return _instance;
            else
                Debug.LogError(string.Format("[MonoSingleton] {0} does not exists!", typeof(T).ToString()));

            return _instance;
        }
    }

    /// <summary>
    /// Clear the reference when the application quits. Override when necessary and call base.OnApplicationQuit() last.
    /// </summary>
    protected virtual void OnApplicationQuit()
    {
        _instance = null;
    }
}