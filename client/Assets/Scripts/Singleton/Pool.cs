﻿using UnityEngine;
using SharedLib;
using System;

public class Pool : MonoSingleton<Pool>
{
    public UnityPool<BaseEffect> Effects { get; private set; }

	void Awake()
    {
        Effects = new UnityPool<BaseEffect>(BaseEffect.Create, CreateTransform(typeof(BaseEffect)));
    }

    private Transform CreateTransform(Type t)
    {
        return new GameObject("[Pool]" + t).transform;
    }
}
