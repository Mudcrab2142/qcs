﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SpellTraceConfig : ScriptableObject
{
    public enum TraceType
    {
        Box,
        Sphere
    }

    [Serializable]
    public class TraceConfigItem
    {
        public float Time;
        public TraceType Type;
        public Vector3 Center;
        public Vector3 Size;
        public Quaternion Orientation;
        public bool Enabled;

        public TraceConfigItem()
        {
            Orientation = Quaternion.identity;
        }
    }

    public List<TraceConfigItem> TraceList;
}
