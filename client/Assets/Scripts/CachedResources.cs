﻿using UnityEngine;
using System.Collections.Generic;

public static class CachedResources
{
    static Dictionary<string, Object> _resourceCache = new Dictionary<string, Object>();

    public static T Load<T>(string path) where T : Object
    {
        Object result;
        if (_resourceCache.TryGetValue(path, out result))
            return result as T;
        result = Resources.Load<T>(path);
        _resourceCache[path] = result;
        return (T)result;
    }
}
