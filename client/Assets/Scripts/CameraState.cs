﻿using UnityEngine;

public struct CameraState
{
    public byte Id;
	public float Forward;
	public float Right;
    public float DefaultDistance;
	public float MaxDistance;
	public float MinDistance;
	public float Height;
    public float SmoothFollow;
    public float YMinLimit;
    public float YMaxLimit;
    public float XMinLimit;
    public float XMaxLimit;
    public float CullingHeight;
    public float CullingMinDist;
    public bool UseZoom;   
    public Vector2 FixedAngle;
    public TPCameraMode CameraMode;
}

public enum TPCameraMode
{
    FreeDirectional,
    FixedAngle,
    FixedPoint
}