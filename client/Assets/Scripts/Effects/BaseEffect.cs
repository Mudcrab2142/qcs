﻿using UnityEngine;
using System.Collections;
using SharedLib;
using System;

public class BaseEffect : MonoBehaviour, IUnityPoolable
{
    public static BaseEffect Create(string effectName)
    {
        var r = Instantiate(CachedResources.Load<BaseEffect>("Effects/" + effectName));
        return r;
    }

    public Action Free { get; set; }

    public virtual void OnPoolEnter()
    {

    }

    public virtual void OnPoolExit()
    {

    }

    public virtual void Play()
    {

    }
}
