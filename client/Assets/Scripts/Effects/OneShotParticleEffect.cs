﻿using UnityEngine;
using System.Collections;

public class OneShotParticleEffect : BaseEffect
{ 
    private ParticleSystem _particleSystem;
    private bool _played;

	void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    public override void Play()
    {
        if (_played)
            return;

        base.Play();
        _particleSystem.Play();
        _played = true;
    }

    public override void OnPoolEnter()
    {
        base.OnPoolEnter();
        _played = false;
    }

    void Update()
    {
        if (_played && _particleSystem.isStopped)
            Free();
    }
}
