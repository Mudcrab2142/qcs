﻿using UnityEngine;
using SharedLib;
using ClientLib;
using UnityEngine.SceneManagement;

public class World : NetworkClientWorld
{
    public static World ClientWorldInstance { get { return Instance as World; } }

    protected override NetFuncStorage CreateNetFuncStorage()
    {
        return new ClientNetFuncStorage(NetworkRoot);
    }

    protected override AsyncOperation LoadWorld(string worldName)
    {    
        CameraController.CreateInstance(false);
        return SceneManager.LoadSceneAsync(worldName, LoadSceneMode.Additive);
    }

    protected override void OnConnectedToServer()
    {
        Debug.Log("Connected to server!");
    }

    protected override void WorldUpdate()
    {
        if (Input.GetKeyDown(KeyCode.F11) || Main.Test)
            RPC.CL_CoreCreateCharacter.Call(new NetStruct<string, byte>("govno", 1), (r, c) => { return; });
    }

    protected override void OnDisconnectedFromServer()
    {
       
    }

    protected override void InitWorldEntityFactory()
    {
        AddFactoryMethod(EntityType.Character, BaseCharacter.Create);
        AddFactoryMethod(EntityType.DummyCreature, DummyCreature.Create);
        AddFactoryMethod(EntityType.WorldSpawn, WorldSpawn.Create);
        AddFactoryMethod(EntityType.Airship, Airship.Create);
        AddFactoryMethod(EntityType.AirshipBalloon, AirshipBalloon.Create);
        AddFactoryMethod(EntityType.MagicMissile, MagicMissile.Create);
    }
}
