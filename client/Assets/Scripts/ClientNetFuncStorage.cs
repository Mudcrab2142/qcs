﻿using System;
using Lidgren.Network;
using SharedLib;
using UnityEngine;

public class ClientNetFuncStorage : ClientLib.ClientNetFuncStorage
{
    public ClientNetFuncStorage(LidgrenRoot connection) : base(connection) { }

    protected override void InitHandlers()
    {
        S_NotifyDamageDeal.SetHandler(NotifyDamageDeal);
        S_CharacterDeath.SetHandler(CharacterDeath);
        S_CharacterRespawn.SetHandler(CharacterRespawn);

        S_EntityMagicMissileExplode.SetHandler(MagicMissile.Explode);
    }

    private void NotifyDamageDeal(NetStruct<int, Damage> data, NetConnection connection)
    {
        var entity = World.Instance.GetEntity<WorldEntity>(data.Value1);
        if (entity)
            UIWorldDamage.Create(entity.transform.position, data.Value2);
    }

    private void CharacterDeath(string killerName, NetConnection connection)
    {
        UIRoot.Instance.DeathScreen.SetKiller(killerName);
    }

    private bool CharacterRespawn(ProtoVector3 position, NetConnection connection)
    {
        var character = World.Instance.ControlledCharacter.Owner;
        if(character != null)
        {
            character.transform.position = position;
            return true;
        }
        else
        {
            Debug.LogError("TODO: DISCONNECT");
        }
        return false;
    }

}
