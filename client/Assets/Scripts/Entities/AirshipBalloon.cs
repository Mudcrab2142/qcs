﻿using UnityEngine;
using SharedLib;

public class AirshipBalloon : BaseEntity
{
    public override bool UseLerp { get { return true; } }
    public override float LerpTime { get { return 0.2f; } }

    public static AirshipBalloon Create(NetworkBaseEntity networkEntity)
    {
        var obj = Instantiate(CachedResources.Load<GameObject>("Entities/Airship/airship_balloon"));
        var component = obj.AddComponent<AirshipBalloon>();
        return component;
    }
}
