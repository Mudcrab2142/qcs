﻿using SharedLib;
using UnityEngine;

public class WorldSpawn : WorldEntity
{
    public NetworkWorldSpawn NetworkWorldSpawn { get { return (NetworkWorldSpawn)NetworkEntity; } }

    public static WorldSpawn Create(NetworkBaseEntity networkEntity)
    {
        var gameObject = new GameObject("WorldSpawn");
        var component = gameObject.AddComponent<WorldSpawn>();
        return component;
    }

    Light _sun;

    protected override void EntityStart()
    {
        base.EntityStart();
        _sun = new GameObject("Sun").AddComponent<Light>();
        _sun.type = LightType.Directional;
    }

    protected override void EntityThink(float dt)
    {
        base.EntityThink(dt);
        SetThinkDelay(0.5f);
        var angle = NetworkWorldSpawn.Time.GetValue() * 0.25f;
        _sun.transform.localEulerAngles = new Vector3(angle, 70, 0f);
    }
}
