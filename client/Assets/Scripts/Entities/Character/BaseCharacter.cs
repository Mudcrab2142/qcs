﻿using UnityEngine;
using SharedLib;
using ClientLib;

public abstract class BaseCharacter : HumanoidCreature
{
	public static WorldEntity Create(NetworkBaseEntity networkEntity)
    {
        var isMine = ((NetworkCharacter)networkEntity).OwnerId.GetValue() == LidgrenRootClient.Instance.NetworkConnectionId;
        BaseCharacter character;
        var gameObject = Instantiate(Resources.Load("Player")) as GameObject;
        if (isMine)
        {
            character = gameObject.AddComponent<Character>();
        }
        else
        {
            character = gameObject.AddComponent<RemoteCharacter>();
        }

        return character;
    }

    public NetworkCharacter NetworkCharacter { get { return (NetworkCharacter)NetworkEntity; } }

    protected new Rigidbody rigidbody;
    protected CapsuleCollider capsuleCollider;
    protected float colliderRadius;
    protected float colliderHeight;
    protected Vector3 colliderCenter;

    protected override void EntityStart()
    {
        base.EntityStart();
        rigidbody = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        colliderCenter = GetComponent<CapsuleCollider>().center;
        colliderRadius = GetComponent<CapsuleCollider>().radius;
        colliderHeight = GetComponent<CapsuleCollider>().height;
    }
}
