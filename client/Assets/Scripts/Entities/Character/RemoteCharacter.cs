﻿using UnityEngine;
using System.Collections;
using SharedLib;

public class RemoteCharacter : BaseCharacter
{
    protected override void EntityStart()
    {
        base.EntityStart();
        animator.applyRootMotion = false;
        NetworkCharacter.AnimatorDirection.OnValueChanged += OnAnimatorDirectionChanged;
        NetworkCharacter.AnimatorSpeed.OnValueChanged += OnAnimatorSpeedChanged;
        NetworkCharacter.AnimatorLayersData.OnValueChanged += OnAnimatorLayersDataChanged;
    }

    void OnAnimatorDirectionChanged(NetworkVariable variable)
    {
        var data = (NetworkSingle)variable;
        animator.SetFloat("Input_X", data.GetValue());
    }

    void OnAnimatorSpeedChanged(NetworkVariable variable)
    {
        var data = (NetworkSingle)variable;
        animator.SetFloat("Input_Y", data.GetValue());
    }

    void OnAnimatorLayersDataChanged(NetworkVariable variable)
    {
        var data = (NetworkInt32Array)variable;

        var array = data.GetValue();

        for (int i = 0; i < array.Length; i++)
        {
            if (animator.IsInTransition(i))
            {
                animator.Play(array[i], i);
                return;
            }
            var currentInfo = animator.GetCurrentAnimatorStateInfo(i);
            var nextInfo = animator.GetNextAnimatorStateInfo(i);

            if (currentInfo.fullPathHash != array[i] && nextInfo.fullPathHash != array[i])
                animator.CrossFade(array[i], 0.1f, i);
        }
    }
}
