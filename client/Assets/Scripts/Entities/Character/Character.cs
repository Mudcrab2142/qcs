﻿using UnityEngine;
using SharedLib;
using ClientLib;
using System;

public class Character : BaseCharacter
{
    public override bool UseLerp { get { return false; } }

    public CameraController Camera;

    private PlayerMoveState _playerMoveState;
    private PlayerInputState _inputState;
    private AnimatorEventsHandler _animatorEventsHandler;

    protected override void EntityStart()
    {
        base.EntityStart();
        _animatorEventsHandler = gameObject.AddComponent<AnimatorEventsHandler>();
        UIRoot.Instance.HUDController.ShowHUD();
        Camera = CameraController.Instance;
        _playerMoveState = new PlayerMoveState();
        _playerMoveState.SpellCastSpeed = 1f;

        var hips = animator.GetBoneTransform(HumanBodyBones.Hips);
        Camera.OffSetPlayerPivot = Vector3.Distance(transform.position, hips.position);

        NetworkCharacter.CurrentHealth.OnValueChanged += OnHealthChanged;
        NetworkCharacter.MaxHealth.OnValueChanged += OnHealthChanged;
        NetworkCharacter.CurrentStamina.OnValueChanged += OnStaminaChanged;
        NetworkCharacter.MaxStamina.OnValueChanged += OnStaminaChanged;

        NetworkCharacter.CurrentStamina.EnablePrediction(1f);
        NetworkCharacter.Position.EnablePrediction(1f);
        NetworkCharacter.Position.OnPredictionFailed += OnPositionPredictionFailed;



        OnHealthChanged(null);
        OnStaminaChanged(null);
        Camera.Init(transform);
    }

    void OnPositionPredictionFailed(Vector3 obj)
    {
        transform.position = obj;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        NetworkCharacter.CurrentHealth.OnValueChanged -= OnHealthChanged;
        NetworkCharacter.MaxHealth.OnValueChanged -= OnHealthChanged;
        NetworkCharacter.CurrentStamina.OnValueChanged -= OnStaminaChanged;
        NetworkCharacter.MaxStamina.OnValueChanged -= OnStaminaChanged;
    }

    protected override void EntityUpdate()
    {
        base.EntityUpdate();
        
        if (Camera != null)
        {
            Camera.RotateCamera(InputController.Instance.MouseAxis);
            Camera.Zoom(Input.GetAxis("Mouse ScrollWheel"));
        }

        if (!_playerMoveState.InAction && (InputController.Instance.InputAxis.magnitude > float.Epsilon))
        {
            Quaternion newPos = Quaternion.Euler(transform.eulerAngles.x, Camera.transform.eulerAngles.y, transform.eulerAngles.z);
            transform.rotation = Quaternion.Slerp(transform.rotation, newPos, 20f * Time.smoothDeltaTime);
        }

        //    UpdateMovingPlatform();
    }

    protected override void EntityFixedUpdate()
    {
        base.EntityFixedUpdate();

        var test = new PlayerInputState();
        test.Vectical = Input.GetAxis("Vertical");
        test.Horizontal = Input.GetAxis("Horizontal");

        PlayerMove.FixedUpdateMovement(animator, capsuleCollider, ref _playerMoveState, ref test, World.Instance.ClientSpells);
        NetworkCharacter.Position.SetPredictedValue(transform.position);

        test.PredictedPosition = transform.position;
        World.Instance.RPC.CL_CoreSendPlayerInput.Call(test);
        var currStamina = CombatShared.StaminaRegeneration(NetworkCharacter, NetworkClientWorld.Instance.ClientStats);
        NetworkCharacter.CurrentStamina.SetPredictedValue(currStamina);

        ControlCameraState();  
    }


    public void SetCastSpeed(float spd)
    {
        _playerMoveState.SpellCastSpeed = spd;
    }

    public void LockRoll(int bit)
    {
        _playerMoveState.RollLockMask |= (byte)(1 << bit);
    }

    public void UnlockRoll(int bit)
    {
        _playerMoveState.RollLockMask &= (byte)(~(1 << bit));
    }

    public void LockMovement(int bit)
    {
        _playerMoveState.MovementLockMask |= (byte)(1 << bit);
    }

    public void UnlockMovement(int bit)
    {
        _playerMoveState.MovementLockMask &= (byte)(~(1 << bit));
    }

    /*void UpdateMovingPlatform()
    {
        if (MovingPlatform == null)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position + Vector3.up * 0.1f, -Vector3.up, out hit, 0.2f))
            {
                MovingPlatform = hit.collider.GetComponent<Airship>();
                if (MovingPlatform != null)
                {
                    transform.SetParent(MovingPlatform.transform);
                    MovingPlatformCollider = hit.collider;
                }
            }
        }
        else if(MovingPlatformCollider != null)
        {
            RaycastHit hit;
            if (!MovingPlatformCollider.Raycast(new Ray(transform.position + Vector3.up * 0.1f, -Vector3.up), out hit, 100f))
            {
                transform.SetParent(null);
                MovingPlatform = null;
                MovingPlatformCollider = null;
            }
        }

        if(MovingPlatform != null)
        {
            transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
        }
    }*/

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (NetworkCharacter != null)
            Gizmos.DrawSphere(NetworkCharacter.Position.GetLastServerValue(), 0.05f);
    }

    void ControlCameraState()
    {
        if (Camera == null)
            return;

        if (_playerMoveState.Crouch)
            Camera.ChangeState(CameraController.STATE_CROUCH, true);
        else
            Camera.ChangeState(CameraController.STATE_DEFAULT, true);
    }

    protected override void OnDeath()
    {
        base.OnDeath();
        Debug.Log("Character death");
        UIRoot.Instance.HideAllGameWindows();
        UIRoot.Instance.DeathScreen.TryShow();
    }

    protected override void OnRevive()
    {
        base.OnRevive();
        Debug.Log("Character revive");
        UIRoot.Instance.DeathScreen.TryHide();
    }

    private void OnHealthChanged(NetworkVariable obj)
    {
        UIRoot.Instance.HUDController.SetHealth(NetworkCharacter.CurrentHealth.GetValue(), NetworkCharacter.MaxHealth.GetValue());
    }

    private void OnStaminaChanged(NetworkVariable obj)
    {
        UIRoot.Instance.HUDController.SetStamina(NetworkCharacter.CurrentStamina.GetValue(), NetworkCharacter.MaxStamina.GetValue());
    }
}

