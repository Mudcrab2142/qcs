﻿using UnityEngine;
using System.Collections;
using SharedLib;
using System.Collections.Generic;
using Lidgren.Network;

public class MagicMissile : BaseEntity
{
    static readonly Dictionary<NetworkMagicMissile.MissileType, string> _prefabNames = new Dictionary<NetworkMagicMissile.MissileType, string>()
    {
        { NetworkMagicMissile.MissileType.DarkMatter, "dark_matter" }
    };

    static readonly Dictionary<NetworkMagicMissile.MissileType, string> _explosionEffectNames = new Dictionary<NetworkMagicMissile.MissileType, string>()
    {
        { NetworkMagicMissile.MissileType.DarkMatter, "dark_matter_explode" }
    };

    static readonly string PREFAB_PATH = "Entities/MagicMissile/";

    #region RPC Handlers
    public static void Explode(NetStruct<int, ProtoVector3> data, NetConnection connection)
    {
        var missileEntity = World.Instance.GetEntity<MagicMissile>(data.Value1);
        if (missileEntity == null)
            return;

        missileEntity.Explode(data.Value2);
    }
    #endregion

    public override bool UseLerp { get { return true; } }
    public NetworkMagicMissile.MissileType Type { get; private set; }

    public static MagicMissile Create(NetworkBaseEntity networkEntity)
    {
        var type = (NetworkMagicMissile.MissileType)((NetworkMagicMissile)networkEntity).Type.GetValue();
        var prefabName = _prefabNames[type];
        var obj = Instantiate(CachedResources.Load<GameObject>(PREFAB_PATH + prefabName));
        var component = obj.AddComponent<MagicMissile>();
        component.Type = type;
        return component;
    }

    private void Explode(Vector3 position)
    {
        var effect = Pool.Instance.Effects.GetObject(_explosionEffectNames[Type]);
        effect.transform.position = position;
        effect.Play();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(NetworkEntity.Position.GetValue(), 0.1f);
    }
}
