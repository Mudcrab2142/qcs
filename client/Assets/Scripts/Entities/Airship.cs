﻿using UnityEngine;
using SharedLib;
using System;

public class Airship : BaseEntity
{
    public NetworkAirship NetworkAirship { get { return (NetworkAirship)NetworkEntity; } }
    public override bool UseLerp { get { return true; } }
    public override float LerpTime { get { return 0.2f; } }

    private bool _isDriving;

    private byte _inpAltitude;
    private byte _inpThrottle;
    private byte _inpRotation;

    public static Airship Create(NetworkBaseEntity networkEntity)
    {
        var obj = Instantiate(CachedResources.Load<GameObject>("Entities/Airship/airship"));
        var component = obj.AddComponent<Airship>();
        return component;
    }

    protected override void EntityStart()
    {
        base.EntityStart();
        NetworkAirship.DriverEntityId.OnValueChanged += OnDriverChanged;
    }

    protected override void EntityUpdate()
    {
        base.EntityUpdate();
        if (_isDriving)
        {
            if (InputController.Instance.AirshipUp && _inpAltitude < 103)
            {
                _inpAltitude++;
                SendControls();
            }

            if (InputController.Instance.AirshipDown && _inpAltitude > 97)
            {
                _inpAltitude--;
                SendControls();
            }

            if (InputController.Instance.AirshipForward && _inpThrottle < 103)
            {
                _inpThrottle++;
                SendControls();
            }

            if (InputController.Instance.AirshipBack && _inpThrottle > 97)
            {
                _inpThrottle--;
                SendControls();
            }

            if (InputController.Instance.AirshipLeft && _inpRotation < 103)
            {
                _inpRotation++;
                SendControls();
            }

            if (InputController.Instance.AirshipRight && _inpRotation > 97)
            {
                _inpRotation--;
                SendControls();
            }

            if (InputController.Instance.Interact1Press)
                World.Instance.RPC.CL_EntityAirshipExit.Call(NetworkAirship.EntityId);
        }
    }

    void SendControls()
    {
        World.Instance.RPC.CL_EntityAirshipControl.Call(new NetStruct<int, byte, byte, byte>(NetworkAirship.EntityId, _inpAltitude, _inpThrottle, _inpRotation));
    }

    private void OnDriverChanged(NetworkVariable obj)
    {
        var c = World.ClientWorldInstance.ControlledCharacter;
        if (c == null)
            return;

        var character = c.Owner as Character;
        if (character == null)
            return;

        var newDriver = ((NetworkInt32)obj).GetValue();
        if (!_isDriving && character.EntityId == newDriver)
            Enter(character);

        if (_isDriving && character.EntityId != newDriver)
            Exit(character);
    }

    void Enter(Character character)
    {
        _isDriving = true;
        UIRoot.Instance.HUDController.ShowAirshipHUD();
        var c = (Character)World.ClientWorldInstance.ControlledCharacter.Owner;
        c.LockMovement(0);
        NetworkAirship.InputAltitude.OnValueChanged += OnInputAltitudeUpdate;
        OnInputAltitudeUpdate(NetworkAirship.InputAltitude);
        NetworkAirship.InputThrottle.OnValueChanged += OnInputThrottleUpdate;
        OnInputThrottleUpdate(NetworkAirship.InputThrottle);
        NetworkAirship.InputRotation.OnValueChanged += OnInputRotationUpdate;
        OnInputRotationUpdate(NetworkAirship.InputRotation);
    }

    private void OnInputThrottleUpdate(NetworkVariable obj)
    {
        _inpThrottle = ((NetworkByte)obj).GetValue();
        Debug.Log("Throttle: " + _inpThrottle);
    }

    private void OnInputAltitudeUpdate(NetworkVariable obj)
    {
        _inpAltitude = ((NetworkByte)obj).GetValue();
    }

    private void OnInputRotationUpdate(NetworkVariable obj)
    {
        _inpRotation = ((NetworkByte)obj).GetValue();
        Debug.Log("Rotation: " + _inpRotation);
    }

    void Exit(Character character)
    {
        _isDriving = false;
        UIRoot.Instance.HUDController.HideAirshipHUD();
        var c = (Character)World.ClientWorldInstance.ControlledCharacter.Owner;
        c.UnlockMovement(0);

        NetworkAirship.InputAltitude.OnValueChanged -= OnInputAltitudeUpdate;
        NetworkAirship.InputThrottle.OnValueChanged -= OnInputThrottleUpdate;
        NetworkAirship.InputRotation.OnValueChanged -= OnInputRotationUpdate;
    }
}