﻿using SharedLib;
using UnityEngine;

public abstract class BaseEntity : WorldEntity
{
    public int EntityId { get { return NetworkEntity ? NetworkEntity.EntityId : -1; } }
    public virtual bool UseLerp { get { return false; } }
    public virtual float LerpTime { get { return 0.1f; } }

    private Vector3 _targetPosition;
    private Vector3 _targetRotation;
    private float _currentMoveSpeed;

    protected override void EntityStart()
    {
        if (UseLerp)
        {
            NetworkEntity.Position.OnValueChanged += OnPositionChanged;
            OnPositionChanged(NetworkEntity.Position);
            if (NetworkEntity.UseSimpleRotation)
            {
                NetworkEntity.SimpleRotation.OnValueChanged += OnSimpleRotationChanged;
                OnSimpleRotationChanged(NetworkEntity.SimpleRotation);
            }
            else
            {
                NetworkEntity.FullRotation.OnValueChanged += OnFullRotationChanged;
                OnFullRotationChanged(NetworkEntity.FullRotation);
            }
        }
    }

    protected override void EntityUpdate()
    {
        if (UseLerp)
        {
            transform.position = Vector3.MoveTowards(transform.position, _targetPosition, _currentMoveSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(_targetRotation), Time.deltaTime * 5f);
        }
    }

    protected virtual void Awake()
    {

    }

    void OnPositionChanged(NetworkVariable variable)
    {
        var data = variable as NetworkVector3;
        _targetPosition = data.GetValue();
        _currentMoveSpeed = (_targetPosition - transform.position).magnitude / LerpTime;
    }

    void OnSimpleRotationChanged(NetworkVariable variable)
    {
        var data = variable as NetworkSingle;
        _targetPosition = new Vector3(0f, data.GetValue(), 0f);
    }

    void OnFullRotationChanged(NetworkVariable variable)
    {
        var data = variable as NetworkVector3;
        _targetRotation = data.GetValue();
    }

    protected void LerpTransform()
    {

    }
}
