﻿using SharedLib;
using UnityEngine;

public class DummyCreature : Creature
{

    public static DummyCreature Create(NetworkBaseEntity networkEntity)
    {
        var obj = GameObject.CreatePrimitive(PrimitiveType.Capsule);
        var c = obj.AddComponent<DummyCreature>();
        c.gameObject.layer = ClientLayers.ENEMY;
   /*     var rb = c.gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.constraints = RigidbodyConstraints.FreezeAll;*/
        return c;
    }
	
}
