﻿using UnityEngine;
using System.Collections;
using SharedLib;
using ProtoBuf.Meta;

public abstract class Creature : BaseEntity
{
    public NetworkCreature NetworkCreature { get { return (NetworkCreature)NetworkEntity; } }

    public override bool UseLerp { get { return true; } }

    protected Animator animator { get; private set; }

    private byte _oldFlags;

    public bool IsDead { get; private set; }

    protected override void EntityStart()
    {
        base.EntityStart();
        animator = GetComponent<Animator>();
        NetworkCreature.CreatureFlags.OnValueChanged += OnCreatureFlagsChanged;
        OnCreatureFlagsChanged(NetworkCreature.CreatureFlags);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        NetworkCreature.CreatureFlags.OnValueChanged -= OnCreatureFlagsChanged;
    }

    public void TakeHit(BaseEntity entity, Vector3 hitPosition)
    {
        SpawnBlood(hitPosition, entity.transform.eulerAngles);
    }

    public virtual void SpawnBlood(Vector3 hitPosition, Vector3 hitRotation)
    {
        var effect = Pool.Instance.Effects.GetObject("hit_blood");
        effect.transform.position = hitPosition;
        effect.transform.eulerAngles = hitRotation;
        effect.Play();
    }

    private void OnCreatureFlagsChanged(NetworkVariable obj)
    {
        var newFlags = ((NetworkByte)obj).GetValue();

        switch(CheckFlag(_oldFlags, newFlags, NetworkCreature.FLAG_DEAD))
        {
            case 1:
                IsDead = true;
                OnDeath();
                break;
            case -1:
                IsDead = false;
                OnRevive();
                break;
        }

        _oldFlags = newFlags;
    }

    sbyte CheckFlag(byte oldFlags, byte newFlags, byte flag)
    {
        var of = (oldFlags & flag) != 0;
        var nf = (newFlags & flag) != 0;

        if (nf && !of)
            return 1;

        if (!nf && of)
            return -1;

        return 0;
    }

    protected virtual void OnDeath() { }
    protected virtual void OnRevive() { }
}
