﻿using UnityEngine;
using SharedLib;
using System.Collections.Generic;

public class HumanoidCreature : Creature
{
	public NetworkHumanoidCreature NetworkHumanoidCreature { get { return (NetworkHumanoidCreature)NetworkEntity; } }
    public EquipmentController EquipmentController { get; private set; }

    protected override void EntityStart()
    {
        base.EntityStart();
        EquipmentController = gameObject.GetComponent<EquipmentController>();
        NetworkHumanoidCreature.Equipment.OnValueChanged += OnEquipmentChanged;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        NetworkHumanoidCreature.Equipment.OnValueChanged -= OnEquipmentChanged;
    }

    private void OnEquipmentChanged(NetworkVariable obj)
    {
        var data = SerializeTools.ProtoBufDeserialize<Dictionary<EquipmentPosition, string>>(((NetworkByteArray)obj).GetValue());

#if UNITY_EDITOR
        Debug.Log("EQUIPMENT UPDATE!");
        foreach(var item in data)
            Debug.Log(string.Format("[{0}] {1}", item.Key, item.Value));
#endif

        EquipmentController.UpdateEquipment(data);
    }
}
