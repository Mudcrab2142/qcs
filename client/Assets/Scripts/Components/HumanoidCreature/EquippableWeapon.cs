﻿using UnityEngine;

public abstract class EquippableWeapon : MonoBehaviour
{
    public abstract void OnEquip();
}
