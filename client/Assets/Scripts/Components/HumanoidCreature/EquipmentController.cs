﻿using SharedLib;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentController : MonoBehaviour
{
    const string EQUIPMENT_WEAPONS_PATH = "Weapons/";

    public Transform WeaponMountPoint;

    public EquippableWeapon Weapon;

    public void UpdateEquipment(Dictionary<EquipmentPosition, string> data)
    {
        if (data.ContainsKey(EquipmentPosition.Weapon))
            EquipWeapon(data[EquipmentPosition.Weapon]);
    }


    void EquipWeapon(string wp)
    {
        var obj = Instantiate(Resources.Load(EQUIPMENT_WEAPONS_PATH + wp)) as GameObject;
        obj.transform.SetParent(WeaponMountPoint);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localEulerAngles = Vector3.zero;
        Weapon = obj.GetComponent<EquippableWeapon>();
        Weapon.OnEquip();
    }

}
