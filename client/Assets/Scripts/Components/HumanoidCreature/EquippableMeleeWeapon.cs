﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Xft;

public class EquippableMeleeWeapon : EquippableWeapon
{
    public Transform TrailStartPosition;
    public Transform TrailEndPosition;

    private XWeaponTrail _trailDistort;
    private XWeaponTrail _trail;

    private List<int> _hitTargets = new List<int>();

    private Action<int> _hitAction;
    private bool _trigger;

    void OnDrawGizmos()
    {
        if (!TrailEndPosition || !TrailEndPosition)
            return;

        Gizmos.color = Color.green;

        Gizmos.DrawSphere(TrailStartPosition.position, 0.025f);
        Gizmos.DrawSphere(TrailEndPosition.position, 0.025f);
        Gizmos.DrawLine(TrailStartPosition.position, TrailEndPosition.position);
    }

    public override void OnEquip()
    {
        _trailDistort = gameObject.AddComponent<XWeaponTrail>();
        _trail = gameObject.AddComponent<XWeaponTrail>();
        _trailDistort.PointEnd = _trail.PointEnd = TrailEndPosition;
        _trailDistort.PointStart = _trail.PointStart = TrailStartPosition;
        _trailDistort.MyMaterial = Resources.Load<Material>("Materials/WeaponTrails/slash_distort");
        _trail.MyMaterial = Resources.Load<Material>("Materials/WeaponTrails/slash");
        _trail.Deactivate();
        _trailDistort.Deactivate();
        var rb = gameObject.AddComponent<Rigidbody>();
        rb.isKinematic = true;
    }

    public void EnableTrail()
    {
        _trailDistort.Activate();
        _trail.Activate();
    }

    public void DisableTrail(float fadeTime = 0.3f)
    {
        _trailDistort.StopSmoothly(fadeTime);
        _trail.StopSmoothly(fadeTime);
    }

    public void ResetHitTargets()
    {
        _hitTargets.Clear();
    }

    public void SetHitAction(Action<int> a)
    {
        _trigger = true;
        _hitAction = a;
    }

    public void ResetHitAction()
    {
        _trigger = false;
        _hitAction = null;
    }

    void OnTriggerEnter(Collider c)
    {
        if (!c.attachedRigidbody || !_trigger)
            return;

        var target = c.attachedRigidbody.GetComponent<Creature>();
        if (target && !_hitTargets.Contains(target.EntityId))
        {
            _hitTargets.Add(target.EntityId);
            _hitAction(target.EntityId);
            target.TakeHit((BaseEntity)World.ClientWorldInstance.ControlledCharacter.Owner, c.transform.position);
            Debug.Log("Hit target " + target.name);
        }
    }
}
