﻿using UnityEngine;
using SharedLib;

public class AnimatorStateSpellBindCastSpeed : StateMachineBehaviour
{
    const string SPEED_PARAMETER_NAME = "SpeedMultiplier";

    private static bool _lock;
    private static float _nextSpeed;

    public SpellName Spell;
    private Character _character;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _character = animator.gameObject.GetComponent<Character>();
        if (!_character || !World.ClientWorldInstance)
            return;
        
        var speed = World.ClientWorldInstance.ClientSpells.SpellInfo[Spell].CastSpeed;

        if (_lock)
        {
            _nextSpeed = speed;
        }
        else
        {
            _character.SetCastSpeed(speed);
            _nextSpeed = 1f;
            _lock = true;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!_character || !World.ClientWorldInstance)
            return;

        _character.SetCastSpeed(_nextSpeed);
        _lock = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
