﻿using UnityEngine;
using System.Collections;

public class AnimatorStateCameraFOVCurve : StateMachineBehaviour
{
    public AnimationCurve Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 0));
    public float Value = 1f;

    private float _defaultFOV;
    private CameraController _camera;
    private Character _character;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _camera = CameraController.Instance;
        _defaultFOV = _camera.Camera.fieldOfView;
        _character = animator.gameObject.GetComponent<Character>();
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!_camera || !_character)
            return;

        var curveX = Mathf.Clamp(stateInfo.normalizedTime, 0f, 1f);
        _camera.Camera.fieldOfView = _defaultFOV + Curve.Evaluate(curveX) * Value;
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _camera.Camera.fieldOfView = _defaultFOV;
        _camera = null;
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
