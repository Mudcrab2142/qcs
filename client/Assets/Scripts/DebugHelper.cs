﻿using UnityEngine;
using System.Collections;

public class DebugHelper
{
    public static void DrawBox(Vector3 center, Vector3 size, Quaternion rotation, Color color, float time)
    {
        Vector3 v3FrontTopLeft;
        Vector3 v3FrontTopRight;
        Vector3 v3FrontBottomLeft;
        Vector3 v3FrontBottomRight;
        Vector3 v3BackTopLeft;
        Vector3 v3BackTopRight;
        Vector3 v3BackBottomLeft;
        Vector3 v3BackBottomRight;

        v3FrontTopLeft = new Vector3(-size.x, size.y, -size.z);  // Front top left corner
        v3FrontTopRight = new Vector3(size.x, size.y, -size.z);  // Front top right corner
        v3FrontBottomLeft = new Vector3(-size.x, -size.y, -size.z);  // Front bottom left corner
        v3FrontBottomRight = new Vector3(size.x, -size.y, -size.z);  // Front bottom right corner
        v3BackTopLeft = new Vector3(-size.x, size.y, size.z);  // Back top left corner
        v3BackTopRight = new Vector3(size.x, size.y, size.z);  // Back top right corner
        v3BackBottomLeft = new Vector3(-size.x, -size.y, size.z);  // Back bottom left corner
        v3BackBottomRight = new Vector3(size.x, -size.y, size.z);  // Back bottom right corner

        v3FrontTopLeft = rotation * v3FrontTopLeft;
        v3FrontTopRight = rotation * v3FrontTopRight;
        v3FrontBottomLeft = rotation * v3FrontBottomLeft;
        v3FrontBottomRight = rotation * v3FrontBottomRight;
        v3BackTopLeft = rotation * v3BackTopLeft;
        v3BackTopRight = rotation * v3BackTopRight;
        v3BackBottomLeft = rotation * v3BackBottomLeft;
        v3BackBottomRight = rotation * v3BackBottomRight;

        v3FrontTopLeft += center;
        v3FrontTopRight += center;
        v3FrontBottomLeft += center;
        v3FrontBottomRight += center;
        v3BackTopLeft += center;
        v3BackTopRight += center;
        v3BackBottomLeft += center;
        v3BackBottomRight += center;

        Debug.DrawLine(v3FrontTopLeft, v3FrontTopRight, color, time);
        Debug.DrawLine(v3FrontTopRight, v3FrontBottomRight, color, time);
        Debug.DrawLine(v3FrontBottomRight, v3FrontBottomLeft, color, time);
        Debug.DrawLine(v3FrontBottomLeft, v3FrontTopLeft, color, time);

        Debug.DrawLine(v3BackTopLeft, v3BackTopRight, color, time);
        Debug.DrawLine(v3BackTopRight, v3BackBottomRight, color, time);
        Debug.DrawLine(v3BackBottomRight, v3BackBottomLeft, color, time);
        Debug.DrawLine(v3BackBottomLeft, v3BackTopLeft, color, time);

        Debug.DrawLine(v3FrontTopLeft, v3BackTopLeft, color, time);
        Debug.DrawLine(v3FrontTopRight, v3BackTopRight, color, time);
        Debug.DrawLine(v3FrontBottomRight, v3BackBottomRight, color, time);
        Debug.DrawLine(v3FrontBottomLeft, v3BackBottomLeft, color, time);
    }

}
