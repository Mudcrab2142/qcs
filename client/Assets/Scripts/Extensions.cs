﻿using UnityEngine;

public static class Extensions
{
    /// <summary>
    /// Normalizeds the angle. between -180 and 180 degrees
    /// </summary>
    /// <param Name="eulerAngle">Euler angle.</param>
    public static Vector3 NormalizeAngle(this Vector3 eulerAngle)
    {
        var delta = eulerAngle;

        if (delta.x > 180) delta.x -= 360;
        else if (delta.x < -180) delta.x += 360;

        if (delta.y > 180) delta.y -= 360;
        else if (delta.y < -180) delta.y += 360;

        if (delta.z > 180) delta.z -= 360;
        else if (delta.z < -180) delta.z += 360;

        return new Vector3((int)delta.x, (int)delta.y, (int)delta.z);//round values to angle;
    }

    public static Vector3 Difference(this Vector3 vector, Vector3 otherVector)
    {
        return otherVector - vector;
    }
    public static void SetActiveChildren(this GameObject gameObjet, bool value)
    {
        foreach (Transform child in gameObjet.transform)
            child.gameObject.SetActive(value);
    }

    public static void SetLayerRecursively(this GameObject obj, int layer)
    {
        obj.layer = layer;

        foreach (Transform child in obj.transform)
        {
            child.gameObject.SetLayerRecursively(layer);
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        do
        {
            if (angle < -360)
                angle += 360;
            if (angle > 360)
                angle -= 360;
        } while (angle < -360 || angle > 360);

        return Mathf.Clamp(angle, min, max);
    }


    /// <summary>
    /// Lerp between CameraStates
    /// </summary>
    /// <param name="to"></param>
    /// <param name="from"></param>
    /// <param name="time"></param>
    public static CameraState Slerp(this CameraState to, CameraState from, float time)
    {
        to.Id = from.Id;
        to.Forward = Mathf.Lerp(to.Forward, from.Forward, time);
        to.Right = Mathf.Lerp(to.Right, from.Right, time);
        to.DefaultDistance = Mathf.Lerp(to.DefaultDistance, from.DefaultDistance, time);
        to.MaxDistance = Mathf.Lerp(to.MaxDistance, from.MaxDistance, time);
        to.MinDistance = Mathf.Lerp(to.MinDistance, from.MinDistance, time);
        to.Height = Mathf.Lerp(to.Height, from.Height, time);
        to.FixedAngle = Vector2.Lerp(to.FixedAngle, from.FixedAngle, time);
        to.SmoothFollow = Mathf.Lerp(to.SmoothFollow, from.SmoothFollow, time);
        to.YMinLimit = Mathf.Lerp(to.YMinLimit, from.YMinLimit, time);
        to.YMaxLimit = Mathf.Lerp(to.YMaxLimit, from.YMaxLimit, time);
        to.XMinLimit = Mathf.Lerp(to.XMinLimit, from.XMinLimit, time);
        to.XMaxLimit = Mathf.Lerp(to.XMaxLimit, from.XMaxLimit, time);
        to.CullingHeight = Mathf.Lerp(to.CullingHeight, from.CullingHeight, time);
        to.CullingMinDist = Mathf.Lerp(to.CullingMinDist, from.CullingMinDist, time);
        to.CameraMode = from.CameraMode;
        to.UseZoom = from.UseZoom;
        return to;
    }


    public static ClipPlanePoints NearClipPlanePoints(this Camera camera, Vector3 pos, float clipPlaneMargin)
    {
        var clipPlanePoints = new ClipPlanePoints();

        var transform = camera.transform;
        var halfFOV = (camera.fieldOfView / 2) * Mathf.Deg2Rad;
        var aspect = camera.aspect;
        var distance = camera.nearClipPlane;
        var height = distance * Mathf.Tan(halfFOV);
        var width = height * aspect;
        height *= 1 + clipPlaneMargin;
        width *= 1 + clipPlaneMargin;
        clipPlanePoints.LowerRight = pos + transform.right * width;
        clipPlanePoints.LowerRight -= transform.up * height;
        clipPlanePoints.LowerRight += transform.forward * distance;

        clipPlanePoints.LowerLeft = pos - transform.right * width;
        clipPlanePoints.LowerLeft -= transform.up * height;
        clipPlanePoints.LowerLeft += transform.forward * distance;

        clipPlanePoints.UpperRight = pos + transform.right * width;
        clipPlanePoints.UpperRight += transform.up * height;
        clipPlanePoints.UpperRight += transform.forward * distance;

        clipPlanePoints.UpperLeft = pos - transform.right * width;
        clipPlanePoints.UpperLeft += transform.up * height;
        clipPlanePoints.UpperLeft += transform.forward * distance;

        return clipPlanePoints;
    }
}

public struct ClipPlanePoints
{
    public Vector3 UpperLeft;
    public Vector3 UpperRight;
    public Vector3 LowerLeft;
    public Vector3 LowerRight;
}
