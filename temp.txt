using UnityEngine;
using SharedLib;
using ClientLib;

public class Character : BaseCharacter
{
    const float NETWORK_SEND_RATE = 0.02f;

    const int LAYER_GROUND = 1;

    public override bool UseLerp { get { return false; } }
    public bool InAction { get { return JumpOver || StepUp || ClimbUp || UsingLadder || Jump; } }

    public CameraController Camera;

    private float _nextNetworkSend;
    private PhysicMaterial FRICTION_PHYSICS;
    private PhysicMaterial DEFAULT_PHYSICS;


    private bool JUMP_AIR_CONTROL = true;
    private float JUMP_FORWARD = 5f;
    private float JUMP_FORCE = 4f;
    private float GROUND_CHECK_DISTANCE = 0.5f;
    private float STEP_OFFSET_END = 0.36f;
    private float STEP_OFFSET_START = 0.05f;
    private float STEP_OFFSET_FORWARD = 0.05f;
    private float EXTRA_GRAVITY = 4f;
    private float LANDING_ANIMATION_VELOCITY = -5f;





    
    

    private RaycastHit GroundHit;
    private float InpForward;
    private float InpDirection;
    private float VerticalVelocity;
    private bool LockPlayer;
    private float ExtraStrafeSpeed = 0f;
    private float GroundDistance;
    private byte RollLockMask;
    private byte MovementLockMask;
    private AnimatorStateInfo StateInfo;
    private float SpellCastSpeed = 1f;
    private bool OnGround;
    private bool AutoCrouch;
    private bool Crouch;
    private bool LandHigh;
    private bool Jump;
    private bool IsJumping;
    private bool JumpOver;
    private bool StepUp;
    private bool ClimbUp;
    private bool EnterLadderBottom;
    private bool EnterLadderTop;
    private bool UsingLadder;
    private bool ExitLadderBottom;
    private bool ExitLadderTop;
    private Airship MovingPlatform;
    private Collider MovingPlatformCollider;


    private byte _oldFlags;



    private Transform _matchTarget;
    




    protected override void EntityStart()
    {
        base.EntityStart();
        UIRoot.Instance.HUDController.ShowHUD();
        Camera = CameraController.Instance;
        var hips = animator.GetBoneTransform(HumanBodyBones.Hips);

        Camera.OffSetPlayerPivot = Vector3.Distance(transform.position, hips.position);

        FRICTION_PHYSICS = new PhysicMaterial();
        FRICTION_PHYSICS.name = "frictionPhysics";
        FRICTION_PHYSICS.staticFriction = 0.6f;
        FRICTION_PHYSICS.dynamicFriction = 0.6f;

        DEFAULT_PHYSICS = new PhysicMaterial();
        DEFAULT_PHYSICS.name = "defaultPhysics";
        DEFAULT_PHYSICS.staticFriction = 0f;
        DEFAULT_PHYSICS.dynamicFriction = 0f;

        NetworkCharacter.CurrentHealth.OnValueChanged += OnHealthChanged;
        NetworkCharacter.MaxHealth.OnValueChanged += OnHealthChanged;
        NetworkCharacter.CurrentStamina.OnValueChanged += OnStaminaChanged;
        NetworkCharacter.MaxStamina.OnValueChanged += OnStaminaChanged;

        NetworkCharacter.CurrentStamina.EnablePrediction(1f);

        OnHealthChanged(null);
        OnStaminaChanged(null);
        Camera.Init(transform);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        NetworkCharacter.CurrentHealth.OnValueChanged -= OnHealthChanged;
        NetworkCharacter.MaxHealth.OnValueChanged -= OnHealthChanged;
        NetworkCharacter.CurrentStamina.OnValueChanged -= OnStaminaChanged;
        NetworkCharacter.MaxStamina.OnValueChanged -= OnStaminaChanged;
    }

    protected override void EntityUpdate()
    {
        base.EntityUpdate();
        SendNetworkData();

      //  CheckForwardAction();

        CheckStates();
        CheckConditions();

        HandleInput();                  // handle input from controller, keyboard&mouse or touch
        RotateWithCamera();             // rotate the character with the camera    

    //    UpdateMovingPlatform();
    }

    protected override void EntityFixedUpdate()
    {
        base.EntityFixedUpdate();
         
        var currStamina = CombatShared.StaminaRegeneration(NetworkCharacter, NetworkClientWorld.Instance.ClientStats);
        NetworkCharacter.CurrentStamina.SetPredictedValue(currStamina);

        CheckGround();
        ControlHeight();
        ControlMovement();
        UpdateAnimator();
        ControlCameraState();  
    }

    void SendNetworkData()
    {
        if (_nextNetworkSend <= Time.time)
        {
            var pos = transform.position;
            var velocity = rigidbody.velocity;
            World.Instance.RPC.CL_CharacterTransformUpdate.Call(new NetStruct<ProtoVector3, ProtoVector3, float>(pos, velocity, transform.eulerAngles.y));

            var layerStates = new int[animator.layerCount];
            for (var i = 0; i < animator.layerCount; i++)
                layerStates[i] = animator.GetCurrentAnimatorStateInfo(i).fullPathHash;

            World.Instance.RPC.CL_CharacterAnimationDataUpdate.Call(new NetStruct<int[], float, float>(layerStates, animator.GetFloat("Input_X"), animator.GetFloat("Input_Y")));
            _nextNetworkSend = Time.time + NETWORK_SEND_RATE;
        }
    }

    public void SetCastSpeed(float spd)
    {
        SpellCastSpeed = spd;
    }

    public void LockRoll(int bit)
    {
        RollLockMask |= (byte)(1 << bit);
    }

    public void UnlockRoll(int bit)
    {
        RollLockMask &= (byte)(~(1 << bit));
    }

    public void LockMovement(int bit)
    {
        MovementLockMask |= (byte)(1 << bit);
    }

    public void UnlockMovement(int bit)
    {
        MovementLockMask &= (byte)(~(1 << bit));
    }


    void HandleInput()
    {
        if (Camera != null)
        {
            Camera.RotateCamera(InputController.Instance.MouseAxis);
            Camera.Zoom(Input.GetAxis("Mouse ScrollWheel"));
        }

        if (!LockPlayer && MovementLockMask == 0)
        {
            animator.SetBool("Input_AttackPress", InputController.Instance.AttackPress);
            animator.SetBool("Input_AttackHold", InputController.Instance.AttackHold);
            animator.SetFloat("Input_X", InputController.Instance.InputAxis.x, 0.15f, Time.deltaTime);
            animator.SetFloat("Input_Y", InputController.Instance.InputAxis.y, 0.2f, Time.deltaTime);

            animator.SetBool("Input_ForwardDoublePress", InputController.Instance.ForwardDoublePress);
            animator.SetBool("Input_BackDoublePress", InputController.Instance.BackDoublePress);
            animator.SetBool("Input_LeftDoublePress", InputController.Instance.LeftDoublePress);
            animator.SetBool("Input_RightDoublePress", InputController.Instance.RightDoublePress);

            animator.SetFloat("Stat_AttackSpeed", CalculateAttackSpeed());

            CrouchInput();
            JumpInput();
        }
        else
        {
            InpForward = 0f;
        }
    }

    /*void UpdateMovingPlatform()
    {
        if (MovingPlatform == null)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position + Vector3.up * 0.1f, -Vector3.up, out hit, 0.2f))
            {
                MovingPlatform = hit.collider.GetComponent<Airship>();
                if (MovingPlatform != null)
                {
                    transform.SetParent(MovingPlatform.transform);
                    MovingPlatformCollider = hit.collider;
                }
            }
        }
        else if(MovingPlatformCollider != null)
        {
            RaycastHit hit;
            if (!MovingPlatformCollider.Raycast(new Ray(transform.position + Vector3.up * 0.1f, -Vector3.up), out hit, 100f))
            {
                transform.SetParent(null);
                MovingPlatform = null;
                MovingPlatformCollider = null;
            }
        }

        if(MovingPlatform != null)
        {
            transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
        }
    }*/

    void CheckStates()
    {
        var currState = animator.GetCurrentAnimatorStateInfo(0);
        animator.SetBool("InState_CombatSword1H", currState.IsTag("CombatSword1H"));
        animator.SetBool("InState_Roll", currState.IsTag("Roll"));
    }

    void CheckConditions()
    {
        animator.SetBool("Can_Roll", CanRoll() && CanUseSpell(SpellName.Roll));
        animator.SetBool("Can_SwordMastery1H", CanUseSpell(SpellName.SwordMastery1H));
        animator.SetBool("Can_CuttingStrike", CanUseSpell(SpellName.CuttingStrike));
        animator.SetBool("Can_DarkMatter", CanUseSpell(SpellName.DarkMatter));
    }

    float CalculateAttackSpeed()
    {
        var attackSpeed = World.ClientWorldInstance.ClientStats.AttackSpeed.Value;
        return attackSpeed * SpellCastSpeed;
    }

    bool CanUseSpell(SpellName s)
    {
        return World.ClientWorldInstance.ClientSpells.SpellInfo[s].CanUse();
    }

    bool CanRoll()
    {
        return RollLockMask == 0;
    }

    void ControlMovement()
    {
        InpForward = InputController.Instance.InputAxis.y;
        InpDirection = InputController.Instance.InputAxis.x;
    }

    void ControlHeight()
    {
        if (Crouch && !JumpOver)
        {
            capsuleCollider.center = colliderCenter / 1.4f;
            capsuleCollider.height = colliderHeight / 1.4f;
        }
        else if (JumpOver)
        {
            capsuleCollider.center = colliderCenter / 0.8f;
            capsuleCollider.height = colliderHeight / 2f;
        }
        else if (UsingLadder)
        {
            capsuleCollider.radius = colliderRadius / 1.25f;
        }
        else
        {
            // back to the original values
            capsuleCollider.center = colliderCenter;
            capsuleCollider.radius = colliderRadius;
            capsuleCollider.height = colliderHeight;
        }
    }

    void CheckGround()
    {
        CheckGroundDistance();
        // change the physics material to very slip when not grounded
        capsuleCollider.material = (OnGround) ? FRICTION_PHYSICS : DEFAULT_PHYSICS;
        // we don't want to stick the character grounded if one of these bools is true
        bool groundStickConditions = !JumpOver && !StepUp && !ClimbUp && !UsingLadder;

        if (groundStickConditions)
        {
            var onStep = StepOffset();
            if (GroundDistance <= 0.05f)
            {
                OnGround = true;
                // keeps the character grounded and prevents bounceness on ramps
                if (!onStep)
                    rigidbody.velocity = Vector3.ProjectOnPlane(rigidbody.velocity, GroundHit.normal);
            }
            else
            {
                if (GroundDistance >= GROUND_CHECK_DISTANCE)
                {
                    OnGround = false;
                    // check vertical velocity
                    VerticalVelocity = rigidbody.velocity.y;
                    // apply extra gravity when falling
                    if (!onStep)
                        transform.position -= Vector3.up * (EXTRA_GRAVITY * Time.deltaTime);
                }
                else if (!onStep && !Jump)
                    transform.position -= Vector3.up * (EXTRA_GRAVITY * Time.deltaTime);
            }
        }
    }

    void CheckGroundDistance()
    {
        if (capsuleCollider != null)
        {
            var radius = capsuleCollider.radius * 0.9f;
            var dist = Mathf.Infinity;
            var pos = transform.position + Vector3.up * (capsuleCollider.radius);
            var ray1 = new Ray(transform.position + new Vector3(0, colliderHeight / 2, 0), Vector3.down);
            var ray2 = new Ray(pos, -Vector3.up);
            if (Physics.Raycast(ray1, out GroundHit, Mathf.Infinity, LAYER_GROUND))
                dist = transform.position.y - GroundHit.point.y;

            if (Physics.SphereCast(ray2, radius, out GroundHit, Mathf.Infinity, LAYER_GROUND))
            {
                if (dist > (GroundHit.distance - capsuleCollider.radius * 0.1f))
                    dist = (GroundHit.distance - capsuleCollider.radius * 0.1f);
            }
            GroundDistance = dist;
        }
    }

    bool StepOffset()
    {
        if (InputController.Instance.InputAxis.sqrMagnitude < 0.1 || !OnGround)
            return false;

        var hit = new RaycastHit();
        var rayStep = new Ray((transform.position + new Vector3(0, STEP_OFFSET_END, 0) + transform.forward * (capsuleCollider.radius + STEP_OFFSET_FORWARD)), Vector3.down);

        if (Physics.Raycast(rayStep, out hit, STEP_OFFSET_END - STEP_OFFSET_START, LAYER_GROUND))
            if (hit.point.y >= (transform.position.y) && hit.point.y <= (transform.position.y + STEP_OFFSET_END))
            {
                var heightPoint = new Vector3(transform.position.x, hit.point.y + 0.1f, transform.position.z);
                transform.position = Vector3.Slerp(transform.position, heightPoint, (InpForward * 3.5f) * Time.deltaTime);
                return true;
            }
        return false;
    }

    GameObject CheckActionObject()
    {
        bool checkConditions = OnGround && !LandHigh && !InAction;
        GameObject _object = null;

        if (checkConditions)
        {
            RaycastHit hitInfoAction;
            Vector3 yOffSet = new Vector3(0f, -0.5f, 0f);
            Vector3 fwd = transform.TransformDirection(Vector3.forward);

            if (Physics.Raycast(transform.position - yOffSet, fwd, out hitInfoAction, 0.45f))
            {
                _object = hitInfoAction.transform.gameObject;
            }
        }
        return _object;
    }

    void RotateWithCamera()
    {
        if (!InAction && (InputController.Instance.InputAxis.magnitude > float.Epsilon))
        {
            Quaternion newPos = Quaternion.Euler(transform.eulerAngles.x, Camera.transform.eulerAngles.y, transform.eulerAngles.z);
            transform.rotation = Quaternion.Slerp(transform.rotation, newPos, 20f * Time.smoothDeltaTime);
        }
    }

    public void UpdateAnimator()
    {
        StateInfo = animator.GetCurrentAnimatorStateInfo(0);

        LandHighAnimation();
        JumpOverAnimation();
        ClimbUpAnimation();
        StepUpAnimation();
        JumpAnimation();
        LadderAnimation();

        ExtraMoveSpeed();
        animator.SetBool("Crouch", Crouch);
        animator.SetBool("OnGround", OnGround);
        animator.SetFloat("GroundDistance", GroundDistance);
        animator.SetFloat("VerticalVelocity", VerticalVelocity);

    }

    void ExtraMoveSpeed()
    {
        if (StateInfo.IsName("Grounded.Strafing Movement") || StateInfo.IsName("Grounded.Strafing Crouch"))
        {
            var newSpeed_Y = (ExtraStrafeSpeed * InpForward);
            var newSpeed_X = (ExtraStrafeSpeed * InpDirection);
            newSpeed_Y = Mathf.Clamp(newSpeed_Y, -ExtraStrafeSpeed, ExtraStrafeSpeed);
            newSpeed_X = Mathf.Clamp(newSpeed_X, -ExtraStrafeSpeed, ExtraStrafeSpeed);
            transform.position += transform.forward * (newSpeed_Y * Time.fixedDeltaTime);
            transform.position += transform.right * (newSpeed_X * Time.fixedDeltaTime);
        }
    }

    void LadderAnimation()
    {
        // resume the states of the ladder in one bool 
        UsingLadder =
            StateInfo.IsName("Ladder.EnterLadderBottom") ||
            StateInfo.IsName("Ladder.ExitLadderBottom") ||
            StateInfo.IsName("Ladder.ExitLadderTop") ||
            StateInfo.IsName("Ladder.EnterLadderTop") ||
            StateInfo.IsName("Ladder.ClimbLadder");

        // just to prevent any wierd blend between this animations
        if (UsingLadder)
        {
            Jump = false;
        }

        // make sure to lock the player when entering or exiting a ladder
        var lockOnLadder =
            StateInfo.IsName("Ladder.EnterLadderBottom") ||
            StateInfo.IsName("Ladder.ExitLadderBottom") ||
            StateInfo.IsName("Ladder.ExitLadderTop") ||
            StateInfo.IsName("Ladder.EnterLadderTop");

        LockPlayer = lockOnLadder;

        LadderBottom();
        LadderTop();
    }

    void LadderBottom()
    {
        animator.SetBool("EnterLadderBottom", EnterLadderBottom);
        animator.SetBool("ExitLadderBottom", ExitLadderBottom);

        // enter ladder from bottom
        if (StateInfo.IsName("Ladder.EnterLadderBottom"))
        {
            capsuleCollider.isTrigger = true;
            rigidbody.useGravity = false;

            // we are using matchtarget to find the correct X & Z to start climb the ladder
            // this information is provided by the cursorObject on the object, that use the script TriggerAction 
            // in this state we are sync the position based on the AvatarTarget.Root, but you can use leftHand, left Foot, etc.
            if (!animator.IsInTransition(0))
                MatchTarget(_matchTarget.position, _matchTarget.rotation, AvatarTarget.Root, new MatchTargetWeightMask(new Vector3(1, 1, 1), 1), 0.25f, 0.9f);

            if (StateInfo.normalizedTime >= 0.75f)
                EnterLadderBottom = false;
        }

        // exit ladder bottom
        if (StateInfo.IsName("Ladder.ExitLadderBottom"))
        {
            capsuleCollider.isTrigger = false;
            rigidbody.useGravity = true;

            if (StateInfo.normalizedTime >= 0.4f)
            {
                ExitLadderBottom = false;
                UsingLadder = false;
            }
        }
    }

    void LadderTop()
    {
        animator.SetBool("EnterLadderTop", EnterLadderTop);
        animator.SetBool("ExitLadderTop", ExitLadderTop);

        // enter ladder from top            
        if (StateInfo.IsName("Ladder.EnterLadderTop"))
        {
            capsuleCollider.isTrigger = true;
            rigidbody.useGravity = false;

            // we are using matchtarget to find the correct X & Z to start climb the ladder
            // this information is provided by the cursorObject on the object, that use the script TriggerAction 
            // in this state we are sync the position based on the AvatarTarget.Root, but you can use leftHand, left Foot, etc.
            if (StateInfo.normalizedTime < 0.25f && !animator.IsInTransition(0))
                MatchTarget(_matchTarget.position, _matchTarget.rotation, AvatarTarget.Root, new MatchTargetWeightMask(new Vector3(1, 0, 0.1f), 1), 0f, 0.25f);
            else if (!animator.IsInTransition(0))
                MatchTarget(_matchTarget.position, _matchTarget.rotation, AvatarTarget.Root, new MatchTargetWeightMask(new Vector3(1, 1, 1), 1), 0.25f, 0.7f);

            if (StateInfo.normalizedTime >= 0.7f)
                EnterLadderTop = false;
        }

        // exit ladder top
        if (StateInfo.IsName("Ladder.ExitLadderTop"))
        {
            if (StateInfo.normalizedTime >= 0.85f)
            {
                capsuleCollider.isTrigger = false;
                rigidbody.useGravity = true;
                ExitLadderTop = false;
                UsingLadder = false;
            }
        }
    }

    void JumpAnimation()
    {
        animator.SetBool("Jump", Jump);
        var newSpeed = (JUMP_FORWARD * InpForward);

        IsJumping = StateInfo.IsName("Action.Jump") || StateInfo.IsName("Action.JumpMove") || StateInfo.IsName("Airborne.FallingFromJump");
        animator.SetBool("IsJumping", IsJumping);

        if (StateInfo.IsName("Action.Jump"))
        {
            // apply extra height to the jump
            if (StateInfo.normalizedTime < 0.85f)
            {
                rigidbody.velocity = new Vector3(rigidbody.velocity.x, JUMP_FORCE, rigidbody.velocity.z);
                transform.position += transform.up * (JUMP_FORCE * Time.fixedDeltaTime);
            }
            // end jump animation
            if (StateInfo.normalizedTime >= 0.85f)
                Jump = false;
            // apply extra speed forward
            if (StateInfo.normalizedTime >= 0.65f && JUMP_AIR_CONTROL)
                transform.position += transform.forward * (newSpeed * Time.fixedDeltaTime);
            else if (StateInfo.normalizedTime >= 0.65f && !JUMP_AIR_CONTROL)
                transform.position += transform.forward * Time.fixedDeltaTime;
        }

        if (StateInfo.IsName("Action.JumpMove"))
        {
            // apply extra height to the jump
            if (StateInfo.normalizedTime < 0.85f)
            {
                rigidbody.velocity = new Vector3(rigidbody.velocity.x, JUMP_FORCE, rigidbody.velocity.z);
                transform.position += transform.up * (JUMP_FORCE * Time.fixedDeltaTime);
            }
            // end jump animation
            if (StateInfo.normalizedTime >= 0.55f)
                Jump = false;
            // apply extra speed forward
            if (JUMP_AIR_CONTROL)
                transform.position += transform.forward * (newSpeed * Time.fixedDeltaTime);
            else
                transform.position += transform.forward * Time.fixedDeltaTime;
        }

        // apply extra speed forward when falling
        if (StateInfo.IsName("Airborne.FallingFromJump") && JUMP_AIR_CONTROL)
            transform.position += transform.forward * (newSpeed * Time.fixedDeltaTime);
        else if (StateInfo.IsName("Airborne.FallingFromJump") && !JUMP_AIR_CONTROL)
            transform.position += transform.forward * Time.fixedDeltaTime;
    }

    void LandHighAnimation()
    {
        animator.SetBool("LandHigh", LandHigh);

        // if the character fall from a great height, landhigh animation
        if (!OnGround && VerticalVelocity <= LANDING_ANIMATION_VELOCITY && GroundDistance <= 0.5f)
            LandHigh = true;

        if (LandHigh && StateInfo.IsName("Airborne.LandHigh"))
        {
            if (StateInfo.normalizedTime > 0.9f)
                LandHigh = false;
        }
    }

    void StepUpAnimation()
    {
        animator.SetBool("StepUp", StepUp);

        if (StateInfo.IsName("Action.StepUp"))
        {
            if (StateInfo.normalizedTime > 0.1f && StateInfo.normalizedTime < 0.3f)
            {
                gameObject.GetComponent<Collider>().isTrigger = true;
                rigidbody.useGravity = false;
            }

            // we are using matchtarget to find the correct height of the object                
            if (!animator.IsInTransition(0))
                MatchTarget(_matchTarget.position, _matchTarget.rotation, AvatarTarget.LeftHand, new MatchTargetWeightMask(new Vector3(0, 1, 1), 0), 0f, 0.5f);

            if (StateInfo.normalizedTime > 0.9f)
            {
                gameObject.GetComponent<Collider>().isTrigger = false;
                rigidbody.useGravity = true;
                StepUp = false;
            }
        }
    }

    void JumpOverAnimation()
    {
        animator.SetBool("JumpOver", JumpOver);

        if (StateInfo.IsName("Action.JumpOver"))
        {
            if (StateInfo.normalizedTime > 0.1f && StateInfo.normalizedTime < 0.3f)
                rigidbody.useGravity = false;

            // we are using matchtarget to find the correct height of the object
            if (!animator.IsInTransition(0))
                MatchTarget(_matchTarget.position, _matchTarget.rotation,
                            AvatarTarget.LeftHand, new MatchTargetWeightMask
                            (new Vector3(0, 1, 1), 0), 0.1f * (1 - StateInfo.normalizedTime), 0.3f * (1 - StateInfo.normalizedTime));

            if (StateInfo.normalizedTime >= 0.7f)
            {
                rigidbody.useGravity = true;
                JumpOver = false;
            }
        }
    }

    void ClimbUpAnimation()
    {
        animator.SetBool("ClimbUp", ClimbUp);

        if (StateInfo.IsName("Action.ClimbUp"))
        {
            if (StateInfo.normalizedTime > 0.1f && StateInfo.normalizedTime < 0.3f)
            {
                rigidbody.useGravity = false;
                gameObject.GetComponent<Collider>().isTrigger = true;
            }

            // we are using matchtarget to find the correct height of the object
            if (!animator.IsInTransition(0))
                MatchTarget(_matchTarget.position, _matchTarget.rotation,
                           AvatarTarget.LeftHand, new MatchTargetWeightMask
                           (new Vector3(0, 1, 1), 0), 0f, 0.2f);

            if (StateInfo.normalizedTime >= 0.85f)
            {
                gameObject.GetComponent<Collider>().isTrigger = false;
                rigidbody.useGravity = true;
                ClimbUp = false;
            }
        }
    }

    void MatchTarget(Vector3 matchPosition, Quaternion matchRotation, AvatarTarget target, MatchTargetWeightMask weightMask, float normalisedStartTime, float normalisedEndTime)
    {
        if (animator.isMatchingTarget)
            return;

        float normalizeTime = Mathf.Repeat(animator.GetCurrentAnimatorStateInfo(0).normalizedTime, 1f);
        if (normalizeTime > normalisedEndTime)
            return;

        animator.MatchTarget(matchPosition, matchRotation, target, weightMask, normalisedStartTime, normalisedEndTime);
    }

    void ControlCameraState()
    {
        if (Camera == null)
            return;

        if (Crouch)
            Camera.ChangeState(CameraController.STATE_CROUCH, true);
        else
            Camera.ChangeState(CameraController.STATE_DEFAULT, true);
    }

    void CrouchInput()
    {
        if (AutoCrouch)
            Crouch = true;
        else
        {
            if (Input.GetButtonDown("Y") && OnGround && !InAction)
                Crouch = !Crouch;
        }
    }

    void JumpInput()
    {
        bool jumpConditions = !Crouch && OnGround && !InAction;

        if (Input.GetButtonDown("X") && jumpConditions)
            Jump = true;
    }

    void CheckForwardAction()
    {
        var hitObject = CheckActionObject();
        if (hitObject != null)
        {
            try
            {
                if (hitObject.CompareTag("ClimbUp"))
                    DoAction(hitObject, ref ClimbUp);
                else if (hitObject.CompareTag("StepUp"))
                    DoAction(hitObject, ref StepUp);
                else if (hitObject.CompareTag("JumpOver"))
                    DoAction(hitObject, ref JumpOver);
                else if (hitObject.CompareTag("AutoCrouch"))
                    AutoCrouch = true;
                else if (hitObject.CompareTag("EnterLadderBottom") && !Jump)
                    DoAction(hitObject, ref EnterLadderBottom);
                else if (hitObject.CompareTag("EnterLadderTop") && !Jump)
                    DoAction(hitObject, ref EnterLadderTop);
            }
            catch (UnityException e)
            {
                Debug.LogWarning(e.Message);
            }
        }
    }

    void DoAction(GameObject hitObject, ref bool action)
    {
        var triggerAction = hitObject.transform.GetComponent<TriggerAction>();
        if (!triggerAction)
        {
            Debug.LogWarning("Missing TriggerAction Component on " + hitObject.transform.name + "Object");
            return;
        }

        if (Input.GetButton("A") && !InAction || triggerAction.autoAction && !InAction)
        {
            // turn the action bool true and call the animation
            action = true;

            // find the cursorObject height to match with the character animation
            _matchTarget = triggerAction.target;
            // align the character rotation with the object rotation
            var rot = hitObject.transform.rotation;
            transform.rotation = rot;
        }
    }

    void OnTriggerStay(Collider other)
    {
        try
        {
            // if you are using the ladder and reach the exit from the bottom
            if (other.CompareTag("ExitLadderBottom") && UsingLadder)
            {
                if (Input.GetButtonDown("B") || InpForward <= -0.05f && !EnterLadderBottom)
                    ExitLadderBottom = true;
            }
            // if you are using the ladder and reach the exit from the top
            if (other.CompareTag("ExitLadderTop") && UsingLadder && !EnterLadderTop)
            {
                if (InpForward >= 0.05f)
                    ExitLadderTop = true;
            }
        }
        catch (UnityException e)
        {
            Debug.LogWarning(e.Message);
        }
    }

    protected override void OnDeath()
    {
        base.OnDeath();
        Debug.Log("Character death");
        UIRoot.Instance.HideAllGameWindows();
        UIRoot.Instance.DeathScreen.TryShow();
    }

    protected override void OnRevive()
    {
        base.OnRevive();
        Debug.Log("Character revive");
        UIRoot.Instance.DeathScreen.TryHide();
    }

    private void OnHealthChanged(NetworkVariable obj)
    {
        UIRoot.Instance.HUDController.SetHealth(NetworkCharacter.CurrentHealth.GetValue(), NetworkCharacter.MaxHealth.GetValue());
    }

    private void OnStaminaChanged(NetworkVariable obj)
    {
        UIRoot.Instance.HUDController.SetStamina(NetworkCharacter.CurrentStamina.GetValue(), NetworkCharacter.MaxStamina.GetValue());
    }
}

