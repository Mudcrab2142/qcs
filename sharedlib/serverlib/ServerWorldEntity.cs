﻿using Lidgren.Network;
using SharedLib;
using System;
using UnityEngine;

namespace ServerLib
{
    public abstract class ServerWorldEntity : WorldEntity
    {
        /// <summary>
        /// Spawns a new WorldEntity
        /// </summary>
        public static bool Spawn(EntityType type, out ServerWorldEntity entity, params object[] args)
        {
            if (!NetworkServerWorld.Instance)
            {
                entity = null;
                Debug.LogError("[WorldEntity] NetworkServerWorld not found!");
                return false;
            }

            entity = NetworkServerWorld.Instance.CreateEntity(type, args) as ServerWorldEntity;
            if (entity == null)
                return false;

            entity.NetworkEntity = (NetworkBaseEntity)entity.gameObject.AddComponent(entity.NetworkType);
            entity.NetworkEntity.EntityClassType = entity.EntityType;
            entity.UpdateAction += entity.UpdateNetworkData;
            NetworkBaseWorld.WorldInstance.AddWorldEntity(entity);
            entity.UpdateNetworkData();
            return true;
        }

        public static bool SpawnImmediate(EntityType type, out ServerWorldEntity entity, params object[] args)
        {
            if(Spawn(type, out entity, args))
            {
                var buffer = LidgrenRootServer.DefaultSendBuffer;
                buffer.BeginMessage(NetworkMessageType.ServerEntityImmediateSpawn, false);
                entity.NetworkEntity.StreamWrite(buffer, true);
                buffer.EndMessage();
                LidgrenRootServer.Instance.SendToAllInArea(buffer, NetDeliveryMethod.ReliableOrdered, ServerConstants.SERVICE_SEND_CHANNEL, entity.transform.position, 100f);
                return true;
            }

            return false;
        }

        public static bool Spawn<T>(EntityType type, out T entity, params object[] args) where T : ServerWorldEntity
        {
            entity = null;

            ServerWorldEntity temp;
            if (!Spawn(type, out temp, args))
                return false;

            entity = temp as T;
            return entity != null;
        }

        public abstract Type NetworkType { get; }
        public abstract EntityType EntityType { get; }
        public virtual void UpdateNetworkData() { }

        public void NetworkDestroy(bool immediate = false)
        {
            UpdateAction -= UpdateNetworkData;
            LockUpdate = true;
            NetworkServerWorld.Instance.QueueDestroy(this);

            if(immediate)
            {
                var buffer = LidgrenRootServer.DefaultSendBuffer;
                buffer.BeginMessage(NetworkMessageType.ServerEntityImmediateDestroy, false);
                buffer.WriteInt32(NetworkEntity.EntityId);
                buffer.EndMessage();
                LidgrenRootServer.Instance.SendToAllInArea(buffer, Lidgren.Network.NetDeliveryMethod.ReliableOrdered, ServerConstants.SERVICE_SEND_CHANNEL, transform.position, 100f);
            }
        }
    }
}
