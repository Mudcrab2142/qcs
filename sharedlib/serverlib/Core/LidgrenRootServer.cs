﻿using System;
using Lidgren.Network;
using System.Collections.Generic;
using UnityEngine;
using SharedLib;

namespace ServerLib
{
    public sealed class LidgrenRootServer : LidgrenRoot
    {
        public static LidgrenRootServer Instance => LidgrenInstance as LidgrenRootServer;

        public event Action<UserClient> OnClientConnected;
        public event Action<UserClient> OnClientDisconnected;

        public readonly Dictionary<long, UserClient> Clients = new Dictionary<long, UserClient>();

        public override float ServerTime => Time.time;

        void Awake()
        {
            var config = new NetPeerConfiguration("huskar666");
            InitConfig(ref config);

            config.MaximumConnections = 100;
            config.Port = 14888;

            netPeer = new NetServer(config);
            netPeer.Start();


            foreach (var variable in ConVar.Variables)
            {
                if (variable.Type != ConVarType.ServerVariable)
                    continue;

                if ((variable.Flags & (~ConVarFlags.InitOnly)) == 0)
                    continue;

                variable.OnValueUpdate += UpdateServerInfo;
            }
        }

        void OnDestroy()
        {
            foreach (var variable in ConVar.Variables)
            {
                if (variable.Type != ConVarType.ServerVariable)
                    continue;

                if ((variable.Flags & (~ConVarFlags.InitOnly)) == 0)
                    continue;

                variable.OnValueUpdate -= UpdateServerInfo;
            }
        }

        void UpdateServerInfo(string variableName, string variableValue)
        {
            DefaultSendBuffer.BeginMessage(NetworkMessageType.RCONUpdateVariable);
            DefaultSendBuffer.WriteString(variableName);
            DefaultSendBuffer.WriteString(variableValue);
            DefaultSendBuffer.EndMessage();

            SendToAll(DefaultSendBuffer, NetDeliveryMethod.ReliableOrdered, ServerConstants.SERVICE_SEND_CHANNEL);
        }

        protected override void OnConnectEvent(NetConnection connection)
        {
            if (Clients.ContainsKey(connection.RemoteUniqueIdentifier))
                return;

            var userClient = new UserClient(connection);
            Clients.Add(connection.RemoteUniqueIdentifier, userClient);
            if (OnClientConnected != null)
                OnClientConnected(userClient);
        }

        protected override void OnDisconnectEvent(NetConnection connection)
        {
            if (!Clients.ContainsKey(connection.RemoteUniqueIdentifier))
                return;

            if (OnClientDisconnected != null)
                OnClientDisconnected(Clients[connection.RemoteUniqueIdentifier]);
            Clients.Remove(connection.RemoteUniqueIdentifier);
        }

        protected override void OnDataEvent(NetConnection connection, NetworkMessageType type)
        {
            base.OnDataEvent(connection, type);

            switch (type)
            {
                case NetworkMessageType.ClientRequestServerInfo:
                    ProcessServerInfoRequest(connection);
                    return;
                case NetworkMessageType.RCONCommand:
                    ProcessConsoleCommand(connection);
                    return;
                case NetworkMessageType.ClientRequestFullSnapshot:
                    ProcessFullSnapshotRequest(connection);
                    return;
            }
        }

        public UserClient FindClient(NetConnection connection)
        {
            if (connection == null)
                return null;

            UserClient result;
            if (Clients.TryGetValue(connection.RemoteUniqueIdentifier, out result))
                return result;

            return null;
        }

        private void ProcessFullSnapshotRequest(NetConnection connection)
        {
            var client = FindClient(connection);
            if (client == null)
                return;

            var x = NetworkReader.ReadInt16();
            var y = NetworkReader.ReadInt16();
            var l = NetworkReader.ReadByte();

            client.FullCellDataRequestList.Add((((((ulong)0 | (ushort)x) << 16) | (ushort)y) << 8) | l);
        }

        private void ProcessServerInfoRequest(NetConnection connection)
        {
            short count = 0;
            int countPosition = 0;
            int endPosition = 0;

            DefaultSendBuffer.BeginMessage(NetworkMessageType.ServerInfo, true);
            DefaultSendBuffer.WriteInt64(connection.RemoteUniqueIdentifier);
            DefaultSendBuffer.WriteSingle(Time.time);

            countPosition = DefaultSendBuffer.CurrentPosition;
            DefaultSendBuffer.Seek(DefaultSendBuffer.CurrentPosition + 2);

            foreach (var item in ConVar.Variables)
            {
                DefaultSendBuffer.WriteString(item.Name);
                DefaultSendBuffer.WriteString(item.ValueString);
                count++;
            }

            endPosition = DefaultSendBuffer.CurrentPosition;
            DefaultSendBuffer.Seek(countPosition);
            DefaultSendBuffer.WriteInt16(count);
            DefaultSendBuffer.Seek(endPosition);
            DefaultSendBuffer.EndMessage();

            SendToClient(connection, DefaultSendBuffer, NetDeliveryMethod.ReliableOrdered, ServerConstants.SERVICE_SEND_CHANNEL);
        }

        private void ProcessConsoleCommand(NetConnection connection)
        {
            var str = NetworkReader.ReadString();
            var data = str.Split(new char[] { ' ' }, 2);
            Debug.Log("[ConVar] " + str);
            ConVar target = ConVar.Get(data[0]);
            if (target != null)
            {
                switch (target.Type)
                {
                    case ConVarType.ServerCommand:
                        target.Invoke(data.Length >= 2 ? data[1] : null, connection);
                        break;
                    case ConVarType.ServerVariable:
                        if (data.Length == 2)
                            target.SetSerializedValue(data[1]);
                        break;
                }
            }
        }

        internal void SendToClient(NetConnection connection, NetworkSendBuffer data, NetDeliveryMethod deliveryMethod, int channelId)
        {
            var msg = netPeer.CreateMessage();
            msg.Write(data.SendBuffer, 0, data.CurrentPosition);
            netPeer.SendMessage(msg, connection, deliveryMethod, channelId);
            netPeer.FlushSendQueue();
        }

        internal void SendToAll(NetworkSendBuffer data, NetDeliveryMethod deliveryMethod, int channelId)
        {
            var msg = netPeer.CreateMessage();
            msg.Write(data.SendBuffer, 0, data.CurrentPosition);

            foreach (var connection in netPeer.Connections)
            {
                netPeer.SendMessage(msg, connection, deliveryMethod, channelId);
                netPeer.FlushSendQueue();
            }
        }

        internal void SendToAllInArea(NetworkSendBuffer data, NetDeliveryMethod deliveryMethod, int channelId, Vector3 center, float areaSize)
        {
            areaSize /= 2f;
            foreach (var client in Clients)
            {
                if (client.Value.ControlledNetworkEntity == null)
                    continue;

                var pos = client.Value.ControlledNetworkEntity.Position.GetValue();
                if (pos.x > center.x - areaSize && pos.x < center.x + areaSize && pos.z > center.z - areaSize && pos.z < center.z + areaSize)
                    SendToClient(client.Value.Peer, data, NetDeliveryMethod.ReliableOrdered, channelId);
            }
        }

        internal override void NetFuncSendDirectCall(NetworkSendBuffer data, NetConnection targetConnection)
        {
            SendToClient(targetConnection, data, NetDeliveryMethod.ReliableOrdered, ServerConstants.RPC_SEND_CHANNEL);
        }

        internal override void NetFuncBroadcastCall(NetworkSendBuffer data, Vector3 center, float areaSize)
        {
            areaSize /= 2f;
            foreach(var client in Clients)
            {
                if (client.Value.ControlledNetworkEntity == null)
                    continue;

                var pos = client.Value.ControlledNetworkEntity.Position.GetValue();
                if (pos.x > center.x - areaSize && pos.x < center.x + areaSize && pos.z > center.z - areaSize && pos.z < center.z + areaSize)
                    SendToClient(client.Value.Peer, data, NetDeliveryMethod.ReliableOrdered, ServerConstants.RPC_SEND_CHANNEL);
            }
        }
    }
}
