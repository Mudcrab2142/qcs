﻿namespace ServerLib
{
    internal class ServerConstants
    {
        internal const byte CHARACTERS_LAYER = 1;
        internal const byte OTHERS_LAYER = 2;

        internal const int RPC_SEND_CHANNEL = 0;
        internal const int CHARACTERS_SEND_CHANNEL = 1;
        internal const int OTHERS_SEND_CHANNEL = 2;
        internal const int GLOBAL_SEND_CHANNEL = 3;
        internal const int SERVICE_SEND_CHANNEL = 20;

        internal const float CHARACTERS_SEND_RATE = 0.05f;
        internal const float OTHERS_SEND_RATE = 0.1f;

        internal const float CELL_SIZE = 200f;
    }
}
