﻿using SharedLib;
using Lidgren.Network;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ServerLib
{
    public abstract class NetworkServerWorld : NetworkBaseWorld
    {
        public static NetworkServerWorld Instance => WorldInstance as NetworkServerWorld;
        public LidgrenRootServer NetworkRoot { get; private set; }

        internal Dictionary<int, NetworkBaseEntity> NetworkEntities = new Dictionary<int, NetworkBaseEntity>();

        private Dictionary<EntityType, Func<object[], WorldEntity>> _entityCreators = new Dictionary<EntityType, Func<object[], WorldEntity>>();

        internal Dictionary<CellPoint, NetworkCell> Cells = new Dictionary<CellPoint, NetworkCell>();
        internal NetworkCellLayer GlobalEntities = new NetworkCellLayer(null, 0.2f, 0, ServerConstants.GLOBAL_SEND_CHANNEL);
        private Queue<ServerWorldEntity> _destroyQueue = new Queue<ServerWorldEntity>();

        public override IFinder Finder => _finder;
        private ServerFinder _finder = new ServerFinder();

        private int _entityIdCounter;

        public static void CreateWorld<T>() where T : NetworkServerWorld
        {
            var obj = new GameObject(typeof(T).ToString());
            WorldInstance = obj.AddComponent<T>();
            Instance.LoadWorld((string)ConVar.S_Map.Value, () => Instance.WorldStart());

            Instance.NetworkRoot = LidgrenRoot.CreateInstance<LidgrenRootServer>();
            Instance.NetworkRoot.transform.SetParent(Instance.transform);

            LidgrenRootServer.Instance.OnClientConnected += Instance.OnClientConnected;
            LidgrenRootServer.Instance.OnClientDisconnected += Instance.OnClientDisconnected;

            Instance.OnAddWorldEntity += n => { Instance.OnAddEntity(n.NetworkEntity); };
            Instance.OnRemoveWorldEntity += n => { Instance.OnRemoveEntity(n.NetworkEntity); };

            WorldInstance.Init();
            //   Instance.WorldStart();
        }

        internal void QueueDestroy(ServerWorldEntity serverWorldEntity)
        {
            _destroyQueue.Enqueue(serverWorldEntity);
        }

        protected abstract void OnClientConnected(UserClient connection);
        protected abstract void OnClientDisconnected(UserClient connection);

        protected abstract void WorldStart();

        protected override sealed void Update()
        {
            base.Update();

            while (_destroyQueue.Count > 0)
                RemoveWorldEntity(_destroyQueue.Dequeue());

            var timeStamp = Time.time;

            foreach (var item in Cells.Values)
                item.UpdateLayers(timeStamp);

            GlobalEntities.Update(timeStamp);

            foreach (var client in NetworkRoot.Clients)
            {
                foreach (var cell in Cells)
                {
                    try
                    {
                        if (client.Value.ShouldReceiveData(cell.Value.Point))
                        {
                            cell.Value.SendData(client.Value);
                            cell.Value.SendNeighbourData(client.Value);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                }

                if (client.Value.Ready)
                {
                    GlobalEntities.Send(client.Value);
                }
            }
        }

        void OnAddEntity(NetworkBaseEntity entity)
        {
            entity.EntityId = _entityIdCounter++;
            networkEntities.Add(entity.EntityId, entity);

            if (entity.UseNetworkCells)
            {
                entity.Position.OnValueChanged += OnTransformEntityPositionChanged;
                OnTransformEntityPositionChanged(entity.Position);
            }
            else
            {
                GlobalEntities.AddEntity(entity);
            }
        }

        void OnRemoveEntity(NetworkBaseEntity entity)
        {
            if (entity.UseNetworkCells)
                Cells[new CellPoint(entity.CellX, entity.CellY)].RemoveEntity(entity);
            else
                GlobalEntities.RemoveEntity(entity);

            Destroy(entity.gameObject);
        }

        /// <summary>
        /// Zone optimizations
        /// </summary>
        void OnTransformEntityPositionChanged(NetworkVariable variable)
        {
            var owner = variable.Owner;
            var pos = owner.Position.GetValue();
            var cellX = (short)(Mathf.CeilToInt((pos.x) / ServerConstants.CELL_SIZE));
            var cellY = (short)(Mathf.CeilToInt((pos.z) / ServerConstants.CELL_SIZE));

            if (cellX <= 0)
                cellX--;

            if (cellY <= 0)
                cellY--;

            if (owner.InsideCell && cellX == owner.CellX && cellY == owner.CellY)
                return;

            var point = new CellPoint(cellX, cellY);

            if (!Cells.ContainsKey(point))
                Cells.Add(point, new NetworkCell(point, Cells));

            if (owner.InsideCell)
                Cells[new CellPoint(owner.CellX, owner.CellY)].RemoveEntity(owner);

            Cells[point].AddEntity(owner);

            owner.CellX = cellX;
            owner.CellY = cellY;

            owner.InsideCell = true;
        }

        internal WorldEntity CreateEntity(EntityType type, params object[] args)
        {
            if (!_entityCreators.ContainsKey(type))
            {
                Debug.LogError(string.Format("[NetworkWorld] No factory method found for type {0}!", type));
                return null;
            }
            try
            {
                var entity = _entityCreators[type](args);
                return entity;
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("[Factory error] Invalid input params for {0}! \n {1}", type, e.StackTrace);
                return null;
            }
        }

        protected void AddFactoryMethod(EntityType type, Func<object[], WorldEntity> function)
        {
            if (_entityCreators.ContainsKey(type))
            {
                Debug.LogError(string.Format("[NetworkWorld] Factory method for type {0} already exists!", type));
                return;
            }

            _entityCreators.Add(type, function);
        }
    }
}
