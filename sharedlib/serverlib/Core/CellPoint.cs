﻿namespace ServerLib
{
    internal struct CellPoint
    {
        internal readonly static CellPoint Null = new CellPoint(0, 0);

        internal short X;
        internal short Y;

        internal CellPoint(short x, short y)
        {
            X = x;
            Y = y;
        }
    }
}
