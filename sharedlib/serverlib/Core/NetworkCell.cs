﻿using SharedLib;
using System.Collections.Generic;

namespace ServerLib
{
    internal class NetworkCell
    {
        static readonly List<CellPoint> _neighbours = new List<CellPoint>()
        {
            new CellPoint(1, 1),
            new CellPoint(0, 1),
            new CellPoint(-1, 1),
            new CellPoint(1, 0),
            new CellPoint(-1, 0),
            new CellPoint(1, -1),
            new CellPoint(0, -1),
            new CellPoint(-1, -1)
        };

        private Dictionary<CellPoint, NetworkCell> _neigbourCells = new Dictionary<CellPoint, NetworkCell>();
        private Dictionary<byte, NetworkCellLayer> _cellLayers = new Dictionary<byte, NetworkCellLayer>();
        internal readonly CellPoint Point;

        internal NetworkCell(CellPoint point, Dictionary<CellPoint, NetworkCell> dict)
        {
            Point = point;

            _cellLayers.Add(ServerConstants.CHARACTERS_LAYER, new NetworkCellLayer(this, ServerConstants.CHARACTERS_SEND_RATE, ServerConstants.CHARACTERS_LAYER, ServerConstants.CHARACTERS_SEND_CHANNEL));
            _cellLayers.Add(ServerConstants.OTHERS_LAYER, new NetworkCellLayer(this, ServerConstants.OTHERS_SEND_RATE, ServerConstants.OTHERS_LAYER, ServerConstants.OTHERS_SEND_CHANNEL));

            //init neighbours
            if (dict != null)
            {
                foreach (var offset in _neighbours)
                {
                    var p = new CellPoint((short)(point.X + offset.X), (short)(point.Y + offset.Y));
                    if (dict.ContainsKey(p))
                    {
                        SetNeighbour(dict[p], offset);
                        dict[p].SetNeighbour(this, new CellPoint((short)(-offset.X), (short)(-offset.Y)));
                    }
                }
            }
        }

        internal void SetNeighbour(NetworkCell cell, CellPoint point)
        {
            _neigbourCells.Add(point, cell);
        }

        internal void SendNeighbourData(UserClient client)
        {
            foreach (var item in _neigbourCells)
                item.Value.SendData(client);
        }

        internal void SendData(UserClient client)
        {
            foreach (var item in _cellLayers)
                item.Value.Send(client);
        }

        internal void UpdateLayers(float time)
        {
            foreach (var item in _cellLayers)
                item.Value.Update(time);
        }

        internal void AddEntity<T>(T entity) where T : NetworkBaseEntity
        {
            GetActualCellForEntityType(entity.EntityClassType).AddEntity(entity);
        }

        internal void RemoveEntity<T>(T entity) where T : NetworkBaseEntity
        {
            GetActualCellForEntityType(entity.EntityClassType).RemoveEntity(entity);
        }

        private NetworkCellLayer GetActualCellForEntityType(EntityType type)
        {
            switch (type)
            {
                case EntityType.Character:
                    return _cellLayers[ServerConstants.CHARACTERS_LAYER];

                default:
                    return _cellLayers[ServerConstants.OTHERS_LAYER];
            }
        }
    }
}
