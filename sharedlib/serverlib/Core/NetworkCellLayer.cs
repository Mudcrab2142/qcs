﻿using Lidgren.Network;
using SharedLib;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ServerLib
{
    internal class NetworkCellLayer
    {
        private static int _idCounter = 0;

        private NetworkSendBuffer _currentDeltaSnapshot = new NetworkSendBuffer(65535);
        private NetworkSendBuffer _currentFullSnapshot = new NetworkSendBuffer(65535);

        private readonly NetworkCell _owner;
        private readonly byte _layerId;
        private readonly float _sendRate;
        private readonly int _sendChannelId;

        private float _nextUpdateTime;
        private uint _tick;
        private readonly int _cellId;

        private readonly Dictionary<int, NetworkBaseEntity> _entities = new Dictionary<int, NetworkBaseEntity>();
        private NetworkBaseEntity[] _values = new NetworkBaseEntity[0];

        private ulong _cellHash;

        internal NetworkCellLayer(NetworkCell owner, float sendRate, byte layerId, int sendChannelId)
        {
            _owner = owner;
            _sendRate = sendRate;
            _layerId = layerId;
            _sendChannelId = sendChannelId;
            _cellId = _idCounter++;

            if(owner != null)
                _cellHash = (((((ulong)0 | (ushort)owner.Point.X) << 16) | (ushort)owner.Point.Y) << 8) | layerId;
            else
                _cellHash = (((((ulong)0 | (ushort)short.MaxValue) << 16) | (ushort)short.MaxValue) << 8) | layerId;
        }

        internal void Update(float time)
        {
            if (_nextUpdateTime > Time.time)
                return;

            GenerateSnapshot(_currentDeltaSnapshot);
            GenerateSnapshot(_currentFullSnapshot, true);

            _nextUpdateTime = Time.time + _sendRate;
            _tick++;
        }

        internal void AddEntity(NetworkBaseEntity entity)
        {
            _entities.Add(entity.EntityId, entity);
            _values = _entities.Values.ToArray();
            entity.ResetVariableFlags();
        }

        internal void RemoveEntity(NetworkBaseEntity entity)
        {
            _entities.Remove(entity.EntityId);
            _values = _entities.Values.ToArray();
        }

        private void GenerateSnapshot(NetworkSendBuffer buf, bool forceFull = false)
        {
            buf.BeginMessage(NetworkMessageType.ServerEntityStateUpdate, true);
            buf.WriteBool(forceFull);
            buf.WriteInt16((short)_entities.Count);
            buf.WriteInt16(_owner != null ? _owner.Point.X : short.MaxValue);
            buf.WriteInt16(_owner != null ? _owner.Point.Y : short.MaxValue);
            buf.WriteByte(_layerId);
            buf.WriteUInt32(_tick);

            for (int i = 0; i < _values.Length; i++)
                _values[i].StreamWrite(buf, forceFull);

            buf.EndMessage();
        }

        internal void Send(UserClient client)
        {
            uint lastSendedTick;
            client.LastSendedWorldSnapshots.TryGetValue(_cellHash, out lastSendedTick);

            if (_tick == lastSendedTick)
                return;

            if (client.FullCellDataRequestList.Contains(_cellHash))
            {
                LidgrenRootServer.Instance.SendToClient(client.Peer, _currentFullSnapshot, NetDeliveryMethod.ReliableOrdered, _sendChannelId);
                client.FullCellDataRequestList.Remove(_cellHash);
            }
            else
            {
                LidgrenRootServer.Instance.SendToClient(client.Peer, _currentDeltaSnapshot, NetDeliveryMethod.ReliableOrdered, _sendChannelId);
            }

            client.LastSendedWorldSnapshots[_cellHash] = _tick;
        }
    }
}
