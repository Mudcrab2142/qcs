﻿using SharedLib;
using Lidgren.Network;
using System.Collections.Generic;

namespace ServerLib
{
    public class UserClient
    {
        public bool Ready => ControlledNetworkEntity;

        public int CellX => ControlledNetworkEntity.CellX;
        public int CellY => ControlledNetworkEntity.CellY;
        
        public NetworkBaseEntity ControlledNetworkEntity { get; set; }

        public NetConnection Peer;
        public long ConnectionId => Peer.RemoteUniqueIdentifier;

        public UserClient(NetConnection peer)
        {
            Peer = peer;
        }

        public readonly List<ulong> FullCellDataRequestList = new List<ulong>();
        public readonly Dictionary<ulong, uint> LastSendedWorldSnapshots = new Dictionary<ulong, uint>();

        #region ZoneOptimizations
        internal bool ShouldReceiveData(CellPoint point)
        {
            return Ready && InZone(point.X, point.Y);
        }

        bool InZone(int x, int y)
        {
            return (x == Decrease(CellX) && y == Increase(CellY)) ||
                (x == CellX && y == Increase(CellY)) ||
                (x == Increase(CellX) && y == Increase(CellY)) ||

                (x == Decrease(CellX) && y == CellY) ||
                (x == CellX && y == CellY) ||
                (x == Increase(CellX) && y == CellY) ||

                (x == Decrease(CellX) && y == Decrease(CellY)) ||
                (x == CellX && y == Decrease(CellY)) ||
                (x == Increase(CellX) && y == Decrease(CellY));
        }

        int Increase(int value)
        {
            value++;
            if (value == 0)
                value++;
            return value;
        }

        int Decrease(int value)
        {
            value--;
            if (value == 0)
                value--;
            return value;
        }
        #endregion
    }
}
