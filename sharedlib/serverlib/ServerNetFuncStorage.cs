﻿using System;
using Lidgren.Network;
using SharedLib;
using UnityEngine;

namespace ServerLib
{
    public class ServerNetFuncStorage : NetFuncStorage
    {
        public ServerNetFuncStorage(LidgrenRoot root) : base(root)
        { 
            CL_CoreWorldLoaded.SetHandler(CoreWorldLoaded);
            CL_CoreCreateCharacter.SetHandler(CoreCreateCharacter);
            CL_CoreCharacterLoaded.SetHandler(CoreCharacterLoaded);
            CL_CoreSendPlayerInput.SetHandler(CoreSendPlayerInput);

            CL_InventoryMoveItem.SetHandler(InventoryMoveItem);
            CL_InventoryEquipItem.SetHandler(InventoryEquipItem);
            CL_InventoryUnequipItem.SetHandler(InventoryUnequipItem);

            CL_CharacterRespawn.SetHandler(CharacterRespawn);

            CL_SpellUse.SetHandler(SpellUse);
            CL_SpellTrigger.SetHandler(SpellTrigger);
            CL_SpellDirectionalCast.SetHandler(SpellDirectionalCast);
            CL_SpellLearn.SetHandler(SpellLearn);

            CL_CraftStart.SetHandler(CraftStart);

            CL_MainStatUpgrade.SetHandler(MainStatUpgrade);

            CL_EntityAirshipEnter.SetHandler(Airship.Enter);
            CL_EntityAirshipExit.SetHandler(Airship.Exit);
            CL_EntityAirshipControl.SetHandler(Airship.Control);
        }

        private void CoreWorldLoaded(NetConnection connection)
        {
            World.WorldInstance.RPC.S_CoreRequestClientInfo.Call(World.ServerWorldInstance.OnClientInfoReceived, connection);
        }

        private void CoreCharacterLoaded(NetConnection connection)
        {
            var character = LidgrenRootServer.Instance.Clients[connection.RemoteUniqueIdentifier].ControlledNetworkEntity.Owner as Character;
            character.Inventory.SendUpdate();
            character.Stats.SendMainStats();
            character.Stats.SendSecondaryStats();
            character.Stats.SendCraftStats();
            character.Spells.SendFreePoints();
            character.Spells.SendSpellData();
            character.Craft.SendClientRecipes();
            character.Craft.SendClientSessions();
        }

        private void CoreSendPlayerInput(PlayerInputState data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return;

            character.InputStates.Enqueue(data);
        }


        private bool CoreCreateCharacter(NetStruct<string, byte> data, NetConnection connection)
        {
            if (!World.WorldInstance)
                return false;

            return World.CreateCharacter(data.Value1, data.Value2, connection);
        }

        private bool InventoryMoveItem(NetStruct<short, short, short, short> data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return false;

            return character.Inventory.MoveItem(data.Value1, data.Value2, data.Value3, data.Value4);
        }

        private bool InventoryEquipItem(NetStruct<short, short, byte> data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return false;

            return character.Inventory.EquipItem(data.Value1, data.Value2, data.Value3);
        }

        private bool InventoryUnequipItem(NetStruct<byte, short, short> data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return false;

            return character.Inventory.UnequipItem(data.Value1, data.Value2, data.Value3);
        }

        public void CharacterRespawn(NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return;

            if (!character.Dead)
                return;

            character.Respawn(new Vector3(0, 5, 0));
        }

        private void SpellUse(NetStruct<SpellName, string> data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return;

            if (character.Spells.Level[data.Value1] == 0)
                return;

            character.Spells.SpellBehaviours[data.Value1].Use(data.Value2);
        }

        private void SpellTrigger(NetStruct<SpellName, int, string> data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return;

            if (character.Spells.Level[data.Value1] == 0)
                return;

            character.Spells.SpellBehaviours[data.Value1].Trigger(data.Value2, data.Value3);
        }

        private void SpellDirectionalCast(NetStruct<SpellName, ProtoVector3, ProtoVector3, string> data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return;

            if (character.Spells.Level[data.Value1] == 0)
                return;

            character.Spells.SpellBehaviours[data.Value1].DirectionalCast(data.Value2, data.Value3, data.Value4);
        }


        private bool SpellLearn(SpellName data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return false;

            return character.Spells.ClientLearnSpell(data);
        }

        private void CraftStart(NetStruct<CraftRecipeId, NetStruct<short, short, int>[]> data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return;

            character.Craft.Craft(data.Value1, data.Value2);
        }

        private bool MainStatUpgrade(MainStatType data, NetConnection sender)
        {
            Character character;
            if (!CheckConnection(sender, out character))
                return false;

            return character.LearnMainStat(data);
        }

        public static bool CheckConnection(NetConnection sender, out Character senderCharacter)
        {
            senderCharacter = null;

            if (!LidgrenRootServer.Instance || !LidgrenRootServer.Instance.Clients.ContainsKey(sender.RemoteUniqueIdentifier))
                return false;

            senderCharacter = LidgrenRootServer.Instance.Clients[sender.RemoteUniqueIdentifier].ControlledNetworkEntity.Owner as Character;
            if (!senderCharacter)
                return false;

            return true;
        }

        protected override void InitHandlers()
        {
            //only for client now
        }
    }
}
