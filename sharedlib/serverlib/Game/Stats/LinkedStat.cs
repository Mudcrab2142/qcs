﻿namespace ServerLib
{
    public abstract class LinkedStat : BaseStat
    {
        public LinkedStat(byte type, float baseValue, float maxValue) : base(type, baseValue, maxValue)
        {

        }

        public void Link(MainStat mainStat, float value)
        {
            AddModifier(this, new LinkedStatModifier(mainStat, value));
            mainStat.OnStatUpdate += n => UpdateStat();
            UpdateStat();
        }
    }
}
