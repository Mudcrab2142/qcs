﻿using SharedLib;

namespace ServerLib
{
    public class LinkedStatModifier : IStatModifier
    {
        MainStat _mainStat;
        float _modifierValue;

        public LinkedStatModifier(MainStat mainStat, float modifierValue)
        {
            _mainStat = mainStat;
            _modifierValue = modifierValue;
        }

        public void Modify(ref StatValue value)
        {
            value.Additional += _mainStat.Total * _modifierValue;
        }
    }
}
