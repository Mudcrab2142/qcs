﻿using SharedLib;
using System;
using System.Collections.Generic;

namespace ServerLib
{
    public abstract class BaseStat
    {
        public event Action<BaseStat> OnStatUpdate;
        private StatValue _value = new StatValue();
        internal StatValue Value => _value;

        public float Base => _value.Base;
        public float Additional => _value.Additional;
        public float Total => Base + Additional;

        public abstract Type StatType { get; }

        private readonly Dictionary<object, IStatModifier> _modifiers = new Dictionary<object, IStatModifier>();

        public BaseStat(byte type, float baseValue, float maxValue)
        {
            _value.Type = type;
            _value.Base = baseValue;
        }

        public void SetBaseValue(float value)
        {
            _value.Base = value;
            UpdateStat();
        }

        public void AddBaseValue(float value)
        {
            SetBaseValue(_value.Base + value);
        }

        internal void AddModifier(object initiator, IStatModifier modifier)
        {
            if (_modifiers.ContainsKey(initiator))
                return;

            _modifiers.Add(initiator, modifier);
            UpdateStat();
        }

        internal void RemoveModifier(object initiator)
        {
            if (!_modifiers.ContainsKey(initiator))
                return;

            _modifiers.Remove(initiator);
            UpdateStat();
        }

        protected void UpdateStat()
        {
            _value.Additional = 0;

            foreach (var item in _modifiers)
                item.Value.Modify(ref _value);

            if (OnStatUpdate != null)
                OnStatUpdate(this);
        }
    }
}
