﻿using SharedLib;
using System;

namespace ServerLib
{
    public class CraftStat : BaseStat
    {
        public override Type StatType => typeof(CraftStat);

        public CraftStat(CraftStatType type, float baseValue, float maxValue) : base((byte)type, baseValue, maxValue)
        {

        }
    }
}
