﻿using SharedLib;
using System.Collections.Generic;
using System;

namespace ServerLib
{
    public class ServerStatStorage : StatStorage
    {
        public readonly Dictionary<MainStatType, MainStat> Main = new Dictionary<MainStatType, MainStat>();
        public readonly Dictionary<SecondaryStatType, SecondaryStat> Secondary = new Dictionary<SecondaryStatType, SecondaryStat>();
        public readonly Dictionary<CraftStatType, CraftStat> Craft = new Dictionary<CraftStatType, CraftStat>();

        private readonly IStatsOwner _owner;
        private readonly IEquipmentOwner _eqOwner;
        private readonly ICombatCharacterEntity _combatCharacter;

        private readonly List<StatValue> _tempList = new List<StatValue>();

        public ServerStatStorage(IStatsOwner owner)
        {
            _owner = owner;
            _eqOwner = owner as IEquipmentOwner;
            _combatCharacter = owner as ICombatCharacterEntity;

            InitMainStat(MainStatType.Strength, 5, 100);
            InitMainStat(MainStatType.Constitution, 5, 100);
            InitMainStat(MainStatType.Dexterity, 5, 100);
            InitMainStat(MainStatType.Intelligence, 5, 100);
            InitMainStat(MainStatType.Luck, 5, 100);

            InitSecondaryStat(SecondaryStatType.MinDamage, 1, float.MaxValue);
            Secondary[SecondaryStatType.MinDamage].Link(Main[MainStatType.Strength], 1.5f);

            InitSecondaryStat(SecondaryStatType.MaxDamage, 1, float.MaxValue);
            Secondary[SecondaryStatType.MaxDamage].Link(Main[MainStatType.Strength], 2.5f);

            InitSecondaryStat(SecondaryStatType.Health, 100, float.MaxValue);
            Secondary[SecondaryStatType.Health].Link(Main[MainStatType.Constitution], 10f);

            InitSecondaryStat(SecondaryStatType.Stamina, 200, float.MaxValue);
            InitSecondaryStat(SecondaryStatType.StaminaRegeneration, 20, float.MaxValue);

            InitSecondaryStat(SecondaryStatType.Armor, 0, float.MaxValue);
            Secondary[SecondaryStatType.Armor].Link(Main[MainStatType.Strength], 2f);

            InitSecondaryStat(SecondaryStatType.AttackSpeed, 1, 3);
            Secondary[SecondaryStatType.AttackSpeed].Link(Main[MainStatType.Dexterity], 0.05f);

            InitCraftStat(CraftStatType.Blacksmithing, 0f, float.MaxValue);

            if(_eqOwner != null)
            {
                _eqOwner.Equipment.OnItemEquip += OnItemEquip;
                _eqOwner.Equipment.OnItemUnequip += OnItemUnequip;
            }
        }

        ~ServerStatStorage()
        {
            if (_eqOwner != null)
            {
                _eqOwner.Equipment.OnItemEquip -= OnItemEquip;
                _eqOwner.Equipment.OnItemUnequip -= OnItemUnequip;
            }
        }

        private void InitMainStat(MainStatType type, float baseValue, float maxValue)
        {
            if (Main.ContainsKey(type))
                return;

            var stat = new MainStat(type, baseValue, maxValue);
            Main.Add(type, stat);

            if (_owner is IStatsPlayerOwner)
                stat.OnStatUpdate += n => SendMainStats(); 
        }

        private void InitSecondaryStat(SecondaryStatType type, float baseValue, float maxValue)
        {
            if (Secondary.ContainsKey(type))
                return;

            var stat = new SecondaryStat(type, baseValue, maxValue);
            Secondary.Add(type, stat);

            if (_owner is IStatsPlayerOwner)
                stat.OnStatUpdate += n => SendSecondaryStats();
        }

        private void InitCraftStat(CraftStatType type, float baseValue, float maxValue)
        {
            if (Craft.ContainsKey(type))
                return;

            var stat = new CraftStat(type, baseValue, maxValue);
            Craft.Add(type, stat);

            if (_owner is IStatsPlayerOwner)
                stat.OnStatUpdate += n => SendCraftStats();
        }

        public void SendMainStats()
        {
            _tempList.Clear();
            foreach (var item in Main)
                _tempList.Add(item.Value.Value);
            NetworkBaseWorld.WorldInstance.RPC.S_MainStatsUpdate.Call(_tempList, ((IStatsPlayerOwner)_owner).Peer);
        }

        public void SendSecondaryStats()
        {
            _tempList.Clear();
            foreach (var item in Secondary)
                _tempList.Add(item.Value.Value);
            NetworkBaseWorld.WorldInstance.RPC.S_SecondaryStatsUpdate.Call(_tempList, ((IStatsPlayerOwner)_owner).Peer);
        }

        public void SendCraftStats()
        {
            _tempList.Clear();
            foreach (var item in Craft)
                _tempList.Add(item.Value.Value);
            NetworkBaseWorld.WorldInstance.RPC.S_CraftStatsUpdate.Call(_tempList, ((IStatsPlayerOwner)_owner).Peer);
        }

        private void OnItemEquip(ItemInstance item, byte slot)
        {
            //-----------------------MAIN-----------------------------
            if (item.HasAttribute(AttributeType.Strength))
                Main[MainStatType.Strength].AddModifier(item, new StatModifier(StatModifier.ModifyType.AddValue, (int)item[AttributeType.Strength]));

            if (item.HasAttribute(AttributeType.Constitution))
                Main[MainStatType.Constitution].AddModifier(item, new StatModifier(StatModifier.ModifyType.AddValue, (int)item[AttributeType.Constitution]));

            if (item.HasAttribute(AttributeType.Dexterity))
                Main[MainStatType.Dexterity].AddModifier(item, new StatModifier(StatModifier.ModifyType.AddValue, (int)item[AttributeType.Dexterity]));

            if (item.HasAttribute(AttributeType.Intelligence))
                Main[MainStatType.Intelligence].AddModifier(item, new StatModifier(StatModifier.ModifyType.AddValue, (int)item[AttributeType.Intelligence]));

            if (item.HasAttribute(AttributeType.Luck))
                Main[MainStatType.Luck].AddModifier(item, new StatModifier(StatModifier.ModifyType.AddValue, (int)item[AttributeType.Luck]));

            //--------------------SECONDARY---------------------------
            if (item.HasAttribute(AttributeType.Armor))
                Secondary[SecondaryStatType.Armor].AddModifier(item, new StatModifier(StatModifier.ModifyType.AddValue, (int)item[AttributeType.Armor]));

            if (item.HasAttribute(AttributeType.Damage))
            {
                MinMax<short> damage = item[AttributeType.Damage];
                Secondary[SecondaryStatType.MinDamage].AddModifier(item, new StatModifier(StatModifier.ModifyType.AddValue, damage.Min));
                Secondary[SecondaryStatType.MaxDamage].AddModifier(item, new StatModifier(StatModifier.ModifyType.AddValue, damage.Max));
            }
        }

        private void OnItemUnequip(ItemInstance item, byte slot)
        {
            foreach (var stat in Main)
                stat.Value.RemoveModifier(item);

            foreach (var stat in Secondary)
                stat.Value.RemoveModifier(item);
        }

        public override StatValue GetStat(MainStatType type)
        {
            return Main.ContainsKey(type) ? Main[type].Value : NullValue;
        }

        public override StatValue GetStat(SecondaryStatType type)
        {
            return Secondary.ContainsKey(type) ? Secondary[type].Value : NullValue;
        }

        public override StatValue GetStat(CraftStatType type)
        {
            return Craft.ContainsKey(type) ? Craft[type].Value : NullValue;
        }

        internal ICombatCreatureEntity GetOwner()
        {
            return _combatCharacter;
        }
    }
}
