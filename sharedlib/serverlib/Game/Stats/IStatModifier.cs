﻿using SharedLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerLib
{
    public interface IStatModifier
    {
        void Modify(ref StatValue value);
    }
}
