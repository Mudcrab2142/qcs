﻿using System;
using SharedLib;

namespace ServerLib
{
    public class SecondaryStat : LinkedStat
    {
        public override Type StatType => typeof(SecondaryStat);

        public SecondaryStat(SecondaryStatType type, float baseValue, float maxValue) : base((byte)type, baseValue, maxValue)
        {

        }
    }
}
