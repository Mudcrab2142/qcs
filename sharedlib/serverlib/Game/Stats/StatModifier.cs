﻿using SharedLib;

namespace ServerLib
{
    public class StatModifier : IStatModifier
    {
        public enum ModifyType
        {
            AddValue,
            AddBaseValuePercent
        }

        private readonly ModifyType _type;
        private readonly float _value;

        public StatModifier(ModifyType type, float value)
        {
            _type = type;
            _value = value;
        }

        public void Modify(ref StatValue value)
        {
            switch (_type)
            {
                case ModifyType.AddValue:
                    value.Additional += _value;
                    break;

                case ModifyType.AddBaseValuePercent:
                    value.Additional += _value * value.Base;
                    break;
            }
        }
    }
}
