﻿using System;
using SharedLib;

namespace ServerLib
{
    public class MainStat : BaseStat
    {
        public override Type StatType => typeof(MainStat);

        public MainStat(MainStatType type, float baseValue, float maxValue) : base((byte)type, baseValue, maxValue)
        {

        }
    }
}
