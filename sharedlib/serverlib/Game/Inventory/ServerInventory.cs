﻿using SharedLib;
using System.Linq;

namespace ServerLib
{
    public class ServerInventory : Inventory
    {
        private readonly IInventoryOwner _owner;

        public ServerInventory(IInventoryOwner owner, short xSize, short ySize)
        {
            Backpack = new InventoryData(xSize, ySize);
            mask = new bool[xSize, ySize];
            _owner = owner;
        }

        /// <summary>
        /// Add new item to inventory
        /// </summary>
        public bool AddItem(ItemInstance item)
        {
            short targetCellX, targetCellY;

            if (item.Stackable)
            {
                InventoryEntry entry;
                if (FindSameItem(item, out entry))
                {
                    entry.Item.Count += item.Count;
                    SendUpdate();
                    return true;
                }
            }

            if (!FindFreeCell(item, out targetCellX, out targetCellY))
                return false;

            AddItemInternal(item, targetCellX, targetCellY);

            SendUpdate();
            return true;
        }

        public InventoryEntry GetItemEntry(short cellX, short cellY)
        {
            var entry = Backpack.Storage.FirstOrDefault(x => x.PositionX == cellX && x.PositionY == cellY);
            if (entry == null)
                return null;
            return entry;
        }

        /// <summary>
        /// Remove item from inventory using position
        /// </summary>
        public bool RemoveItem(short cellX, short cellY, int count)
        {
            var entry = Backpack.Storage.FirstOrDefault(x => x.PositionX == cellX && x.PositionY == cellY);
            if (entry == null)
                return false;

            RemoveItemInternal(entry, count);

            SendUpdate();
            return true;
        }

        /// <summary>
        /// Move item to another cell
        /// </summary>
        public bool MoveItem(short cellX, short cellY, short targetCellX, short targetCellY)
        {
            var entry = Backpack.Storage.FirstOrDefault(x => x.PositionX == cellX && x.PositionY == cellY);
            if (entry == null)
                return false;

            UpdateMask(cellX, cellY, entry.Item.Width, entry.Item.Height, false);

            if (!FitItem(entry.Item, targetCellX, targetCellY))
            {
                UpdateMask(cellX, cellY, entry.Item.Width, entry.Item.Height, true);
                return false;
            }

            entry.PositionX = targetCellX;
            entry.PositionY = targetCellY;

            UpdateMask(targetCellX, targetCellY, entry.Item.Width, entry.Item.Height, true);

            SendUpdate();
            return true;
        }

        /// <summary>
        /// Stack items in inventory
        /// </summary>
        public bool StackItem(short cellX, short cellY, short targetX, short targetY)
        {
            var originEntry = Backpack.Storage.FirstOrDefault(x => x.PositionX == cellX && x.PositionY == cellY);
            if (originEntry == null)
                return false;

            var targetEntry = Backpack.Storage.FirstOrDefault(x => x.PositionX == targetX && x.PositionY == targetY);
            if (targetEntry == null)
                return false;

            if (!originEntry.Item.Stackable)
                return false;

            if (!originEntry.Item.CanStack(targetEntry.Item))
                return false;

            var maxStackCount = targetEntry.Item.BaseItem.MaxStackCount;

            targetEntry.Item.Count += originEntry.Item.Count;
            if (targetEntry.Item.Count <= maxStackCount)
            {
                RemoveItemInternal(originEntry, originEntry.Item.Count);
            }
            else
            {
                originEntry.Item.Count = targetEntry.Item.Count - maxStackCount;
                targetEntry.Item.Count = maxStackCount;
            }

            SendUpdate();
            return true;
        }

        public bool EquipItem(short cellX, short cellY, byte eqSlot)
        {
            var eqOwner = _owner as IEquipmentOwner;
            if (eqOwner == null)
                return false;

            var itemEntry = Backpack.Storage.FirstOrDefault(x => x.PositionX == cellX && x.PositionY == cellY);
            if (itemEntry == null)
                return false;

            var targetEquipmentSlot = eqOwner.Equipment[eqSlot];
            if (targetEquipmentSlot == null)
                return false;

            var itemEquipPosition = (EquipmentPosition)itemEntry.Item[AttributeType.EquipmentPosition];

            if (targetEquipmentSlot.Position != itemEquipPosition)
                return false;

            if (targetEquipmentSlot.Item == null)
            {
                RemoveItemInternal(itemEntry, itemEntry.Item.Count);
                eqOwner.Equipment.ChangeEquip(eqSlot, itemEntry.Item);
            }
            else //if(!targetEquipmentSlot.Item.HasAttribute(AttributeType.Cursed))
            {
                short freeX, freeY;
                UpdateMask(itemEntry.PositionX, itemEntry.PositionY, itemEntry.Item.Width, itemEntry.Item.Height, false);
                if (FindFreeCell(targetEquipmentSlot.Item, out freeX, out freeY))
                {
                    RemoveItemInternal(itemEntry, itemEntry.Item.Count);
                    AddItemInternal(targetEquipmentSlot.Item, freeX, freeY);
                    eqOwner.Equipment.ChangeEquip(eqSlot, itemEntry.Item);
                }
                else
                {
                    UpdateMask(itemEntry.PositionX, itemEntry.PositionY, itemEntry.Item.Width, itemEntry.Item.Height, true);
                    return false;
                }
            }

            SendUpdate();
            return true;
        }

        public bool UnequipItem(byte eqSlot, short toX, short toY)
        {
            var eqOwner = _owner as IEquipmentOwner;
            if (eqOwner == null)
                return false;

            var targetEquipmentSlot = eqOwner.Equipment[eqSlot];
            if (targetEquipmentSlot == null)
                return false;

            if (targetEquipmentSlot.Item == null)
                return false;

            if (FitItem(targetEquipmentSlot.Item, toX, toY))
            {
                AddItemInternal(targetEquipmentSlot.Item, toX, toY);
                eqOwner.Equipment.ChangeEquip(eqSlot, null);
                SendUpdate();
                return true;
            }

            return false;
        }

        public void SendUpdate()
        {
            var owner = _owner as IInventoryPlayerOwner;
            if (owner != null)
                World.Instance.RPC.S_InventoryUpdate.Call(Backpack, owner.Peer);
        }

        #region Private
        private void AddItemInternal(ItemInstance item, short targetCellX, short targetCellY)
        {
            var entry = new InventoryEntry(item, targetCellX, targetCellY);
            Backpack.Storage.Add(entry);
            UpdateMask(targetCellX, targetCellY, item.Width, item.Height, true);
        }

        private void RemoveItemInternal(InventoryEntry entry, int count)
        {
            if (entry.Item.Count > count)
            {
                entry.Item.Count -= count;
            }
            else
            {
                Backpack.Storage.Remove(entry);
                UpdateMask(entry.PositionX, entry.PositionY, entry.Item.Width, entry.Item.Height, false);
            }
        }

        private bool FindSameItem(ItemInstance item, out InventoryEntry resultItem)
        {
            resultItem = Backpack.Storage.FirstOrDefault(entry => entry.Item.CanStack(item));
            return resultItem != null;
        }
        #endregion
    }
}
