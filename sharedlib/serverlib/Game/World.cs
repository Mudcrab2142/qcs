﻿using SharedLib;
using System.Collections.Generic;
using UnityEngine;
using System;
using Lidgren.Network;
using UnityEngine.SceneManagement;

namespace ServerLib
{
    public class World : NetworkServerWorld
    {
        public static World ServerWorldInstance => (World)Instance;

        public static bool CreateCharacter(string data, byte team, NetConnection connection)
        {
            var client = LidgrenRootServer.Instance.FindClient(connection);

            if (client.ControlledNetworkEntity != null)
                return false;

            Character character;
            if(ServerWorldEntity.Spawn<Character>(EntityType.Character, out character, LidgrenRootServer.Instance.FindClient(connection))) 
                character.transform.position = new Vector3(0, 5, 0);

            client.ControlledNetworkEntity = character.NetworkEntity;
            return true;
        }

        private WorldSpawn _worldSpawn;
        public WorldSpawn WorldSpawn => _worldSpawn;

        private Dictionary<long, Character> _characters = new Dictionary<long, Character>();

        protected override NetFuncStorage CreateNetFuncStorage()
        {
            return new ServerNetFuncStorage(NetworkRoot);
        }

        protected override void WorldStart()
        {
            if (!ServerWorldEntity.Spawn<WorldSpawn>(EntityType.WorldSpawn, out _worldSpawn))
                throw new Exception("Error starting world.");
                
            ServerWorldEntity entity;

           // for (var i = 0; i < 200; i++)
           // {
                if (ServerWorldEntity.Spawn(EntityType.DummyCreature, out entity))
                    entity.transform.position = new Vector3(5, 1, 0);
          //  }

   /*         for (var i = 0; i < 200; i++)
            {
                if (ServerWorldEntity.Spawn(EntityType.DummyCreature, out entity))
                    entity.transform.position = new Vector3(10, 1, 0);
            }*/

            if (ServerWorldEntity.Spawn(EntityType.WorldItem, out entity, new ItemInstance(ItemId.Apple, ItemQuality.Artifact, 10)))
                entity.transform.position = new Vector3(6, 1, 2);

            ServerWorldEntity.Spawn(EntityType.Airship, out entity, new Vector3(-1f, -0.28f, -15.82f), new Vector3(0f, 90f, 0f));
            ServerWorldEntity.Spawn(EntityType.Airship, out entity, new Vector3(-8.54f, -0.28f, 23.28f), Vector3.zero);
        }

        protected override void OnClientConnected(UserClient client)
        {
            
        }

        public void OnClientInfoReceived(NetStruct<string, int> data, NetConnection clientConnection)
        {
            var id = data.Value1;
            var version = data.Value2;
            var client = LidgrenRootServer.Instance.FindClient(clientConnection);

            if (client == null)
                return;

            RPC.S_CoreSendCharactersList.Call(new NetStruct<string, int>[0], clientConnection);
        }

        protected override void OnClientDisconnected(UserClient client)
        {
            if (!_characters.ContainsKey(client.Peer.RemoteUniqueIdentifier))
                return;

            _characters[client.Peer.RemoteUniqueIdentifier].NetworkDestroy();
          //  ServerWorldEntity.Remove(_characters[client.Peer.RemoteUniqueIdentifier]);
            _characters.Remove(client.Peer.RemoteUniqueIdentifier);
        }

        protected override void WorldUpdate()
        {

        }

        protected override void InitWorldEntityFactory()
        {
            AddFactoryMethod(EntityType.Dummy, Dummy.Create);
            AddFactoryMethod(EntityType.WorldSpawn, WorldSpawn.Create);
            AddFactoryMethod(EntityType.Character, Character.Create);
            AddFactoryMethod(EntityType.DummyCreature, DummyCreature.Create);
            AddFactoryMethod(EntityType.WorldItem, WorldItem.Create);
            AddFactoryMethod(EntityType.Airship, Airship.Create);
            AddFactoryMethod(EntityType.AirshipBalloon, AirshipBalloon.Create);
            AddFactoryMethod(EntityType.MagicMissile, MagicMissile.Create);
        }

        protected override AsyncOperation LoadWorld(string worldName)
        {
            return SceneManager.LoadSceneAsync(worldName, LoadSceneMode.Additive);
        }
    }
}
