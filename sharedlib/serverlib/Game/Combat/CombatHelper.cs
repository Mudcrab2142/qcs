﻿using SharedLib;
using UnityEngine;

namespace ServerLib
{
    public static class CombatHelper
    {
        /// <summary>
        /// Final damage method, removes health, sends notification
        /// </summary>
        public static void Damage(ICombatBaseEntity sender, ICombatBaseEntity target, Damage dmg)
        {
            CalculateResistance(ref dmg, target);

            if (sender is ICombatCharacterEntity)
                NetworkServerWorld.Instance.RPC.S_NotifyDamageDeal.Call(new NetStruct<int, Damage>(target.NetworkEntity.EntityId, dmg), ((ICombatCharacterEntity)sender).Peer);

            if (target is ICombatCharacterEntity)
                NetworkServerWorld.Instance.RPC.S_NotifyDamageReceive.Call(new NetStruct<int, Damage>(sender.NetworkEntity.EntityId, dmg), ((ICombatCharacterEntity)target).Peer);

            if (target is Creature)
            {
                ((Creature)target).TakeDamage(sender as WorldEntity, dmg);
            }
        }

        static void CalculateResistance(ref Damage dmg, ICombatBaseEntity target)
        {
            var targetStats = target is IStatsOwner ? ((IStatsOwner)target).Stats : null;
            if (targetStats != null) {
                switch (dmg.Type)
                {
                    case DamageType.Physical:
                        dmg.Amount = (short)(CombatShared.PhysicalDamageResistance(targetStats.Armor.Value) * dmg.Amount);
                        break;
                }
            }
        }
    }
}
