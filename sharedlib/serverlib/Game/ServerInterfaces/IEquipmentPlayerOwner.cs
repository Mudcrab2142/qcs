﻿using Lidgren.Network;

namespace ServerLib
{
    public interface IEquipmentPlayerOwner : IEquipmentOwner
    {
        NetConnection Peer { get; }
    }
}
