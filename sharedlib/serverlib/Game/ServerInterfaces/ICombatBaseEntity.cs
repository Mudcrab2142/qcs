﻿using SharedLib;

namespace ServerLib
{
    public interface ICombatBaseEntity
    {   
        NetworkBaseEntity NetworkEntity { get; }
        ServerEffectStorage Effects { get; }
    }
}
