﻿using Lidgren.Network;

namespace ServerLib
{
    public interface IInventoryPlayerOwner : IInventoryOwner
    {
        NetConnection Peer { get; }
    }
}
