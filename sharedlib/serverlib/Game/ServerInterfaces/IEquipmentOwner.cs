﻿namespace ServerLib
{
    public interface IEquipmentOwner
    {
        ServerEquipment Equipment { get; }
    }
}
