﻿using Lidgren.Network;
using SharedLib;
using System;

namespace ServerLib
{
    public interface ICombatCharacterEntity : ICombatCreatureEntity, IStatsOwner
    {
        event Action OnLevelUp;
        NetConnection Peer { get; }
        ServerSpellStorage Spells { get; }
        ServerEquipment Equipment { get; }
        NetworkCharacter NetworkCharacter { get; }
    }
}
