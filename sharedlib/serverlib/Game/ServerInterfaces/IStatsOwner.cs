﻿using SharedLib;

namespace ServerLib
{
    public interface IStatsOwner
    {
        ServerStatStorage Stats { get; }
    }
}
