﻿using Lidgren.Network;

namespace ServerLib
{
    public interface IStatsPlayerOwner : IStatsOwner
    {
        NetConnection Peer { get; }
    }
}
