﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerLib
{
    public interface IInventoryOwner
    {
        ServerInventory Inventory { get; }
    }
}
