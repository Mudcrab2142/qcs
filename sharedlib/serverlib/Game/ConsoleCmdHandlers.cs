﻿using Lidgren.Network;
using SharedLib;
using System;

namespace ServerLib
{
    public static class ConsoleCmdHandlers
    {
        public static void Init()
        {
            ConVar.CMD_AddItem.SetHandler(AddItem);
            ConVar.CMD_Kill.SetHandler(Kill);
        }

        static bool GetCharacter(NetConnection connection, out Character character)
        {
            character = null;
            if (!LidgrenRootServer.Instance)
                return false;

            var client = LidgrenRootServer.Instance.FindClient(connection);
            if (client == null)
                return false;

            character = client.ControlledNetworkEntity.Owner as Character;
            return character != null;
        }

        static void AddItem(string args, NetConnection connection)
        {
            Character target;
            short id;
            if (!GetCharacter(connection, out target) || !short.TryParse(args, out id) || !Enum.IsDefined(typeof(ItemId), id))
                return;

            var item = new ItemInstance((ItemId)id, ItemQuality.Common);
            target.Inventory.AddItem(item);
        }

        static void Kill(string args, NetConnection connection)
        {
            Character target;
            if (!GetCharacter(connection, out target))
                return;

            if (target.Dead)
                return;

            CombatHelper.Damage(target, target, new Damage((short)target.MaxHealth, DamageType.Pure));
        }
    }
}
