﻿using SharedLib;
using System.Collections.Generic;

namespace ServerLib
{
    public class ServerEffectStorage : EffectStorage
    {
        public readonly ICombatBaseEntity Owner;

        private readonly List<EffectBehaviour> _effects = new List<EffectBehaviour>();
        private readonly List<EffectBehaviour> _removeList = new List<EffectBehaviour>();
        
        public ServerEffectStorage(ICombatBaseEntity owner)
        {
            Owner = owner;
            ((ServerWorldEntity)Owner).UpdateAction += Update;
        }

        ~ServerEffectStorage()
        {
            ((ServerWorldEntity)Owner).UpdateAction -= Update;
        }

        void Update()
        {
            foreach (var effect in _removeList)
                _effects.Remove(effect);

            foreach (var effect in _effects)
                effect.Update();
        }

        public void ApplyEffect(EffectBehaviour effect)
        {
            _effects.Add(effect);
            effect.Start();
            if (Owner is ICombatCharacterEntity)
                NetworkBaseWorld.WorldInstance.RPC.S_EffectAdd.Call(effect.EffectInfo, ((ICombatCharacterEntity)Owner).Peer);
        }

        public bool FinishEffect(EffectBehaviour effect, EffectBehaviour.FinishType finishType)
        {
            if (_effects.Contains(effect))
            {
                effect.Finish(finishType);
                _removeList.Add(effect);
                if (Owner is ICombatCharacterEntity)
                    NetworkBaseWorld.WorldInstance.RPC.S_EffectRemove.Call(_effects.IndexOf(effect), ((ICombatCharacterEntity)Owner).Peer);

                return true;
            }
            return false;
        }
    }
}
