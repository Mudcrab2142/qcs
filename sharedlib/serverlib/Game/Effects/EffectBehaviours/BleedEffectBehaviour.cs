﻿using System;
using SharedLib;
using UnityEngine;

namespace ServerLib
{
    public class BleedEffectBehaviour : EffectBehaviour
    {
        public BleedEffectInfo BleedInfo => (BleedEffectInfo)EffectInfo;
        public override EffectName Name => EffectName.Bleed;

        private float _lastDmgTime;
        private float _nextDmgTime => _lastDmgTime + BleedInfo.DamagePeriod;

        public readonly WorldEntity Sender;

        internal BleedEffectBehaviour(EffectInfo info, ICombatBaseEntity target, WorldEntity sender) : base(info, target)
        {
            Sender = sender;
        }

        protected override void EffectStart()
        {
            _lastDmgTime = EffectStartTime;
        }

        protected override void EffectTick()
        {
            if(Time.time >= _nextDmgTime)
            {
                CombatHelper.Damage(Sender as ICombatBaseEntity, Owner, new Damage(BleedInfo.Damage, BleedInfo.DamageType));
                _lastDmgTime = Time.time;
            }
        }

        protected override void EffectFinish(FinishType finishType)
        {
            
        }
    }
}
