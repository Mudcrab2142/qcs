﻿using SharedLib;
using UnityEngine;

namespace ServerLib
{
    public abstract class EffectBehaviour
    {
        public enum FinishType
        {
            Timeout,
            Dispel
        }

        public abstract EffectName Name { get; }

        protected abstract void EffectStart();
        protected abstract void EffectTick();
        protected abstract void EffectFinish(FinishType finishType);

        public readonly EffectInfo EffectInfo;

        public float EffectStartTime { get; private set; }
        public float Duration => EffectInfo.Duration;
        public bool UnlimitedDuration => Duration < 0f;
        public readonly ICombatBaseEntity Owner;

        public EffectBehaviour(EffectInfo info, ICombatBaseEntity owner)
        {
            EffectInfo = info;
            Owner = owner;
        }

        public void Start()
        {
            EffectStart();
            EffectStartTime = Time.time;
        }
        
        public void Update()
        {
            if(!UnlimitedDuration && EffectStartTime + Duration <= Time.time)
            {
                Owner.Effects.FinishEffect(this, FinishType.Timeout);
                return;
            }

            EffectTick();
        }

        public void Finish(FinishType finishType)
        {
            EffectFinish(finishType);
        }
    }
}
