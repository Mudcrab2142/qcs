﻿using SharedLib;
using System;

namespace ServerLib
{
    public sealed class ServerEquipment : Equipment
    {
        public event Action<ItemInstance, byte> OnItemEquip;
        public event Action<ItemInstance, byte> OnItemUnequip;
        public event Action<ItemInstance, byte> OnEquipmentChange;
        IEquipmentOwner _owner;
        
        public ServerEquipment(IEquipmentOwner owner) : base()
        {
            _owner = owner;

            Weapon.OnEquipmentChange += SendEquipment;
            Helmet.OnEquipmentChange += SendEquipment;
            Cuirass.OnEquipmentChange += SendEquipment;
            Leggings.OnEquipmentChange += SendEquipment;
            Boots.OnEquipmentChange += SendEquipment;
            Gloves.OnEquipmentChange += SendEquipment;
        }

        ~ServerEquipment()
        {
            Weapon.OnEquipmentChange -= SendEquipment;
            Helmet.OnEquipmentChange -= SendEquipment;
            Cuirass.OnEquipmentChange -= SendEquipment;
            Leggings.OnEquipmentChange -= SendEquipment;
            Boots.OnEquipmentChange -= SendEquipment;
            Gloves.OnEquipmentChange -= SendEquipment;
        }

        void SendEquipment(EquipmentSlot slot)
        {
            if (NetworkBaseWorld.WorldInstance == null)
                return;

            var plOwner = _owner as IInventoryPlayerOwner;
            if (plOwner != null)
                NetworkBaseWorld.WorldInstance.RPC.S_EquipmentUpdateSlot.Call(new NetStruct<byte, ItemInstance>(slot.SlotId, slot.Item), plOwner.Peer);
        }

        public void ChangeEquip(byte slot, ItemInstance item)
        {
            var targetSlot = this[slot];
            if (targetSlot == null)
                return;

            if (targetSlot.Item != null)
                CommonHelper.SafeCall(OnItemUnequip, targetSlot.Item, slot);

            targetSlot.ChangeItem(item);

            if (item != null)
                CommonHelper.SafeCall(OnItemEquip, item, slot);

            CommonHelper.SafeCall(OnEquipmentChange, item, slot);
        }

        public bool CanEquip(byte slot, ItemInstance item)
        {
            var targetSlot = this[slot];
            if (targetSlot == null)
                return false;

            if (targetSlot.Position != item[AttributeType.EquipmentPosition])
                return false;

            return true;
        }

        public bool CanUnequip(byte slot)
        {
            var targetSlot = this[slot];
            if (targetSlot == null)
                return false;

            if (targetSlot.Item == null)
                return false;

            if (targetSlot.Item[AttributeType.Cursed])
                return false;

            return true;
        }
    }
}
