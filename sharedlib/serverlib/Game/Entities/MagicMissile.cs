﻿using System;
using SharedLib;
using UnityEngine;
using System.Collections.Generic;

namespace ServerLib
{
    public class MagicMissile : BaseEntity
    {
        static readonly Dictionary<NetworkMagicMissile.MissileType, BaseMagicMissileBehaviour> _behaviours = new Dictionary<NetworkMagicMissile.MissileType, BaseMagicMissileBehaviour>()
        {
            { NetworkMagicMissile.MissileType.DarkMatter, new DarkMatterMagicMissileBehaviour() }
        };

        public override EntityType EntityType => EntityType.MagicMissile;
        public override Type NetworkType => typeof(NetworkMagicMissile);
        public NetworkMagicMissile NetworkMagicMissile => (NetworkMagicMissile)NetworkEntity;

        public static MagicMissile Create(params object[] args)
        {
            var obj = new GameObject("MagicMissile");
            var component = obj.AddComponent<MagicMissile>();
            component.Type = (NetworkMagicMissile.MissileType)args[0];
            obj.transform.position = component.StartPosition = (Vector3)args[1];
            obj.transform.forward = component.StartDirection = (Vector3)args[2];
            component.Sender = (Character)args[3];
            component.SpellInfo = (SpellInfo)args[4];
            component._behaviour = _behaviours[component.Type];
            return component;
        }

        private BaseMagicMissileBehaviour _behaviour;

        internal Character Sender { get; private set; }
        internal Vector3 StartPosition { get; private set; }
        internal Vector3 StartDirection { get; private set; }
        internal float StartSpeed { get; private set; }
        internal Rigidbody Rigidbody { get; private set; }
        internal float LifeTime { get; private set; }

        public NetworkMagicMissile.MissileType Type { get; private set; }
        public SpellInfo SpellInfo { get; private set; }

        protected override void EntityStart()
        {
            StartSpeed = _behaviour.GetStartSpeed(this);
            LifeTime = _behaviour.GetLifeTime(this);

            Rigidbody = gameObject.AddComponent<Rigidbody>();
            Rigidbody.isKinematic = true;
            NetworkMagicMissile.Type.SetValue((short)Type);

            var collider = gameObject.AddComponent<SphereCollider>();
            collider.isTrigger = true;
            collider.radius = 0.1f;
        }

        protected override void EntityFixedUpdate()
        {
            base.EntityFixedUpdate();
            _behaviour.Move(this, Time.fixedDeltaTime);
        }

        protected override void EntityUpdate()
        {
            base.EntityUpdate();
            LifeTime -= Time.deltaTime;
            if (LifeTime < 0f)
            {
                if (_behaviour.ExplodeOnDeath)
                    Explode();
                else
                    NetworkDestroy(true);
            }
        }

        void OnTriggerEnter(Collider col)
        {
            if (col == Sender.Collider)
                return;

            Explode();
            var rigidbody = col.attachedRigidbody;
            if (rigidbody != null && !rigidbody.isKinematic)
                rigidbody.AddForceAtPosition(transform.forward * 200f, transform.position, ForceMode.Impulse);
        }

        void Explode()
        {
            foreach (var creature in PhysicsHelper.FindCreaturesInRadius(transform.position, _behaviour.GetDamageRadius(this)))
                CombatHelper.Damage(Sender, creature, _behaviour.GetDamage(this));

            World.Instance.RPC.S_EntityMagicMissileExplode.AreaCall(new NetStruct<int, ProtoVector3>(NetworkEntity.EntityId, transform.position), transform.position, 200f);
            NetworkDestroy(true);
        }
    }
}
