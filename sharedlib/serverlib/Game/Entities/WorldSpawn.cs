﻿using SharedLib;
using System;
using UnityEngine;

namespace ServerLib
{
    public class WorldSpawn : ServerWorldEntity
    {
        public override EntityType EntityType => EntityType.WorldSpawn;
        public override Type NetworkType => typeof(NetworkWorldSpawn);
        public NetworkWorldSpawn NetworkWorldSpawn => (NetworkWorldSpawn)NetworkEntity;

        public static WorldSpawn Create(params object[] args)
        {
            var gameObject = new GameObject("WorldSpawn");
            var component = gameObject.AddComponent<WorldSpawn>();
            return component;
        }

        private short _time;

        protected override void EntityThink(float dt)
        {
            base.EntityThink(dt);

            SetThinkDelay(1f);

            _time++;
            if (_time > 1440)
                _time = 0;

            NetworkWorldSpawn.Time.SetValue(_time);
        }
    }
}
