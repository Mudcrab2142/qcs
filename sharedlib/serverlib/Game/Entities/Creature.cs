﻿using SharedLib;
using System;
using UnityEngine;

namespace ServerLib
{
    public abstract class Creature : BaseEntity, IInventoryOwner, IStatsOwner, ICombatBaseEntity
    {
        public override Type NetworkType => typeof(NetworkCreature);
        public NetworkCreature NetworkCreature => (NetworkCreature)NetworkEntity;

        public ServerInventory Inventory { get; protected set; }
        public ServerStatStorage Stats { get; protected set; }
        public ServerEffectStorage Effects { get; protected set; }

        public WorldEntity LastDamageSender { get; private set; }
                
        public ushort MaxHealth => 200;
        public ushort CurrentHealth { get; set; }
        public bool Dead { get; private set; }
        public bool PreDeathState { get; private set; }

        protected override void EntityStart()
        {
            base.EntityStart();
            Inventory = new ServerInventory(this, 8, 6);
            Stats = new ServerStatStorage(this);
            Effects = new ServerEffectStorage(this);
            CurrentHealth = MaxHealth;
        }

        protected override void EntityUpdate()
        {
            base.EntityUpdate();
            if (CurrentHealth == 0 && !PreDeathState && !Dead)
            {
                PreDeathState = true;
                PreDeath();
            }
        }

        /// <summary>
        /// Called when creature receives damage
        /// </summary>
        public virtual void TakeDamage(WorldEntity sender, Damage dmg)
        {
            CurrentHealth -= (ushort)Mathf.Clamp(dmg.Amount, 0, CurrentHealth);
            LastDamageSender = sender;
        }

        /// <summary>
        /// Creature died
        /// </summary>
        protected virtual void Death()
        {

        }

        /// <summary>
        /// Called when creature received lethal damage
        /// </summary>
        protected virtual void PreDeath()
        {
            Die();
        }

        /// <summary>
        /// Kill creature
        /// </summary>
        public void Die()
        {
            Dead = true;
            PreDeathState = false;
            NetworkCreature.CreatureFlags.SetValue((byte)(NetworkCreature.CreatureFlags.GetValue() | NetworkCreature.FLAG_DEAD));
            Death();
        }

        public void Revive()
        {
            NetworkCreature.CreatureFlags.SetValue((byte)(NetworkCreature.CreatureFlags.GetValue() & ~NetworkCreature.FLAG_DEAD));
            CurrentHealth = MaxHealth;
            Dead = false;
        }

        public override void UpdateNetworkData()
        {
            NetworkEntity.Position.SetValue(transform.position);
            if (NetworkEntity.UseSimpleRotation)
                NetworkEntity.SimpleRotation.SetValue(transform.eulerAngles.y);
            else
                NetworkEntity.FullRotation.SetValue(transform.eulerAngles);
            NetworkCreature.CurrentHealth.SetValue(CurrentHealth);
            NetworkCreature.MaxHealth.SetValue(MaxHealth);
        }
    }
}
