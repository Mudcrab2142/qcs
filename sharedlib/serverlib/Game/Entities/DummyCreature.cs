﻿using SharedLib;
using UnityEngine;

namespace ServerLib
{
    public class DummyCreature : Creature
    {
        public override EntityType EntityType => EntityType.DummyCreature;

        public static DummyCreature Create(params object[] data)
        {
            var gameObject = new GameObject("DummyCreature");
            var component = gameObject.AddComponent<DummyCreature>();
            return component;
        }

        Vector3 _originalPos;

        protected override void EntityStart()
        {
            base.EntityStart();
            var collider = gameObject.AddComponent<SphereCollider>();
            collider.radius = 0.4f;
            _originalPos = transform.position;
        }

        public override void TakeDamage(WorldEntity sender, Damage dmg)
        {
            base.TakeDamage(sender, dmg);
            if (sender is Character)
            {
                ((Character)sender).AddExp(50);
                ((Character)sender).Inventory.AddItem(new ItemInstance(ItemId.IronOre, ItemQuality.Common));
            }
        }
    }
}
