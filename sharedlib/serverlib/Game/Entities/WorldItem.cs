﻿using System;
using SharedLib;
using UnityEngine;
using Lidgren.Network;

namespace ServerLib
{
    public class WorldItem : BaseEntity
    {
        public override EntityType EntityType => EntityType.WorldItem;

        public override Type NetworkType => typeof(NetworkWorldItem);

        public NetworkWorldItem NetworkWorldItem => (NetworkWorldItem)NetworkEntity;

        public static WorldItem Create(params object[] data)
        {
            var itemInstance = data[0] as ItemInstance;
            if(itemInstance == null)
            {
                Debug.LogError("[WorldItem] Input data type is not ItemInstance!");
                return null;
            }

            var gameObject = new GameObject("WorldItem");
            var component = gameObject.AddComponent<WorldItem>();
            component.Item = itemInstance;
            return component;
        }

        protected override void EntityStart()
        {
            base.EntityStart();
            NetworkWorldItem.Id.SetValue(Item.Id);
        }

        #region RPCHandlers
        public static void Pick(int entityId, NetConnection connection)
        {

        }
        #endregion

        public ItemInstance Item { get; private set; }

    }
}
