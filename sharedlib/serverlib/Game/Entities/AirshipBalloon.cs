﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedLib;
using UnityEngine;

namespace ServerLib
{
    public class AirshipBalloon : BaseEntity
    {
        public override EntityType EntityType => EntityType.AirshipBalloon;
        public override Type NetworkType => typeof(NetworkBaseEntity);

        public static AirshipBalloon Create(params object[] args)
        {
            var obj = Instantiate(Resources.Load("airship_balloon")) as GameObject;
            var component = obj.AddComponent<AirshipBalloon>();
            obj.transform.position = (Vector3)args[0];
            obj.transform.eulerAngles = (Vector3)args[1];
            return component;
        }
    }
}
