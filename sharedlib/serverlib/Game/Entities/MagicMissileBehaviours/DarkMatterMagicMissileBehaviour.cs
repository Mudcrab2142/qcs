﻿using System;
using SharedLib;
using Random = UnityEngine.Random;

namespace ServerLib
{
    public class DarkMatterMagicMissileBehaviour : BaseMagicMissileBehaviour
    {
        public override Damage GetDamage(MagicMissile missile)
        {
            var spellInfo = (DarkMatterInfo)missile.Sender.Spells.SpellInfo[SpellName.DarkMatter];
            var dmg = spellInfo.GetDamage(spellInfo.SpellLevel);
            return new Damage((short)Random.Range(dmg.Min, dmg.Max), spellInfo.DamageType);
        }

        public override float GetDamageRadius(MagicMissile missile)
        {
            return ((DarkMatterInfo)missile.SpellInfo).DamageRadius;
        }

        public override void Move(MagicMissile missile, float dt)
        {
            missile.Rigidbody.position += missile.transform.forward * missile.StartSpeed * dt;
        }

        public override float GetStartSpeed(MagicMissile missile)
        {
            return ((DarkMatterInfo)missile.SpellInfo).MissileSpeed;
        }

        public override float GetLifeTime(MagicMissile missile)
        {
            return 5f;
        }
    }
}
