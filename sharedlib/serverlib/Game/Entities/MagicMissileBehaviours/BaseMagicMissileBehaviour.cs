﻿using SharedLib;

namespace ServerLib
{
    public abstract class BaseMagicMissileBehaviour
    {
        public virtual bool ExplodeOnDeath => false;

        public abstract void Move(MagicMissile missile, float dt);
        public abstract float GetDamageRadius(MagicMissile missile);
        public abstract Damage GetDamage(MagicMissile missile);
        public abstract float GetStartSpeed(MagicMissile missile);
        public abstract float GetLifeTime(MagicMissile missile);
    }
}
