﻿using SharedLib;
using System;
using UnityEngine;

namespace ServerLib
{
    public class Dummy : BaseEntity
    {
        public override EntityType EntityType => EntityType.DummyCreature;
        public override Type NetworkType => typeof(NetworkBaseEntity);

        public static Dummy Create(params object[] data)
        {
            var gameObject = new GameObject("Dummy");
            var component = gameObject.AddComponent<Dummy>();
            return component;
        }
    }
}
