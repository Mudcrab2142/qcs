﻿using Lidgren.Network;
using SharedLib;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ServerLib
{
    public class Character : HumanoidCreature, IInventoryPlayerOwner, IStatsPlayerOwner, IEquipmentPlayerOwner, ICombatCharacterEntity, IPlayerControllable
    {
        public event Action OnLevelUp;

        public NetworkCharacter NetworkCharacter { get { return (NetworkCharacter)NetworkEntity; } }
        public override EntityType EntityType => EntityType.Character;
        public override Type NetworkType => typeof(NetworkCharacter);

        public static Character Create(params object[] client)
        {
            var gameObject = Instantiate(Resources.Load("Player")) as GameObject;
            var component = gameObject.AddComponent<Character>();
            component.OwnerNetworkId = ((UserClient)client[0]).ConnectionId;
            component.Peer = ((UserClient)client[0]).Peer;

            component.Collider = gameObject.GetComponent<CapsuleCollider>();
           // component.Collider.isTrigger = true;
            return component;
        }

        public readonly Queue<PlayerInputState> InputStates = new Queue<PlayerInputState>();
        private PlayerMoveState _moveState = new PlayerMoveState();

        public long OwnerNetworkId { get; private set; }
        public NetConnection Peer { get; private set; }
        public ServerSpellStorage Spells { get; private set; }
        public ServerCraftStorage Craft { get; private set; }

        public Animator Animator { get; private set; }
        public AnimatorEventsHandler AnimatorEventsHandler { get; private set; }
        public CapsuleCollider Collider { get; private set; }

        private int _statStr = 5;
        private int _statDex = 5;
        private int _statCon = 5;
        private int _statInt = 5;
        private int _statLck = 5;

        private int _freeStatPoints = 10;
        private int _currentExp;
        private byte _currentLevel;


        private Vector3 _targetPosition;
        private Vector3 _clientSpeed;
        private float _lerpSpeed;
        private bool _lerp;

        public float MaxStamina => Stats.Stamina.Value;
        public float CurrentStamina
        {
            get
            {
                return NetworkCharacter.CurrentStamina.GetValue();
            }
            private set
            {
                NetworkCharacter.CurrentStamina.SetValue(value);
            }
        }

        public Vector3 ViewDirection { get; private set; }

        protected override void EntityStart()
        {
            base.EntityStart();

            _moveState.SpellCastSpeed = 1f;

            Animator = gameObject.GetComponent<Animator>();
            AnimatorEventsHandler = gameObject.AddComponent<AnimatorEventsHandler>();
            AnimatorEventsHandler.SetPlayer(this);

            Spells = new ServerSpellStorage(Stats, this);
            Craft = new ServerCraftStorage(Inventory, Stats, this);

            //apply loaded stats
            Stats.Main[MainStatType.Strength].SetBaseValue(_statStr);
            Stats.Main[MainStatType.Dexterity].SetBaseValue(_statDex);
            Stats.Main[MainStatType.Constitution].SetBaseValue(_statCon);
            Stats.Main[MainStatType.Intelligence].SetBaseValue(_statInt);
            Stats.Main[MainStatType.Luck].SetBaseValue(_statLck);

            World.Instance.RPC.S_FreeStatPointsUpdate.Call(_freeStatPoints, Peer);
            World.Instance.RPC.S_ExperienceUpdate.Call(_currentExp, Peer);

            _currentLevel = GameConstants.Experience.GetLevel(_currentExp);

            //test
            //    Inventory.AddItem(new ItemInstance(ItemId.Sword, ItemQuality.Common));
            Inventory.AddItem(new ItemInstance(ItemId.Sword, ItemQuality.Common, ItemAttribute.Name("Hueviy mech"), ItemAttribute.Dexterity(-4)));
            Inventory.AddItem(new ItemInstance(ItemId.Sword, ItemQuality.Common, ItemAttribute.Name("Bistriy mech"), ItemAttribute.Dexterity(50)));
            // Inventory.AddItem(new ItemInstance(ItemId.Sword, ItemQuality.Common, ItemAttribute.Cursed(true), ItemAttribute.Damage(10, 15)));
            Inventory.AddItem(new ItemInstance(ItemId.Apple, ItemQuality.Common, 4, ItemAttribute.Luck(3), ItemAttribute.Damage(4, 346)));
            Inventory.AddItem(new ItemInstance(ItemId.IronOre, ItemQuality.Common, 10));
            Inventory.AddItem(new ItemInstance(ItemId.LeatherCuirass, ItemQuality.Common, ItemAttribute.Cursed(true), ItemAttribute.Intelligence(-5), ItemAttribute.Strength(6)));
            Inventory.AddItem(new ItemInstance(ItemId.GandalfStaff, ItemQuality.Common, ItemAttribute.Cursed(true), ItemAttribute.Strength(6)));
            Craft.AddRecipe(CraftRecipeId.CopperIngot);
            // Craft.AddRecipe(CraftRecipeId.Sword);

            NetworkCharacter.OwnerId.SetValue(OwnerNetworkId);
        }

        public bool LearnMainStat(MainStatType stat)
        {
            if (_freeStatPoints == 0)
                return false;

            _freeStatPoints--;
            World.Instance.RPC.S_FreeStatPointsUpdate.Call(_freeStatPoints, Peer);

            switch (stat)
            {
                case MainStatType.Strength:
                    _statStr++;
                    Stats.Main[MainStatType.Strength].SetBaseValue(_statStr);
                    break;
                case MainStatType.Constitution:
                    _statCon++;
                    Stats.Main[MainStatType.Constitution].SetBaseValue(_statCon);
                    break;
                case MainStatType.Dexterity:
                    _statDex++;
                    Stats.Main[MainStatType.Dexterity].SetBaseValue(_statDex);
                    break;
                case MainStatType.Intelligence:
                    _statInt++;
                    Stats.Main[MainStatType.Intelligence].SetBaseValue(_statInt);
                    break;
                case MainStatType.Luck:
                    _statLck++;
                    Stats.Main[MainStatType.Luck].SetBaseValue(_statLck);
                    break;
            }
            return true;
        }

        public void AddExp(int exp)
        {
            if (exp < 0)
                return;
            _currentExp += exp;
            World.Instance.RPC.S_ExperienceUpdate.Call(_currentExp, Peer);
            //check levelup
            var newLevel = GameConstants.Experience.GetLevel(_currentExp);
            if (newLevel != _currentLevel && newLevel > _currentLevel)
            {
                for (var i = _currentLevel; i < newLevel; i++)
                {
                    _freeStatPoints++;
                    _currentLevel = newLevel;
                    CommonHelper.SafeCall(OnLevelUp);
                    World.Instance.RPC.S_NotifyLevelUp.Call(_currentLevel, Peer);
                }
                World.Instance.RPC.S_FreeStatPointsUpdate.Call(_freeStatPoints, Peer);
            }
        }

        public override void UpdateNetworkData()
        {
            base.UpdateNetworkData();
            NetworkCharacter.MaxStamina.SetValue(MaxStamina);
        }

        protected override void EntityFixedUpdate()
        {
            base.EntityFixedUpdate();
            var currStamina = CombatShared.StaminaRegeneration(NetworkCharacter, Stats);
            NetworkCharacter.CurrentStamina.SetValue(currStamina);

            PlayerInputState state;

            if (InputStates.Count == 0)
                state = new PlayerInputState();
            else
                state = InputStates.Dequeue();

            PlayerMove.FixedUpdateMovement(Animator, Collider, ref _moveState, ref state, Spells);
        }

        protected override void EntityUpdate()
        {
            base.EntityUpdate();

            if (_lerp)
            {
                transform.position = Vector3.MoveTowards(transform.position, _targetPosition, _lerpSpeed * Time.deltaTime);
                if (Vector3.Distance(transform.position, _targetPosition) < 0.001f)
                    _lerp = false;
            }
            else
            {
                transform.position += _clientSpeed * Time.deltaTime;
            }
        }

        public void Respawn(Vector3 position)
        {
            if (!Dead)
                return;

            transform.position = position;
            World.Instance.RPC.S_CharacterRespawn.Call(position, (b, connection) =>
            {
                Revive();
            }, Peer);
        }

        protected override void Death()
        {
            base.Death();
            World.Instance.RPC.S_CharacterDeath.Call(LastDamageSender ? LastDamageSender.name : "null", Peer);
        }

        public void SetTransformData(Vector3 position, Vector3 speed)
        {
            _targetPosition = position;
            _clientSpeed = speed;
            _lerpSpeed = Vector3.Distance(transform.position, _targetPosition) / (0.02f + Peer.AverageRoundtripTime / 2f);
            _lerp = true;
        }

        void OnDrawGizmos()
        {
            Gizmos.DrawIcon(transform.position, "gnome.png", true);
        }
    }
}
