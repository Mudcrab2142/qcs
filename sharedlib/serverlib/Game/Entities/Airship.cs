﻿using System;
using SharedLib;
using UnityEngine;
using Lidgren.Network;

namespace ServerLib
{
    public class Airship : BaseEntity
    {
        #region RPC Handlers
        public static void Enter(int id, NetConnection connection)
        {
            Debug.LogFormat("Requesting {0} airship controls", id);
            Character character;
            if (!ServerNetFuncStorage.CheckConnection(connection, out character))
                return;

            var airshipEntity = World.Instance.GetEntity<Airship>(id);
            if (airshipEntity == null)
                return;

            airshipEntity.Enter(character);
        }

        public static void Exit(int id, NetConnection connection)
        {
            Character character;
            if (!ServerNetFuncStorage.CheckConnection(connection, out character))
                return;

            var airshipEntity = World.Instance.GetEntity<Airship>(id);
            if (airshipEntity == null)
                return;

            airshipEntity.Exit(character);
        }

        public static void Control(NetStruct<int, byte, byte, byte> data, NetConnection connection)
        {
            Character character;
            if (!ServerNetFuncStorage.CheckConnection(connection, out character))
                return;

            var airshipEntity = World.Instance.GetEntity<Airship>(data.Value1);
            if (airshipEntity == null)
                return;

            airshipEntity.Control(character, data.Value2, data.Value3, data.Value4);
        }
        #endregion

        const float UP_FORCE_0 = 1180;
        const float UP_FORCE_1 = 1200;
        const float UP_FORCE_2 = 1250;
        const float UP_FORCE_3 = 1300;

        const float DOWN_FORCE_0 = 1160;
        const float DOWN_FORCE_1 = 1150;
        const float DOWN_FORCE_2 = 1100;
        const float DOWN_FORCE_3 = 1050;

        const float FORWARD_FORCE_1 = 100;
        const float FORWARD_FORCE_2 = 250;
        const float FORWARD_FORCE_3 = 500;

        const float BACK_FORCE_1 = -50;
        const float BACK_FORCE_2 = -150;
        const float BACK_FORCE_3 = -250;

        const float ROTATION_FORCE_1 = 20;
        const float ROTATION_FORCE_2 = 30;
        const float ROTATION_FORCE_3 = 40;

        public override EntityType EntityType => EntityType.Airship;
        public override Type NetworkType => typeof(NetworkAirship);
        public NetworkAirship NetworkAirship => (NetworkAirship)NetworkEntity;

        public static Airship Create(params object[] args)
        {
            var obj = Instantiate(Resources.Load("airship")) as GameObject;
            obj.transform.position = (Vector3)args[0];
            obj.transform.eulerAngles = (Vector3)args[1];
            var component = obj.AddComponent<Airship>();
            ServerWorldEntity balloonEntity;
            ServerWorldEntity.Spawn(EntityType.AirshipBalloon, out balloonEntity, (Vector3)args[0] + new Vector3(0, 10, 0), (Vector3)args[1]);
            component.Balloon = (AirshipBalloon)balloonEntity;
            return component;
        }

        public AirshipBalloon Balloon { get; private set; }

        private float _targetAltitude;
        private Rigidbody _shipRigidbody;
        private Rigidbody _balloonRigidbody;
        private Character _driver;

        protected override void EntityStart()
        {
            base.EntityStart();
            _targetAltitude = transform.position.y;
            _shipRigidbody = GetComponent<Rigidbody>();
            _balloonRigidbody = Balloon.GetComponent<Rigidbody>();
            foreach (var joint in GetComponents<ConfigurableJoint>())
                joint.connectedBody = _balloonRigidbody;
        }

        void Enter(Character character)
        {
            if (_driver != null)
                return;

            _driver = character;
            NetworkAirship.DriverEntityId.SetValue(character.NetworkEntity.EntityId);
        }

        void Exit(Character character)
        {
            if (_driver == null)
                return;

            _driver = null;
            NetworkAirship.DriverEntityId.SetValue(-1);
        }

        void Control(Character character, byte alt, byte thr, byte rot)
        {
            if (_driver != character)
                return;

            NetworkAirship.InputAltitude.SetValue(alt);
            NetworkAirship.InputThrottle.SetValue(thr);
            NetworkAirship.InputRotation.SetValue(rot);
        }

        protected override void EntityFixedUpdate()
        {
            base.EntityFixedUpdate();
            var nAirship = NetworkAirship;
            Vector3 verticalForce = Vector3.up;
            Vector3 throttle = _shipRigidbody.transform.forward;
            Vector3 rotationForce = _shipRigidbody.transform.right;
            switch (nAirship.InputAltitude.GetValue())
            {
                case 101:
                    verticalForce *= UP_FORCE_1;
                    break;
                case 102:
                    verticalForce *= UP_FORCE_2;
                    break;
                case 103:
                    verticalForce *= UP_FORCE_3;
                    break;
                case 99:
                    verticalForce *= DOWN_FORCE_1;
                    break;
                case 98:
                    verticalForce *= DOWN_FORCE_2;
                    break;
                case 97:
                    verticalForce *= DOWN_FORCE_3;
                    break;
                default:
                    verticalForce *= transform.position.y > _targetAltitude ? DOWN_FORCE_0 : UP_FORCE_0;
                    break;
            }

            switch (nAirship.InputThrottle.GetValue())
            {
                case 101:
                    throttle *= FORWARD_FORCE_1;
                    break;
                case 102:
                    throttle *= FORWARD_FORCE_2;
                    break;
                case 103:
                    throttle *= FORWARD_FORCE_3;
                    break;
                case 99:
                    throttle *= BACK_FORCE_1;
                    break;
                case 98:
                    throttle *= BACK_FORCE_2;
                    break;
                case 97:
                    throttle *= BACK_FORCE_3;
                    break;
                default:
                    throttle *= 0f;
                    break;
            }

            switch (nAirship.InputRotation.GetValue())
            {
                case 101:
                    rotationForce *= -ROTATION_FORCE_1;
                    break;
                case 102:
                    rotationForce *= -ROTATION_FORCE_2;
                    break;
                case 103:
                    rotationForce *= -ROTATION_FORCE_3;
                    break;
                case 99:
                    rotationForce *= ROTATION_FORCE_1;
                    break;
                case 98:
                    rotationForce *= ROTATION_FORCE_2;
                    break;
                case 97:
                    rotationForce *= ROTATION_FORCE_3;
                    break;
                default:
                    rotationForce *= 0f;
                    break;
            }

            _balloonRigidbody.AddForce(verticalForce);

            if (nAirship.InputThrottle.GetValue() != 100)
                _shipRigidbody.AddForce(throttle);
            if(nAirship.InputRotation.GetValue() != 100)
                _shipRigidbody.AddForceAtPosition(rotationForce, transform.position + transform.forward * 6f);
        }
    }
}
