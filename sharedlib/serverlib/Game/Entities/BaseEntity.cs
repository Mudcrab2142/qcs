﻿namespace ServerLib
{
    public abstract class BaseEntity : ServerWorldEntity
    {
        public override void UpdateNetworkData()
        {
            NetworkEntity.Position.SetValue(transform.position);
            if (NetworkEntity.UseSimpleRotation)
                NetworkEntity.SimpleRotation.SetValue(transform.eulerAngles.y);
            else
                NetworkEntity.FullRotation.SetValue(transform.eulerAngles);
        }
    }
}
