﻿using SharedLib;
using System.Collections.Generic;

namespace ServerLib
{
    public abstract class HumanoidCreature : Creature, IEquipmentOwner, ICombatCreatureEntity
    {
        public ServerEquipment Equipment { get; private set; }
        public NetworkHumanoidCreature NetworkHumanoidCreature { get { return (NetworkHumanoidCreature)NetworkEntity; } }

        protected override void EntityStart()
        {
            //create equipment before stats
            Equipment = new ServerEquipment(this);
            base.EntityStart();

            Equipment.OnItemEquip += UpdateNetworkEquip;
            Equipment.OnItemUnequip += UpdateNetworkEquip;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Equipment.OnItemEquip -= UpdateNetworkEquip;
            Equipment.OnItemUnequip -= UpdateNetworkEquip;
        }

        void UpdateNetworkEquip(ItemInstance item, byte slot)
        {
            var data = new Dictionary<EquipmentPosition, string>();
            if (Equipment.Weapon.Item != null)
                data.Add(EquipmentPosition.Weapon, Equipment.Weapon.Item[AttributeType.Model]);

            if (Equipment.Helmet.Item != null)
                data.Add(EquipmentPosition.Helmet, "test");

            if (Equipment.Cuirass.Item != null)
                data.Add(EquipmentPosition.Cuirass, "test");

            if (Equipment.Leggings.Item != null)
                data.Add(EquipmentPosition.Leggings, "test");

            if (Equipment.Boots.Item != null)
                data.Add(EquipmentPosition.Boots, "test");

            if (Equipment.Gloves.Item != null)
                data.Add(EquipmentPosition.Gloves, "test");

            NetworkHumanoidCreature.Equipment.SetValue(SerializeTools.ProtoBufSerialize(data));
        }
    }

}
