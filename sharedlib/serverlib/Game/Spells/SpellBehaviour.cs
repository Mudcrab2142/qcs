﻿using SharedLib;
using UnityEngine;

namespace ServerLib
{
    public abstract class SpellBehaviour
    {
        public readonly ICombatCreatureEntity Owner;
        public readonly SpellInfo Info;    

        public SpellBehaviour(SpellInfo info, ICombatCreatureEntity owner)
        {
            Owner = owner;
            Info = info;
        }

        public virtual void OnSpellLevelChanged(byte newLevel) { }
        public virtual void OnEquipmentChanged(ItemInstance item, byte slot) { }

        public virtual void Use(string args) { }
        public virtual void Trigger(int targetEntityId, string args) { }
        public virtual void DirectionalCast(Vector3 origin, Vector3 direction, string args) { }
    }
}
