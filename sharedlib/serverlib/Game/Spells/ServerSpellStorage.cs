﻿using SharedLib;
using System.Collections.Generic;

namespace ServerLib
{
    public class ServerSpellStorage : SpellStorage
    {
        public readonly Dictionary<SpellName, SpellBehaviour> SpellBehaviours = new Dictionary<SpellName, SpellBehaviour>();

        private ICombatCharacterEntity _owner;

        public int FreePoints { get; protected set; }

        public ServerSpellStorage(StatStorage stats, ICombatCharacterEntity owner) : base(stats)
        {
            _owner = owner;
            SpellBehaviours.Add(SpellName.SwordMastery1H, new SwordMastery1HBehaviour((SwordMastery1HInfo)SpellInfo[SpellName.SwordMastery1H], owner));
            SpellBehaviours.Add(SpellName.CuttingStrike, new CuttingStrikeBehaviour((CuttingStrikeInfo)SpellInfo[SpellName.CuttingStrike], owner));
            SpellBehaviours.Add(SpellName.Roll, new RollBehaviour((RollInfo)SpellInfo[SpellName.Roll], owner));
            SpellBehaviours.Add(SpellName.DarkMatter, new DarkMatterBehaviour((DarkMatterInfo)SpellInfo[SpellName.DarkMatter], owner));
            owner.OnLevelUp += OnOwnerLevelUp;
            owner.Equipment.OnEquipmentChange += OnEquipmentChange;
        }

        ~ServerSpellStorage()
        {
            _owner.OnLevelUp -= OnOwnerLevelUp;
            _owner.Equipment.OnEquipmentChange -= OnEquipmentChange;
        }

        public bool ClientLearnSpell(SpellName spell)
        {
            var currentSpellLevel = Level[spell];
            if (currentSpellLevel >= SpellInfo[spell].MaxLevel || FreePoints <= 0)
                return false;

            Level[spell]++;
            FreePoints--;

            SpellBehaviours[spell].OnSpellLevelChanged(Level[spell]);

            SendFreePoints();
            SendSpellData();  
            NetworkBaseWorld.WorldInstance.RPC.S_SpellUpdateData.Call(Level, _owner.Peer);
            return true;
        }

        void OnOwnerLevelUp()
        {
            FreePoints++;
            SendFreePoints();
        }

        void OnEquipmentChange(ItemInstance item, byte slot)
        {
            foreach (var s in SpellBehaviours)
                if(s.Value.Info.SpellLevel > 0)
                    s.Value.OnEquipmentChanged(item, slot);
        }

        public void SendSpellData()
        {
            World.Instance.RPC.S_SpellUpdateData.Call(Level, _owner.Peer);
        }

        public void SendFreePoints()
        {
            World.Instance.RPC.S_SpellUpdateFreePoints.Call(FreePoints, _owner.Peer);
        }

        public ICombatCharacterEntity GetOwner()
        {
            return _owner;
        }
    }
}
