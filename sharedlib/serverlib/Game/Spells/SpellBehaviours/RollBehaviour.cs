﻿using SharedLib;


namespace ServerLib
{
    public class RollBehaviour : SpellBehaviour
    {
        public RollInfo RollInfo => (RollInfo)Info;

        public RollBehaviour(RollInfo info, ICombatCreatureEntity owner) : base(info, owner)
        {

        }

        public override void Use(string args)
        {
            var networkCharacter = Owner.NetworkEntity as NetworkCharacter;
            if (networkCharacter)
                networkCharacter.CurrentStamina.SetValue(networkCharacter.CurrentStamina.GetValue() - RollInfo.StaminaCost);
        }
    }
}
