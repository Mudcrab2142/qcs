﻿using SharedLib;
using Random = UnityEngine.Random;

namespace ServerLib
{
    public class CuttingStrikeBehaviour : SpellBehaviour
    {
        public CuttingStrikeInfo CuttingStrikeInfo => (CuttingStrikeInfo)Info;

        public CuttingStrikeBehaviour(SpellInfo info, ICombatCreatureEntity owner) : base(info, owner)
        {

        }

        public override void Trigger(int targetEntityId, string args)
        {
            var target = World.Instance.GetEntity<ICombatBaseEntity>(targetEntityId);
            if (target == null)
                return;

            var resultDamage = Random.Range(CuttingStrikeInfo.HitDamage.Min, CuttingStrikeInfo.HitDamage.Max);
            var level = CuttingStrikeInfo.SpellLevel;
            var bleedInfo = new BleedEffectInfo("kek", CuttingStrikeInfo.GetBleedDuration(level), CuttingStrikeInfo.GetBleedPeriodicDamage(level), CuttingStrikeInfo.BleedPeriod);
            var bleedEffect = new BleedEffectBehaviour(bleedInfo, target, Owner as WorldEntity);

            target.Effects.ApplyEffect(bleedEffect);
            CombatHelper.Damage(Owner, target, new Damage((short)resultDamage, CuttingStrikeInfo.DamageType));
        }
    }
}
