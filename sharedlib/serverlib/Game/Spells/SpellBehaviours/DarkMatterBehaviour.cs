﻿using SharedLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ServerLib
{
    public class DarkMatterBehaviour : SpellBehaviour
    {
        public DarkMatterInfo DarkMatterInfo => (DarkMatterInfo)Info;

        public DarkMatterBehaviour(DarkMatterInfo info, ICombatCreatureEntity owner) : base(info, owner)
        {

        }

        public override void DirectionalCast(Vector3 origin, Vector3 direction, string args)
        {
            ServerWorldEntity missile;
            ServerWorldEntity.SpawnImmediate(EntityType.MagicMissile, out missile, NetworkMagicMissile.MissileType.DarkMatter, origin, direction, Owner as Character, Info);
        }
    }
}
