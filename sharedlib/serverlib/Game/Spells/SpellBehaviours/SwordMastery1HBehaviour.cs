﻿using SharedLib;
using System;
using Random = UnityEngine.Random;

namespace ServerLib
{
    public class SwordMastery1HBehaviour : SpellBehaviour
    {
        public SwordMastery1HInfo SwordMastery1HInfo => (SwordMastery1HInfo)Info;

        private SecondaryStat _minDmgStat => Owner.Stats.Secondary[SecondaryStatType.MinDamage];
        private SecondaryStat _maxDmgStat => Owner.Stats.Secondary[SecondaryStatType.MaxDamage];

        private bool _modifiersActive = false;

        public SwordMastery1HBehaviour(SwordMastery1HInfo info, ICombatCreatureEntity owner) : base(info, owner)
        {

        }

        public override void Use(string args)
        {
            var networkCharacter = Owner.NetworkEntity as NetworkCharacter;
            if (networkCharacter)
                networkCharacter.CurrentStamina.SetValue(networkCharacter.CurrentStamina.GetValue() - SwordMastery1HInfo.StaminaCost);
        }

        public override void Trigger(int targetEntityId, string args)
        {
            var target = World.Instance.GetEntity<ICombatBaseEntity>(targetEntityId);
            if (target == null)
                return;

            int resultDamage = 0;
            MinMax<short> dmg = new MinMax<short>();
            switch (args)
            {
                case "1":
                    dmg = SwordMastery1HInfo.GetBasicAttackDamage(SwordMastery1HInfo.SpellLevel, 1);
                    break;
                case "2":
                    dmg = SwordMastery1HInfo.GetBasicAttackDamage(SwordMastery1HInfo.SpellLevel, 2);
                    break;
                case "3":
                    dmg = SwordMastery1HInfo.GetBasicAttackDamage(SwordMastery1HInfo.SpellLevel, 3);
                    break;
            }
            resultDamage = Random.Range(dmg.Min, dmg.Max);
            //TODO: ADD CRIT CHANCE AND POWER


            CombatHelper.Damage(Owner, target, new Damage((short)resultDamage, SwordMastery1HInfo.DamageType));
        }

        public override void OnEquipmentChanged(ItemInstance item, byte slot)
        {
            if (slot != GameConstants.Equipment.WEAPON)
                return;

            if (_modifiersActive)
            {
                if (item == null || item.BaseItem.Type != ItemType.WeaponSword1H)
                    RemoveModifiers();
            }
            else
            {
                if (item.BaseItem.Type == ItemType.WeaponSword1H)
                    AddModifiers();
            }
        }

        public override void OnSpellLevelChanged(byte newLevel)
        {
            RemoveModifiers();
            AddModifiers();
        }

        private void AddModifiers()
        {
            var dmgMod = SwordMastery1HInfo.GetBonusDamagePercent(SwordMastery1HInfo.SpellLevel);
            _minDmgStat.AddModifier(this, new StatModifier(StatModifier.ModifyType.AddBaseValuePercent, dmgMod));
            _maxDmgStat.AddModifier(this, new StatModifier(StatModifier.ModifyType.AddBaseValuePercent, dmgMod));
            _modifiersActive = true;
        }

        private void RemoveModifiers()
        {
            _minDmgStat.RemoveModifier(this);
            _maxDmgStat.RemoveModifier(this);
            _modifiersActive = false;
        }
    }
}
