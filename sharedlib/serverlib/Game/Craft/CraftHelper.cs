﻿using SharedLib;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ServerLib
{
    public static class CraftHelper
    {
        const float WEIGHT_DECREASE_PERCENT = 0.75f;
        const float EXP_PER_CRAFT_SECOND = 0.25f;

        static Random _random = new Random();

        static List<AttributeType> _supportedMultiplyAttributes = new List<AttributeType>()
        {
            AttributeType.Armor,
            AttributeType.Damage,
            AttributeType.Strength,
            AttributeType.Dexterity,
            AttributeType.Constitution,
            AttributeType.Intelligence,
            AttributeType.Luck
        };

        static Dictionary<ItemQuality, float> _qualityChanceBonusTable = new Dictionary<ItemQuality, float>()
        {
            { ItemQuality.Poor, -0.15f },
            { ItemQuality.Common, -0.1f },
            { ItemQuality.Uncommon, 0f },
            { ItemQuality.Rare, 0.05f },
            { ItemQuality.Legendary, 0.1f },
            { ItemQuality.Epic, 0.2f },
            { ItemQuality.Artifact, 10f }
        };

        static Dictionary<ItemQuality, float> _qualityBaseStatsBonus = new Dictionary<ItemQuality, float>()
        {
            { ItemQuality.Poor, 0.75f },
            { ItemQuality.Common, 1f },
            { ItemQuality.Uncommon, 1.25f },
            { ItemQuality.Rare, 1.5f },
            { ItemQuality.Legendary, 1.75f },
            { ItemQuality.Epic, 2f },
            { ItemQuality.Artifact, 4f }        //artifact items are nor craftable
        };

        static Dictionary<ItemQuality, float> _qualityExpMultiplier = new Dictionary<ItemQuality, float>()
        {
            { ItemQuality.Poor, 0.25f },
            { ItemQuality.Common, 0.5f },
            { ItemQuality.Uncommon, 1f },
            { ItemQuality.Rare, 1.25f },
            { ItemQuality.Legendary, 1.5f },
            { ItemQuality.Epic, 1.75f },
            { ItemQuality.Artifact, 2f }        //artifact items are nor craftable
        };

        public static ItemInstance GenerateCraftedItem(CraftRecipe recipe, CraftSession session, ServerStatStorage stats)
        {
            var skillDiff = stats.Craft[recipe.Type].Value.Value - recipe.MasteryLevel;
            var qualityBonusChance = session.Ingredients.Sum(x => _qualityChanceBonusTable[x.Quality] * x.Count) / session.Ingredients.Sum(x => x.Count);
            var itemQuality = GenerateItemQuality(skillDiff, recipe, qualityBonusChance);
            var itemWeight = session.Ingredients.Sum(x => x.Weight) * WEIGHT_DECREASE_PERCENT;

            var resultItem = new ItemInstance(recipe.Item, itemQuality);

            if (resultItem.BaseItem.Type == ItemType.WeaponSword1H || resultItem.BaseItem.Type == ItemType.Armor)
            {
                if (resultItem.Stackable)
                {
                    resultItem.Count = recipe.Count;
                    itemWeight /= recipe.Count;
                }
                resultItem.AddAttribute(ItemAttribute.Weight(itemWeight));
            }

            GenerateQualityBonuses(resultItem);

            return resultItem;
        }

        public static float CalculateCraftExp(CraftRecipe recipe, ServerStatStorage stats, ItemQuality quality)
        {
            var skillDiff = stats.Craft[recipe.Type].Value.Value - recipe.MasteryLevel;
            var e = skillDiff > 0 ? 10f / (10f + skillDiff) : 1f;
            return recipe.CraftTime * EXP_PER_CRAFT_SECOND * e * _qualityExpMultiplier[quality];
        }

        static void GenerateQualityBonuses(ItemInstance item)
        {
            var multiplier = _qualityBaseStatsBonus[item.Quality];
            foreach (var att in item.BaseItem.Attributes.Where(x => _supportedMultiplyAttributes.Contains(x.Key)))
                item.AddAttribute(new ItemAttribute() { Type = att.Key, Value = att.Value.Clone().Multiply(multiplier) });
        }

        static ItemQuality GenerateItemQuality(float skillDiff, CraftRecipe recipe, float bonusChance)
        {
            if (_random.NextDouble() + bonusChance <= GetQualityChance(0.1f, skillDiff, 39f, 200f, 5f))         //min 40 diff skill
                return ItemQuality.Legendary;

            if (_random.NextDouble() + bonusChance <= GetQualityChance(0.3f, skillDiff, 19f, 200f, 5f))         //min 20 diff skill
                return ItemQuality.Epic;

            if (_random.NextDouble() + bonusChance <= GetQualityChance(0.5f, skillDiff, 13f, 200f, 5f))         //min 14 diff skill
                return ItemQuality.Rare;

            if (_random.NextDouble() + bonusChance <= GetQualityChance(0.75f, skillDiff, -9f, 200f, 25f))         //min -8 diff skill
                return ItemQuality.Uncommon;

            if (_random.NextDouble() + bonusChance <= GetQualityChance(0.8f, skillDiff, -25f, 200f, 40f))         //min -24 diff skill
                return ItemQuality.Common;

            return ItemQuality.Poor;
        }

        /// <summary>
        /// Chance = maxChance - A/(skillDiff + B)^2
        /// </summary>
        static double GetQualityChance(float maxChance, float skillDiff, float skillDiffMin, float a, float b)
        {
            if (skillDiff < skillDiffMin)
                skillDiff = skillDiffMin;
            return maxChance - a / Math.Pow(skillDiff + b, 2);
        }
    }
}
