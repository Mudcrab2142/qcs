﻿using SharedLib;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ServerLib
{
    public class ServerCraftStorage : CraftStorage
    {
        readonly ServerInventory _serverInventory;
        readonly Character _owner;

        public ServerCraftStorage(Inventory ownerInventory, StatStorage ownerStats, Character owner) : base(ownerInventory, ownerStats)
        {
            _serverInventory = (ServerInventory)ownerInventory;
            _owner = owner;
            _owner.UpdateAction += Update;
        }

        ~ServerCraftStorage()
        {
            _owner.UpdateAction -= Update;
        }

        private void Update()
        {
            if (CraftSessions.Count == 0)
                return;

            var session = CraftSessions[0];
            session.CraftProgress += Time.deltaTime;

            if (session.CraftProgress >= session.CraftTime)
                FinishCraft(session);
        }

        public bool Craft(CraftRecipeId recipeId, NetStruct<short, short, int>[] items)
        {
            var recipeInfo = GetRecipeInfo(recipeId);

            //find all items and check counts
            var craftIngredients = new List<InventoryEntry>();
            for (var i = 0; i < items.Length; i++)
            {
                var item = _serverInventory.GetItemEntry(items[i].Value1, items[i].Value2);
                if (item == null || item.Item.Count < items[i].Value3)
                    return false;

                craftIngredients.Add(item);
            }

            //create new session
            var craftSession = new CraftSession(recipeId, recipeInfo.CraftTime, craftIngredients.Select(x => x.Item).ToArray());
            CraftSessions.Add(craftSession);

            //remove ingredients from inventory
            foreach(var item in items)
                _owner.Inventory.RemoveItem(item.Value1, item.Value2, item.Value3);

            SendClientSessions();
            return true;
        }

        private void FinishCraft(CraftSession session)
        {
            CraftSessions.Remove(session);
            SendClientSessions();

            var recipeInfo = GetRecipeInfo(session.Recipe);

            var item = CraftHelper.GenerateCraftedItem(recipeInfo, session, _owner.Stats);
            _owner.Inventory.AddItem(item);

            var exp = CraftHelper.CalculateCraftExp(recipeInfo, _owner.Stats, item.Quality);
            _owner.Stats.Craft[recipeInfo.Type].AddBaseValue(exp);
        }

        public bool AddRecipe(CraftRecipeId recipe)
        {
            if (KnownRecipes.Add(recipe))
            {
                SendClientRecipes();
                return true;
            }
            return false;
        }

        public void SendClientSessions()
        {
            World.Instance.RPC.S_CraftSessionListUpdate.Call(CraftSessions, _owner.Peer);
        }

        public void SendClientRecipes()
        {
            World.Instance.RPC.S_CraftRecipeListUpdate.Call(KnownRecipes, _owner.Peer);
        }
    }
}
