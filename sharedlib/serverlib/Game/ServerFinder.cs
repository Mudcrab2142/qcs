﻿using SharedLib;

namespace ServerLib
{
    public class ServerFinder : IFinder
    {
        public Equipment FindEquipment(int entityId = 0)
        {
            var entity = World.Instance.GetEntity<IEquipmentOwner>(entityId);
            if (entity == null)
                return null;

            return entity.Equipment;
        }

        public Inventory FindInventory(int entityId = 0)
        {
            var entity = World.Instance.GetEntity<IInventoryOwner>(entityId);
            if (entity == null)
                return null;

            return entity.Inventory;
        }

        public NetworkCharacter FindNetworkCharacter(object o = null)
        {
            var spells = o as ServerSpellStorage;
            if (spells != null)
                return spells.GetOwner().NetworkEntity as NetworkCharacter;

            return null;
        }
    }
}
