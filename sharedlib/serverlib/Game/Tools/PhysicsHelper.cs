﻿using System.Collections.Generic;
using UnityEngine;

namespace ServerLib
{
    public static class PhysicsHelper
    {
        static readonly Collider[] _colliders = new Collider[1024];
        static readonly List<Creature> _creaturesResult = new List<Creature>();

        public static List<Creature> FindCreaturesInRadius(Vector3 position, float radius)
        {
            _creaturesResult.Clear();
            var count = Physics.OverlapSphereNonAlloc(position, radius, _colliders);

            for(var i = 0; i < count; i++)
            {
                var c = _colliders[i].GetComponent<Creature>();
                if (c != null && !_creaturesResult.Contains(c))
                    _creaturesResult.Add(c);
            }

            return _creaturesResult;
        }
    }
}
