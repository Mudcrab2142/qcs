﻿using SharedLib;
using System;
using System.Collections.Generic;

namespace ClientLib
{
    public class ClientEffectStorage : EffectStorage
    {
        public event Action<EffectInfo> OnEffectAdd;
        public event Action<EffectInfo> OnEffectRemove;

        public readonly List<EffectInfo> Effects = new List<EffectInfo>();

        internal void AddEffect(EffectInfo info)
        {
            if (OnEffectAdd != null)
                OnEffectAdd(info);

            Effects.Add(info);
        }

        internal void RemoveEffect(int position)
        {
            if (position < 0 || Effects.Count <= position)
                return;

            if (OnEffectRemove != null)
                OnEffectRemove(Effects[position]);

            Effects.RemoveAt(position);
        }
    }
}
