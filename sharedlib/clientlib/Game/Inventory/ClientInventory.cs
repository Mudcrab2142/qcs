﻿using SharedLib;
using System;
using UnityEngine;

namespace ClientLib
{
    public class ClientInventory : Inventory
    {
        public event Action<InventoryData> OnUpdate;

        internal void Update(InventoryData data)
        {
            Backpack = data;
            if (OnUpdate != null)
                OnUpdate(data);

            if (mask == null || mask.GetLength(0) != data.SizeX || mask.GetLength(1) != data.SizeY)
                mask = new bool[data.SizeX, data.SizeY];

            UpdateMask(0, 0, data.SizeX, data.SizeY, false);

            foreach (var item in data.Storage)
                UpdateMask(item.PositionX, item.PositionY, item.Item.Width, item.Item.Height, true);
        }

        public void TryMoveItem(short fromX, short fromY, short toX, short toY, int count, Action onSuccess, Action onFail)
        {
            if (!NetworkBaseWorld.WorldInstance)
                return;

            Debug.Log(string.Format("[InventoryOperation] MoveItem from ({0},{1}) to ({2},{3}).", fromX, fromY, toX, toY));
            NetworkBaseWorld.WorldInstance.RPC.CL_InventoryMoveItem.Call(new NetStruct<short, short, short, short>(fromX, fromY, toX, toY), (result, connection) =>
            {
                Debug.Log(string.Format("[InventoryOperationResult] MoveItem from ({0},{1}) to ({2},{3}). {4}!", fromX, fromY, toX, toY, result ? "Success" : "Failed"));
                if (result)
                    onSuccess();
                else
                    onFail();
            });
        }

        public void TryEquipItem(short fromX, short fromY, byte to, Action onSuccess, Action onFail)
        {
            if (!NetworkBaseWorld.WorldInstance)
                return;

            Debug.Log(string.Format("[InventoryOperation] EquipItem from ({0},{1}) to ({2}).", fromX, fromY, to));

            NetworkBaseWorld.WorldInstance.RPC.CL_InventoryEquipItem.Call(new NetStruct<short, short, byte>(fromX, fromY, to), (result, connection) =>
            {
                Debug.Log(string.Format("[InventoryOperation] EquipItem from ({0},{1}) to ({2}). {3}!", fromX, fromY, to, result ? "Success" : "Failed"));
                if (result)
                    onSuccess();
                else
                    onFail();
            });
        }

        public void TryUnequipItem(byte from, short toX, short toY, Action onSuccess, Action onFail)
        {
            if (!NetworkBaseWorld.WorldInstance)
                return;

            Debug.Log(string.Format("[InventoryOperation] Unequip from ({0}) to ({1},{2}).", from, toX, toY));

            NetworkBaseWorld.WorldInstance.RPC.CL_InventoryUnequipItem.Call(new NetStruct<byte, short, short>(from, toX, toY), (result, connection) =>
            {
                Debug.Log(string.Format("[InventoryOperation] Unequip from ({0}) to ({1},{2}). {3}.", from, toX, toY, result ? "Success" : "Failed"));
                if (result)
                    onSuccess();
                else
                    onFail();
            });
        }
    }
}
