﻿using SharedLib;
using UnityEngine;

namespace ClientLib
{
    public abstract class BasePredictedSpell
    {
        internal virtual void PredictUse(string args) { }
        internal virtual void PredictTrigger(int target, string args) { }
        internal virtual void PredictDirectionalCast(Vector3 origin, Vector3 target, string args) { }

        protected NetworkCharacter owner => NetworkClientWorld.Instance.ControlledCharacter;
    }
}
