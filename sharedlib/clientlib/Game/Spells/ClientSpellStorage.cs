﻿using Lidgren.Network;
using SharedLib;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ClientLib
{
    public class ClientSpellStorage : SpellStorage
    {
        public event Action OnSpellLevelUpdate;
        public readonly EventProperty<int> FreePoints = new EventProperty<int>();

        private readonly Dictionary<SpellName, BasePredictedSpell> _predictedSpells = new Dictionary<SpellName, BasePredictedSpell>();

        public ClientSpellStorage(StatStorage stats) : base(stats)
        {
            AddPredictedSpell(SpellName.Roll, new RollPredicted((RollInfo)SpellInfo[SpellName.Roll]));
            AddPredictedSpell(SpellName.SwordMastery1H, new SwordMastery1HPredicted((SwordMastery1HInfo)SpellInfo[SpellName.SwordMastery1H]));
        }

        public void UseSpell(SpellName name, string args = "")
        {
            BasePredictedSpell predicted;
            if (_predictedSpells.TryGetValue(name, out predicted))
                predicted.PredictUse(args);

            NetworkClientWorld.Instance.RPC.CL_SpellUse.Call(new NetStruct<SpellName, string>(name, args));
        }

        public void TriggerSpell(SpellName name, int target, string args = "")
        {
            BasePredictedSpell predicted;
            if (_predictedSpells.TryGetValue(name, out predicted))
                predicted.PredictTrigger(target, args);

            NetworkClientWorld.Instance.RPC.CL_SpellTrigger.Call(new NetStruct<SpellName, int, string>(name, target, args));
        }

        public void DirectionalCastSpell(SpellName name, Vector3 origin, Vector3 direction, string args = "")
        {
            BasePredictedSpell predicted;
            if (_predictedSpells.TryGetValue(name, out predicted))
                predicted.PredictDirectionalCast(origin, direction, args);

            NetworkClientWorld.Instance.RPC.CL_SpellDirectionalCast.Call(new NetStruct<SpellName, ProtoVector3, ProtoVector3, string>(name, origin, direction, args));
        }

        public void TryLearnSpell(SpellName name, Action<bool, NetConnection> callback)
        {
            NetworkClientWorld.Instance.RPC.CL_SpellLearn.Call(name, callback);
        }

        internal void UpdateLevel(Dictionary<SpellName, byte> data)
        {
            Level = data;
            CommonHelper.SafeCall(OnSpellLevelUpdate);
        }

        public void AddPredictedSpell(SpellName name, BasePredictedSpell behaviour)
        {
            _predictedSpells[name] = behaviour;
        }
    }
}
