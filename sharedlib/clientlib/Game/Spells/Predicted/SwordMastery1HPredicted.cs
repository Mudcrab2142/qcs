﻿using System;
using SharedLib;

namespace ClientLib
{
    public class SwordMastery1HPredicted : BasePredictedSpell
    {
        private readonly SwordMastery1HInfo _spellInfo;

        public SwordMastery1HPredicted(SwordMastery1HInfo spellInfo)
        {
            _spellInfo = spellInfo;
        }

        internal override void PredictUse(string args)
        {
            owner.CurrentStamina.SetPredictedValue(owner.CurrentStamina.GetValue() - _spellInfo.StaminaCost);
        }
    }
}
