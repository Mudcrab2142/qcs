﻿using SharedLib;
using System;

namespace ClientLib
{
    public class RollPredicted : BasePredictedSpell
    {
        private readonly RollInfo _spellInfo;

        public RollPredicted(RollInfo spellInfo)
        {
            _spellInfo = spellInfo;
        }

        internal override void PredictUse(string args)
        {
            owner.CurrentStamina.SetPredictedValue(owner.CurrentStamina.GetValue() - _spellInfo.StaminaCost);
        }
    }
}
