﻿using SharedLib;
using System;
using System.Collections.Generic;

namespace ClientLib
{
    public class ClientStatStorage : StatStorage
    {
        public event Action OnMainStatsUpdate;
        public event Action OnSecondaryStatsUpdate;
        public event Action OnCraftStatsUpdate;

        internal Dictionary<MainStatType, StatValue> Main = new Dictionary<MainStatType, StatValue>();
        internal Dictionary<SecondaryStatType, StatValue> Secondary = new Dictionary<SecondaryStatType, StatValue>();
        internal Dictionary<CraftStatType, StatValue> Craft = new Dictionary<CraftStatType, StatValue>();

        internal void UpdateMainStats(List<StatValue> data)
        {
            foreach(var item in data)
                Main[(MainStatType)item.Type] = item;

            CommonHelper.SafeCall(OnMainStatsUpdate);
        }

        internal void UpdateSecondaryStats(List<StatValue> data)
        {
            foreach (var item in data)
                Secondary[(SecondaryStatType)item.Type] = item;

            CommonHelper.SafeCall(OnSecondaryStatsUpdate);
        }

        internal void UpdateCraftStats(List<StatValue> data)
        {
            foreach (var item in data)
                Craft[(CraftStatType)item.Type] = item;

            CommonHelper.SafeCall(OnCraftStatsUpdate);
        }

        public void TryUpgradeStat(MainStatType stat)
        {
            NetworkClientWorld.Instance.RPC.CL_MainStatUpgrade.Call(stat, (d, c) => { return; });
        }

        public override StatValue GetStat(MainStatType type)
        {
            return Main.ContainsKey(type) ? Main[type] : NullValue;
        }

        public override StatValue GetStat(SecondaryStatType type)
        {
            return Secondary.ContainsKey(type) ? Secondary[type] : NullValue;
        }

        public override StatValue GetStat(CraftStatType type)
        {
            return Craft.ContainsKey(type) ? Craft[type] : NullValue;
        }
    }
}
