﻿using SharedLib;

namespace ClientLib
{
    public class ClientFinder : IFinder
    {
        public Equipment FindEquipment(int entityId = 0)
        {
            return NetworkClientWorld.Instance.ClientEquipment;
        }

        public Inventory FindInventory(int entityId = 0)
        {
            return NetworkClientWorld.Instance.ClientInventory;
        }

        public NetworkCharacter FindNetworkCharacter(object o = null)
        {
            return NetworkClientWorld.Instance.ControlledCharacter;
        }
    }
}
