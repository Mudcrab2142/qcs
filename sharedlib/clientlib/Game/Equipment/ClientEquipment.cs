﻿using SharedLib;
using System;

namespace ClientLib
{
    public sealed class ClientEquipment : Equipment
    {
        public event Action OnEquipmentUpdate;

        internal void UpdateSlot(byte slot, ItemInstance item)
        {
            var targetSlot = this[slot];
            if (targetSlot == null)
                return;

            targetSlot.ChangeItem(item);

            if (OnEquipmentUpdate != null)
                OnEquipmentUpdate();  
        }
    }
}
