﻿using SharedLib;
using System;
using System.Collections.Generic;

namespace ClientLib
{
    public class ClientCraftStorage : CraftStorage
    {
        public event Action OnKnownRecipesUpdate;
        public event Action OnCraftSessionsUpdate;

        internal ClientCraftStorage(Inventory inv, StatStorage stats) : base(inv, stats)
        {

        }

        internal void UpdateKnownRecipes(HashSet<CraftRecipeId> data)
        {
            foreach (var item in data)
                KnownRecipes.Add(item);

            CommonHelper.SafeCall(OnKnownRecipesUpdate);
        }

        internal void UpdateCraftSessions(List<CraftSession> data)
        {
            CraftSessions.Clear();
            CraftSessions.AddRange(data);
            CommonHelper.SafeCall(OnCraftSessionsUpdate);
        }
    }
}
