﻿using SharedLib;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ClientLib
{
    public abstract class NetworkClientWorld : NetworkBaseWorld
    {
        private readonly Dictionary<int, float> _entityUpdateTimeStamps = new Dictionary<int, float>();
        private readonly Queue<int> _delayedEntityEvents = new Queue<int>();

        private Dictionary<EntityType, Func<NetworkBaseEntity, WorldEntity>> _entityCreators = new Dictionary<EntityType, Func<NetworkBaseEntity, WorldEntity>>();
        private Dictionary<ulong, uint> _receivedCellLayerTicks = new Dictionary<ulong, uint>();
        private Dictionary<ulong, float> _fullCellDataRequestTimes = new Dictionary<ulong, float>();

        public override IFinder Finder => _finder;
        private ClientFinder _finder = new ClientFinder(); 

        public ClientInventory ClientInventory { get; private set; }
        public ClientStatStorage ClientStats { get; private set; }
        public ClientEquipment ClientEquipment { get; private set; }
        public ClientSpellStorage ClientSpells { get; private set; }
        public ClientEffectStorage ClientEffects { get; private set; }
        public ClientCraftStorage ClientCraft { get; private set; }
        public EventProperty<int> ClientExp { get; private set; }
        public EventProperty<int> ClientFreeStatPoints { get; private set; }
        public EventProperty<NetStruct<string, int>[]> ClientCharactersList { get; private set; }

        public NetworkCharacter ControlledCharacter { get; private set; }

        protected abstract void OnConnectedToServer();
        protected abstract void OnDisconnectedFromServer();

        public static NetworkClientWorld Instance => WorldInstance as NetworkClientWorld;
        public LidgrenRootClient NetworkRoot { get; private set; }

        public static void JoinWorld<T>(string serverIp, int serverPort) where T : NetworkClientWorld
        {
            var obj = new GameObject(typeof(T).ToString());
            WorldInstance = obj.AddComponent<T>();

            Instance.ClientInventory = new ClientInventory();
            Instance.ClientStats = new ClientStatStorage();
            Instance.ClientEquipment = new ClientEquipment();
            Instance.ClientSpells = new ClientSpellStorage(Instance.ClientStats);
            Instance.ClientEffects = new ClientEffectStorage();
            Instance.ClientCraft = new ClientCraftStorage(Instance.ClientInventory, Instance.ClientStats);
            Instance.ClientExp = new EventProperty<int>();
            Instance.ClientFreeStatPoints = new EventProperty<int>();
            Instance.ClientCharactersList = new EventProperty<NetStruct<string, int>[]>();

            Instance.NetworkRoot = LidgrenRoot.CreateInstance<LidgrenRootClient>();
            Instance.NetworkRoot.Connect(serverIp, serverPort);
            Instance.NetworkRoot.transform.SetParent(WorldInstance.transform); 
            Instance.NetworkRoot.OnConnectedToServer += () => { Instance.NetworkRoot.RequestServerInfo(); Instance.OnConnectedToServer(); };
            Instance.NetworkRoot.OnDisconnectedFromServer += Instance.OnDisconnectedFromServer;
            Instance.NetworkRoot.OnServerInfoReceived += () => { Instance.LoadWorld((string)ConVar.S_Map.Value, () => { Instance.RPC.CL_CoreWorldLoaded.Call(); }); };
            Instance.OnRemoveWorldEntity += Instance.RemoveEntity;

            WorldInstance.Init();
        }

        public static void ProcessConsoleCommand(string cmd)
        {
            var data = cmd.Split(new[] { ' ' }, 2);
            if (data.Length == 0)
                return;

            ConVar target = ConVar.Get(data[0]);
            if (target == null)
                return;
            Debug.LogFormat("[ConVar] Execute \"{0}\"", cmd);
            if ((target.Type == ConVarType.ServerCommand || target.Type == ConVarType.ServerVariable) && Instance)
                Instance.NetworkRoot.SendConsoleCommand(cmd);
        }

        private NetworkBaseEntity CreateNetworkEntity(Type t, int entityId, EntityType type, bool hasFullData)
        {
            var gameObject = new GameObject(string.Format("[{0}]{1}", entityId, t));
            gameObject.transform.parent = transform;
            var result = gameObject.AddComponent(t) as NetworkBaseEntity;
            result.EntityId = entityId;
            result.HasFullData = hasFullData;
            result.EntityClassType = type;
            return result;
        }

        internal void ProcessImmediateSpawnMessage()
        {
            var entityId = NetworkReader.ReadInt32();
            NetworkBaseEntity nEntity;

            if (!networkEntities.TryGetValue(entityId, out nEntity))
            {
                var netType = NetworkDict.GetNetworkEntityType(NetworkReader.ReadByte());
                var entityType = NetworkReader.ReadInt16();
                int size = NetworkReader.ReadInt16();

                nEntity = CreateNetworkEntity(netType, entityId, (EntityType)entityType, true);
                nEntity.StreamRead();
                networkEntities[entityId] = nEntity;
                CreateWorldEntity(nEntity);
            }
        }

        internal void ProcessImmediateDestroyMessage()
        {
            var entityId = NetworkReader.ReadInt32();

            NetworkBaseEntity nEntity;

            if (networkEntities.TryGetValue(entityId, out nEntity) && nEntity.Owner != null)
            {
                nEntity.Owner.LockUpdate = true;
                nEntity.Owner.gameObject.SetActive(false);
            }
        }

        internal void ProcessEntityUpdateMessage()
        {
            var full = NetworkReader.ReadBool();
            var entitiesCount = NetworkReader.ReadInt16();
            var cellX = NetworkReader.ReadInt16();
            var cellY = NetworkReader.ReadInt16();
            var layerId = NetworkReader.ReadByte();
            var tick = NetworkReader.ReadUInt32();

            ulong cellHash = (((((ulong)0 | (ushort)cellX) << 16) | (ushort)cellY) << 8) | layerId;
            uint lastReceivedTick;

            if ((_receivedCellLayerTicks.TryGetValue(cellHash, out lastReceivedTick) && lastReceivedTick + 1 == tick) || full)
            {
                _receivedCellLayerTicks[cellHash] = tick;
                for (var i = 0; i < entitiesCount; i++)
                {
                    bool deltaData = false;
                    var entityId = NetworkReader.ReadInt32();
                    var netType = NetworkDict.GetNetworkEntityType(NetworkReader.ReadByte());
                    var entityType = NetworkReader.ReadInt16();
                    int size = NetworkReader.ReadInt16();
                    if (size < 0)
                    {
                        size = -size;
                        deltaData = true;
                    }

                    NetworkBaseEntity nEntity;

                    //create or update entity
                    if (!networkEntities.TryGetValue(entityId, out nEntity))
                    { 
                        nEntity = CreateNetworkEntity(netType, entityId, (EntityType)entityType, !deltaData);
                        networkEntities[entityId] = nEntity;
                        if (!deltaData)
                            _delayedEntityEvents.Enqueue(entityId);
                    }

                    nEntity.Tick = tick;
                    nEntity.StreamRead();

                    _entityUpdateTimeStamps[entityId] = Time.time;

                    if (!nEntity.HasFullData)
                    {
                        if (!deltaData)
                        {
                            nEntity.HasFullData = true;
                            _delayedEntityEvents.Enqueue(entityId);
                        }
                    }
                }
            }
            else 
            {
                Debug.LogFormat("[{0}/{1}]Snapshot dropped :( x: {2} y:{3} l:{4} hash:{5}", lastReceivedTick, tick, cellX, cellY, layerId, cellHash);
                float t;
                _fullCellDataRequestTimes.TryGetValue(cellHash, out t);
                if (t + 0.5f < Time.time)
                {
                    NetworkRoot.RequestFullSnapshot(cellX, cellY, layerId);
                    _fullCellDataRequestTimes[cellHash] = Time.time;
                }
            }

            //remove bad entities
            var badIds = _entityUpdateTimeStamps.Where(x => x.Value + ClientConstants.ENTITY_TIMEOUT < Time.time).ToArray();
            for (int i = 0; i < badIds.Length; i++)
                RemoveWorldEntity(worldEntities[badIds[i].Key]);
        }

        void RemoveEntity(WorldEntity entity)
        {
            _entityUpdateTimeStamps.Remove(entity.NetworkEntity.EntityId);
            Destroy(entity.NetworkEntity.gameObject);
        }

        protected override sealed void Update()
        {
            base.Update();

            for (var i = 0; i < ClientConstants.MAX_SPAWN_EVENTS_PER_UPDATE; i++)
            {
                if (_delayedEntityEvents.Count == 0)
                    break;

                var id = _delayedEntityEvents.Dequeue();
                if (networkEntities.ContainsKey(id))
                    CreateWorldEntity(networkEntities[id]);
            }
        }

        private void CreateWorldEntity(NetworkBaseEntity networkEntity)
        {
            if (!_entityCreators.ContainsKey(networkEntity.EntityClassType))
            {
                Debug.LogError(string.Format("[NetworkWorld] No factory method found for type {0}!", networkEntity.EntityClassType));
                return;
            }

            var entity = _entityCreators[networkEntity.EntityClassType](networkEntity);
            if (entity)
            {
                if(ControlledCharacter == null && networkEntity is NetworkCharacter)
                {
                    var networkCharacter = (NetworkCharacter)networkEntity;
                    if (networkCharacter.OwnerId.GetValue() == LidgrenRootClient.Instance.NetworkConnectionId)
                        ControlledCharacter = networkCharacter;
                }

                entity.NetworkEntity = networkEntity;

                entity.transform.position = entity.NetworkEntity.Position.GetValue();
                entity.transform.eulerAngles = entity.NetworkEntity.FullRotation.GetValue();

                entity.transform.SetParent(networkEntity.transform);
                AddWorldEntity(entity);
            }
        }

        protected void AddFactoryMethod(EntityType type, Func<NetworkBaseEntity, WorldEntity> function)
        {
            if (_entityCreators.ContainsKey(type))
            {
                Debug.LogError(string.Format("[NetworkWorld] Factory method for type {0} already exists!", type));
                return;
            }

            _entityCreators.Add(type, function);
        }
    }
}
