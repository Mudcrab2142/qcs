﻿namespace ClientLib
{
    internal static class ClientConstants
    {
        internal const float ENTITY_TIMEOUT = 10f;
        internal const int MAX_SPAWN_EVENTS_PER_UPDATE = 2;
    }
}
