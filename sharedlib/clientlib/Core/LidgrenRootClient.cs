﻿using System;
using Lidgren.Network;
using UnityEngine;
using SharedLib;
using System.Collections.Generic;

namespace ClientLib
{
    public sealed class LidgrenRootClient : LidgrenRoot
    {
        public static LidgrenRootClient Instance => LidgrenInstance as LidgrenRootClient;

        public event Action OnConnectedToServer;
        public event Action OnDisconnectedFromServer;
        public event Action OnServerInfoReceived;

        public long NetworkConnectionId { get; private set; }
        public static readonly EventProperty<int> BytesReceivedPerSecond = new EventProperty<int>();
        public static readonly EventProperty<int> BytesSentPerSecond = new EventProperty<int>();
        public static readonly EventProperty<int> PacketsReceivedPerSecond = new EventProperty<int>();
        public static readonly EventProperty<int> PacketsSentPerSecond = new EventProperty<int>();
        public override float ServerTime => _remoteServerTime + (Time.time - _infoReceivedTime);

        private float _remoteServerTime;
        private float _infoReceivedTime;

        private int _lastStatTime;
        private int _lastReceivedBytes;
        private int _lastSentBytes;
        private int _lastReceivedPackets;
        private int _lastSentPackets;

        internal void Connect(string ip, int port)
        {
            NetPeerConfiguration config = new NetPeerConfiguration("huskar666");
            InitConfig(ref config);

            netPeer = new NetClient(config);
            netPeer.Start();
            netPeer.Connect(ip, port);
        }

        protected override void OnConnectEvent(NetConnection connection)
        {
            if (OnConnectedToServer != null)
                OnConnectedToServer();
        }

        protected override void OnDisconnectEvent(NetConnection connection)
        {
            if (OnDisconnectedFromServer != null)
                OnDisconnectedFromServer();
        }

        protected override void OnDataEvent(NetConnection connection, NetworkMessageType type)
        {
            base.OnDataEvent(connection, type);
            switch (type)
            {
                case NetworkMessageType.ServerEntityStateUpdate:
                    NetworkClientWorld.Instance.ProcessEntityUpdateMessage();
                    break;
                case NetworkMessageType.ServerEntityImmediateSpawn:
                    NetworkClientWorld.Instance.ProcessImmediateSpawnMessage();
                    break;
                case NetworkMessageType.ServerEntityImmediateDestroy:
                    NetworkClientWorld.Instance.ProcessImmediateDestroyMessage();
                    break;
                case NetworkMessageType.ServerInfo:
                    NetworkConnectionId = NetworkReader.ReadInt64();

                    _infoReceivedTime = Time.time;
                    _remoteServerTime = NetworkReader.ReadSingle() + connection.AverageRoundtripTime / 2f;

                    ParseServerInfo();
                    CommonHelper.SafeCall(OnServerInfoReceived);
                    break;
                case NetworkMessageType.RCONUpdateVariable:
                    var variableName = NetworkReader.ReadString();
                    var variableValue = NetworkReader.ReadString();

                    ConVar target = ConVar.Get(variableName);
                    if (target != null)
                        target.SetSerializedValue(variableName, true);
                    else
                        Debug.LogErrorFormat("[ConVar] Error setting value {0}:{1}. Variable not found!", variableName, variableValue);
                    break;
            }
        }

        protected override void Update()
        {
            base.Update();

            var intTime = (int)Time.time;
            if (_lastStatTime != (int)Time.time)
            {
                _lastStatTime = intTime;

                BytesReceivedPerSecond.Value = netPeer.Statistics.ReceivedBytes - _lastReceivedBytes;
                BytesSentPerSecond.Value = netPeer.Statistics.SentBytes - _lastSentBytes;
                PacketsReceivedPerSecond.Value = netPeer.Statistics.ReceivedPackets - _lastReceivedPackets;
                PacketsSentPerSecond.Value = netPeer.Statistics.SentPackets - _lastSentPackets;

                _lastReceivedBytes = netPeer.Statistics.ReceivedBytes;
                _lastSentBytes = netPeer.Statistics.SentBytes;
                _lastSentPackets = netPeer.Statistics.SentPackets;
                _lastReceivedPackets = netPeer.Statistics.ReceivedPackets;
            }
        }

        internal void RequestFullSnapshot(short x, short y, byte layer)
        {
            DefaultSendBuffer.BeginMessage(NetworkMessageType.ClientRequestFullSnapshot);
            DefaultSendBuffer.WriteInt16(x);
            DefaultSendBuffer.WriteInt16(y);
            DefaultSendBuffer.WriteByte(layer);
            DefaultSendBuffer.EndMessage();

            SendToServer(DefaultSendBuffer, NetDeliveryMethod.ReliableOrdered);
        }

        /// <summary>
        /// Send ServerInfo request
        /// </summary>
        internal void RequestServerInfo(string password = "")
        {
            DefaultSendBuffer.BeginMessage(NetworkMessageType.ClientRequestServerInfo);
            DefaultSendBuffer.WriteString(password);
            DefaultSendBuffer.EndMessage();

            SendToServer(DefaultSendBuffer, NetDeliveryMethod.ReliableOrdered);
        }

        internal void SendConsoleCommand(string data)
        {
            DefaultSendBuffer.BeginMessage(NetworkMessageType.RCONCommand);
            DefaultSendBuffer.WriteString(data);
            DefaultSendBuffer.EndMessage();

            SendToServer(DefaultSendBuffer, NetDeliveryMethod.ReliableOrdered);
        }

        private void ParseServerInfo()
        {
            var count = NetworkReader.ReadInt16();
            for (var i = 0; i < count; i++)
            {
                var targetName = NetworkReader.ReadString();
                var targetValue = NetworkReader.ReadString();

                ConVar target = ConVar.Get(targetName);
                if (target != null)
                    target.SetSerializedValue(targetValue);
                else
                    Debug.LogErrorFormat("[ConVar] Error setting value {0}:{1}. Variable not found!", targetName, targetValue);
            }
        }

        private void SendToServer(NetworkSendBuffer buf, NetDeliveryMethod deliveryType)
        {
            var netClient = (NetClient)netPeer;
            var msg = netClient.CreateMessage();
            msg.Write(buf.SendBuffer, 0, buf.CurrentPosition);
            netClient.SendMessage(msg, deliveryType);
            netClient.FlushSendQueue();
        }

        internal override void NetFuncSendDirectCall(NetworkSendBuffer data, NetConnection targetConnection = null)
        {
            SendToServer(data, NetDeliveryMethod.ReliableOrdered);
        }

        internal override void NetFuncBroadcastCall(NetworkSendBuffer data, Vector3 position, float areaSize)
        {
            throw new NotImplementedException("[NetworkFunction] Client can not broadcast functions :(");
        }
    }
}
