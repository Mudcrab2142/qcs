﻿using SharedLib;
using Lidgren.Network;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ClientLib
{
    public abstract class ClientNetFuncStorage : NetFuncStorage
    {
        public ClientNetFuncStorage(LidgrenRoot root) : base(root)
        {
            S_CoreRequestClientInfo.SetHandler(CoreRequestClientInfo);
            S_CoreSendCharactersList.SetHandler(CoreSendCharactersList);

            S_InventoryUpdate.SetHandler(InventoryUpdate);
            S_MainStatsUpdate.SetHandler(MainStatsUpdate);
            S_SecondaryStatsUpdate.SetHandler(SecondaryStatsUpdate);
            S_CraftStatsUpdate.SetHandler(CraftStatsUpdate);
            S_EquipmentUpdateSlot.SetHandler(EquipmentUpdateSlot);

            S_FreeStatPointsUpdate.SetHandler(FreeStatPointsUpdate);
            S_ExperienceUpdate.SetHandler(ExperienceUpdate);

            S_SpellUpdateData.SetHandler(SpellUpdateData);
            S_SpellUpdateFreePoints.SetHandler(SpellUpdateFreePoints);

            S_EffectAdd.SetHandler(EffectAdd);
            S_EffectRemove.SetHandler(EffectRemove);

            S_CraftRecipeListUpdate.SetHandler(CraftRecipeListUpdate);
            S_CraftSessionListUpdate.SetHandler(CraftSessionListUpdate);
        }

        private NetStruct<string, int> CoreRequestClientInfo(NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return new NetStruct<string, int>("null", -1);

            return new NetStruct<string, int>("test", 1);
        }

        private void CoreSendCharactersList(NetStruct<string, int>[] data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            Debug.LogFormat("Characters list received! Count = {0}.", data.Length);
            NetworkClientWorld.Instance.ClientCharactersList.Value = data;
        }

        private void InventoryUpdate(InventoryData data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientInventory.Update(data);
        }

        private void MainStatsUpdate(List<StatValue> data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientStats.UpdateMainStats(data);
        }

        private void SecondaryStatsUpdate(List<StatValue> data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientStats.UpdateSecondaryStats(data);
        }

        private void CraftStatsUpdate(List<StatValue> data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientStats.UpdateCraftStats(data);
        }

        private void EquipmentUpdateSlot(NetStruct<byte, ItemInstance> arg1, NetConnection arg2)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientEquipment.UpdateSlot(arg1.Value1, arg1.Value2);
        }

        private void FreeStatPointsUpdate(int data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientFreeStatPoints.Value = data;
        }

        private void ExperienceUpdate(int data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientExp.Value = data;
        }

        private void SpellUpdateData(Dictionary<SpellName, byte> data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientSpells.UpdateLevel(data);
        }

        private void SpellUpdateFreePoints(int data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientSpells.FreePoints.Value = data;
        }

        private void EffectAdd(EffectInfo data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientEffects.AddEffect(data);
        }

        private void EffectRemove(int data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientEffects.RemoveEffect(data);
        }

        private void CraftRecipeListUpdate(HashSet<CraftRecipeId> data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientCraft.UpdateKnownRecipes(data);
        }

        private void CraftSessionListUpdate(List<CraftSession> data, NetConnection connection)
        {
            if (!NetworkClientWorld.Instance)
                return;

            NetworkClientWorld.Instance.ClientCraft.UpdateCraftSessions(data);
        }
    }
}
