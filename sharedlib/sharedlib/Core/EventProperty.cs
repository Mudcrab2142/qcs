﻿using System;

namespace SharedLib
{
    public class EventProperty<T>
    {
        public event Action<T> OnValueChanged;
        private T _value;

        public T Value
        {
            get { return _value; }
            set
            {
                _value = value;
                if (OnValueChanged != null)
                    OnValueChanged(value);
            }
        }

        public static implicit operator T (EventProperty<T> d)
        {
            return d._value;
        }
    }
}
