﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SharedLib
{
    public class ConVar
    {
        private static Dictionary<string, ConVar> _variables = new Dictionary<string, ConVar>();
        internal static IEnumerable<ConVar> Variables => _variables.Values;

        #region Variables
        public static readonly ConVar S_CraftSpeedRate = ServerFloatVariable("sv_craftspeedrate", 1f, ConVarFlags.ServerInfo);
        public static readonly ConVar S_Map = ServerStringVariable("map", "Test", ConVarFlags.InitOnly | ConVarFlags.ServerInfo);
        public static readonly ConVar S_MaxPlayers = ServerIntVariable("max_players", 100, ConVarFlags.InitOnly | ConVarFlags.ServerInfo);
        public static readonly ConVar S_Password = ServerStringVariable("password", "");

        public static readonly ConVar CMD_AddItem = ServerCommand("add_item");
        public static readonly ConVar CMD_Kill = ServerCommand("kill");
        #endregion

        public static ConVar Get(string name)
        {
            ConVar result;

            if (_variables.TryGetValue(name, out result))
                return result;

            return null;
        }

        static ConVar ServerCommand(string name)
        {
            return new ConVar(ConVarType.ServerCommand, name);
        }

        static ConVar ServerIntVariable(string name, int defaultValue, ConVarFlags flags = ConVarFlags.None)
        {
            return new ConVar(ConVarType.ServerVariable, ConVarValueType.Int, name, defaultValue);
        }

        static ConVar ServerFloatVariable(string name, float defaultValue, ConVarFlags flags = ConVarFlags.None)
        {
            return new ConVar(ConVarType.ServerVariable, ConVarValueType.Float, name, defaultValue);
        }

        static ConVar ServerStringVariable(string name, string defaultValue, ConVarFlags flags = ConVarFlags.None)
        {
            return new ConVar(ConVarType.ServerVariable, ConVarValueType.String, name, defaultValue);
        }

        public readonly ConVarType Type;
        public readonly ConVarValueType ValueType;
        public readonly ConVarFlags Flags;
        public readonly string Name;
        public event Action<string, string> OnValueUpdate;

        private Action<string, NetConnection> _action;
        private object _storagedValue;
        private string _storagedStringValue;

        public object Value => _storagedValue;
        public string ValueString => _storagedStringValue;

        private ConVar(ConVarType type, string name)
        {
            Type = type;
            Name = name;

            if (_variables.ContainsKey(name))
            {
                Debug.LogErrorFormat("[ConVar] {0} dublicate!", name);
                return;
            }

            _variables.Add(name, this);
        }

        private ConVar(ConVarType type, ConVarValueType valueType, string name, object defaultValue, ConVarFlags flags = ConVarFlags.None) : this(type, name)
        {
            ValueType = valueType;
            _storagedValue = defaultValue;
            Flags = flags;
        }

        public void Invoke(string args, NetConnection connection)
        {
            if (_action != null)
                _action.Invoke(args, connection);
        }

        public void SetSerializedValue(string data, bool skipFlags = false)
        {
            switch (ValueType)
            {
                case ConVarValueType.Int:
                    int intValue;
                    if (int.TryParse(data, out intValue))
                        SetValueInternal(data, ConVarValueType.Int, skipFlags);
                    break;
                case ConVarValueType.Float:
                    float floatValue;
                    if (float.TryParse(data, out floatValue))
                        SetValueInternal(data, ConVarValueType.Float, skipFlags);
                    break;
                case ConVarValueType.String:
                    SetValueInternal(data, ConVarValueType.String, skipFlags);
                    break;
            }
        }

        public void SetValue(int data)
        {
            SetValueInternal(data, ConVarValueType.Int, false);
        }

        public void SetValue(float data)
        {
            SetValueInternal(data, ConVarValueType.Float, false);
        }

        public void SetValue(string data)
        {
            SetValueInternal(data, ConVarValueType.String, false);
        }

        void SetValueInternal(object value, ConVarValueType valueType, bool skipFlags)
        {
            if(!skipFlags && (Flags & (~ConVarFlags.InitOnly)) == 0)
            {
                Debug.LogWarningFormat("[ConVar] {0} SetValue aborted! Tried to set InitOnly variable.", Name);
                return;
            }

            if (ValueType != valueType)
            {
                Debug.LogWarningFormat("[ConVar] {0} SetValue aborted! Invalid set value type.", Name);
                return;
            }
            _storagedStringValue = value.ToString();
            _storagedValue = value;
            Debug.LogFormat("[ConVar] {0} changed to {1}.", Name, _storagedValue);
            CommonHelper.SafeCall(OnValueUpdate, Name, _storagedStringValue);
        }
        
        public void SetHandler(Action<string, NetConnection> handler)
        {
            _action = handler;
        }
    }
}
