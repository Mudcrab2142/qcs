﻿namespace SharedLib
{
    public enum ConVarType
    {
        ClientVariable,
        ServerVariable,
        ServerCommand
    }
}
