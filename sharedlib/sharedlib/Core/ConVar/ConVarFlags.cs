﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedLib
{
    [Flags]
    public enum ConVarFlags
    {
        None,
        InitOnly,
        ServerInfo,
    }
}
