﻿namespace SharedLib
{
    public enum ConVarValueType
    {
        Null,
        Int,
        Float,
        String
    }
}
