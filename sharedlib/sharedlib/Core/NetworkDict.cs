﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SharedLib
{
    internal static class NetworkDict
    {
        private readonly static Dictionary<Type, byte> _typeIdDict = new Dictionary<Type, byte>();        // 0 - undefined
        private readonly static Dictionary<byte, Type> _idTypeDict = new Dictionary<byte, Type>();

        private static bool _initialized;

        static void Init()
        {
            _typeIdDict.Add(typeof(NetworkBaseEntity), 0);
            _idTypeDict.Add(0, typeof(NetworkBaseEntity));
            foreach (var type in Assembly.GetCallingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(NetworkBaseEntity))))
            {
                var id = (byte)(_typeIdDict.Count + 1);
                _typeIdDict.Add(type, id);
                _idTypeDict.Add(id, type);
            }

            _initialized = true;
        }

        static void CheckInit()
        {
            if (!_initialized)
                Init();
        }

        public static Type GetNetworkEntityType(byte id)
        {
            CheckInit();
            return _idTypeDict[id];
        }

        public static byte GetNetworkEntityTypeId(Type type)
        {
            CheckInit();
            return _typeIdDict[type];
        }
    }
}
