﻿using System;
using System.Text;

namespace SharedLib
{
    /// <summary>
    /// Deserialize methods from UNet library
    /// </summary>
    public static class NetworkReader
    {
        const int MAX_STRING_LENGTH = 1024 * 32;

        private static byte[] _buffer;
        private static byte[] _stringReaderBuffer = new byte[MAX_STRING_LENGTH];
        internal static int Position { get; private set; }
        private static UIntFloat _floatConverter = new UIntFloat();
        private static Encoding _encoding = new UTF8Encoding();

        public static void SetBuffer(byte[] buf)
        {
            _buffer = buf;
            Position = 0;
        }

        internal static byte[] GetBuffer()
        {
            return _buffer;
        }

        public static int ReadInt32()
        {
            uint value = 0;
            value |= ReadByte();
            value |= (uint)(ReadByte() << 8);
            value |= (uint)(ReadByte() << 16);
            value |= (uint)(ReadByte() << 24);
            return (int)value;
        }

        public static float ReadSingle()
        {
            _floatConverter.uintValue = ReadUInt32();
            return _floatConverter.floatValue;
        }

        public static bool ReadBool()
        {
            return ReadByte() == 1;
        }

        public static short ReadInt16()
        {
            ushort value = 0;
            value |= ReadByte();
            value |= (ushort)(ReadByte() << 8);
            return (short)value;
        }

        public static ushort ReadUInt16()
        {
            ushort value = 0;
            value |= ReadByte();
            value |= (ushort)(ReadByte() << 8);
            return value;
        }

        public static uint ReadUInt32()
        {
            byte a0 = ReadByte();
            if (a0 < 241)
                return a0;

            byte a1 = ReadByte();
            if (a0 >= 241 && a0 <= 248)
                return (uint)(240 + 256 * (a0 - 241) + a1);

            byte a2 = ReadByte();
            if (a0 == 249)
                return (uint)(2288 + 256 * a1 + a2);

            byte a3 = ReadByte();
            if (a0 == 250)
                return a1 + (((uint)a2) << 8) + (((uint)a3) << 16);

            byte a4 = ReadByte();
            if (a0 >= 251)
                return a1 + (((uint)a2) << 8) + (((uint)a3) << 16) + (((uint)a4) << 24);

            throw new IndexOutOfRangeException("ReadPackedUInt32() failure: " + a0);
        }

        public static long ReadInt64()
        {
            ulong value = 0;

            ulong other = ReadByte();
            value |= other;

            other = ((ulong)ReadByte()) << 8;
            value |= other;

            other = ((ulong)ReadByte()) << 16;
            value |= other;

            other = ((ulong)ReadByte()) << 24;
            value |= other;

            other = ((ulong)ReadByte()) << 32;
            value |= other;

            other = ((ulong)ReadByte()) << 40;
            value |= other;

            other = ((ulong)ReadByte()) << 48;
            value |= other;

            other = ((ulong)ReadByte()) << 56;
            value |= other;

            return (long)value;
        }

        public static string ReadString()
        {
            var numBytes = ReadUInt16();
            if (numBytes == 0)
                return "";

            if (numBytes >= MAX_STRING_LENGTH)
            {
                throw new IndexOutOfRangeException("ReadString() too long: " + numBytes);
            }

            ReadBytes(_stringReaderBuffer, numBytes);

            char[] chars = _encoding.GetChars(_stringReaderBuffer, 0, numBytes);
            return new string(chars);
        }

        public static void ReadBytes(byte[] buffer, int count)
        {
            if (Position + count > _buffer.Length)
            {
                throw new IndexOutOfRangeException("NetworkReader: ReadBytes out of range: (" + count + ")");
            }

            for (ushort i = 0; i < count; i++)
            {
                buffer[i] = _buffer[Position + i];
            }
            Position += count;
        }

        public static byte[] ReadByteArray()
        {
            var size = ReadInt32();
            var result = new byte[size];
            ReadBytes(result, size);
            return result;
        }

        public static byte ReadByte()
        {
            if (Position >= _buffer.Length)
            {
                throw new IndexOutOfRangeException("NetworkReader: ReadByte out of range");
            }
            return _buffer[Position++];
        }
    }
}
