﻿namespace SharedLib
{
    public enum NetworkMessageType : byte
    {
        Null,

        ServerEntityStateUpdate,
        ServerEntityImmediateSpawn,
        ServerEntityImmediateDestroy,
        NetFuncCall,

        RCONCommand,
        RCONSetVariable,
        RCONUpdateVariable,

        ClientRequestServerInfo,
        ClientRequestFullSnapshot,
        ServerInfo,

        CompressedMessage
    }
}
