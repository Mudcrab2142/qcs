﻿using System;

namespace SharedLib
{
    public static class CommonHelper
    {
        public static void SafeCall(Action action)
        {
            if (action != null)
                action();
        }

        public static void SafeCall<T>(Action<T> action, T arg1)
        {
            if (action != null)
                action(arg1);
        }

        public static void SafeCall<T, T2>(Action<T, T2> action, T arg1, T2 arg2)
        {
            if (action != null)
                action(arg1, arg2);
        }
    }
}
