﻿namespace SharedLib
{
    public class NetworkBool : NetworkVariable<bool>
    {
        public NetworkBool(bool value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadBool());
        }

        protected override bool NetEquals(bool value)
        {
            return value == storagedValue;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteBool(storagedValue);
        }
    }
}
