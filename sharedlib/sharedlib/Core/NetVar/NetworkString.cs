﻿namespace SharedLib
{
    public class NetworkString : NetworkVariable<string>
    {
        public NetworkString(string value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadString());
        }

        protected override bool NetEquals(string value)
        {
            return storagedValue == value;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteString(storagedValue);
        }
    }
}
