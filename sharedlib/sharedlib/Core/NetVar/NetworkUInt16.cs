﻿using UnityEngine;

namespace SharedLib
{
    public class NetworkUInt16 : NetworkVariable<ushort>
    {
        public NetworkUInt16(ushort value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadUInt16());
        }

        protected override bool NetEquals(ushort value)
        {
            return value == storagedValue;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteUInt16(storagedValue);
        }

        protected override bool PEquals(ushort value1, ushort value2, float error)
        {
            return Mathf.Abs(value1 - value2) < error;
        }
    }
}
