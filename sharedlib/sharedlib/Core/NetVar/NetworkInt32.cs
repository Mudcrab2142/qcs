﻿namespace SharedLib
{
    public sealed class NetworkInt32 : NetworkVariable<int>
    {
        public NetworkInt32(int value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadInt32());
        }

        protected override bool NetEquals(int value)
        {
            return value == storagedValue;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteInt32(storagedValue);
        }
    }
}
