﻿using System;

namespace SharedLib
{
    public class NetworkSingle : NetworkVariable<float>
    {
        public NetworkSingle(float value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadSingle());
        }

        protected override bool NetEquals(float value)
        {
            return value == storagedValue;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteSingle(storagedValue);
        }

        protected override bool PEquals(float value1, float value2, float error)
        {
            return Math.Abs(value2 - value1) <= error;
        }
    }
}
