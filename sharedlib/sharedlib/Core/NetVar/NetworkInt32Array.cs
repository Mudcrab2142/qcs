﻿namespace SharedLib
{
    public class NetworkInt32Array : NetworkVariable<int[]>
    {
        public NetworkInt32Array(int[] value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            var data = new int[NetworkReader.ReadInt32()];
            for (int i = 0; i < data.Length; i++)
                data[i] = NetworkReader.ReadInt32();
            SetValue(data);
        }

        protected override bool NetEquals(int[] value)
        {
            if (value.Length != storagedValue.Length)
                return false;

            for (int i = 0; i < value.Length; i++)
                if (value[i] != storagedValue[i])
                    return false;

            return true;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteInt32(storagedValue.Length);
            for (int i = 0; i < storagedValue.Length; i++)
                buf.WriteInt32(storagedValue[i]);
        }
    }
}
