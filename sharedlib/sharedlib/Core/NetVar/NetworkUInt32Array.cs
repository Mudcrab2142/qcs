﻿namespace SharedLib
{
    internal class NetworkUInt32Array : NetworkVariable<uint[]>
    {
        public NetworkUInt32Array(uint[] value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            var data = new uint[NetworkReader.ReadInt32()];
            for (int i = 0; i < data.Length; i++)
                data[i] = NetworkReader.ReadUInt32();
            SetValue(data);
        }

        protected override bool NetEquals(uint[] value)
        {
            if (value.Length != storagedValue.Length)
                return false;

            for (int i = 0; i < value.Length; i++)
                if (value[i] != storagedValue[i])
                    return false;

            return true;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteInt32(storagedValue.Length);
            for (int i = 0; i < storagedValue.Length; i++)
                buf.WriteUInt32(storagedValue[i]);
        }
    }
}
