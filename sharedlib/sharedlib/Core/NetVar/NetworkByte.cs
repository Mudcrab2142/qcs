﻿namespace SharedLib
{
    public class NetworkByte : NetworkVariable<byte>
    {
        public NetworkByte(byte value) : base()
        {
            storagedValue = value;
        }

        protected override bool NetEquals(byte value)
        {
            return value == storagedValue;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadByte());
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteByte(storagedValue);
        }
    }
}
