﻿using System;
using UnityEngine;

namespace SharedLib
{
    public class NetworkVector3 : NetworkVariable<Vector3>
    {
        private float _minThreshold = 0.01f;

        public NetworkVector3(Vector3 value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(new Vector3(NetworkReader.ReadSingle(), NetworkReader.ReadSingle(), NetworkReader.ReadSingle()));
        }

        internal void SetMinThreshold(float value)
        {
            _minThreshold = value;
        }

        protected override bool NetEquals(Vector3 value)
        {
            return Math.Abs(value.x - storagedValue.x) < _minThreshold && Math.Abs(value.y - storagedValue.y) < _minThreshold && Math.Abs(value.z - storagedValue.z) < _minThreshold;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteSingle(storagedValue.x);
            buf.WriteSingle(storagedValue.y);
            buf.WriteSingle(storagedValue.z);
        }

        protected override bool PEquals(Vector3 value1, Vector3 value2, float error)
        {
            return Vector3.Distance(value1, value2) < error;
        }
    }
}
