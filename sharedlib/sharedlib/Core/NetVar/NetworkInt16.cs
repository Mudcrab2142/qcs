﻿namespace SharedLib
{
    public class NetworkInt16 : NetworkVariable<short>
    {
        public NetworkInt16(short value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadInt16());
        }

        protected override bool NetEquals(short value)
        {
            return value == storagedValue;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteInt16(storagedValue);
        }
    }
}
