﻿namespace SharedLib
{
    public class NetworkUInt32 : NetworkVariable<uint>
    {
        public NetworkUInt32(uint value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadUInt32());
        }

        protected override bool NetEquals(uint value)
        {
            return storagedValue == value;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteUInt32(storagedValue);
        }
    }
}
