﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SharedLib
{
    public abstract class NetworkVariable
    {
        public event Action<NetworkVariable> OnValueChanged;
        public NetworkBaseEntity Owner { get; private set; }

        private bool _isDirty;

        public bool ShouldSend => _isDirty;

        public abstract bool IsPredicted { get; }

        protected NetworkVariable()
        {
            OnValueChanged += (n) => SetDirty(true);
            SetDirty(true);
        }
        
        internal void SetOwner(NetworkBaseEntity owner)
        {
            Owner = owner;
        }

        protected void ValueChanged()
        {
            CommonHelper.SafeCall(OnValueChanged, this);
        }

        internal void SetDirty(bool flag)
        {
            _isDirty = flag;
        }
        
        public abstract void NetworkRead();

        internal void NetworkWrite(NetworkSendBuffer targetBuffer, bool forceWrite)
        {
            Write(targetBuffer);
            if (!forceWrite)
            {
                SetDirty(false);
            }      
        }

        public abstract void UpdatePredictHistory();

        protected abstract void Write(NetworkSendBuffer targetBuffer);
    }

    public abstract class NetworkVariable<T> : NetworkVariable 
    {
        public event Action<T> OnPredictionFailed;

        private const int MAX_PREDICTION_FAILED_TICKS = 3;

        protected T storagedValue;
        private T _lastReceivedValue;

        public override bool IsPredicted => _predicted;
        private float _maxError; 
        private bool _predicted;
        private SortedDictionary<uint, T> _receiveHistory;
        private SortedDictionary<uint, T> _predictHistory;

        public void SetValue(T value)
        {
            if (_predicted)
            {
                _lastReceivedValue = value;
                _receiveHistory[Owner.Tick] = value;
                return;
            }

            if (NetEquals(value))
                return;

            storagedValue = value;
            ValueChanged();
        }

        public void EnablePrediction(float maxError)
        {
            _predicted = true;
            _receiveHistory = _receiveHistory ?? new SortedDictionary<uint, T>();
            _predictHistory = _predictHistory ?? new SortedDictionary<uint, T>();
            _maxError = maxError;
            Owner.Owner.UpdateAction += PredictionUpdate;
        }

        public void DisablePrediction()
        {
            if (!_predicted)
                return;

            _predicted = false;
            _receiveHistory.Clear();
            _predictHistory.Clear();
            Owner.Owner.UpdateAction -= PredictionUpdate;
        }

        public override void UpdatePredictHistory()
        {
            _receiveHistory[Owner.Tick] = _lastReceivedValue;
        }

        public T GetLastServerValue()
        {
            if (_receiveHistory.Count == 0)
                return default(T);

            return _receiveHistory.Last().Value;
        }

        private unsafe void PredictionUpdate()
        {
          //  Debug.LogFormat("PredictedValue: {0}. ServerValue: {1}. PredictHistorySize: {2}.", storagedValue, _receiveHistory.Count > 0 ? _receiveHistory.Last().Value.ToString() : "none", _predictHistory.Count);

            const int MAX_CHECKS_PER_FRAME = 10;
            uint* removeList = stackalloc uint[MAX_CHECKS_PER_FRAME];

            var checks = 0;

            foreach (var item in _predictHistory)
            {
                if (checks >= MAX_CHECKS_PER_FRAME)
                    break;
                
                var tries = 0;
                bool success = false;
                
                for (var i = 0; i < MAX_PREDICTION_FAILED_TICKS; i++)
                {
                    var predictedTick = (uint)(item.Key + i + 1);

                    T compareValue;
                    if (_receiveHistory.TryGetValue(predictedTick, out compareValue))
                    {
                        if (PEquals(compareValue, item.Value, _maxError))
                            success = true;
                        tries++;
                    }
                    else if ((int)Owner.Tick - predictedTick > 10)      //no problemmos, remove this shiet
                        success = true;
                }

                if (success)
                {
                    removeList[checks] = item.Key;
                }

                //prediction failed, set last server variable
                if (tries >= MAX_PREDICTION_FAILED_TICKS && !success)
                {
                    Debug.LogFormat("[NetworkVariable] Prediction failed. PredictedValue = {0}. Reverted to last server value = {1}.", item.Value, _receiveHistory.Last().Value);
                   /* for (var i = 0; i < MAX_PREDICTION_FAILED_TICKS; i++)
                    {
                        var predictedTick = (uint)(item.Key + i + 1);

                        T compareValue;
                        if (_receiveHistory.TryGetValue(predictedTick, out compareValue))
                            Debug.Log(compareValue);
                    }*/

                    if (_receiveHistory.Count > 0)
                    {
                        storagedValue = _receiveHistory.Last().Value;
                        CommonHelper.SafeCall(OnPredictionFailed, storagedValue);
                        ValueChanged();
                    }

                    _receiveHistory.Clear();
                    _predictHistory.Clear();
                    break;
                }
                checks++;
            }

            for (var i = 0; i < MAX_CHECKS_PER_FRAME; i++)
            {
                if (removeList[i] == 0)
                    continue;

                _predictHistory.Remove(removeList[i]);
            }
        }

        public void SetPredictedValue(T value)
        {
            if (!_predicted)
            {
                Debug.LogWarning("[NetworkVariable] Trying to predict not predicted variable!");
                return;
            }

            storagedValue = value;
            _predictHistory[Owner.Tick] = value;
            ValueChanged();
        }

        public T GetValue()
        {
            return storagedValue;
        }

        protected abstract bool NetEquals(T value);

        protected virtual bool PEquals(T value1, T value2, float error)
        {
            return true;
        }
    }
}
