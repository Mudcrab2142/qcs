﻿namespace SharedLib
{
    public class NetworkByteArray : NetworkVariable<byte[]>
    {
        public NetworkByteArray(byte[] value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            var data = new byte[NetworkReader.ReadInt32()];
            for (int i = 0; i < data.Length; i++)
                data[i] = NetworkReader.ReadByte();
            SetValue(data);
        }

        protected override bool NetEquals(byte[] value)
        {
            if (value.Length != storagedValue.Length)
                return false;

            for (int i = 0; i < value.Length; i++)
                if (value[i] != storagedValue[i])
                    return false;

            return true;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteInt32(storagedValue.Length);
            for (int i = 0; i < storagedValue.Length; i++)
                buf.WriteByte(storagedValue[i]);
        }
    }
}
