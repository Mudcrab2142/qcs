﻿namespace SharedLib
{
    public sealed class NetworkInt64 : NetworkVariable<long>
    {
        public NetworkInt64(long value) : base()
        {
            storagedValue = value;
        }

        public override void NetworkRead()
        {
            SetValue(NetworkReader.ReadInt64());
        }

        protected override bool NetEquals(long value)
        {
            return value == storagedValue;
        }

        protected override void Write(NetworkSendBuffer buf)
        {
            buf.WriteInt64(storagedValue);
        }
    }
}
