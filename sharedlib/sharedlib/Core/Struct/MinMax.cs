﻿using ProtoBuf;
using System;

namespace SharedLib
{
    [ProtoContract]
    public struct MinMax<T> : IEquatable<MinMax<T>> where T : IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        [ProtoMember(1)]
        public T Min;
        [ProtoMember(2)]
        public T Max;

        public MinMax(T min, T max)
        {
            Min = min;
            Max = max;
        }

        public bool Equals(MinMax<T> other)
        {
            return other.Min.Equals(Min) && other.Max.Equals(Max);
        }
    }
}
