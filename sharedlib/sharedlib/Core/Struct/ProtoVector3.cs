﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct ProtoVector3
    {
        public float X;
        public float Y;
        public float Z;

        public ProtoVector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static implicit operator UnityEngine.Vector3(ProtoVector3 v)
        {
            return new UnityEngine.Vector3(v.X, v.Y, v.Z);
        }

        public static implicit operator ProtoVector3(UnityEngine.Vector3 v)
        {
            return new ProtoVector3(v.x, v.y, v.z);
        }

    }
}
