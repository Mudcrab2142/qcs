﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct NetStruct<T1, T2>
    {
        public T1 Value1;
        public T2 Value2;

        public NetStruct(T1 value1, T2 value2)
        {
            Value1 = value1;
            Value2 = value2;
        }
    }

    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct NetStruct<T1, T2, T3>
    {
        public T1 Value1;
        public T2 Value2;
        public T3 Value3;

        public NetStruct(T1 value1, T2 value2, T3 value3)
        {
            Value1 = value1;
            Value2 = value2;
            Value3 = value3;
        }
    }

    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct NetStruct<T1, T2, T3, T4>
    {
        public T1 Value1;
        public T2 Value2;
        public T3 Value3;
        public T4 Value4;

        public NetStruct(T1 value1, T2 value2, T3 value3, T4 value4)
        {
            Value1 = value1;
            Value2 = value2;
            Value3 = value3;
            Value4 = value4;
        }
    }
}
