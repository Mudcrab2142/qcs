﻿using System;
using System.Linq;
using UnityEngine;

namespace SharedLib
{
    public class NetworkBaseEntity : MonoBehaviour
    {
        public const byte MAX_NETWORK_VARIABLES = 64;

        [HideInInspector]
        public int EntityId { get; internal set; }
        [HideInInspector]
        public bool HasFullData { get; internal set; }    //client only
        [HideInInspector]
        public bool NetworkEntity { get; internal set; }  //server only

        public Type NetworkClassType { get; private set; }
        public EntityType EntityClassType { get; internal set; }
        private readonly NetworkVariable[] _bindedVariables = new NetworkVariable[MAX_NETWORK_VARIABLES];
        private byte _bindCount;
        private bool _firstSend = true;

        public WorldEntity Owner { get; internal set; }

        public NetworkVector3 Position = new NetworkVector3(Vector3.zero);
        public NetworkVector3 FullRotation = new NetworkVector3(Vector3.zero);
        public NetworkSingle SimpleRotation = new NetworkSingle(0);

        public virtual bool UseSimpleRotation => false;
        public virtual bool UseNetworkCells => true;

        //Only for server optimizations
        internal short CellX;
        internal short CellY;
        internal bool InsideCell;
        //--------------------------------
        //Client only
        internal uint Tick;

        void Awake()
        {
            InitVariables();
            NetworkClassType = GetType();
        }

        internal void StreamWrite(NetworkSendBuffer buf, bool forceWrite = false)
        {
            //EntityHeader
            buf.WriteInt32(EntityId);
            buf.WriteByte(NetworkDict.GetNetworkEntityTypeId(NetworkClassType));
            buf.WriteInt16((short)EntityClassType);
            buf.Seek(buf.CurrentPosition + 2);        //leave place for data length
            var dataStartPos = buf.CurrentPosition;

            WriteVariablesBitMask(buf, forceWrite);

            for (byte i = 0; i < _bindCount; i++)
                if (_bindedVariables[i].ShouldSend || forceWrite)
                    _bindedVariables[i].NetworkWrite(buf, forceWrite);

            var dataLength = (short)(buf.CurrentPosition - dataStartPos);

            buf.Seek(dataStartPos - 2);
            buf.WriteInt16((_firstSend || forceWrite) ? dataLength : (short)(-dataLength));
            buf.Seek(dataStartPos + dataLength);

            if (_firstSend)
                _firstSend = false;
        }

        internal void ResetVariableFlags()
        {
            for (var i = 0; i < _bindCount; i++)
                _bindedVariables[i].SetDirty(true);
        }

        public void StreamRead()
        {
            var bitMask = ReadVariablesBitMask();
            for (byte i = 0; i < _bindCount; i++)
            {
                var variable = _bindedVariables[i];
                if ((bitMask & (1 << i)) != 0)
                    variable.NetworkRead();
                else if (variable.IsPredicted)
                    variable.UpdatePredictHistory();
            }
        }

        protected virtual void InitVariables()
        {
            BindVariable(Position);
            BindVariable(UseSimpleRotation ? SimpleRotation : (NetworkVariable)FullRotation);
        }

        protected void BindVariable(NetworkVariable variable)
        {
            if (_bindCount >= MAX_NETWORK_VARIABLES)
            {
                Debug.LogError("Max binded network variables reached!");
                return;
            }
            variable.SetOwner(this);
            _bindedVariables[_bindCount] = variable;
            _bindCount++;
        }

        public void UnBindVariable(NetworkVariable variable)
        {
            if (_bindedVariables.Contains(variable))
            {
                var temp = _bindedVariables.ToList();
                temp.Remove(variable);
                for (int i = 0; i < temp.Count; i++)
                    _bindedVariables[i] = temp[i];
                _bindCount--;
            }
        }
        
        void WriteVariablesBitMask(NetworkSendBuffer buf, bool forceWrite)
        {
            long result = 0;
            for (int i = 0; i < _bindCount; i++)
                if (_bindedVariables[i].ShouldSend || forceWrite)
                    result = (result | ((long)1 << i));
            
            if (_bindCount < 8)
            {
                buf.WriteByte((byte)result);
                return;
            }

            if (_bindCount < 16)
            {
                buf.WriteInt16((short)result);
                return;
            }

            if (_bindCount < 32)
            {
                buf.WriteInt32((int)result);
                return;
            }

            if (_bindCount < 64)
            {
                buf.WriteInt64(result);
                return;
            }
        }

        long ReadVariablesBitMask()
        {
            if (_bindCount < 8)
                return NetworkReader.ReadByte();

            if (_bindCount < 16)
                return NetworkReader.ReadInt16();

            return NetworkReader.ReadInt64();
        }
    }
}
