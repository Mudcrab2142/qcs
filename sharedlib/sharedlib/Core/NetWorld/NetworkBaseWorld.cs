﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SharedLib
{
    public abstract class NetworkBaseWorld : MonoBehaviour
    {
        public static NetworkBaseWorld WorldInstance { get; set; }
        public NetFuncStorage RPC { get; internal set; }
        public ItemStorage ItemStorage { get; private set; }
        public abstract IFinder Finder { get; }

        public event Action<WorldEntity> OnAddWorldEntity;
        public event Action<WorldEntity> OnRemoveWorldEntity;

        private AsyncOperation _loadingOperation;
        private Action _loadingCallback;
        private Scene _scene;

        public static void ShutdownWorld()
        {
            if (!WorldInstance)
                throw new Exception("World not found!");

            Destroy(WorldInstance.gameObject);
            WorldInstance = null;
        }

        protected Dictionary<int, NetworkBaseEntity> networkEntities = new Dictionary<int, NetworkBaseEntity>();
        protected Dictionary<int, WorldEntity> worldEntities = new Dictionary<int, WorldEntity>();
        private SortedDictionary<int, List<WorldEntity>> _sortedEntities = new SortedDictionary<int, List<WorldEntity>>();

        internal void Init()
        {
            Debug.Log(string.Format("[NetworkWorld] Starting world {0}!", GetType()));
            InitWorldEntityFactory();
            Debug.Log("[NetworkWorld] WorldEntity factory initialized!");
            RPC = CreateNetFuncStorage();
            Debug.Log("[NetworkWorld] RPC initialized!");
            ItemStorage = new ItemStorage();
            Debug.Log("[NetworkWorld] ItemStorage initialized!");
        }

        internal NetworkBaseEntity GetNetworkEntity(int id)
        {
            if (networkEntities.ContainsKey(id))
                return networkEntities[id];

            return null;
        }

        internal void AddWorldEntity(WorldEntity entity)
        {
            if (OnAddWorldEntity != null)
                OnAddWorldEntity(entity);

            entity.NetworkEntity.Owner = entity;
            worldEntities.Add(entity.NetworkEntity.EntityId, entity);
            entity.InternalStart();

            List<WorldEntity> _targetList;
            if (!_sortedEntities.TryGetValue(entity.ExecutionOrder, out _targetList))
                _sortedEntities[entity.ExecutionOrder] = new List<WorldEntity>() { entity };
            else
                _sortedEntities[entity.ExecutionOrder].Add(entity);
        }

        /// <summary>
        /// DO NOT CALL
        /// </summary>
        internal void RemoveWorldEntity(WorldEntity entity)
        {
            var id = entity.NetworkEntity.EntityId;

            if (!worldEntities.ContainsKey(id))
                return;

            if (OnRemoveWorldEntity != null)
                OnRemoveWorldEntity(entity);

            networkEntities.Remove(id);
            worldEntities.Remove(id);
            _sortedEntities[entity.ExecutionOrder].Remove(entity);
        }

        public T GetEntity<T>(int id) where T : class
        {
            if (!worldEntities.ContainsKey(id))
                return null;

            return worldEntities[id] as T;
        }

        protected virtual void Update()
        {
            WorldUpdate();
            WorldEntity current = null;
            try
            {
                foreach (var item in _sortedEntities.Keys)
                {
                    var list = _sortedEntities[item];
                    for (var i = 0; i < list.Count; i++)
                    {
                        current = list[i];
                        current.InternalUpdate();
                    }
                }
            }
            catch(Exception e)
            {
                Debug.LogError(e);
                if(current != null)
                    RemoveWorldEntity(current);
            }

            if(_loadingOperation != null && _loadingOperation.isDone)
            {
                _loadingOperation = null;
                if (_loadingCallback != null)
                {
                    _loadingCallback();
                    SceneManager.SetActiveScene(_scene);
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            WorldEntity current = null;
            try
            {
                foreach (var item in _sortedEntities.Keys)
                {
                    var list = _sortedEntities[item];
                    for (var i = 0; i < list.Count; i++)
                    {
                        current = list[i];
                        current.InternalFixedUpdate();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                if (current != null)
                    RemoveWorldEntity(current);
            }
        }

        protected abstract void InitWorldEntityFactory();
        protected abstract void WorldUpdate();
        protected abstract NetFuncStorage CreateNetFuncStorage();
        protected abstract AsyncOperation LoadWorld(string worldName);

        protected void LoadWorld(string worldName, Action callback)
        {
            _loadingOperation = LoadWorld(worldName);
            _loadingCallback = callback;
            _scene = SceneManager.GetSceneByName(worldName);
        }
    }
}
