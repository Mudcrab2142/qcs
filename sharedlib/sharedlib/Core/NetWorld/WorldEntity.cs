﻿using System;
using UnityEngine;

namespace SharedLib
{
    public abstract class WorldEntity : MonoBehaviour
    {
        public NetworkBaseEntity NetworkEntity { get; internal set; }

        internal event Action UpdateAction;

        public virtual int ExecutionOrder => 0;

        protected virtual void EntityUpdate() { }
        protected virtual void EntityFixedUpdate () { }
        protected virtual void EntityStart() { }
        protected virtual void EntityThink(float dt) { }
        protected virtual void OnDestroy() { }

        private float _nextThinkTime;
        private float _lastThinkTime;

        internal bool LockUpdate { get; set; }

        internal void InternalStart()
        {
            EntityStart();
        }

        internal void InternalUpdate()
        {
            if (LockUpdate)
                return;

            if (UpdateAction != null)
                UpdateAction();

            if (Time.time > _nextThinkTime)
            {
                EntityThink(Time.time - _lastThinkTime);
                _lastThinkTime = Time.time;
            }

            EntityUpdate();
        }

        internal void InternalFixedUpdate()
        {
            if (LockUpdate)
                return;

            EntityFixedUpdate();
        }

        protected void SetThinkDelay(float time)
        {
            _nextThinkTime = Time.time + time; 
        }
    }
}
