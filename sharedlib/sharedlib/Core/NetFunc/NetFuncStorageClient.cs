﻿namespace SharedLib
{
    public abstract partial class NetFuncStorage
    {
        internal NetworkFunctionVoid CL_CoreWorldLoaded;
        public NetworkFunction<NetStruct<string, byte>, bool> CL_CoreCreateCharacter;
        public NetworkFunctionVoid CL_CoreCharacterLoaded;
        public NetworkFunction<PlayerInputState> CL_CoreSendPlayerInput;

        internal NetworkFunction<NetStruct<short, short, short, short>, bool> CL_InventoryMoveItem;
        internal NetworkFunction<NetStruct<short, short, byte>, bool> CL_InventoryEquipItem;
        internal NetworkFunction<NetStruct<byte, short, short>, bool> CL_InventoryUnequipItem;

        internal NetworkFunction<NetStruct<SpellName, string>> CL_SpellUse;
        internal NetworkFunction<NetStruct<SpellName, int, string>> CL_SpellTrigger;
        internal NetworkFunction<NetStruct<SpellName, ProtoVector3, ProtoVector3, string>> CL_SpellDirectionalCast;
        internal NetworkFunction<SpellName, bool> CL_SpellLearn;

        public NetworkFunction<NetStruct<ProtoVector3, ProtoVector3, float>> CL_CharacterTransformUpdate;
        public NetworkFunction<NetStruct<int[], float, float>> CL_CharacterAnimationDataUpdate;
        public NetworkFunctionVoid CL_CharacterRespawn;

        public NetworkFunction<NetStruct<CraftRecipeId, NetStruct<short, short, int>[]>> CL_CraftStart;

        public NetworkFunction<MainStatType, bool> CL_MainStatUpgrade;

        public NetworkFunction<int> CL_EntityAirshipEnter;
        public NetworkFunction<int> CL_EntityAirshipExit;
        public NetworkFunction<NetStruct<int, byte, byte, byte>> CL_EntityAirshipControl;
    }
}
