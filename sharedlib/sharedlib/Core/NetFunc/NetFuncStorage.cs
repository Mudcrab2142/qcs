﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace SharedLib
{
    public abstract partial class NetFuncStorage
    {
        internal readonly Dictionary<ushort, NetworkFunction> IdToNetFunc = new Dictionary<ushort, NetworkFunction>();
        internal readonly Dictionary<NetworkFunction, ushort> NetFuncToId = new Dictionary<NetworkFunction, ushort>();
        internal readonly LidgrenRoot Connection;

        protected abstract void InitHandlers();

        public NetFuncStorage(LidgrenRoot connection)
        {
            Connection = connection;
            ushort counter = 0;
            var bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            var fields = GetType().GetFields(bindingFlags);
            foreach (var field in fields)
            {
                if (!field.FieldType.IsSubclassOf(typeof(NetworkFunction)))
                    continue;

                var name = field.Name;
                var funcInstance = Activator.CreateInstance(field.FieldType, this, name);
                field.SetValue(this, funcInstance);

                IdToNetFunc.Add(counter, (NetworkFunction)funcInstance);
                NetFuncToId.Add((NetworkFunction)funcInstance, counter);

                Debug.Log(string.Format("[{0}]NetworkFunction {1} bound!", counter, name));

                counter++;
            }

            InitHandlers();
        }

        public NetworkFunction GetFunction(ushort id)
        {
            if (!IdToNetFunc.ContainsKey(id))
                return null;
            return IdToNetFunc[id];
        }
    }
}
