﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SharedLib
{
    public abstract class NetworkFunction
    {
        protected uint callCounter;
        protected NetFuncStorage funcStorage;
        protected string name;

        public abstract void Invoke(NetConnection sender, uint callId);
        public abstract void InvokeCallback(NetConnection sender, uint callId);

        public NetworkFunction(NetFuncStorage storage, string funcName)
        {
            funcStorage = storage;
            name = funcName;
        }

        protected NetworkSendBuffer GenerateMessage(bool isCallback, uint callId, byte[] data)
        {
            LidgrenRoot.DefaultSendBuffer.BeginMessage(NetworkMessageType.NetFuncCall);
            LidgrenRoot.DefaultSendBuffer.WriteBool(isCallback);
            LidgrenRoot.DefaultSendBuffer.WriteUInt16(funcStorage.NetFuncToId[this]);
            LidgrenRoot.DefaultSendBuffer.WriteUInt32(callId);
            if (data != null)
                LidgrenRoot.DefaultSendBuffer.WriteByteArray(data);
            LidgrenRoot.DefaultSendBuffer.EndMessage();

            return LidgrenRoot.DefaultSendBuffer;
        }
    }

    public sealed class NetworkFunctionVoid : NetworkFunction
    {
        private Action<NetConnection> _handler;

        public NetworkFunctionVoid(NetFuncStorage storage, string funcName) : base(storage, funcName) { }

        public void SetHandler(Action<NetConnection> handler)
        {
            _handler = handler;
        }

        public void Call(NetConnection connection = null)
        {
            funcStorage.Connection.NetFuncSendDirectCall(GenerateMessage(false, callCounter++, null), connection);
        }

        public void AreaCall(Vector3 position, float areaSize)
        {
            funcStorage.Connection.NetFuncBroadcastCall(GenerateMessage(false, callCounter++, null), position, areaSize);
        }

        public override void Invoke(NetConnection sender, uint callId)
        {
            if (_handler == null)
            {
                Debug.LogWarning(string.Format("[NetworkFunction] {0} call received! But no handlers :(", name));
                return;
            }

            _handler.Invoke(sender);
        }

        public override void InvokeCallback(NetConnection sender, uint callId)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class NetworkFunctionVoid<OUT> : NetworkFunction
    {
        private Func<NetConnection, OUT> _handler;
        private Dictionary<uint, Action<OUT, NetConnection>> _callBacks = new Dictionary<uint, Action<OUT, NetConnection>>();

        public NetworkFunctionVoid(NetFuncStorage storage, string funcName) : base(storage, funcName) { }

        public void SetHandler(Func<NetConnection, OUT> handler)
        {
            _handler = handler;
        }

        public void Call(Action<OUT, NetConnection> callBack, NetConnection connection = null)
        {
            var callId = callCounter++;
            _callBacks.Add(callId, callBack);
            funcStorage.Connection.NetFuncSendDirectCall(GenerateMessage(false, callId, null), connection);
        }

        public override void Invoke(NetConnection sender, uint callId)
        {
            if (_handler == null)
            {
                Debug.LogWarning(string.Format("[NetworkFunction] {0} call received! But no handlers :(", name));
                return;
            }

            funcStorage.Connection.NetFuncSendDirectCall(GenerateMessage(true, callId, SerializeTools.ProtoBufSerialize<OUT>(_handler.Invoke(sender))), sender);
        }

        public override void InvokeCallback(NetConnection sender, uint callId)
        {
            if (!_callBacks.ContainsKey(callId))
            {
                Debug.LogWarning(string.Format("[NetworkFunction] Callback received but handler not found in {0}!", name));
                return;
            }

            _callBacks[callId].Invoke(SerializeTools.ProtoBufDeserialize<OUT>(NetworkReader.ReadByteArray()), sender);
            _callBacks.Remove(callId);
        }
    }

    public sealed class NetworkFunction<IN> : NetworkFunction
    {
        private Action<IN, NetConnection> _handler;

        public NetworkFunction(NetFuncStorage storage, string funcName) : base(storage, funcName) { }

        public void SetHandler(Action<IN, NetConnection> handler)
        {
            _handler = handler;
        }

        public void Call(IN arg, NetConnection targetConnection = null)
        {
            funcStorage.Connection.NetFuncSendDirectCall(GenerateMessage(false, callCounter++, SerializeTools.ProtoBufSerialize(arg)), targetConnection);
        }

        public void AreaCall(IN arg, Vector3 position, float areaSize)
        {
            funcStorage.Connection.NetFuncBroadcastCall(GenerateMessage(false, callCounter++, SerializeTools.ProtoBufSerialize(arg)), position, areaSize);
        }

        public override void Invoke(NetConnection sender, uint callId)
        {
            if (_handler == null)
            {
                Debug.LogWarning(string.Format("[NetworkFunction] {0} call received! But no handlers :(", name));
                return;
            }

            var data = SerializeTools.ProtoBufDeserialize<IN>(NetworkReader.ReadByteArray());
            _handler.Invoke(data, sender);
        }

        public override void InvokeCallback(NetConnection sender, uint callId)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class NetworkFunction<IN, OUT> : NetworkFunction
    {
        private Func<IN, NetConnection, OUT> _handler;
        private Dictionary<uint, Action<OUT, NetConnection>> _callBacks = new Dictionary<uint, Action<OUT, NetConnection>>();

        public NetworkFunction(NetFuncStorage storage, string funcName) : base(storage, funcName) { }

        public void SetHandler(Func<IN, NetConnection, OUT> handler)
        {
            _handler = handler;
        }

        public void Call(IN arg, Action<OUT, NetConnection> callBack, NetConnection targetConnection = null)
        {
            var callId = callCounter++;
            _callBacks.Add(callId, callBack);
            funcStorage.Connection.NetFuncSendDirectCall(GenerateMessage(false, callId, SerializeTools.ProtoBufSerialize(arg)), targetConnection);
        }

        public override void Invoke(NetConnection sender, uint callId)
        {
            if (_handler == null)
            {
                Debug.LogWarning(string.Format("[NetworkFunction] {0} call received! But no handlers :(", name));
                return;
            }

            var data = SerializeTools.ProtoBufDeserialize<IN>(NetworkReader.ReadByteArray());
            funcStorage.Connection.NetFuncSendDirectCall(GenerateMessage(true, callId, SerializeTools.ProtoBufSerialize(_handler.Invoke(data, sender))), sender);
        }

        public override void InvokeCallback(NetConnection sender, uint callId)
        {
            if (!_callBacks.ContainsKey(callId))
            {
                Debug.LogWarning(string.Format("[NetworkFunction] Callback received but handler not found in {0}!", name));
                return;
            }

            _callBacks[callId].Invoke(SerializeTools.ProtoBufDeserialize<OUT>(NetworkReader.ReadByteArray()), sender);
            _callBacks.Remove(callId);
        }
    }
}
