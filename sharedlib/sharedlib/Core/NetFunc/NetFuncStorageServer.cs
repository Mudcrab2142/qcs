﻿using System.Collections.Generic;

namespace SharedLib
{
    public abstract partial class NetFuncStorage
    {
        internal NetworkFunctionVoid<NetStruct<string, int>> S_CoreRequestClientInfo;
        internal NetworkFunction<NetStruct<string, int>[]> S_CoreSendCharactersList;

        internal NetworkFunction<InventoryData> S_InventoryUpdate;

        internal NetworkFunction<List<StatValue>> S_MainStatsUpdate;
        internal NetworkFunction<List<StatValue>> S_SecondaryStatsUpdate;
        internal NetworkFunction<List<StatValue>> S_CraftStatsUpdate;
        internal NetworkFunction<int> S_ExperienceUpdate;  
        internal NetworkFunction<int> S_FreeStatPointsUpdate;

        internal NetworkFunction<NetStruct<byte, ItemInstance>> S_EquipmentUpdateSlot;

        public NetworkFunction<ProtoVector3, bool> S_CharacterRespawn;
        public NetworkFunction<string> S_CharacterDeath;

        public NetworkFunction<NetStruct<int, Damage>> S_NotifyDamageReceive;
        public NetworkFunction<NetStruct<int, Damage>> S_NotifyDamageDeal;
        public NetworkFunction<int> S_NotifyLevelUp;

        internal NetworkFunction<Dictionary<SpellName, byte>> S_SpellUpdateData;
        internal NetworkFunction<int> S_SpellUpdateFreePoints;

        internal NetworkFunction<EffectInfo> S_EffectAdd;
        internal NetworkFunction<int> S_EffectRemove;

        internal NetworkFunction<HashSet<CraftRecipeId>> S_CraftRecipeListUpdate;
        internal NetworkFunction<List<CraftSession>> S_CraftSessionListUpdate;

        public NetworkFunction<NetStruct<int, ProtoVector3>> S_EntityMagicMissileExplode;
    }
}
