﻿using System;
using System.Text;

namespace SharedLib
{
    public class NetworkSendBuffer
    {
        const int MAX_STRING_LENGTH = 1024 * 32;
        const int MIN_SIZE_TO_COMPRESS = 1000;

        private static byte[] _tempCompressionArray = new byte[100000];

        private byte[] _stringWriteBuffer = new byte[MAX_STRING_LENGTH];
        private int _position;
        private UIntFloat _floatConverter = new UIntFloat();
        private bool _messageStarted;
        private Encoding _encoding = new UTF8Encoding();
        private bool _useCompression;
        internal int CurrentPosition => _position;

        internal byte[] SendBuffer { get; private set; }

        internal NetworkSendBuffer(int size)
        {
            SendBuffer = new byte[size];
        }

        public void Seek(int position)
        {
            if (position < 0 || position >= SendBuffer.Length)
                position = 0;

            _position = position;
        }

        public void WriteInt32(int value)
        {
            WriteByte4((byte)(value & 0xff), (byte)((value >> 8) & 0xff), (byte)((value >> 16) & 0xff), (byte)((value >> 24) & 0xff));
        }

        public void WriteInt64(long value)
        {
            WriteByte8(
                (byte)(value & 0xff),
                (byte)((value >> 8) & 0xff),
                (byte)((value >> 16) & 0xff),
                (byte)((value >> 24) & 0xff),
                (byte)((value >> 32) & 0xff),
                (byte)((value >> 40) & 0xff),
                (byte)((value >> 48) & 0xff),
                (byte)((value >> 56) & 0xff));
        }

        public void WriteUInt32(uint value)
        {
            if (value <= 240)
            {
                WriteByte((byte)value);
                return;
            }
            if (value <= 2287)
            {
                WriteByte((byte)((value - 240) / 256 + 241));
                WriteByte((byte)((value - 240) % 256));
                return;
            }
            if (value <= 67823)
            {
                WriteByte((byte)249);
                WriteByte((byte)((value - 2288) / 256));
                WriteByte((byte)((value - 2288) % 256));
                return;
            }
            if (value <= 16777215)
            {
                WriteByte((byte)250);
                WriteByte((byte)(value & 0xFF));
                WriteByte((byte)((value >> 8) & 0xFF));
                WriteByte((byte)((value >> 16) & 0xFF));
                return;
            }

            // all other values of uint
            WriteByte((byte)251);
            WriteByte((byte)(value & 0xFF));
            WriteByte((byte)((value >> 8) & 0xFF));
            WriteByte((byte)((value >> 16) & 0xFF));
            WriteByte((byte)((value >> 24) & 0xFF));
        }

        public void WriteInt16(short value)
        {
            WriteByte2((byte)(value & 0xff), (byte)((value >> 8) & 0xff));
        }

        public void WriteUInt16(ushort value)
        {
            WriteByte2((byte)(value & 0xff), (byte)((value >> 8) & 0xff));
        }

        public void WriteSingle(float value)
        {
            _floatConverter.floatValue = value;
            WriteUInt32(_floatConverter.uintValue);
        }

        public void WriteBool(bool value)
        {
            WriteByte(value ? (byte)1 : (byte)0);
        }

        public void WriteString(string value)
        {
            if (value == null)
            {
                WriteByte2(0, 0);
                return;
            }

            int len = _encoding.GetByteCount(value);

            if (len >= MAX_STRING_LENGTH)
            {
                throw new IndexOutOfRangeException("Serialize(string) too long: " + value.Length);
            }

            WriteUInt16((ushort)(len));
            int numBytes = _encoding.GetBytes(value, 0, value.Length, _stringWriteBuffer, 0);
            WriteBytes(_stringWriteBuffer, (ushort)numBytes);
        }

        public void WriteByte(byte b)
        {
            CheckWriteBytes(1);
            SendBuffer[_position] = b;
            _position++;
        }

        public void WriteByte2(byte b1, byte b2)
        {
            CheckWriteBytes(2);
            SendBuffer[_position] = b1;
            SendBuffer[_position + 1] = b2;
            _position += 2;
        }

        public void WriteByte4(byte b1, byte b2, byte b3, byte b4)
        {
            CheckWriteBytes(4);
            SendBuffer[_position] = b1;
            SendBuffer[_position + 1] = b2;
            SendBuffer[_position + 2] = b3;
            SendBuffer[_position + 3] = b4;
            _position += 4;
        }

        public void WriteByte8(byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8)
        {
            CheckWriteBytes(8);
            SendBuffer[_position] = b1;
            SendBuffer[_position + 1] = b2;
            SendBuffer[_position + 2] = b3;
            SendBuffer[_position + 3] = b4;
            SendBuffer[_position + 4] = b5;
            SendBuffer[_position + 5] = b6;
            SendBuffer[_position + 6] = b7;
            SendBuffer[_position + 7] = b8;
            _position += 8;
        }

        public void WriteBytes(byte[] buf, int count)
        {
            CheckWriteBytes(count);

            if (count == buf.Length)
            {
                buf.CopyTo(SendBuffer, _position);
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    SendBuffer[_position + i] = buf[i];
                }
            }
            _position += count;
        }

        public void WriteByteArray(byte[] data)
        {
            WriteInt32(data.Length);
            WriteBytes(data, data.Length);
        }

        public void BeginMessage(NetworkMessageType msg, bool useCompression = false)
        {
            if(_messageStarted)
                throw new Exception("Message already started!");

            _position = 0;
            _messageStarted = true;
            _useCompression = useCompression;
            WriteByte((byte)msg);
        }

        public void EndMessage()
        {
            if (!_messageStarted)
                throw new Exception("Message not started!");

            _messageStarted = false;

            if (_useCompression && _position > MIN_SIZE_TO_COMPRESS)
                Compress(this);
        }

        private void CheckWriteBytes(int size)
        {
            if (CurrentPosition + size >= SendBuffer.Length)
            {
                var newBuffer = new byte[SendBuffer.Length * 2];
                Buffer.BlockCopy(SendBuffer, 0, newBuffer, 0, SendBuffer.Length);
                SendBuffer = newBuffer;
            }
        }

        static void Compress(NetworkSendBuffer target)
        {
            var size = target.CurrentPosition;
            LZ4Sharp.LZ4.Compress(target.SendBuffer, 0, target.CurrentPosition, _tempCompressionArray, 0);
            target.BeginMessage(NetworkMessageType.CompressedMessage);
            target.WriteInt32(size);
            target.WriteBytes(_tempCompressionArray, size);
            target.EndMessage();
        }
    }
}
