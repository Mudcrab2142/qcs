﻿using Lidgren.Network;
using UnityEngine;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("clientlib")]
[assembly: InternalsVisibleTo("serverlib")]
namespace SharedLib
{
    public abstract class LidgrenRoot : MonoBehaviour
    {
        internal static LidgrenRoot LidgrenInstance { get; private set; }
        internal static readonly NetworkSendBuffer DefaultSendBuffer = new NetworkSendBuffer(65535);

        protected static NetPeer netPeer;

        private static byte[] _decompressedBuffer = new byte[1024 * 1024];

        public abstract float ServerTime { get; }

        internal static T CreateInstance<T>() where T : LidgrenRoot
        {
            if (LidgrenInstance)
                throw new System.Exception("Cant create multiple instances of LidgrenRoot!");

            var obj = new GameObject(typeof(T).ToString());
            LidgrenInstance = obj.AddComponent<T>();
            return LidgrenInstance as T;
        }

        internal static void ShutDown()
        {
            if (LidgrenInstance)
                throw new System.Exception("Lidgren instance not found!");

            netPeer.Shutdown("ggwp");
            Destroy(LidgrenInstance.gameObject);
            LidgrenInstance = null;
        }

        protected void InitConfig(ref NetPeerConfiguration config)
        {
            config.AutoFlushSendQueue = false;
            config.AutoExpandMTU = false;
            config.MaximumTransmissionUnit = 1200;
            config.UnreliableSizeBehaviour = NetUnreliableSizeBehaviour.NormalFragmentation;
            config.ConnectionTimeout = 10f;
            config.PingInterval = 1;

            config.SimulatedMinimumLatency = 0.05f;
            config.SimulatedRandomLatency = 0.0f;
        }

        protected virtual void Update()
        {
            if (netPeer == null)
                return;

            NetIncomingMessage msg;

            while ((msg = netPeer.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        Debug.Log(msg.ReadString());
                        break;

                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        if (status == NetConnectionStatus.Connected)
                            OnConnectEvent(msg.SenderConnection);
                        if (status == NetConnectionStatus.Disconnected)
                            OnDisconnectEvent(msg.SenderConnection);
                        break;

                    case NetIncomingMessageType.Data:
                        NetworkReader.SetBuffer(msg.Data);
                        var msgType = (NetworkMessageType)NetworkReader.ReadByte();
                        OnDataEvent(msg.SenderConnection, msgType);
                        break;
                }
                netPeer.Recycle(msg);
            }
        }

        protected abstract void OnConnectEvent(NetConnection connection);


        protected abstract void OnDisconnectEvent(NetConnection connection);


        protected virtual void OnDataEvent(NetConnection connection, NetworkMessageType type)
        {
            if (type == NetworkMessageType.NetFuncCall)
            {
                var callBack = NetworkReader.ReadBool();
                var funcId = NetworkReader.ReadUInt16();
                var callId = NetworkReader.ReadUInt32();
                //  Debug.Log(string.Format("[RPC CALL] ID = {0}", funcId));

                if (!callBack)
                    NetworkBaseWorld.WorldInstance.RPC.GetFunction(funcId).Invoke(connection, callId);
                else
                    NetworkBaseWorld.WorldInstance.RPC.GetFunction(funcId).InvokeCallback(connection, callId);
            }

            if (type == NetworkMessageType.CompressedMessage)
            {
                var size = NetworkReader.ReadInt32();
                Decompress(size);
                NetworkReader.SetBuffer(_decompressedBuffer);
                var msgType = (NetworkMessageType)NetworkReader.ReadByte();
                OnDataEvent(connection, msgType);
            }
        }

        unsafe void Decompress(int length)
        {
            fixed (byte* cPtr = &NetworkReader.GetBuffer()[NetworkReader.Position])
            {
                fixed (byte* dPtr = &_decompressedBuffer[0])
                {
                    LZ4Sharp.LZ4.DecompressKnownSize(cPtr, dPtr, length);
                }
            }
        }

        internal abstract void NetFuncSendDirectCall(NetworkSendBuffer data, NetConnection connection);
        internal abstract void NetFuncBroadcastCall(NetworkSendBuffer data, Vector3 position, float areaSize);
    }
}
