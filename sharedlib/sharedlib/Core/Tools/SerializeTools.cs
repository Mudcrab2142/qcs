﻿using System.IO;

namespace SharedLib
{
    public static class SerializeTools
    {
        public static byte[] ProtoBufSerialize<T>(T obj)
        {
            using (var buffer = new MemoryStream())
            {
                ProtoBuf.Serializer.Serialize(buffer, obj);
                return buffer.ToArray();
            }
        }

        public static T ProtoBufDeserialize<T>(byte[] data)
        {
            using (var buffer = new MemoryStream(data))
            {
                return ProtoBuf.Serializer.Deserialize<T>(buffer);
            }
        }
    }
}
