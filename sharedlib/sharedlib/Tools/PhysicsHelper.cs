﻿using System.Collections.Generic;
using UnityEngine;

namespace SharedLib
{
    public static class PhysicsHelper
    {
        static readonly Collider[] _results = new Collider[1024];

        public static List<T> BoxOverlap<T>(Vector3 position, Vector3 size, Quaternion rotation) where T : Component
        {
            var count = Physics.OverlapBoxNonAlloc(position, size, _results, rotation);

            // DebugHelper.DrawBox(position, size, rotation, Color.red, 5f);
            var result = new List<T>();
            for (var i = 0; i < count; i++)
            {
                var c = _results[i].attachedRigidbody.GetComponent<T>();

                if (!result.Contains(c) && c != null)
                    result.Add(c);
            }
            return result;
        }
    }
}