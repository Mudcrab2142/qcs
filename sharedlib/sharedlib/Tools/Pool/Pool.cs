﻿using System;
using System.Collections.Generic;

namespace SharedLib
{
    public class Pool<T>
    {
        private readonly Stack<T> _objects;
        public readonly Func<T> ObjectGenerator;

        public bool IsEmpty { get { return _objects.Count == 0; } }

        public Pool(Func<T> objGenerator)
        {
            if (objGenerator == null) throw new ArgumentNullException();
            _objects = new Stack<T>();
            ObjectGenerator = objGenerator;
        }

        public void Precache(int count)
        {
            for (int i = 0; i < count; i++)
                PutObject(ObjectGenerator());
        }

        public T GetObject()
        {
            while (_objects.Count > 0)
            {
                var obj = _objects.Pop();
                if (obj == null)
                    continue;
                return obj;
            }
            return ObjectGenerator();
        }

        public void PutObject(T item)
        {
            _objects.Push(item);
        }

        public T[] GetObjectReferences()
        {
            return _objects.ToArray();
        }

        public void Clear()
        {
            _objects.Clear();
        }
    }
}
