﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SharedLib
{
    public interface IPoolable
    {
        void OnPoolEnter();
        void OnPoolExit();
    }

    public interface IUnityPoolable : IPoolable
    {
        Action Free { get; set; }
        GameObject gameObject { get; }
    }
}
