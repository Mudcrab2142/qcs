﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SharedLib
{
    public class UnityPool<T> where T : UnityEngine.Object, IUnityPoolable
    {
        readonly Dictionary<string, Pool<T>> _objectPools;
        readonly Func<string, T> _objectGenerator;
        readonly Dictionary<string, Transform> _objParents;
        readonly Transform _poolParent;

        public UnityPool(Func<string, T> objGenerator, Transform parent)
        {
            if (objGenerator == null) throw new ArgumentNullException();
            _objectPools = new Dictionary<string, Pool<T>>();
            _objParents = new Dictionary<string, Transform>();
            _objectGenerator = objGenerator;
            _poolParent = parent;
        }

        /// <summary>
        /// Precache objects
        /// </summary>
        public void Precache(string prefabName, int count)
        {
            Pool<T> pool;

            if (!_objectPools.TryGetValue(prefabName, out pool))
                pool = CreateNewPool(prefabName);

            for (var i = 0; i < count; i++)
                pool.PutObject(CreateNewObject(prefabName));
        }

        /// <summary>
        /// Get object from pool (if there is no such pool create one)
        /// </summary>
        /// <param name="prefabName">Prefab name</param>
        /// <returns></returns>
        public T GetObject(string prefabName)
        {
            Pool<T> pool;

            if (!_objectPools.TryGetValue(prefabName, out pool))
                pool = CreateNewPool(prefabName);

            var result = !pool.IsEmpty ? pool.GetObject() : CreateNewObject(prefabName);
            result.gameObject.SetActive(true);
            result.OnPoolExit();
            return result;
        }

        /// <summary>
        /// Put object back to pool
        /// </summary>
        /// <param name="item"></param>
        public void PutObject(T item)
        {
            Pool<T> pool;

            if (!_objectPools.TryGetValue(item.gameObject.name, out pool))
                pool = CreateNewPool(item.gameObject.name);

            item.gameObject.SetActive(false);
            item.gameObject.transform.SetParent(_objParents[item.gameObject.name]);
            pool.PutObject(item);
            item.OnPoolEnter();
        }

        T CreateNewObject(string prefabName)
        {
            var obj = _objectPools[prefabName].ObjectGenerator();
            obj.gameObject.name = prefabName;

            obj.gameObject.SetActive(false);

            obj.gameObject.transform.SetParent(_objParents[prefabName]);
            obj.Free = () => PutObject(obj);
            return obj;
        }

        Pool<T> CreateNewPool(string prefabName)
        {
            var pool = new Pool<T>(() => _objectGenerator(prefabName));
            _objectPools.Add(prefabName, pool);
            var newParent = new GameObject("[Pool]" + prefabName).transform;
            newParent.SetParent(_poolParent);
            _objParents.Add(prefabName, newParent);
            return pool;
        }

        void Clear()
        {
            //Clear Pools
            foreach (var key in _objectPools.Keys)
            {
                foreach (var item in _objectPools[key].GetObjectReferences())
                    UnityEngine.Object.Destroy(item);

                _objectPools[key].Clear();
            }
            _objectPools.Clear();

            //Clear Parent Objects
            foreach (var item in _objParents.Values)
                UnityEngine.Object.Destroy(item);
            _objParents.Clear();
        }
    }
}
