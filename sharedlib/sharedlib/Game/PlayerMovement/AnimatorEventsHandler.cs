﻿using UnityEngine;
using UnityEngine.Events;

namespace SharedLib
{
    public class AnimatorEventsHandler : MonoBehaviour
    {
        public class AnimationEvent : UnityEvent { }
        public class AnimationEvent<T1> : UnityEvent<T1> { }
        public class AnimationEvent<T0, T1> : UnityEvent<T0, T1> { }
        public class AnimationEvent<T0, T1, T2> : UnityEvent<T0, T1, T2> { }

        public AnimationEvent<SpellName, string> SpellUse { get; private set; }
        public AnimationEvent<SpellName, SpellTraceConfig.TraceConfigItem, string> SpellTrace { get; private set; }
        public AnimationEvent<SpellName, Ray, string> SpellDirectionalCast { get; private set; }

        public AnimationEvent<int> LockRoll { get; private set; }
        public AnimationEvent<int> UnlockRoll { get; private set; }

        public IPlayerControllable Player { get; private set; }

        void Awake()
        {
            SpellUse = new AnimationEvent<SpellName, string>();
            SpellTrace = new AnimationEvent<SpellName, SpellTraceConfig.TraceConfigItem, string>();
            SpellDirectionalCast = new AnimationEvent<SpellName, Ray, string>();

            LockRoll = new AnimationEvent<int>();
            UnlockRoll = new AnimationEvent<int>();
        }

        public void SetPlayer(IPlayerControllable player)
        {
            Player = player;
        }
    }
}
