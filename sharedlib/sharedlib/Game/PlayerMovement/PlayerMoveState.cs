﻿using UnityEngine;

namespace SharedLib
{
    public struct PlayerMoveState
    {
        public bool InAction { get { return JumpOver || StepUp || ClimbUp || UsingLadder || Jump; } }

        public RaycastHit GroundHit;
        public float InpForward;
        public float InpDirection;
        public float VerticalVelocity;
        public bool LockPlayer;
        public float ExtraStrafeSpeed;
        public float GroundDistance;
        public byte RollLockMask;
        public byte MovementLockMask;
        public AnimatorStateInfo StateInfo;
        public float SpellCastSpeed;
        public bool OnGround;
        public bool AutoCrouch;
        public bool Crouch;
        public bool LandHigh;
        public bool Jump;
        public bool IsJumping;
        public bool JumpOver;
        public bool StepUp;
        public bool ClimbUp;
        public bool EnterLadderBottom;
        public bool EnterLadderTop;
        public bool UsingLadder;
        public bool ExitLadderBottom;
        public bool ExitLadderTop;
        public GameObject MovingPlatform;
        public Collider MovingPlatformCollider;
        public Transform MatchTarget;
    }
}
