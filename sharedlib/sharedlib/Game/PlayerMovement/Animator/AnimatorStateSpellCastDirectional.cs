﻿using UnityEngine;

namespace SharedLib
{
    public class AnimatorStateSpellCastDirectional : StateMachineBehaviour
    {
        public SpellName Spell;
        public float NormalizedTimeOffset;
        public string Args;
        public float HeightOffset;

        public bool _casted;
        private AnimatorEventsHandler _handler;

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _handler = animator.gameObject.GetComponent<AnimatorEventsHandler>();
            _casted = false;
        }


        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!_casted && _handler != null && stateInfo.normalizedTime >= NormalizedTimeOffset)
            {
                _casted = true;
                _handler.SpellDirectionalCast.Invoke(Spell, new Ray(_handler.transform.position + Vector3.up * HeightOffset, _handler.Player.ViewDirection), Args);
            }
        }
    }
}
