﻿using UnityEngine;

namespace SharedLib
{
    public class AnimatorStateSpellUse : StateMachineBehaviour
    {
        public SpellName Spell;
        public string Args;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var handler = animator.GetComponent<AnimatorEventsHandler>();
            if (handler != null)
                handler.SpellUse.Invoke(Spell, Args);
        }
    }

}
