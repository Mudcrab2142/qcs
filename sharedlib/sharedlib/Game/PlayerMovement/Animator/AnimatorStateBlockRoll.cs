﻿using UnityEngine;

namespace SharedLib
{
    public class AnimatorStateBlockRoll : StateMachineBehaviour
    {
        public float LockTime = 1f;
        private AnimatorEventsHandler _handler;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _handler = animator.gameObject.GetComponent<AnimatorEventsHandler>();
            if (_handler != null)
                _handler.LockRoll.Invoke(0);
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (_handler == null)
                return;

            if (stateInfo.normalizedTime >= LockTime)
                _handler.UnlockRoll.Invoke(0);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (_handler != null)
                _handler.UnlockRoll.Invoke(0);
        }
    }
}
