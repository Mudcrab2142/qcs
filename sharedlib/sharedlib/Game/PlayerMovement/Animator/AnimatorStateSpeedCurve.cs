﻿using UnityEngine;

namespace SharedLibs
{
    public class AnimatorStateSpeedCurve : StateMachineBehaviour
    {
        public AnimationCurve Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 0));
        public float Value = 1f;

        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var curveX = Mathf.Clamp(stateInfo.normalizedTime, 0f, 1f);
            animator.speed = 1f + Curve.Evaluate(curveX) * Value;
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.speed = 1f;
        }
    }
}
