﻿using System.Collections.Generic;
using UnityEngine;

namespace SharedLib
{
    public class AnimatorStateSpellTrace : StateMachineBehaviour
    {
        public SpellName Spell;
        public SpellTraceConfig Config;
        public string Args;
        private List<int> _targetHits = new List<int>();
        private List<int> _tracesInvoked = new List<int>();
        private AnimatorEventsHandler _handler;

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _targetHits.Clear();
            _tracesInvoked.Clear();
            _handler = animator.gameObject.GetComponent<AnimatorEventsHandler>();
        }

        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (Config == null || _handler == null)
                return;

            Check(stateInfo);
        }

        void Check(AnimatorStateInfo stateInfo)
        {
            for (var i = 0; i < Config.TraceList.Count; i++)
            {
                if (_tracesInvoked.Contains(i))
                    continue;

                var config = Config.TraceList[i];
                if (config.Time <= stateInfo.normalizedTime)
                {
                    Trigger(config);
                    _tracesInvoked.Add(i);
                }
            }
        }

        void Trigger(SpellTraceConfig.TraceConfigItem item)
        {
            var origin = _handler.transform.position + _handler.transform.rotation * item.Center;
            var rotation = _handler.transform.rotation * item.Orientation;

            _handler.SpellTrace.Invoke(Spell, item, Args);
            /*foreach (var creature in PhysicsHelper.BoxOverlapCreatures(origin, item.Size, rotation))
            {
                if (_targetHits.Contains(creature.NetworkEntity.EntityId))
                    continue;

                World.ClientWorldInstance.ClientSpells.TriggerSpell(Spell, creature.NetworkEntity.EntityId, Args);
                creature.TakeHit(_character, creature.transform.position);
                _targetHits.Add(creature.NetworkEntity.EntityId);
            }*/
        }
    }

}
