﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct PlayerInputState
    {
        public float Horizontal;
        public float Vectical;
        public ProtoVector3 PredictedPosition;

        public float Yaw;
        public float DirectionX;
        public float DirectionY;
        public float DirectionZ;

        public bool AttackPress;
        public bool AttackHold;

        public bool ForwardDoublePress;
        public bool BackDoublePress;
        public bool LeftDoublePress;
        public bool RightDoublePress;

        public bool JumpPress;
        public bool CrouchPress;
    }
}
