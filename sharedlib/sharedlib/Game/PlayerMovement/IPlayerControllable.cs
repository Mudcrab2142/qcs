﻿using UnityEngine;

namespace SharedLib
{
    public interface IPlayerControllable
    {
        Vector3 ViewDirection { get; }
    }
}
