﻿using UnityEngine;

namespace SharedLib
{
    public static class PlayerMove
    {
        const int LAYER_GROUND = 1;
        const bool JUMP_AIR_CONTROL = true;
        const float JUMP_FORWARD = 5f;
        const float JUMP_FORCE = 4f;
        const float GROUND_CHECK_DISTANCE = 0.5f;
        const float STEP_OFFSET_END = 0.36f;
        const float STEP_OFFSET_START = 0.05f;
        const float STEP_OFFSET_FORWARD = 0.05f;
        const float EXTRA_GRAVITY = 4f;
        const float LANDING_ANIMATION_VELOCITY = -5f;
        const float COLLIDER_HEIGHT = 1.8f;
        const float COLLIDER_RADIUS = 0.5f;
        static readonly Vector3 COLLIDER_CENTER = new Vector3(0f, 0.9f, 0f);

        static readonly PhysicMaterial FRICTION_PHYSICS = new PhysicMaterial() { staticFriction = 0.6f, dynamicFriction = 0.6f };
        static readonly PhysicMaterial DEFAULT_PHYSICS = new PhysicMaterial() { staticFriction = 0f, dynamicFriction = 0f };

        public static void FixedUpdateMovement(Animator animator, CapsuleCollider collider, ref PlayerMoveState moveState, ref PlayerInputState inputState, SpellStorage spells)
        {
            var stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            var transform = animator.transform;
            var rigidbody = collider.attachedRigidbody;
            rigidbody.transform.eulerAngles = new Vector3(0f, inputState.Yaw, 0f);

            animator.SetBool("InState_CombatSword1H", stateInfo.IsTag("CombatSword1H"));
            animator.SetBool("InState_Roll", stateInfo.IsTag("Roll"));

            animator.SetBool("Can_Roll", moveState.RollLockMask == 0 && CanUseSpell(SpellName.Roll, spells));
            animator.SetBool("Can_SwordMastery1H", CanUseSpell(SpellName.SwordMastery1H, spells));
            animator.SetBool("Can_CuttingStrike", CanUseSpell(SpellName.CuttingStrike, spells));
            animator.SetBool("Can_DarkMatter", CanUseSpell(SpellName.DarkMatter, spells));

            if (!moveState.LockPlayer && moveState.MovementLockMask == 0)
            {
                animator.SetBool("Input_AttackPress", inputState.AttackPress);
                animator.SetBool("Input_AttackHold", inputState.AttackHold);
                animator.SetFloat("Input_X", inputState.Horizontal/*, 0.15f, Time.fixedDeltaTime*/);
                animator.SetFloat("Input_Y", inputState.Vectical/*, 0.2f, Time.fixedDeltaTime*/);

                animator.SetBool("Input_ForwardDoublePress", inputState.ForwardDoublePress);
                animator.SetBool("Input_BackDoublePress", inputState.BackDoublePress);
                animator.SetBool("Input_LeftDoublePress", inputState.LeftDoublePress);
                animator.SetBool("Input_RightDoublePress", inputState.RightDoublePress);

                animator.SetFloat("Stat_AttackSpeed", spells.Stats.AttackSpeed.Value * moveState.SpellCastSpeed);

                if (moveState.AutoCrouch)
                    moveState.Crouch = true;
                else
                {
                    if (inputState.CrouchPress && moveState.OnGround && !moveState.InAction)
                        moveState.Crouch = !moveState.Crouch;
                }

                if (inputState.JumpPress && (!moveState.Crouch && moveState.OnGround && !moveState.InAction))
                    moveState.Jump = true;
            }
            else
            {
                animator.SetFloat("Input_X", 0f/*, 0.15f, Time.fixedDeltaTime*/);
                animator.SetFloat("Input_Y", 0f/*, 0.2f, Time.fixedDeltaTime*/);
            }


            var radius = collider.radius * 0.9f;
            var dist = Mathf.Infinity;
            var pos = collider.transform.position + Vector3.up * (collider.radius);
            var ray1 = new Ray(collider.transform.position + new Vector3(0, collider.height / 2, 0), Vector3.down);
            var ray2 = new Ray(pos, -Vector3.up);
            if (Physics.Raycast(ray1, out moveState.GroundHit, Mathf.Infinity, LAYER_GROUND))
                dist = collider.transform.position.y - moveState.GroundHit.point.y;

            if (Physics.SphereCast(ray2, radius, out moveState.GroundHit, Mathf.Infinity, LAYER_GROUND))
                if (dist > (moveState.GroundHit.distance - collider.radius * 0.1f))
                    dist = (moveState.GroundHit.distance - collider.radius * 0.1f);

            moveState.GroundDistance = dist;

            collider.material = moveState.OnGround ? FRICTION_PHYSICS : DEFAULT_PHYSICS;

            //ground stick
            if (!moveState.JumpOver && !moveState.StepUp && !moveState.ClimbUp && !moveState.UsingLadder)
            {
                var onStep = StepOffset(ref inputState, ref moveState, collider);
                if (moveState.GroundDistance <= 0.05f)
                {
                    moveState.OnGround = true;
                    if (!onStep)
                        rigidbody.velocity = Vector3.ProjectOnPlane(rigidbody.velocity, moveState.GroundHit.normal);
                }
                else
                {
                    if (moveState.GroundDistance >= GROUND_CHECK_DISTANCE)
                    {
                        moveState.OnGround = false;
                        moveState.VerticalVelocity = rigidbody.velocity.y;
                        if (!onStep)
                            collider.transform.position -= Vector3.up * (EXTRA_GRAVITY * Time.fixedDeltaTime);
                    }
                    else if (!onStep && !moveState.Jump)
                        collider.transform.position -= Vector3.up * (EXTRA_GRAVITY * Time.fixedDeltaTime);
                }
            }

            //collider height
            if (moveState.Crouch && !moveState.JumpOver)
            {
                collider.center = COLLIDER_CENTER / 1.4f;
                collider.height = COLLIDER_HEIGHT / 1.4f;
            }
            else if (moveState.JumpOver)
            {
                collider.center = COLLIDER_CENTER / 0.8f;
                collider.height = COLLIDER_HEIGHT / 2f;
            }
            else if (moveState.UsingLadder)
            {
                collider.radius = COLLIDER_RADIUS / 1.25f;
            }
            else
            {
                collider.center = COLLIDER_CENTER;
                collider.radius = COLLIDER_RADIUS;
                collider.height = COLLIDER_HEIGHT;
            }

            //set input
            moveState.InpForward = inputState.Vectical;
            moveState.InpDirection = inputState.Horizontal;

            LandHighAnimation(animator, ref moveState, stateInfo);
            JumpOverAnimation(animator, rigidbody, ref moveState, stateInfo);
            ClimbUpAnimation(animator, rigidbody, ref moveState, stateInfo);
            StepUpAnimation(animator, collider, rigidbody, ref moveState, stateInfo);
            JumpAnimation(animator, rigidbody, ref moveState, stateInfo);

            if (stateInfo.IsName("Grounded.Strafing Movement") || stateInfo.IsName("Grounded.Strafing Crouch"))
            {
                var newSpeed_Y = (moveState.ExtraStrafeSpeed * moveState.InpForward);
                var newSpeed_X = (moveState.ExtraStrafeSpeed * moveState.InpDirection);
                newSpeed_Y = Mathf.Clamp(newSpeed_Y, -moveState.ExtraStrafeSpeed, moveState.ExtraStrafeSpeed);
                newSpeed_X = Mathf.Clamp(newSpeed_X, -moveState.ExtraStrafeSpeed, moveState.ExtraStrafeSpeed);
                transform.position += transform.forward * (newSpeed_Y * Time.fixedDeltaTime);
                transform.position += transform.right * (newSpeed_X * Time.fixedDeltaTime);
            }

            animator.SetBool("Crouch", moveState.Crouch);
            animator.SetBool("OnGround", moveState.OnGround);
            animator.SetFloat("GroundDistance", moveState.GroundDistance);
            animator.SetFloat("VerticalVelocity", moveState.VerticalVelocity);

            //SUKA ZAEBALO
            var distance = Vector3.Distance(transform.position, inputState.PredictedPosition);
            if (distance < 0.1f)
                transform.position = inputState.PredictedPosition;
            else if (distance < 0.25f)
                transform.position = Vector3.MoveTowards(transform.position, inputState.PredictedPosition, 0.1f * Time.fixedDeltaTime);
        }

        static bool StepOffset(ref PlayerInputState inputState, ref PlayerMoveState moveState, CapsuleCollider collider)
        {
            if (inputState.Horizontal * inputState.Horizontal + inputState.Vectical * inputState.Vectical < 0.1 || !moveState.OnGround)
                return false;

            var hit = new RaycastHit();
            var rayStep = new Ray((collider.transform.position + new Vector3(0, STEP_OFFSET_END, 0) + collider.transform.forward * (collider.radius + STEP_OFFSET_FORWARD)), Vector3.down);

            if (Physics.Raycast(rayStep, out hit, STEP_OFFSET_END - STEP_OFFSET_START, LAYER_GROUND))
                if (hit.point.y >= (collider.transform.position.y) && hit.point.y <= (collider.transform.position.y + STEP_OFFSET_END))
                {
                    var heightPoint = new Vector3(collider.transform.position.x, hit.point.y + 0.1f, collider.transform.position.z);
                    collider.transform.position = Vector3.Slerp(collider.transform.position, heightPoint, (moveState.InpForward * 3.5f) * Time.fixedDeltaTime);
                    return true;
                }
            return false;
        }

        static bool CanUseSpell(SpellName s, SpellStorage spells)
        {
            return spells.SpellInfo[s].CanUse();
        }

        static void LandHighAnimation(Animator animator, ref PlayerMoveState moveState, AnimatorStateInfo stateInfo)
        {
            animator.SetBool("LandHigh", moveState.LandHigh);

            if (!moveState.OnGround && moveState.VerticalVelocity <= LANDING_ANIMATION_VELOCITY && moveState.GroundDistance <= 0.5f)
                moveState.LandHigh = true;

            if (moveState.LandHigh && stateInfo.IsName("Airborne.LandHigh") && stateInfo.normalizedTime > 0.9f)
                moveState.LandHigh = false;
        }

        static void JumpOverAnimation(Animator animator, Rigidbody rigidbody, ref PlayerMoveState moveState, AnimatorStateInfo stateInfo)
        {
            animator.SetBool("JumpOver", moveState.JumpOver);

            if (stateInfo.IsName("Action.JumpOver"))
            {
                if (stateInfo.normalizedTime > 0.1f && stateInfo.normalizedTime < 0.3f)
                    rigidbody.useGravity = false;

                if (!animator.IsInTransition(0))
                {
                    var matchMask = new MatchTargetWeightMask(new Vector3(0, 1, 1), 0);
                    MatchTarget(animator, moveState.MatchTarget.position, moveState.MatchTarget.rotation, AvatarTarget.LeftHand, matchMask, 0.1f * (1 - stateInfo.normalizedTime), 0.3f * (1 - stateInfo.normalizedTime));
                }
                if (stateInfo.normalizedTime >= 0.7f)
                {
                    rigidbody.useGravity = true;
                    moveState.JumpOver = false;
                }
            }
        }

        static void ClimbUpAnimation(Animator animator, Rigidbody rigidbody, ref PlayerMoveState moveState, AnimatorStateInfo stateInfo)
        {
            animator.SetBool("ClimbUp", moveState.ClimbUp);

            if (stateInfo.IsName("Action.ClimbUp"))
            {
                if (stateInfo.normalizedTime > 0.1f && stateInfo.normalizedTime < 0.3f)
                {
                    rigidbody.useGravity = false;
                    animator.GetComponent<Collider>().isTrigger = true;
                }

                var matchMask = new MatchTargetWeightMask(new Vector3(0, 1, 1), 0);
                if (!animator.IsInTransition(0))
                    MatchTarget(animator, moveState.MatchTarget.position, moveState.MatchTarget.rotation, AvatarTarget.LeftHand, matchMask, 0f, 0.2f);

                if (stateInfo.normalizedTime >= 0.85f)
                {
                    animator.GetComponent<Collider>().isTrigger = false;
                    rigidbody.useGravity = true;
                    moveState.ClimbUp = false;
                }
            }
        }

        static void StepUpAnimation(Animator animator, CapsuleCollider collider, Rigidbody rigidbody, ref PlayerMoveState moveState, AnimatorStateInfo stateInfo)
        {
            animator.SetBool("StepUp", moveState.StepUp);

            if (stateInfo.IsName("Action.StepUp"))
            {
                if (stateInfo.normalizedTime > 0.1f && stateInfo.normalizedTime < 0.3f)
                {
                    collider.isTrigger = true;
                    rigidbody.useGravity = false;
                }
            
                if (!animator.IsInTransition(0))
                    MatchTarget(animator, moveState.MatchTarget.position, moveState.MatchTarget.rotation, AvatarTarget.LeftHand, new MatchTargetWeightMask(new Vector3(0, 1, 1), 0), 0f, 0.5f);

                if (stateInfo.normalizedTime > 0.9f)
                {
                    collider.isTrigger = false;
                    rigidbody.useGravity = true;
                    moveState.StepUp = false;
                }
            }
        }

        static void JumpAnimation(Animator animator, Rigidbody rigidbody, ref PlayerMoveState moveState, AnimatorStateInfo stateInfo)
        {
            var transform = animator.transform;
            animator.SetBool("Jump", moveState.Jump);
            var newSpeed = (JUMP_FORWARD * moveState.InpForward);

            moveState.IsJumping = stateInfo.IsName("Action.Jump") || stateInfo.IsName("Action.JumpMove") || stateInfo.IsName("Airborne.FallingFromJump");
            animator.SetBool("IsJumping", moveState.IsJumping);

            if (stateInfo.IsName("Action.Jump"))
            {
                // apply extra height to the jump
                if (stateInfo.normalizedTime < 0.85f)
                {
                    rigidbody.velocity = new Vector3(rigidbody.velocity.x, JUMP_FORCE, rigidbody.velocity.z);
                    transform.position += transform.up * (JUMP_FORCE * Time.fixedDeltaTime);
                }
                // end jump animation
                if (stateInfo.normalizedTime >= 0.85f)
                    moveState.Jump = false;
                // apply extra speed forward
                if (stateInfo.normalizedTime >= 0.65f && JUMP_AIR_CONTROL)
                    transform.position += transform.forward * (newSpeed * Time.fixedDeltaTime);
                else if (stateInfo.normalizedTime >= 0.65f && !JUMP_AIR_CONTROL)
                    transform.position += transform.forward * Time.fixedDeltaTime;
            }

            if (stateInfo.IsName("Action.JumpMove"))
            {
                // apply extra height to the jump
                if (stateInfo.normalizedTime < 0.85f)
                {
                    rigidbody.velocity = new Vector3(rigidbody.velocity.x, JUMP_FORCE, rigidbody.velocity.z);
                    transform.position += transform.up * (JUMP_FORCE * Time.fixedDeltaTime);
                }
                // end jump animation
                if (stateInfo.normalizedTime >= 0.55f)
                    moveState.Jump = false;
                // apply extra speed forward
                if (JUMP_AIR_CONTROL)
                    transform.position += transform.forward * (newSpeed * Time.fixedDeltaTime);
                else
                    transform.position += transform.forward * Time.fixedDeltaTime;
            }

            if (stateInfo.IsName("Airborne.FallingFromJump") && JUMP_AIR_CONTROL)
                transform.position += transform.forward * (newSpeed * Time.fixedDeltaTime);
            else if (stateInfo.IsName("Airborne.FallingFromJump") && !JUMP_AIR_CONTROL)
                transform.position += transform.forward * Time.fixedDeltaTime;
        }

        static void MatchTarget(Animator animator, Vector3 matchPosition, Quaternion matchRotation, AvatarTarget target, MatchTargetWeightMask weightMask, float normalisedStartTime, float normalisedEndTime)
        {
            if (animator.isMatchingTarget)
                return;

            float normalizeTime = Mathf.Repeat(animator.GetCurrentAnimatorStateInfo(0).normalizedTime, 1f);
            if (normalizeTime > normalisedEndTime)
                return;

            animator.MatchTarget(matchPosition, matchRotation, target, weightMask, normalisedStartTime, normalisedEndTime);
        }
    }
}
