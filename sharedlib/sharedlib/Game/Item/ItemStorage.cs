﻿using System.Collections.Generic;

namespace SharedLib
{
    public class ItemStorage
    {
        internal static ItemPrototype DefaultItem = new ItemPrototype(-1, ItemType.Junk, ItemAttribute.Icon("default"));
        internal Dictionary<short, ItemPrototype> Items = new Dictionary<short, ItemPrototype>();

        //export to file later
        public ItemStorage()
        {
            //raw materials
            Init(ItemId.CopperOre, ItemType.Material, ItemAttribute.Icon("inv_ore_copper_01"), ItemAttribute.Weight(0.2f));
            Init(ItemId.TinOre, ItemType.Material, ItemAttribute.Icon("inv_ore_tin_01"), ItemAttribute.Weight(0.2f));
            Init(ItemId.IronOre, ItemType.Material, ItemAttribute.Icon("inv_ore_iron_01"), ItemAttribute.Weight(0.2f));

            //craftable material
            Init(ItemId.CopperIngot, ItemType.Material, ItemAttribute.Icon("inv_ingot_02"), ItemAttribute.Weight(0.8f));
            Init(ItemId.BronzeIngot, ItemType.Material, ItemAttribute.Icon("inv_ingot_bronze"), ItemAttribute.Weight(0.8f));
            Init(ItemId.IronIngot, ItemType.Material, ItemAttribute.Icon("inv_ingot_iron"), ItemAttribute.Weight(0.8f));
           
            //test
            Init(ItemId.Sword, ItemType.WeaponSword1H, ItemAttribute.Name("Sword"), ItemAttribute.Icon("inv_sword_04"), ItemAttribute.Model("test_sword"), ItemAttribute.Weight(4.5f), ItemAttribute.Damage(4, 10), ItemAttribute.EquipmentPosition(EquipmentPosition.Weapon));
            Init(ItemId.Apple, ItemType.Junk, ItemAttribute.Name("Apple"), ItemAttribute.Icon("inv_misc_food_19"), ItemAttribute.Weight(0.1f));
            Init(ItemId.LeatherCuirass, ItemType.Armor, ItemAttribute.Name("Leather Cuirass"), ItemAttribute.Icon("inv_chest_leather_01"), ItemAttribute.Weight(2f), ItemAttribute.EquipmentPosition(EquipmentPosition.Cuirass), ItemAttribute.Strength(1), ItemAttribute.Dexterity(1), ItemAttribute.Armor(4));
            Init(ItemId.GandalfStaff, ItemType.WeaponStaff2H, ItemAttribute.Name("Gandalf's staff"), ItemAttribute.Icon("inv_staff_08"), ItemAttribute.Model("test_staff"), ItemAttribute.Intelligence(10), ItemAttribute.EquipmentPosition(EquipmentPosition.Weapon));
        }

        void Init(ItemId id, ItemType type, params ItemAttribute[] attributes)
        {
            if(!Items.ContainsKey((short)id))
                Items.Add((short)id, new ItemPrototype((short)id, type, attributes));
        }

        public ItemPrototype GetItem(short id)
        {
            if (!Items.ContainsKey(id))
                return DefaultItem;

            return Items[id];
        }
    }
}
