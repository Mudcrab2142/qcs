﻿using ProtoBuf;
using System.Collections.Generic;

namespace SharedLib
{
    [ProtoContract]
    public class ItemInstance
    {
        [ProtoMember(1)]
        public short Id;
        [ProtoMember(2)]
        public int Count;
        [ProtoMember(3)]
        internal SortedDictionary<AttributeType, ItemAttributeValue> OverridenAttributes = new SortedDictionary<AttributeType, ItemAttributeValue>();
        [ProtoMember(4)]
        public ItemQuality Quality;
        
        public ItemAttributeValue this[AttributeType att] => OverridenAttributes.ContainsKey(att) ? OverridenAttributes[att] : BaseItem[att];
        public ItemPrototype BaseItem => NetworkBaseWorld.WorldInstance ? NetworkBaseWorld.WorldInstance.ItemStorage.GetItem(Id) : ItemStorage.DefaultItem;

        public string Name => this[AttributeType.Name];
        public float Weight => (float)this[AttributeType.Weight] * Count;
        public short Width => 1;//this[AttributeType.Width];
        public short Height => 1;//this[AttributeType.Height];
        public bool Stackable => BaseItem.Type != ItemType.WeaponSword1H && BaseItem.Type != ItemType.Armor;
        public string Icon => this[AttributeType.Icon];

        /// <summary>
        /// Server item instance constructor
        /// </summary>
        public ItemInstance(ItemId id, ItemQuality quality, params ItemAttribute[] attributes)
        {
            Id = (short)id;
            Count = 1;
            Quality = quality;

            foreach (var att in attributes)
                if (!OverridenAttributes.ContainsKey(att.Type))
                    OverridenAttributes.Add(att.Type, att.Value);
        }

        /// <summary>
        /// Server item instance constructor
        /// </summary>
        public ItemInstance(ItemId id, ItemQuality quality, int count, params ItemAttribute[] attributes)
        {
            Id = (short)id;
            Count = count;
            Quality = quality;

            if (!Stackable)
                Count = 1;

            foreach (var att in attributes)
                if (!OverridenAttributes.ContainsKey(att.Type))
                    OverridenAttributes.Add(att.Type, att.Value);
        }

        /// <summary>
        /// Protobuf constructor
        /// </summary>
        private ItemInstance()
        {

        }

        public bool HasAttribute(AttributeType type)
        {
            return BaseItem.Attributes.ContainsKey(type) || OverridenAttributes.ContainsKey(type);
        }

        public void AddAttribute(ItemAttribute attribute)
        {
            if (OverridenAttributes.ContainsKey(attribute.Type))
                return;

            OverridenAttributes.Add(attribute.Type, attribute.Value);
        }

        public bool CanStack(ItemInstance target)
        {
            if (Id != target.Id || Quality != target.Quality)
                return false;

            if (OverridenAttributes.Count != target.OverridenAttributes.Count)
                return false;

            foreach(var key in OverridenAttributes.Keys)
            {
                if (!target.OverridenAttributes.ContainsKey(key))
                    return false;

                if (OverridenAttributes[key] != target.OverridenAttributes[key])
                    return false;
            }

            return true;
        }
    }
}
