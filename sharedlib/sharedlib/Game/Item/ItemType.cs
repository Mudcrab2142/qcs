﻿namespace SharedLib
{
    public enum ItemType
    {
        WeaponSword1H,
        WeaponStaff1H,
        WeaponStaff2H,
        Armor,
        Junk,
        Material,
        Food
    }
}
