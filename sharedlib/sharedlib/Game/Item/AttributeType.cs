﻿namespace SharedLib
{
    public enum AttributeType : byte
    {
        //base
        Name,
        Icon,
        Weight,
      //  Width,
      //  Height,
        EquipmentPosition,
        Model,

        //add main stats
        Strength,
        Dexterity,
        Constitution,
        Intelligence,
        Luck,

        //add secondary stats
        Damage,
        Armor,

        //unique modifiers
        Cursed,
    }
}
