﻿using ProtoBuf;
using System;

namespace SharedLib
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    [ProtoInclude(1, typeof(ItemAttributeValue<short>))]
    [ProtoInclude(2, typeof(ItemAttributeValue<float>))]
    [ProtoInclude(3, typeof(ItemAttributeValue<string>))]
    [ProtoInclude(4, typeof(ItemAttributeValue<bool>))]
    [ProtoInclude(5, typeof(ItemAttributeValue<int>))]
    [ProtoInclude(6, typeof(ItemAttributeValue<byte>))]
    [ProtoInclude(7, typeof(ItemAttributeValue<MinMax<int>>))]
    [ProtoInclude(8, typeof(ItemAttributeValue<MinMax<short>>))]
    public abstract class ItemAttributeValue
    {
        public static implicit operator short (ItemAttributeValue obj)
        {
            return (obj as ItemAttributeValue<short>)?.Value ?? 0;
        }

        public static implicit operator int (ItemAttributeValue obj)
        {
            return (obj as ItemAttributeValue<int>)?.Value ?? (obj as ItemAttributeValue<short>)?.Value ?? 0;
        }

        public static implicit operator float (ItemAttributeValue obj)
        {
            return (obj as ItemAttributeValue<float>)?.Value ?? (obj as ItemAttributeValue<short>)?.Value ?? (obj as ItemAttributeValue<int>)?.Value ?? 0;
        }

        public static implicit operator string (ItemAttributeValue obj)
        {
            return (obj as ItemAttributeValue<string>)?.Value ?? "";
        }

        public static implicit operator bool (ItemAttributeValue obj)
        {
            return (obj as ItemAttributeValue<bool>)?.Value ?? false;
        }

        public static implicit operator EquipmentPosition (ItemAttributeValue obj)
        {
            if (obj is ItemAttributeValue<byte>)
                return (EquipmentPosition)(((ItemAttributeValue<byte>)obj).Value);

            return EquipmentPosition.Null;          
        }

        public static implicit operator MinMax<short> (ItemAttributeValue obj)
        {
            return (obj as ItemAttributeValue<MinMax<short>>)?.Value ?? new MinMax<short>(0, 0);
        }

        public static bool operator ==(ItemAttributeValue x, ItemAttributeValue y)
        {
            return Compare(x, y);
        }

        public static bool operator !=(ItemAttributeValue x, ItemAttributeValue y)
        {
            return !Compare(x, y);
        }

        static bool Compare(ItemAttributeValue x, ItemAttributeValue y)
        {
            if (x.ValueType != y.ValueType)
                return false;

            if (!x.ValueCheck(y.ObjValue))
                return false;

            return true;
        }

        protected abstract bool ValueCheck(object target);
        protected abstract object ObjValue { get; }
        internal abstract ItemAttributeValue Clone();
        internal abstract ItemAttributeValue Multiply(float multiplier);
        protected abstract Type ValueType { get; }
    }

    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public sealed class ItemAttributeValue<T> : ItemAttributeValue where T : IEquatable<T>
    {
        protected override object ObjValue => Value;
        protected override Type ValueType => typeof(T);

        internal T Value;

        public ItemAttributeValue(T value)
        {
            Value = value;
        }

        private ItemAttributeValue()
        {

        }

        internal override ItemAttributeValue Clone()
        {
            return new ItemAttributeValue<T>(Value);
        }

        internal override ItemAttributeValue Multiply(float multiplier)
        {
            if(Value is int)
                Value = (T)(object)((int)(object)Value * multiplier);
            else if(Value is float)
                Value = (T)(object)((float)(object)Value * multiplier);
            else if(Value is short)
                Value = (T)(object)((short)(object)Value * multiplier);
            else if(Value is MinMax<short>)
                Value = (T)(object)(new MinMax<short>((short)(((MinMax<short>)(object)Value).Min * multiplier), (short)(((MinMax<short>)(object)Value).Max * multiplier)));
            else if (Value is MinMax<int>)
                Value = (T)(object)(new MinMax<int>((int)(((MinMax<int>)(object)Value).Min * multiplier), (int)(((MinMax<int>)(object)Value).Max * multiplier)));

            return this;
        }

        protected override bool ValueCheck(object value)
        {
            return Value.Equals((T)value);
        }
    }
}
