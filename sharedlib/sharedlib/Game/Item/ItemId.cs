﻿namespace SharedLib
{
    public enum ItemId : short
    {
        //raw resorces
        CopperOre,
        TinOre,
        IronOre,

        CopperIngot,
        BronzeIngot,
        IronIngot,

        Apple,
        Sword,
        GandalfStaff,

        LeatherCuirass
    }
}
