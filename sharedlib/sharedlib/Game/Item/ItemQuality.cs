﻿namespace SharedLib
{
    public enum ItemQuality : byte
    {
        Poor,
        Common,
        Uncommon,
        Rare,
        Epic,
        Legendary,
        Artifact
    }
}
