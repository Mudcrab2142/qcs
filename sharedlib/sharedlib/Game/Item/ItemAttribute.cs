﻿using System.Collections.Generic;

namespace SharedLib
{
    /// <summary>
    /// Item attribute factory class
    /// </summary>
    public struct ItemAttribute
    {
        public AttributeType Type;
        public ItemAttributeValue Value;

        ItemAttribute(AttributeType type, ItemAttributeValue value)
        {
            Type = type;
            Value = value;
        }

        #region Base
        /*   
        public static ItemAttribute Width(short value)
        {
            return new ItemAttribute(AttributeType.Width, new ItemAttributeValue<short>(value));
        }

        public static ItemAttribute Height(short value)
        {
            return new ItemAttribute(AttributeType.Height, new ItemAttributeValue<short>(value));
        }
        */
        public static ItemAttribute Weight(float value)
        {
            return new ItemAttribute(AttributeType.Weight, new ItemAttributeValue<float>(value));
        }

        public static ItemAttribute Name(string value)
        {
            return new ItemAttribute(AttributeType.Name, new ItemAttributeValue<string>(value));
        }

        public static ItemAttribute Icon(string value)
        {
            return new ItemAttribute(AttributeType.Icon, new ItemAttributeValue<string>(value));
        }

        public static ItemAttribute EquipmentPosition(EquipmentPosition position)
        {
            return new ItemAttribute(AttributeType.EquipmentPosition, new ItemAttributeValue<byte>((byte)position));
        }

        public static ItemAttribute Model(string model)
        {
            return new ItemAttribute(AttributeType.Model, new ItemAttributeValue<string>(model));
        }
        #endregion

        #region MainStats
        public static ItemAttribute Strength(short value)
        {
            return new ItemAttribute(AttributeType.Strength, new ItemAttributeValue<short>(value));
        }

        public static ItemAttribute Dexterity(short value)
        {
            return new ItemAttribute(AttributeType.Dexterity, new ItemAttributeValue<short>(value));
        }

        public static ItemAttribute Constitution(short value)
        {
            return new ItemAttribute(AttributeType.Constitution, new ItemAttributeValue<short>(value));
        }

        public static ItemAttribute Intelligence(short value)
        {
            return new ItemAttribute(AttributeType.Intelligence, new ItemAttributeValue<short>(value));
        }

        public static ItemAttribute Luck(short value)
        {
            return new ItemAttribute(AttributeType.Luck, new ItemAttributeValue<short>(value));
        }
        #endregion

        public static ItemAttribute Damage(short min, short max)
        {
            return new ItemAttribute(AttributeType.Damage, new ItemAttributeValue<MinMax<short>>(new MinMax<short>(min, max)));
        }

        public static ItemAttribute Armor(short value)
        {
            return new ItemAttribute(AttributeType.Armor, new ItemAttributeValue<short>(value));
        }

        public static ItemAttribute Cursed(bool value)
        {
            return new ItemAttribute(AttributeType.Cursed, new ItemAttributeValue<bool>(value));
        }
    }
}
