﻿using System.Collections.Generic;

namespace SharedLib
{
    public class ItemPrototype
    {
        internal Dictionary<AttributeType, ItemAttributeValue> Attributes = new Dictionary<AttributeType, ItemAttributeValue>();
        public ItemAttributeValue this[AttributeType att] => Attributes.ContainsKey(att) ? Attributes[att] : null;

        public readonly short Id;
        public readonly ItemType Type;
        public int MaxStackCount => 100;

        internal ItemPrototype(short id, ItemType type, params ItemAttribute[] attributes)
        {
            Id = id;
            Type = type;
            
            foreach(var att in attributes)
                if (!Attributes.ContainsKey(att.Type))
                    Attributes.Add(att.Type, att.Value);

            CheckDefaultAttributes();
        }

        private void AddAttribute(ItemAttribute data)
        {
            if (!Attributes.ContainsKey(data.Type))
                Attributes.Add(data.Type, data.Value);
        }

        void CheckDefaultAttributes()
        {
            /*
            if (!Attributes.ContainsKey(AttributeType.Width))
                AddAttribute(ItemAttribute.Width(1));

            if (!Attributes.ContainsKey(AttributeType.Height))
                AddAttribute(ItemAttribute.Height(1));
            */
            if (!Attributes.ContainsKey(AttributeType.Weight))
                AddAttribute(ItemAttribute.Weight(0f));

            if (!Attributes.ContainsKey(AttributeType.Name))
                AddAttribute(ItemAttribute.Name(Type.ToString()));
        }
    }
}
