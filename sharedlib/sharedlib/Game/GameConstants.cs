﻿using System.Collections.Generic;

namespace SharedLib
{
    public static class GameConstants
    {
        public static class Equipment
        {
            public const byte WEAPON = 1;
            public const byte HELMET = 3;
            public const byte CUIRASS = 4;
            public const byte LEGGINGS = 5;
            public const byte BOOTS = 6;
            public const byte GLOVES = 7;
        }

        public static class Team
        {
            public const byte NONE = 0;

        }

        public static class Experience
        {
            public const byte MAX_LEVEL = 30;

            public static readonly Dictionary<byte, int> ExpNeeded = new Dictionary<byte, int>()
            {
                { 1, 0 },
                { 2, 80 },
                { 3, 185 },
                { 4, 300 },
                { 5, 450 },
                { 6, 700 },
                { 7, 1000 },
                { 8, 1425 },
                { 9, 1900 },
                { 10, 2400 },
                { 11, 3000 },
                { 12, 3750 },
                { 13, 4800 },
                { 14, 6000 },
                { 15, 8000 },
                { 16, 10500 },
                { 17, 13250 },
                { 18, 15800 },
                //chet vpadlu poka
            };

            public static byte GetLevel(int exp)
            {
                for(byte i = 1; i < MAX_LEVEL - 1; i++)
                {
                    if (exp >= ExpNeeded[i] && exp < ExpNeeded[(byte)(i + 1)])
                        return i;
                }
                return MAX_LEVEL;
            }

            public static int GetExpToNextLevel(byte currentLevel)
            {
                if (currentLevel >= MAX_LEVEL || currentLevel == 0)
                    return 0;

                return ExpNeeded[(byte)(currentLevel + 1)] - ExpNeeded[currentLevel];
            }

            public static int GetTotalExpToLevel(byte level)
            {
                if (!ExpNeeded.ContainsKey(level))
                    return 0;

                return ExpNeeded[level];
            }
        }
    }
}
