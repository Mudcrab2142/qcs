﻿namespace SharedLib
{
    public enum EntityType : short
    {
        Dummy,
        WorldSpawn,
        Character,
        DummyCreature,
        WorldItem,
        Airship,
        AirshipBalloon,
        MagicMissile
    }
}
