﻿namespace SharedLib
{
    public enum MainStatType : byte
    {
        Strength,
        Constitution,
        Dexterity,
        Intelligence,
        Luck
    }
}
