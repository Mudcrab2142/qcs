﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract]
    public struct StatValue
    {
        [ProtoMember(1)]
        public float Base;
        [ProtoMember(2)]
        public float Additional;
        [ProtoMember(3)]
        public byte Type;

        public float Value => Base + Additional;
    }
}
