﻿namespace SharedLib
{
    public abstract class StatStorage
    {
        internal StatValue NullValue = new StatValue() { Base = -1 };

        public StatValue Strength => GetStat(MainStatType.Strength);
        public StatValue Constitution => GetStat(MainStatType.Constitution);
        public StatValue Dexterity => GetStat(MainStatType.Dexterity);
        public StatValue Intelligence => GetStat(MainStatType.Intelligence);
        public StatValue Luck => GetStat(MainStatType.Luck);

        public StatValue MinDamage => GetStat(SecondaryStatType.MinDamage);
        public StatValue MaxDamage => GetStat(SecondaryStatType.MaxDamage);
        public StatValue Armor => GetStat(SecondaryStatType.Armor);
        public StatValue AttackSpeed => GetStat(SecondaryStatType.AttackSpeed);
        public StatValue Health => GetStat(SecondaryStatType.Health);
        public StatValue HealthRegeneration => GetStat(SecondaryStatType.HealthRegeneration);
        public StatValue Stamina => GetStat(SecondaryStatType.Stamina);
        public StatValue StaminaRegeneration => GetStat(SecondaryStatType.StaminaRegeneration);
        public StatValue Blacksmithing => GetStat(CraftStatType.Blacksmithing);

        public abstract StatValue GetStat(MainStatType type);
        public abstract StatValue GetStat(SecondaryStatType type);
        public abstract StatValue GetStat(CraftStatType type);
    }
}
