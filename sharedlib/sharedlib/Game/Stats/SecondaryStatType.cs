﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedLib
{
    public enum SecondaryStatType : byte
    {
        MinDamage,
        MaxDamage,
        Armor,
        Health,
        Stamina,
        HealthRegeneration,
        StaminaRegeneration,
        CriticalChance,
        CriticalPower,
        AttackSpeed,
        MovementSpeed,
    }
}
