﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedLib
{
    public interface IFinder
    {
        Inventory FindInventory(int entityId = 0);
        Equipment FindEquipment(int entityId = 0);
        NetworkCharacter FindNetworkCharacter(object o = null);
    }
}
