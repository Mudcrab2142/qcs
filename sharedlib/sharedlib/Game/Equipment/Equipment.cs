﻿using System.Collections.Generic;

namespace SharedLib
{
    public abstract class Equipment
    {
        public readonly EquipmentSlot Weapon;
        public readonly EquipmentSlot Helmet;
        public readonly EquipmentSlot Cuirass;
        public readonly EquipmentSlot Leggings;
        public readonly EquipmentSlot Boots;
        public readonly EquipmentSlot Gloves;

        private Dictionary<byte, EquipmentSlot> _storage;

        public EquipmentSlot this[byte id] => _storage.ContainsKey(id) ? _storage[id] : null;

        public Equipment()
        {
            _storage = new Dictionary<byte, EquipmentSlot>();
            InitSlot(out Weapon, GameConstants.Equipment.WEAPON, EquipmentPosition.Weapon);
            InitSlot(out Helmet, GameConstants.Equipment.HELMET, EquipmentPosition.Helmet);
            InitSlot(out Cuirass, GameConstants.Equipment.CUIRASS, EquipmentPosition.Cuirass);
            InitSlot(out Leggings, GameConstants.Equipment.LEGGINGS, EquipmentPosition.Leggings);
            InitSlot(out Boots, GameConstants.Equipment.BOOTS, EquipmentPosition.Boots);
            InitSlot(out Gloves, GameConstants.Equipment.GLOVES, EquipmentPosition.Gloves);
        }

        private void InitSlot(out EquipmentSlot slot, byte id, EquipmentPosition position)
        {
            slot = new EquipmentSlot(id, position);
            _storage.Add(id, slot);
        }
    }
}
