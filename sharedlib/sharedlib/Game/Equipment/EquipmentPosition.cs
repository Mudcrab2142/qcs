﻿namespace SharedLib
{
    public enum EquipmentPosition : byte
    {
        Null,
        Weapon,
        Helmet,
        Cuirass,
        Leggings,
        Boots,
        Gloves
    }
}
