﻿using System;

namespace SharedLib
{
    public class EquipmentSlot
    {
        public event Action<EquipmentSlot> OnEquipmentChange;

        public ItemInstance Item { get; private set; }

        public readonly byte SlotId;
        public readonly EquipmentPosition Position;

        public EquipmentSlot(byte slotId, EquipmentPosition position)
        {
            SlotId = slotId;
            Position = position;
        }

        public void ChangeItem(ItemInstance item)
        {
            Item = item;
            if (OnEquipmentChange != null)
                OnEquipmentChange(this);
        }
    }
}
