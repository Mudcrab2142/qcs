﻿namespace SharedLib
{
    public enum DamageType : byte
    {
        Physical,
        Dark,
        Pure
    }
}
