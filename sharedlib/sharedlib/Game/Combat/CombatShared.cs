﻿using UnityEngine;

namespace SharedLib
{
    public static class CombatShared
    {
        /// <summary>
        /// Stamina regen (call only in EntityFixedUpdate)
        /// </summary>
        /// <returns>Regenerated stamina value</returns>
        public static float StaminaRegeneration(NetworkCharacter character, StatStorage stats)
        {
            var currStamina = character.CurrentStamina.GetValue();
            var maxStamina = character.MaxStamina.GetValue();
            currStamina += Time.fixedDeltaTime * stats.StaminaRegeneration.Value;
            currStamina = Mathf.Clamp(currStamina, 0f, maxStamina);
            return currStamina;
        }

        public static float PhysicalDamageResistance(float armorValue)
        {
            return armorValue >= 0 ? 1f - (100f / (100f + armorValue)) : -(100f / (100f - armorValue));
        }
    }
}
