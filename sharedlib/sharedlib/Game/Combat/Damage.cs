﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct Damage
    {
        public DamageType Type;
        public short Amount;

        public Damage(short amount, DamageType type)
        {
            Amount = amount;
            Type = type;
        }
    }
}
