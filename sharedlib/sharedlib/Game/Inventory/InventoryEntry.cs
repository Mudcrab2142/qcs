﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract]
    public class InventoryEntry
    {
        [ProtoMember(1)]
        public ItemInstance Item;
        [ProtoMember(2)]
        public short PositionX;
        [ProtoMember(3)]
        public short PositionY;

        /// <summary>
        /// Server constructor
        /// </summary>
        public InventoryEntry(ItemInstance item, short cx, short cy)
        {
            Item = item;
            PositionX = cx;
            PositionY = cy;
        }
        
        /// <summary>
        /// Empty constructor for protobuf
        /// </summary>
        private InventoryEntry()
        {

        }
    }
}
