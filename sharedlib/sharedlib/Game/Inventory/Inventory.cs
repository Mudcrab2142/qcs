﻿using System.Linq;

namespace SharedLib
{
    public abstract class Inventory
    {
        public InventoryData Backpack { get; protected set; }

        public float TotalWeight => Backpack != null ? Backpack.Storage.Sum(x => x.Item.Weight) : 0;

        protected bool[,] mask;

        protected bool FindFreeCell(ItemInstance item, out short cellX, out short cellY)
        {
            cellX = cellY = 0;

            for (short x = 0; x <= Backpack.SizeX - item.Width; x++)
                for (short y = 0; y <= Backpack.SizeY - item.Height; y++)
                {
                    if (!FitItem(item, x, y))
                        continue;

                    cellX = x;
                    cellY = y;
                    return true;
                }

            return false;
        }

        public bool HasItem(ItemId id, int count)
        {
            return Backpack.Storage.Where(x => x.Item.Id == (short)id).Sum(x => x.Item.Count) >= count;
        }

        protected bool FitItem(ItemInstance item, short targetX, short targetY)
        {
            for (var x = targetX; x < targetX + item.Width; x++)
                for (var y = targetY; y < targetY + item.Height; y++)
                    if (x >= Backpack.SizeX || y >= Backpack.SizeY || mask[x, y])
                        return false;

            return true;
        }

        protected void UpdateMask(short posX, short posY, short width, short height, bool value)
        {
            for (var x = posX; x < posX + width; x++)
                for (var y = posY; y < posY + height; y++)
                    mask[x, y] = value;
        }
    }
}
