﻿using ProtoBuf;
using System.Collections.Generic;

namespace SharedLib
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class InventoryData
    {
        public short SizeX;
        public short SizeY;
        public List<InventoryEntry> Storage;

        public InventoryData(short sizeX, short sizeY)
        {
            SizeX = sizeX;
            SizeY = sizeY;
            Storage = new List<InventoryEntry>();
        }

        /// <summary>
        /// Empty protobuf constructor
        /// </summary>
        private InventoryData()
        {

        }
    }
}
