﻿namespace SharedLib
{
    public class NetworkMagicMissile : NetworkBaseEntity
    {
        public enum MissileType : short
        {
            DarkMatter,
        }

        public NetworkInt16 Type = new NetworkInt16(0);

        protected override void InitVariables()
        {
            base.InitVariables();
            BindVariable(Type);
        }
    }
}
