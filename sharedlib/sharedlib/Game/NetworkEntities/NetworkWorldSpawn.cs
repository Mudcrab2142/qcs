﻿namespace SharedLib
{
    public class NetworkWorldSpawn : NetworkBaseEntity
    {
        public override bool UseNetworkCells => false;

        public NetworkInt16 Time = new NetworkInt16(0);

        protected override void InitVariables()
        {
            base.InitVariables();
            UnBindVariable(Position);
            UnBindVariable(UseSimpleRotation ? SimpleRotation : (NetworkVariable)FullRotation);

            BindVariable(Time);
        }
    }
}
