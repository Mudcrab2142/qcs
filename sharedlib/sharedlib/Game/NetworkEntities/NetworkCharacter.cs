﻿namespace SharedLib
{
    public class NetworkCharacter : NetworkHumanoidCreature
    {
        public NetworkInt64 OwnerId = new NetworkInt64(0);

        public NetworkSingle AnimatorDirection = new NetworkSingle(0f);
        public NetworkSingle AnimatorSpeed = new NetworkSingle(0f);
        public NetworkInt32Array AnimatorLayersData = new NetworkInt32Array(new int[0]);

        public NetworkSingle CurrentStamina = new NetworkSingle(0);
        public NetworkSingle MaxStamina = new NetworkSingle(0);
        public NetworkInt32 PlatformEntity = new NetworkInt32(-1);

        protected override void InitVariables()
        {
            base.InitVariables();

            BindVariable(OwnerId);
            BindVariable(AnimatorDirection);
            BindVariable(AnimatorSpeed);
            BindVariable(AnimatorLayersData);
            BindVariable(CurrentStamina);
            BindVariable(MaxStamina);
            BindVariable(PlatformEntity);
        }
    }
}
