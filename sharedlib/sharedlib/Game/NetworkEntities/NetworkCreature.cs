﻿
namespace SharedLib
{
    public class NetworkCreature : NetworkBaseEntity
    {
        public const byte FLAG_DEAD = 1 << 0;
        
        public NetworkUInt16 CurrentHealth = new NetworkUInt16(0);
        public NetworkUInt16 MaxHealth = new NetworkUInt16(0);
        public NetworkByte Team = new NetworkByte(0);
        public NetworkByte CreatureFlags = new NetworkByte(0);

        protected override void InitVariables()
        {
            base.InitVariables();

            BindVariable(CurrentHealth);
            BindVariable(MaxHealth);
            BindVariable(Team);
            BindVariable(CreatureFlags);
        }
    }
}
