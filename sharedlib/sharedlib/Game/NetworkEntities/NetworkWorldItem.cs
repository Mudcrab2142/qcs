﻿namespace SharedLib
{
    public class NetworkWorldItem : NetworkBaseEntity
    {
        public NetworkInt16 Id = new NetworkInt16(0);

        protected override void InitVariables()
        {
            base.InitVariables();
            BindVariable(Id);
        }
    }
}
