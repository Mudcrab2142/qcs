﻿namespace SharedLib
{
    public class NetworkAirship : NetworkBaseEntity
    {
        public NetworkUInt16 CurrentHealth = new NetworkUInt16(0);
        public NetworkUInt16 MaxHealth = new NetworkUInt16(0);
        public NetworkByte Team = new NetworkByte(0);
        public NetworkInt32 DriverEntityId = new NetworkInt32(-1);
        public NetworkByte InputAltitude = new NetworkByte(100);
        public NetworkByte InputThrottle = new NetworkByte(100);
        public NetworkByte InputRotation = new NetworkByte(100);

        protected override void InitVariables()
        {
            base.InitVariables();
            BindVariable(CurrentHealth);
            BindVariable(MaxHealth);
            BindVariable(Team);
            BindVariable(InputAltitude);
            BindVariable(InputThrottle);
            BindVariable(InputRotation);
            BindVariable(DriverEntityId);
        }
    }
}
