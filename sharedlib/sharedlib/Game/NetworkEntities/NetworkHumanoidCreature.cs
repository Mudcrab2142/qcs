﻿namespace SharedLib
{
    public class NetworkHumanoidCreature : NetworkCreature
    {
        public NetworkByteArray Equipment = new NetworkByteArray(new byte[0]);

        protected override void InitVariables()
        {
            base.InitVariables();

            BindVariable(Equipment);
        }
    }
}
