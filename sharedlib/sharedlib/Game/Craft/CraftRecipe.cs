﻿namespace SharedLib
{
    public class CraftRecipe
    {
        public readonly CraftStatType Type;
        public readonly CraftRecipeId RecipeId;
        public readonly ItemId Item;
        public readonly float MasteryLevel;
        public readonly int Count;
        public readonly float CraftTime;
        public readonly CraftIngredient[] Ingredients;
        public readonly CraftCell[] Cells;

        public CraftRecipe(CraftStatType craftType, CraftRecipeId recipeId, ItemId resultItemId, float masteryLevel, int resultItemCount, float craftTime, CraftIngredient[] ingredients, CraftCell[] cells)
        {
            Type = craftType;
            RecipeId = recipeId;
            Item = resultItemId;
            MasteryLevel = masteryLevel;
            Count = resultItemCount;
            CraftTime = craftTime;
            Ingredients = ingredients;
            Cells = cells;
        }
    }
}
