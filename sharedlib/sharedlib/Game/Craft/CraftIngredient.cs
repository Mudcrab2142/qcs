﻿namespace SharedLib
{
    public class CraftIngredient
    {
        public readonly ItemId Id;
        public readonly int Count;

        internal CraftIngredient(ItemId id, int count)
        {
            Id = id;
            Count = count;
        }
    }
}
