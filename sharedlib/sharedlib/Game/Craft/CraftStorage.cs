﻿using System.Collections.Generic;
using UnityEngine;

namespace SharedLib
{
    public class CraftStorage
    {
        static readonly Dictionary<CraftRecipeId, CraftRecipe> _recipes = new Dictionary<CraftRecipeId, CraftRecipe>();
        public readonly HashSet<CraftRecipeId> KnownRecipes = new HashSet<CraftRecipeId>();
        public readonly List<CraftSession> CraftSessions = new List<CraftSession>();

        private readonly Inventory _ownerInventory;
        private readonly StatStorage _ownerStats;

        static CraftStorage()
        {
            InitRecipe(CraftStatType.Blacksmithing, CraftRecipeId.CopperIngot, ItemId.CopperIngot, 10f, 1, 2f, new CraftIngredient[] { new CraftIngredient(ItemId.CopperOre, 4) }, new CraftCell[] { new CraftCell(4) });
           // Iniy

       //     InitRecipe(CraftStatType.Blacksmithing, CraftRecipeId.IronIngot, ItemId.IronIngot, 0f, 1, 5f, new[] { new CraftIngredient(ItemId.IronOre, 5) }, new[] { new CraftCell(5) });
       //     InitRecipe(CraftStatType.Blacksmithing, CraftRecipeId.Sword, ItemId.Sword, 10f, 1, 10f, new CraftIngredient[] { new CraftIngredient(ItemId.IronIngot, 10), new CraftIngredient(ItemId.Apple, 2) }, new CraftCell[] { new CraftCell(10), new CraftCell(2) });
        }

        public CraftStorage(Inventory ownerInventory, StatStorage ownerStats)
        {
            _ownerInventory = ownerInventory;
            _ownerStats = ownerStats;
        }

        static void InitRecipe(CraftStatType type, CraftRecipeId recipeId, ItemId resultItemId, float masteryLevel, int resultItemCount, float craftTime, CraftIngredient[] ingredients, CraftCell[] cells)
        {
            if(_recipes.ContainsKey(recipeId))
            {
                Debug.LogErrorFormat("[CraftRecipeStorage] Already contains recipe with id {0}.", recipeId);
                return;
            }

            var recipe = new CraftRecipe(type, recipeId, resultItemId, masteryLevel, resultItemCount, craftTime, ingredients, cells);
            _recipes.Add(recipeId, recipe);
        }

        public CraftRecipe GetRecipeInfo(CraftRecipeId id)
        {
            CraftRecipe result;
            _recipes.TryGetValue(id, out result);
            return result;
        }
    }
}
