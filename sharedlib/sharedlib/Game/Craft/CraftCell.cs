﻿namespace SharedLib
{
    public class CraftCell
    {
        public readonly int MaxIngredients;

        public CraftCell(int maxIngredients)
        {
            MaxIngredients = maxIngredients;
        }
    }
}
