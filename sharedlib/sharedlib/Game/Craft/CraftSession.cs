﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class CraftSession
    {
        public CraftRecipeId Recipe;
        public float CraftProgress;
        public float CraftTime;
        public ItemInstance[] Ingredients;

        internal CraftSession(CraftRecipeId recipe, float time, ItemInstance[] ingredients)
        {
            Recipe = recipe;
            CraftTime = time;
            Ingredients = ingredients;
        }

        private CraftSession() { }
    }
}
