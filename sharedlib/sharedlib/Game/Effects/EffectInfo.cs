﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract]
    [ProtoInclude(1, typeof(BleedEffectInfo))]
    public abstract class EffectInfo
    {
        [ProtoMember(0)]
        public string SenderName { get; private set; }
        [ProtoMember(1)]
        public float StartTime { get; private set; }
        [ProtoMember(2)]
        public float Duration { get; private set; }

        internal EffectInfo(string sender, float duration)
        {
            SenderName = sender;
            StartTime = LidgrenRoot.LidgrenInstance.ServerTime;
            Duration = duration;
        }

        protected EffectInfo() { }
    }
}
