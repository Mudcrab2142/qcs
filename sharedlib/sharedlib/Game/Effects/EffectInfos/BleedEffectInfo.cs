﻿using ProtoBuf;

namespace SharedLib
{
    [ProtoContract]
    public sealed class BleedEffectInfo : EffectInfo
    {
        [ProtoMember(0)]
        public short Damage { get; private set; }
        [ProtoMember(1)]
        public float DamagePeriod { get; private set; }

        public DamageType DamageType => DamageType.Physical;

        internal BleedEffectInfo(string sender, float duration, short damage, float damagePeriod) : base(sender, duration)
        {
            Damage = damage;
            DamagePeriod = damagePeriod;
        }

        private BleedEffectInfo() : base() { }
    }
}
