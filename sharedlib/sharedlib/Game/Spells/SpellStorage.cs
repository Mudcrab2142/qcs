﻿using System.Collections.Generic;

namespace SharedLib
{
    public abstract class SpellStorage
    {
        public readonly Dictionary<SpellName, SpellInfo> SpellInfo = new Dictionary<SpellName, SpellInfo>();
        public Dictionary<SpellName, byte> Level = new Dictionary<SpellName, byte>();

        public readonly StatStorage Stats;
        
        public SpellStorage(StatStorage stats)
        {
            Stats = stats;

            InitSpellInfo(SpellName.SwordMastery1H, new SwordMastery1HInfo(this), 1);
            InitSpellInfo(SpellName.CuttingStrike, new CuttingStrikeInfo(this));
            InitSpellInfo(SpellName.Roll, new RollInfo(this));

            InitSpellInfo(SpellName.DarkMatter, new DarkMatterInfo(this), 1);
        }

        void InitSpellInfo(SpellName type, SpellInfo info, byte level = 0)
        {
            SpellInfo.Add(type, info);
            Level.Add(type, level);
        }
    }
}
