﻿namespace SharedLib
{
    public abstract class SpellInfo
    {
        public readonly SpellName Name;
        public readonly byte MaxLevel;
        public readonly SpellStorage Owner;
        public abstract float CastSpeed { get; }
        public virtual float Cooldown => -1f;
        public byte SpellLevel => Owner.Level[Name];

        public SpellInfo(SpellStorage owner, SpellName name, byte maxLevel)
        {
            Name = name;
            MaxLevel = maxLevel;
            Owner = owner;
        }

        public virtual bool CanUse()
        {
            return SpellLevel > 0;
        }
    }
}
