﻿namespace SharedLib
{
    public enum SpellName 
    {
        Roll,

        SwordMastery1H,
        SwordMastery2H,

        CuttingStrike,
        Execution,

        DarkMatter
    }
}
