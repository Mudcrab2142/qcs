﻿using System;

namespace SharedLib
{
    public class SwordMastery1HInfo : SpellInfo
    {
        const float CHAIN_DAMAGE_MULTIPLIER = 0.1f;
        public override float CastSpeed => GetCastSpeed(SpellLevel);
        public readonly DamageType DamageType = DamageType.Physical;
        public ushort StaminaCost => 20;

        public SwordMastery1HInfo(SpellStorage storage) : base(storage, SpellName.SwordMastery1H, 3) { }

        public float GetBonusDamagePercent(byte spellLevel)
        {
            if (spellLevel == 1) return 0f;
            if (spellLevel == 2) return 0.075f;
            if (spellLevel == 3) return 0.15f;
            return 0f;
        }

        public MinMax<short> GetBasicAttackDamage(byte spellLevel, int chain)
        {
            var multiplier = GetBasicAttackDamageMultiplier(spellLevel);
            var dmg = new MinMax<short>((short)(Owner.Stats.MinDamage.Value * multiplier), (short)(Owner.Stats.MaxDamage.Value * multiplier));
            multiplier = 1f + chain * CHAIN_DAMAGE_MULTIPLIER;
            return new MinMax<short>((short)(Owner.Stats.MinDamage.Value * multiplier), (short)(Owner.Stats.MaxDamage.Value * multiplier));
        }

        public float GetBasicAttackDamageMultiplier(byte spellLevel)
        {
            if (spellLevel == 3) return 1.25f;
            if (spellLevel == 2) return 1f;
            return 0.75f;
        }

        public float GetCastSpeed(byte spellLevel)
        {
            if (spellLevel == 3) return 1.15f;
            if (spellLevel == 2) return 1.075f;
            return 1f;
        }

        public float GetBonusStatAttackSpeed(byte spellLevel)
        {
            if (spellLevel == 3) return 1.2f;
            if (spellLevel == 2) return 1.1f;
            return 1f;
        }

        public override bool CanUse()
        {
            if (!base.CanUse())
                return false;

            var character = NetworkBaseWorld.WorldInstance.Finder.FindNetworkCharacter(Owner);
            if (character.CurrentStamina.GetValue() < StaminaCost)
                return false;

            var equipment = NetworkBaseWorld.WorldInstance.Finder.FindEquipment(character.EntityId);
            if (equipment.Weapon.Item == null)
                return false;

            return equipment.Weapon.Item.BaseItem.Type == ItemType.WeaponSword1H;
        }
    }
}
