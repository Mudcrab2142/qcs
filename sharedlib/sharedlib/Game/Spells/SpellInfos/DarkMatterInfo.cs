﻿namespace SharedLib
{
    public class DarkMatterInfo : SpellInfo
    {
        public override float CastSpeed => 1f;
        public float MissileSpeed => 15f;
        public float MissileLifeTime = 5f;
        public float DamageRadius = 3f;

        public MinMax<short> Damage => GetDamage(SpellLevel);

        public readonly DamageType DamageType = DamageType.Dark;

        public DarkMatterInfo(SpellStorage storage) : base(storage, SpellName.DarkMatter, 2) { }

        public override bool CanUse()
        {
            if (!base.CanUse())
                return false;

            var character = NetworkBaseWorld.WorldInstance.Finder.FindNetworkCharacter(Owner);

            var equipment = NetworkBaseWorld.WorldInstance.Finder.FindEquipment(character.EntityId);
            if (equipment.Weapon.Item == null)
                return false;

            return equipment.Weapon.Item.BaseItem.Type == ItemType.WeaponStaff1H || equipment.Weapon.Item.BaseItem.Type == ItemType.WeaponStaff2H;
        }

        public MinMax<short> GetDamage(byte level)
        {
            if (level == 2) return new MinMax<short>(2, 100);
            return new MinMax<short>(1, 50);
        }
    }
}
