﻿namespace SharedLib
{
    public class CuttingStrikeInfo : SpellInfo
    {
        public MinMax<short> HitDamage => GetDamage(SpellLevel);
        public override float CastSpeed => 1f;

        public readonly DamageType DamageType = DamageType.Physical;
        public readonly float BleedPeriod = 1f;

        public CuttingStrikeInfo(SpellStorage storage) : base(storage, SpellName.CuttingStrike, 3) { }

        public float GetBleedDuration(byte spellLevel)
        {
            if (spellLevel <= 1) return 8f;
            if (spellLevel == 2) return 10f;
            return 12f;
        }

        public float GetBleedPeriodicBaseDamage(byte spellLevel)
        {
            if (spellLevel <= 1) return 8f;
            if (spellLevel == 2) return 10f;
            return 12f;
        }

        public short GetBleedPeriodicDamage(byte spellLevel)
        {
            return (short)((Owner.Stats.MinDamage.Value + Owner.Stats.MaxDamage.Value) / 2f * GetBleedPeriodicDamageMultiplier(spellLevel) + GetBleedPeriodicBaseDamage(spellLevel));
        }

        public short GetBleedTotalDamage(byte spellLevel)
        {
            return (short)(GetBleedPeriodicDamage(spellLevel) * GetBleedDuration(spellLevel) / BleedPeriod);
        }

        public float GetBleedPeriodicDamageMultiplier(byte spellLevel)
        {
            if (spellLevel <= 1) return 0.05f;
            if (spellLevel == 2) return 0.1f;
            return 0.15f;
        }

        public MinMax<short> GetDamage(byte spellLevel)
        {
            var multiplier = GetDamageMultiplier(spellLevel);
            return new MinMax<short>((short)(Owner.Stats.MinDamage.Value * multiplier), (short)(Owner.Stats.MaxDamage.Value * multiplier));
        }

        public float GetDamageMultiplier(byte spellLevel)
        {
            if (spellLevel <= 1) return 0.8f;
            if (spellLevel == 2) return 1f;
            return 1.2f;
        }
    }
}
