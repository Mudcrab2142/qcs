﻿namespace SharedLib
{
    public class RollInfo : SpellInfo
    {
        public override float CastSpeed => 1f;
        public int StaminaCost => GetStaminaCost(SpellLevel);

        public RollInfo(SpellStorage storage) : base(storage, SpellName.Roll, 2) { }

        public int GetStaminaCost(byte spellLevel)
        {
            if (spellLevel == 2) return 60;
            return 80;
        }

        public override bool CanUse()
        {
            if (!base.CanUse())
                return false;

            var character = NetworkBaseWorld.WorldInstance.Finder.FindNetworkCharacter(Owner);
            return character.CurrentStamina.GetValue() >= StaminaCost;
        }
    }
}
