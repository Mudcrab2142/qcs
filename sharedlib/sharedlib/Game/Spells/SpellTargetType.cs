﻿namespace SharedLib
{
    public enum SpellTargetType : byte
    {
        Entity,
        Entities,
        Point,
        Ray,
        Self
    }
}
